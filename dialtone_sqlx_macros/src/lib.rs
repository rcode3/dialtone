mod sqlx_enum_proxy;

use crate::sqlx_enum_proxy::expand_sqlx_enum_proxy;
use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput};

/// This macro defines FromStr, ToStr, and Into implementations
/// for enums based on another enum. Therefore, the derived enum
/// can proxy for the base enum. The purpose of this is to allow
/// the proxy to track the base enum allowing the base enum to
/// be free of the sqlx types (and therefore, its containing crate
/// will not need the sqlx dependency). This is useful if the base
/// enum is used in a different tier of the application, such as in
/// client side webapps.
#[proc_macro_derive(SqlxEnumProxy, attributes(proxy_for))]
pub fn sqlx_enum_proxy(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    expand_sqlx_enum_proxy(input)
        .unwrap_or_else(syn::Error::into_compile_error)
        .into()
}
