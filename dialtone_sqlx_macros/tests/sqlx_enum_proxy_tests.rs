use dialtone_sqlx_macros::SqlxEnumProxy;
use serde::{Deserialize, Serialize};
use serde_variant::to_variant_name;
use std::str::FromStr;

#[derive(Serialize, Deserialize, PartialEq, Debug)]
enum Test {
    #[serde(rename = "fOO")]
    Foo,
    #[serde(rename = "bAR")]
    Bar,
    #[serde(rename = "bAZ")]
    Baz,
}

#[derive(SqlxEnumProxy, PartialEq, Debug)]
#[proxy_for(Test)]
enum TestProxy {
    Foo,
    Bar,
    Baz,
}

#[test]
fn derive_sqlx_enum_proxy_test() {
    let bar = TestProxy::Bar;
    assert_eq!("bAR", bar.to_string());
    let foo = TestProxy::Foo;
    assert_eq!("fOO", foo.to_string());
    let baz = TestProxy::Baz;
    assert_eq!("bAZ", baz.to_string());

    assert_eq!(TestProxy::Bar, TestProxy::from_str("bAR").unwrap());
    assert_eq!(TestProxy::Foo, TestProxy::from_str("fOO").unwrap());
    assert_eq!(TestProxy::Baz, TestProxy::from_str("bAZ").unwrap());

    assert_eq!(Test::Bar, bar.into());
    assert_eq!(Test::Foo, foo.into());
    assert_eq!(Test::Baz, baz.into());

    assert_eq!(TestProxy::from(&Test::Bar), TestProxy::Bar);
    assert_eq!(TestProxy::from(&Test::Foo), TestProxy::Foo);
    assert_eq!(TestProxy::from(&Test::Baz), TestProxy::Baz);
}
