--
-- Maintains the modified_at in a table
--

create or replace function update_modified_at_column()
    returns trigger as $$
begin
    NEW.modified_at = now();
    return NEW;
end;
$$ language 'plpgsql';


--
-- Users, Passwords
--

create type user_status_type as enum (
    'Active',
    'Suspended',
    'Pending Approval'
    );

-- named 'user_acct' because 'user' is a reserved word
create table user_acct (
    acct varchar not null primary key, -- user_name@host_name
    auth_data jsonb not null,
    last_seen_data jsonb default '[]'::jsonb not null,
    last_login_data jsonb default '[]'::jsonb not null,
    actor_counts jsonb default 
      '{"Person" : 0, "Organization" : 0, "Group" : 0, "Service" : 0, "Application" : 0}'::jsonb 
      not null,
    default_actor_data jsonb,
    system_data jsonb,
    preferences jsonb,
    status user_status_type default 'Pending Approval' not null,
    system_permissions jsonb default '[]'::jsonb not null,
    event_log jsonb default '[]'::jsonb not null,
    created_at timestamptz default now() not null,
    modified_at timestamptz default now() not null
);

create index on user_acct(created_at); -- used for pagination

create trigger update_user_acct_modified_at
    before update on user_acct
    for each row execute procedure update_modified_at_column();

--
-- Site Info
--

create table site_info (
    host_name varchar,
    site_data jsonb not null,
    created_at timestamptz default now() not null,
    modified_at timestamptz default now() not null,
    primary key (host_name)
);

create trigger update_siteinfo_modified_at
    before update on site_info
    for each row execute procedure update_modified_at_column();


--
-- System Roles
--

create type system_role_type as enum(

    -- administrator roles
    'User Administrator',
    'Actor Administrator',
    'Content Administrator',

    -- actor roles
    'Person Owner',
    'Organization Owner',
    'Group Owner',
    'Application Owner',
    'Service Owner'
);

create table system_role (
    role_type system_role_type not null,
    member varchar not null references user_acct(acct),
    host_name varchar null references site_info(host_name),
    created_at timestamptz default now() not null,
    modified_at timestamptz default now() not null,
    primary key(role_type, member, host_name)
);

create or replace function make_system_permissions()
    returns trigger as $$
declare
    user_account varchar;
    user_system_permissions jsonb;
begin
    if (tg_op = 'DELETE') then
        user_account = OLD.member;
    else
        user_account = NEW.member;
    end if;
    select into user_system_permissions 
        jsonb_agg(
            jsonb_build_object(
                'role', role_type,
                'host_name', host_name
            )
        )
    from system_role
    where member=user_account group by member;
    if (user_system_permissions IS NULL) then
        user_system_permissions := '[]'::jsonb;
    end if;
    update user_acct
    set system_permissions=user_system_permissions
    where acct=user_account ;
    return null;
end
$$ language 'plpgsql';

create trigger make_system_permissions
    after update or insert or delete on system_role
    for each row execute procedure make_system_permissions();

create index on system_role(role_type, host_name);

--
-- Actors
--

create type actor_visibility_type as enum (
    'Visible', -- publicly visible and discoverable
    'Invisible', -- visible only to the user and administrators
    'Banned' -- visible only to the administrators
    );

-- should mirror activity pub actor types
create type actor_type as enum (
    'Application',
    'Group',
    'Organization',
    'Person',
    'Service'
    );

create table actor (
    id varchar primary key,
    type actor_type not null,
    acct varchar unique,
    webfinger_json jsonb,
    activity_pub_json jsonb not null,
    owner_data jsonb,
    collection_count integer default 0 not null check (collection_count >= 0),
    owner_count integer default 0 not null check (collection_count >= 0),
    visibility actor_visibility_type default 'Visible',
    system_data jsonb,
    event_log jsonb default '[]'::jsonb not null,
    created_at timestamptz default now() not null,
    modified_at timestamptz default now() not null
);

create index on actor(modified_at); -- used for pagination

create trigger update_actor_modified_at
    before update on actor
    for each row execute procedure update_modified_at_column();

create table actor_owner (
    actor_id varchar references actor(id) not null,
    user_owner varchar references user_acct(acct) not null,
    default_actor boolean default false,
    created_at timestamptz default now() not null,
    modified_at timestamptz default now() not null,
    primary key (actor_id, user_owner)
);

-- creates a unique index for the default actor allowing multiple false
create unique index on actor_owner(user_owner, default_actor) where (default_actor);

create trigger update_actor_owner_modified_at
    before update on actor_owner
    for each row execute procedure update_modified_at_column();

-- updates actor_summary of the user
create or replace function update_user_actor_summary()
    returns trigger as $$
declare
    user_account  varchar;
    person_count integer;
    organization_count integer;
    group_count integer;
    service_count integer;
    application_count integer;
begin
    if (tg_op = 'DELETE') then
        user_account  = OLD.user_owner;
    elsif (tg_op = 'INSERT') then
        user_account  = NEW.user_owner;
    end if;
    select count(*) into person_count from actor_owner,actor 
        where actor.type = 'Person' and actor_owner.actor_id=actor.id and actor_owner.user_owner=user_account ;
    select count(*) into organization_count from actor_owner,actor 
        where actor.type = 'Organization' and actor_owner.actor_id=actor.id and actor_owner.user_owner=user_account ;
    select count(*) into group_count from actor_owner,actor 
        where actor.type = 'Group' and actor_owner.actor_id=actor.id and actor_owner.user_owner=user_account ;
    select count(*) into service_count from actor_owner,actor 
        where actor.type = 'Service' and actor_owner.actor_id=actor.id and actor_owner.user_owner=user_account ;
    select count(*) into application_count from actor_owner,actor 
        where actor.type = 'Application' and actor_owner.actor_id=actor.id and actor_owner.user_owner=user_account ;
    update user_acct
    set actor_counts = 
        json_build_object(
            'Person', person_count,
            'Organization', organization_count,
            'Group', group_count,
            'Service', service_count,
            'Application', application_count
        )
    where 
        acct=user_account ;
    return null;
end
$$ language 'plpgsql';

create trigger update_user_actor_summary
    after insert or delete on actor_owner
    for each row execute procedure update_user_actor_summary();

-- updates the owner_count on actor
create or replace function update_actor_owner_count()
    returns trigger as $$
declare
    actor_id varchar;
begin
    if (tg_op = 'DELETE') then
        actor_id = OLD.actor_id;
        update actor
        set owner_count = owner_count -1
        where id=actor_id;
    elsif (tg_op = 'INSERT') then
        actor_id = NEW.actor_id;
        update actor
        set owner_count = owner_count +1
        where id=actor_id;
    end if;
    return null;
end
$$ language 'plpgsql';

create trigger update_actor_owner_count
    after insert or delete on actor_owner
    for each row execute procedure update_actor_owner_count();

-- updates the default_actor_data on user
create or replace function update_user_default_actor_data()
    returns trigger as $$
begin
    if (tg_op = 'DELETE' and OLD.default_actor = true) then
        update user_acct
        set default_actor_data = null
        where acct=OLD.user_owner;
    elsif (NEW.default_actor = true) then
        update user_acct
        set default_actor_data =
        (
            select
                -- this is equivalent to PublicActor in the Rust code
                json_build_object(
                    'ap', actor.activity_pub_json,
                    'jrd', actor.webfinger_json
                )
            from
                actor
            where
                actor.id=NEW.actor_id
        )
        where acct=NEW.user_owner;
    end if;
    return null;
end
$$ language 'plpgsql';

create trigger update_user_default_actor_data
    after update or insert or delete on actor_owner
    for each row execute procedure update_user_default_actor_data();


--
-- Collection
-- Has the data about the collection.
--
create type collection_visibility_type as enum (
    'PreVisible',
    'Visible',
    'Invisible' -- only admins can change out of Invisible
);

create table collection (
    id varchar primary key,
    actor_owner varchar references actor(id),
    activity_pub_json jsonb not null,
    visibility collection_visibility_type not null default 'Visible',
    owner_data jsonb,
    system_data jsonb,
    event_log jsonb default '[]'::jsonb not null,
    created_at timestamptz default now() not null,
    modified_at timestamptz default now() not null
);

create index on collection(id, actor_owner);

create trigger update_collection_modified_at
    before update on collection
    for each row execute procedure update_modified_at_column();

create or replace function update_actor_collection_count()
    returns trigger as $$
declare
    actor_id varchar;
begin
    if (tg_op = 'DELETE') then
        actor_id = OLD.actor_owner;
        update actor
        set collection_count = collection_count -1
        where id=actor_id;
    elsif (tg_op = 'INSERT') then
        actor_id = NEW.actor_owner;
        update actor
        set collection_count = collection_count +1
        where id=actor_id;
    end if;
    return null;
end
$$ language 'plpgsql';

create trigger update_actor_collection_count
    after insert or delete on collection
    for each row execute procedure update_actor_collection_count();
--
-- Collection Item
-- Has the tuple for insertion and deletion in collection
--

create table collection_item (
    id varchar references collection(id),
    object_ap_id varchar not null, -- any ActivityPub ID
    created_at timestamptz default now() not null,
    modified_at timestamptz default now() not null,
    primary key(id, object_ap_id)
);

create index on collection_item(created_at); -- used for pagination

create index on collection_item(id, object_ap_id);

create trigger update_collection_item_modified_at
    before update on collection_item
    for each row execute procedure update_modified_at_column();

--
-- Job Queues
--

create type job_type as enum (
    'No Operation',
    'Make Thumbnail',
    'Resize Actor Icon',
    'Resize Actor Banner',
    'Fetch Remote Icon',
    'Fetch Remote Banner',
    'Send Activity Pub'
    );

create table job_queue(
    id integer generated always as identity primary key,
    job_type job_type not null,
    details jsonb not null,
    attempts_remaining int default 1 not null,
    no_sooner_than timestamptz default now() not null,
    created_at timestamptz default now() not null
);

create index on job_queue(job_type, attempts_remaining, no_sooner_than);

--
-- AP Object - activitypub objects that can be "posted"
--

-- these should mirror the ActivityPub object type names
create type ap_object_type as enum (
    'Note',
    'Article',
    'Document',
    'Image',
    'Video',
    'Audio',
    'Page'
);

create type ap_object_visibility_type as enum (
    'PreVisible',
    'Visible',
    'Invisible' -- only admins can change out of Invisible
);

create table ap_object (
    id varchar primary key,
    type ap_object_type not null,
    actor_owner varchar references actor(id),
    received_for_host varchar references site_info(host_name) not null,
    activity_pub_json jsonb not null,
    owner_data jsonb,
    visibility ap_object_visibility_type not null default 'Visible',
    system_data jsonb,
    event_log jsonb default '[]'::jsonb not null,
    created_at timestamptz default now() not null,
    modified_at timestamptz default now() not null
);

create index on ap_object(modified_at, received_for_host); -- used for pagination
create index on ap_object(modified_at, actor_owner); -- used for pagination
create index on ap_object(modified_at, actor_owner, type); -- used for pagination

-- these expression indexes support queries such as
-- select activity_pub_json from ap_object where activity_pub_json -> 'to' ? 'http://example.com/foo';
create index on ap_object USING gin ((activity_pub_json->'to'));
create index on ap_object USING gin ((activity_pub_json->'bto'));
create index on ap_object USING gin ((activity_pub_json->'cc'));
create index on ap_object USING gin ((activity_pub_json->'bcc'));
create index on ap_object USING gin ((activity_pub_json->'actor'));
create index on ap_object USING gin ((activity_pub_json->'attributedTo'));

create trigger update_ap_object_modified_at
    before update on ap_object
    for each row execute procedure update_modified_at_column();


