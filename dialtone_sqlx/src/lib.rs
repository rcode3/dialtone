use std::string::FromUtf8Error;

use dialtone_common::ap::id::IdError;
use logic::ap_object::html::HtmlError;
use thiserror::Error;

pub mod control;
pub mod db;
pub mod logic;

#[derive(Error, Debug)]
pub enum DbError {
    #[error(transparent)]
    BadInput(#[from] BadInput),

    #[error("An internal error as occurred: {0}")]
    Internal(&'static str),

    #[error(transparent)]
    Sqlx(#[from] sqlx::Error),
}

#[derive(Error, Debug)]
pub enum BadInput {
    #[error("Invalid data for operation: {0}")]
    InvalidData(&'static str),

    #[error(transparent)]
    Bcrypt(#[from] bcrypt::BcryptError),

    #[error(transparent)]
    Utf8(#[from] FromUtf8Error),

    #[error(transparent)]
    BadHtml(#[from] HtmlError),

    #[error(transparent)]
    InvalidId(#[from] IdError),
}
