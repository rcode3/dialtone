use std::future::Future;

use dialtone_common::ap::{
    collection::{Collection, CollectionType},
    id::{create_collection_id, StandardCollectionNames},
};
use sqlx::{Acquire, Postgres};
use strum::IntoEnumIterator;

use crate::db::collection::insert::insert_into_collection;

/// Creates a collection by creating a collection ID and inserting the
/// collection in a database.
#[allow(clippy::manual_async_fn)]
pub fn create_collection<'a, 'c, A>(
    db: A,
    actor_id: &'a str,
    collection_name: &'a str,
) -> impl Future<Output = Result<String, crate::DbError>> + Send + 'a
where
    A: Acquire<'c, Database = Postgres> + Send + 'a,
{
    async move {
        let mut exec = db.acquire().await?;
        let collection_id = create_collection_id(actor_id, collection_name);
        let activity_pub_json = Collection::builder()
            .id(&collection_id)
            .name(collection_name)
            .collection_type(CollectionType::Collection)
            .items(vec![])
            .build();
        crate::db::collection::create::create_collection(
            &mut *exec,
            actor_id,
            &collection_id,
            &activity_pub_json,
        )
        .await?;
        Ok(collection_id)
    }
}

/// Creates a default set of collections.
/// See [dialtone_common::ap::id::StandardCollectionNames]
#[allow(clippy::manual_async_fn)]
pub fn create_standard_collections<'a, 'c, A>(
    db: A,
    actor_id: &'a str,
) -> impl Future<Output = Result<(), crate::DbError>> + Send + 'a
where
    A: Acquire<'c, Database = Postgres> + Send + 'a,
{
    async move {
        let mut exec = db.acquire().await?;
        for name in StandardCollectionNames::iter() {
            create_collection(&mut *exec, actor_id, &name.to_string()).await?;
        }
        Ok(())
    }
}

/// Place standard collections in the _public_collection collection if they are public.
/// See [dialtone_common::ap::id::StandardCollectionNames]
#[allow(clippy::manual_async_fn)]
pub fn place_standard_collections_in_public<'a, 'c, A>(
    db: A,
    actor_id: &'a str,
) -> impl Future<Output = Result<(), crate::DbError>> + Send + 'a
where
    A: Acquire<'c, Database = Postgres> + Send + 'a,
{
    async move {
        let mut exec = db.acquire().await?;
        let public_collection_id = create_collection_id(
            actor_id,
            &StandardCollectionNames::PublicCollections.to_string(),
        );
        for collection in StandardCollectionNames::iter() {
            let collection_id = create_collection_id(actor_id, &collection.to_string());
            if collection.public_collection() {
                insert_into_collection(&mut *exec, &public_collection_id, &collection_id).await?;
            }
        }
        Ok(())
    }
}
