use std::future::Future;

use crate::db::user::add_event::add_log_event as add_user_log_event;
use crate::{db::system_role::add::add_system_role, DbError};
use dialtone_common::rest::{event_log::LogEvent, users::web_user::SystemRoleType};
use sqlx::{Acquire, PgPool, Postgres};

pub async fn add_system_roles(
    pool: &PgPool,
    system_roles: &[SystemRoleType],
    acct: &str,
    host_name: &str,
) -> Result<(), DbError> {
    // begin transaction
    let mut tx = pool.begin().await?;

    add_system_roles_tx(&mut tx, system_roles, acct, host_name).await?;

    //commit transaction
    tx.commit().await?;

    Ok(())
}

#[allow(clippy::manual_async_fn)]
pub fn add_system_roles_tx<'a, 'c, A>(
    db: A,
    system_roles: &'a [SystemRoleType],
    acct: &'a str,
    host_name: &'a str,
) -> impl Future<Output = Result<(), DbError>> + Send + 'a
where
    A: Acquire<'c, Database = Postgres> + Send + 'a,
{
    async move {
        let mut exec = db.acquire().await?;
        for system_role in system_roles {
            add_system_role(&mut *exec, system_role, acct, host_name).await?;
        }

        if !system_roles.is_empty() {
            // add log event for user
            let all_role_names = system_roles
                .iter()
                .map(|r| r.to_string())
                .collect::<Vec<String>>()
                .join(", ");
            add_user_log_event(
                &mut *exec,
                acct,
                &LogEvent::new(format!(
                    "Added system roles {all_role_names} on {host_name}.",
                )),
            )
            .await?;
        }

        Ok(())
    }
}
