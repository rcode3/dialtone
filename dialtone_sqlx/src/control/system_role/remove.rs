use crate::db::user::add_event::add_log_event as add_user_log_event;
use crate::{db::system_role::remove::remove_system_role, DbError};
use dialtone_common::rest::{event_log::LogEvent, users::web_user::SystemRoleType};
use sqlx::{PgPool, Postgres, Transaction};

pub async fn remove_system_roles(
    pool: &PgPool,
    system_roles: &[SystemRoleType],
    acct: &str,
    host_name: &str,
) -> Result<(), DbError> {
    // begin transaction
    let mut tx = pool.begin().await?;

    remove_system_roles_tx(&mut tx, system_roles, acct, host_name).await?;

    //commit transaction
    tx.commit().await?;

    Ok(())
}

pub async fn remove_system_roles_tx(
    tx: &mut Transaction<'_, Postgres>,
    system_roles: &[SystemRoleType],
    acct: &str,
    host_name: &str,
) -> Result<(), DbError> {
    for system_role in system_roles {
        remove_system_role(&mut *tx, system_role, acct, host_name).await?;
    }

    if !system_roles.is_empty() {
        // add log event for user
        let all_role_names = system_roles
            .iter()
            .map(|r| r.to_string())
            .collect::<Vec<String>>()
            .join(", ");
        add_user_log_event(
            &mut *tx,
            acct,
            &LogEvent::new(format!(
                "Removed system roles {all_role_names} on {host_name}.",
            )),
        )
        .await?;
    }

    Ok(())
}
