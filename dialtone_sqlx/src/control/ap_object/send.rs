use std::collections::HashSet;

use chrono::{DateTime, Utc};
use dialtone_common::{
    ap::{
        activity::create_ap_object_activity,
        actor::Actor,
        ap_object::ApObject,
        collection::CollectionItem,
        id::{
            create_actor_id, create_collection_id, parse_actors_host, ExtraCollectionNames,
            SiteActors, StandardCollectionNames,
        },
    },
    rest::actors::actor_model::OwnedActor,
};
use sqlx::{Executor, PgPool, Postgres};
use tracing::log::info;
use tracing::log::warn;

use crate::{
    db::{
        actor::{fetch_owned::fetch_owned_actor_by_id, fetch_public::fetch_public_actor_by_id},
        collection::{
            insert::{insert_into_collection, insert_into_related_collections},
            page::page_by_id,
        },
        persistent_queue::insert_job::insert_job,
    },
    logic::persistent_queue::job_details::{JobDetails, SendActivityPubJobDetails},
    BadInput, DbError,
};

use super::create::create_ap_object;

// TODO handle any collection. if public but other collection, goes into outbox and that collection
pub async fn send(pg_pool: &PgPool, ap_object: &ApObject, sender: &Actor) -> Result<(), DbError> {
    let ap_object_id = if let Some(ref ap_object_id) = ap_object.id {
        ap_object_id
    } else {
        return Err(DbError::BadInput(BadInput::InvalidData(
            "ap_object does not have an id",
        )));
    };
    let host_name = parse_actors_host(&sender.id).map_err(BadInput::from)?;

    let mut tx = pg_pool.begin().await?;

    // commit the ap_object to the database
    create_ap_object(&mut *tx, ap_object, &host_name).await?;

    let mut deliver_to_followers = false;

    // insert into public collection if addressed to public
    let public_collection_id = create_collection_id(
        &create_actor_id(&host_name, &SiteActors::PublicContent.to_string()),
        &ExtraCollectionNames::NetworkContent.to_string(),
    );
    if ap_object.public_addressed() {
        let outbox_collection_id =
            create_collection_id(&sender.id, &StandardCollectionNames::Outbox.to_string());
        insert_into_collection(&mut *tx, &outbox_collection_id, ap_object_id).await?;
        insert_into_collection(&mut *tx, &public_collection_id, ap_object_id).await?;
        info!(
            "Post by {} for {ap_object_id} addressed to public content.",
            sender.id
        );
        deliver_to_followers = true;
    }

    // deliver to local followers
    if deliver_to_followers {
        insert_into_related_collections(
            &mut *tx,
            &sender.id,
            StandardCollectionNames::Following.into(),
            StandardCollectionNames::Inbox.into(),
            ap_object_id,
        )
        .await?;
        info!(
            "Post by {} of {ap_object_id} delivered to local followers.",
            sender.id
        );
    };

    let actor_owned_data =
        fetch_owned_actor_by_id(&mut *tx, &sender.id)
            .await?
            .ok_or(DbError::Internal(
                "Lookup of owned actor failed when sending activity pub object.",
            ))?;

    // send to explicit addresses
    let mut shared_inboxes: HashSet<String> = HashSet::new();
    for addr in ap_object.addresses() {
        let addr_host = parse_actors_host(addr);
        if let Ok(addr_host) = addr_host {
            if addr_host.eq(&host_name) {
                let target_collection_id =
                    create_collection_id(addr, &StandardCollectionNames::Inbox.to_string());
                insert_into_collection(&mut *tx, &target_collection_id, ap_object_id).await?;
                info!(
                    "Post by {} of {ap_object_id} addressed to {target_collection_id}",
                    sender.id
                );
            } else {
                let remote_actor = fetch_public_actor_by_id(&mut *tx, addr).await?;
                if let Some(remote_actor) = remote_actor {
                    if let Some(shared_inbox) = remote_actor.ap.shared_inbox() {
                        shared_inboxes.insert(shared_inbox.to_owned());
                    } else if let Some(inbox) = &remote_actor.ap.inbox {
                        queue_for_delivery(&mut *tx, &actor_owned_data, inbox, ap_object).await?;
                    } else {
                        warn!("{} has no inbox.", remote_actor.ap.id);
                    }
                } else {
                    warn!("Unknown actor {addr}");
                }
            }
        } else {
            warn!("Cannot extract host from actor id {addr}");
        }
    }

    // send to explicit blind addresses
    for addr in ap_object.blind_addresses() {
        let addr_host = parse_actors_host(addr);
        if let Ok(addr_host) = addr_host {
            if addr_host.eq(&host_name) {
                let target_collection_id =
                    create_collection_id(addr, &StandardCollectionNames::Inbox.to_string());
                insert_into_collection(&mut *tx, &target_collection_id, ap_object_id).await?;
                info!(
                    "Post by {} of {ap_object_id} blind addressed to {target_collection_id}",
                    sender.id
                );
            } else {
                let remote_actor = fetch_public_actor_by_id(&mut *tx, addr).await?;
                if let Some(remote_actor) = remote_actor {
                    if let Some(inbox) = &remote_actor.ap.inbox {
                        queue_for_delivery(&mut *tx, &actor_owned_data, inbox, ap_object).await?;
                    } else {
                        warn!("{} has no inbox.", remote_actor.ap.id);
                    }
                } else {
                    warn!("Unknown actor {addr}");
                }
            }
        } else {
            warn!("Cannot extract host from actor id {addr}");
        }
    }

    // send to followers
    let following_collection =
        create_collection_id(&sender.id, &StandardCollectionNames::Followers.to_string());
    let mut next: Option<&DateTime<Utc>> = None;
    let mut next_date;
    loop {
        let collection_page =
            page_by_id(&mut *tx, None, next, 100, &following_collection, None).await?;
        if let Some(collection_page) = collection_page {
            for item in collection_page.items.iter() {
                if let CollectionItem::Actor(actor) = item {
                    if let Some(shared_inbox) = actor.shared_inbox() {
                        // only put in the shared inbox set if not also a blind address
                        // because blind addresses are not sent via shared inbox.
                        // blind addresses are handled above
                        if !ap_object.any_blind_address(&actor.id) {
                            shared_inboxes.insert(shared_inbox.to_owned());
                        }
                    } else if !(ap_object.any_address(&actor.id)
                        || ap_object.any_blind_address(&actor.id))
                    {
                        // direct delivery of to, cc, bto, and bcc are handled above
                        // so this prevents sending twice
                        if let Some(inbox) = &actor.inbox {
                            queue_for_delivery(&mut *tx, &actor_owned_data, inbox, ap_object)
                                .await?;
                        } else {
                            warn!("{} has no inbox.", actor.id);
                        }
                    }
                }
            }
            if let Some(collection_next) = collection_page.next {
                next_date = DateTime::parse_from_rfc3339(&collection_next)
                    .map_err(|_| DbError::Internal("Bad datetime in collection.next"))?
                    .with_timezone(&Utc);
                next = Some(&next_date);
            }
        } else {
            break;
        }
        if next.is_none() {
            break;
        }
    }

    // send to shared inboxes
    for shared_inbox in shared_inboxes.iter() {
        queue_for_delivery(&mut *tx, &actor_owned_data, shared_inbox, ap_object).await?;
    }

    tx.commit().await?;
    Ok(())
}

async fn queue_for_delivery(
    exec: impl Executor<'_, Database = Postgres>,
    sender: &OwnedActor,
    target_uri: &str,
    ap_object: &ApObject,
) -> Result<(), DbError> {
    let key_pairs = &sender
        .owner_data
        .as_ref()
        .ok_or(DbError::Internal("Sending actor has now onwer data."))?
        .key_pairs;
    let public_key_id = key_pairs
        .first()
        .ok_or(DbError::Internal("Sender has no key pairs."))?
        .key_id
        .to_owned();
    let private_key_pem = key_pairs
        .first()
        .ok_or(DbError::Internal("Sender has no key pairs."))?
        .private_key_pem
        .to_owned();
    let activity = create_ap_object_activity(&sender.ap.id, ap_object);
    let job_details = JobDetails::SendActivityPub(SendActivityPubJobDetails {
        public_key_id,
        private_key_pem,
        activity,
        target_uri: target_uri.to_owned(),
    });
    insert_job(
        exec,
        &crate::db::persistent_queue::JobDbType::SendActivityPub,
        &job_details,
        Some(3),
        None,
    )
    .await?;
    Ok(())
}
