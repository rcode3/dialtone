use dialtone_common::ap::{
    actor::Actor,
    ap_object::ApObject,
    id::{
        create_actor_id, create_collection_id, parse_actors_host, ExtraCollectionNames, SiteActors,
        StandardCollectionNames,
    },
};
use sqlx::PgPool;
use tracing::log::info;
use tracing::log::warn;

use crate::{
    db::collection::insert::{insert_into_collection, insert_into_related_collections},
    BadInput, DbError,
};

use super::create::create_ap_object;

/// Delivers an ApObject.
///
/// Parameters
/// * ap_object: the object to deliver
/// * sender: the actor sending the ap object
/// * host_name: host_name of the receiving site
/// * target_collection_id: collection_id of the targetted inbox
///
/// If present, the target_actor_id will be added
/// the other delivery addresses.
pub async fn deliver(
    pg_pool: &PgPool,
    ap_object: &ApObject,
    sender: &Actor,
    host_name: &str,
    target_collection_id: Option<&str>,
) -> Result<(), DbError> {
    let ap_object_id = if let Some(ref ap_object_id) = ap_object.id {
        ap_object_id
    } else {
        return Err(DbError::BadInput(BadInput::InvalidData(
            "ap_object does not have an id",
        )));
    };
    let mut tx = pg_pool.begin().await?;

    // commit the ap_object to the database
    create_ap_object(&mut *tx, ap_object, host_name).await?;
    let mut deliver_to_followers = false;
    let network_collection_id = create_collection_id(
        &create_actor_id(host_name, &SiteActors::PublicContent.to_string()),
        &ExtraCollectionNames::NetworkContent.to_string(),
    );

    // if it is addresseed to the public, put in in the network content collection
    if ap_object.public_addressed() {
        insert_into_collection(&mut *tx, &network_collection_id, ap_object_id).await?;
        info!(
            "Post by {} for {ap_object_id} addressed to network content.",
            sender.id
        );
        deliver_to_followers = true;
    }

    // deliver to local followers if publicly addressed
    if deliver_to_followers {
        insert_into_related_collections(
            &mut *tx,
            &sender.id,
            StandardCollectionNames::Following.into(),
            StandardCollectionNames::Inbox.into(),
            ap_object_id,
        )
        .await?;
        info!(
            "Post by {} of {ap_object_id} delivered to followers.",
            sender.id
        );
    };

    // if specificially targeted via inbox, deliver to that actor
    if let Some(target_collection_id) = target_collection_id {
        insert_into_collection(&mut *tx, target_collection_id, ap_object_id).await?;
        info!(
            "Post by {} of {ap_object_id} to {target_collection_id}.",
            sender.id
        );
    };

    // deliver to local actors explicitly
    for addr in ap_object.addresses() {
        let addr_host = parse_actors_host(addr);
        if let Ok(addr_host) = addr_host {
            if addr_host.eq(host_name) {
                let target_collection_id =
                    create_collection_id(addr, &StandardCollectionNames::Inbox.to_string());
                insert_into_collection(&mut *tx, &target_collection_id, ap_object_id).await?;
                info!(
                    "Post by {} of {ap_object_id} addressed to {target_collection_id}",
                    sender.id
                );
            }
        } else {
            warn!("Cannot extract host from actor id {addr}");
        }
    }

    tx.commit().await?;
    Ok(())
}
