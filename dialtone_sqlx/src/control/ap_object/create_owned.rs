use crate::db::ap_object::insert::insert_ap_object;
use crate::db::ap_object::{ApObjectDbType, ApObjectVisibilityDbType};
use crate::logic::ap_object::new::new_html_ap_object;
use crate::{BadInput, DbError};
use dialtone_common::ap::ap_object::ApObject;
use dialtone_common::rest::ap_objects::ap_object_model::CreateOwnedApObject;
use sqlx::PgPool;

pub async fn create_owned_ap_object(
    pool: &PgPool,
    host_name: &str,
    preferred_user_name: &str,
    actor_id: &str,
    create_ap_object: &CreateOwnedApObject,
) -> Result<ApObject, DbError> {
    let ap_object = new_html_ap_object(host_name, preferred_user_name, actor_id, create_ap_object)?;
    let _id = ap_object
        .id
        .clone()
        .ok_or(BadInput::InvalidData("no ID created for AP Object"))?;
    let visibility = ApObjectVisibilityDbType::Visible;
    let ap_object_type = ApObjectDbType::from(&create_ap_object.ap_type);
    insert_ap_object(
        pool,
        host_name,
        &ap_object,
        None,
        ap_object_type,
        None,
        visibility,
    )
    .await?;
    Ok(ap_object)
}
