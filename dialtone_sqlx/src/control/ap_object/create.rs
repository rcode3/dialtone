use crate::db::ap_object::insert::insert_ap_object;
use crate::db::ap_object::{ApObjectDbType, ApObjectVisibilityDbType};
use crate::{BadInput, DbError};
use dialtone_common::ap::ap_object::ApObject;
use sqlx::{Executor, Postgres};

pub async fn create_ap_object(
    exec: impl Executor<'_, Database = Postgres>,
    ap_object: &ApObject,
    host_name: &str,
) -> Result<String, DbError> {
    let id = ap_object
        .id
        .clone()
        .ok_or(BadInput::InvalidData("no ID in AP Object"))?;
    let visibility = ApObjectVisibilityDbType::Visible;
    let ap_type = ap_object
        .ap_type
        .as_ref()
        .ok_or(BadInput::InvalidData("AP Object has no type"))?;
    let ap_object_type = ApObjectDbType::from(ap_type);
    insert_ap_object(
        exec,
        host_name,
        ap_object,
        None,
        ap_object_type,
        None,
        visibility,
    )
    .await?;
    Ok(id)
}
