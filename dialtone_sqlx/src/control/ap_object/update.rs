use dialtone_common::rest::ap_objects::ap_object_model::UpdateOwnedApObject;
use sqlx::PgPool;

use crate::{db::ap_object::fetch_owned::fetch_owned_ap_object, DbError};

pub async fn update_owned_ap_object(
    pool: &PgPool,
    ap_object_id: &str,
    update: UpdateOwnedApObject,
) -> Result<Option<String>, DbError> {
    let mut tx = pool.begin().await?;
    let owned_ap_object = fetch_owned_ap_object(&mut tx, ap_object_id).await?;
    let result: Option<String> = match owned_ap_object {
        Some(owned_ap_object) => {
            let owner_data = update.owner_data.to_owned();
            let updated_ap_object =
                crate::logic::ap_object::update::update_ap_object(owned_ap_object.ap, update);
            crate::db::ap_object::update::update_ap_object(
                &mut tx,
                &updated_ap_object,
                owner_data.as_ref(),
            )
            .await?;
            Some(updated_ap_object.id.unwrap())
        }
        None => None,
    };
    tx.commit().await?;
    Ok(result)
}
