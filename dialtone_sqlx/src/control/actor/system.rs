use std::future::Future;

use dialtone_common::ap::{actor::ActorType, id::SiteActors};
use sqlx::{Acquire, Postgres};
use strum::IntoEnumIterator;

use crate::{logic::actor::new_actor::NewActor, DbError};

use super::create_owned::{create_credentialed_actor_with_options, CreateCredentialedOptions};

#[allow(clippy::manual_async_fn)]
pub fn create_site_actors<'a, 'c, A>(
    db: A,
    host_name: &'a str,
    owner_acct: &'a str,
) -> impl Future<Output = Result<(), crate::DbError>> + Send + 'a
where
    A: Acquire<'c, Database = Postgres> + Send + 'a,
{
    async move {
        let mut exec = db.acquire().await?;
        for actor in SiteActors::iter() {
            let create_new_actor = NewActor::builder()
                .preferred_user_name(actor.to_string())
                .actor_type(ActorType::Service)
                .owner(owner_acct)
                .build();
            let extra_collections = actor
                .extra_collections()
                .iter()
                .map(|c| c.to_string())
                .collect::<Vec<String>>();
            let options = CreateCredentialedOptions {
                extra_collections: Some(extra_collections),
                add_to_site_collections: false,
            };
            let _created_actor = create_credentialed_actor_with_options(
                &mut *exec,
                create_new_actor,
                false,
                host_name,
                options,
            )
            .await?
            .ok_or(DbError::Internal("counld not create host actor"))?;
        }
        Ok(())
    }
}
