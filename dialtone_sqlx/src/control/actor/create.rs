use sqlx::PgPool;

use dialtone_common::rest::actors::actor_model::PublicActor;

use crate::db::actor::insert::{insert_actor, InsertActor};
use crate::db::actor::{ActorDbType, ActorVisibilityType};
use crate::DbError;

/// For use in creating actors from other systems.
pub async fn create_actor(
    pool: &PgPool,
    public_actor: PublicActor,
) -> Result<PublicActor, DbError> {
    let insert_data = InsertActor::builder()
        .actor_type(ActorDbType::from(&public_actor.ap.ap_type))
        .and_webfinger_jrd(public_actor.jrd)
        .activity_pub(public_actor.ap)
        .visibility(ActorVisibilityType::Visible)
        .build();
    insert_actor(pool, &insert_data).await?;
    Ok(PublicActor {
        ap: insert_data.activity_pub,
        jrd: insert_data.webfinger_jrd,
    })
}
