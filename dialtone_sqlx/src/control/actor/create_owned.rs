use std::future::Future;

use crate::control::collection::create::{
    create_collection, create_standard_collections, place_standard_collections_in_public,
};
use crate::db::actor::add_event::add_log_event as add_actor_log_event;
use crate::db::actor::insert::{insert_actor, InsertActor};
use crate::db::actor::{ActorDbType, ActorVisibilityType};
use crate::db::actor_owner::insert::insert_actor_owner;
use crate::db::ap_object::fetch_random::fetch_random_ap_object_by_collection;
use crate::db::collection::insert::insert_into_collection;
use crate::db::user::add_event::add_log_event as add_user_log_event;
use crate::logic::actor::new_actor::{
    banner_from_ap_object, icon_from_ap_object, new_actor, new_actor_with_images, NewActor,
};
use crate::DbError;
use dialtone_common::ap::ap_object::ApObjectType;
use dialtone_common::ap::id::{
    create_actor_id, create_collection_id, ExtraCollectionNames, SiteActors,
};
use dialtone_common::containers::credentialed_actor::CredentialedActor;
use dialtone_common::media::{site_banners_collection_id, site_icons_collection_id};
use dialtone_common::rest::actors::actor_model::OwnedActor;
use dialtone_common::rest::event_log::LogEvent;
use sqlx::{Acquire, PgPool, Postgres};

pub async fn create_credentialed_actor(
    pool: &PgPool,
    new_actor_info: NewActor,
    default_actor: bool,
    host_name: &str,
) -> Result<Option<CredentialedActor>, DbError> {
    // begin transaction
    let mut tx = pool.begin().await?;

    let options = CreateCredentialedOptions {
        extra_collections: None,
        add_to_site_collections: true,
    };
    let result = create_credentialed_actor_with_options(
        &mut tx,
        new_actor_info,
        default_actor,
        host_name,
        options,
    )
    .await?;

    tx.commit().await?;
    Ok(result)
}

pub struct CreateCredentialedOptions {
    pub extra_collections: Option<Vec<String>>,
    pub add_to_site_collections: bool,
}

#[allow(clippy::manual_async_fn)]
pub fn create_credentialed_actor_with_options<'a, 'c, A>(
    db: A,
    new_actor_info: NewActor,
    default_actor: bool,
    host_name: &'a str,
    CreateCredentialedOptions {
        extra_collections,
        add_to_site_collections,
    }: CreateCredentialedOptions,
) -> impl Future<Output = Result<Option<CredentialedActor>, crate::DbError>> + Send + 'a
where
    A: Acquire<'c, Database = Postgres> + Send + 'a,
{
    async move {
        let mut exec = db.acquire().await?;

        let banner_ap_object = fetch_random_ap_object_by_collection(
            &mut *exec,
            &ApObjectType::Image,
            &site_banners_collection_id(host_name),
        )
        .await?;
        let banner = banner_from_ap_object(banner_ap_object);

        let icon_ap_object = fetch_random_ap_object_by_collection(
            &mut *exec,
            &ApObjectType::Image,
            &site_icons_collection_id(host_name),
        )
        .await?;
        let icon = icon_from_ap_object(icon_ap_object);

        let new_actor_info = new_actor_with_images(new_actor_info, icon, banner);

        let credentialed_actor = new_actor(host_name, new_actor_info);
        let webfinger_jrd = credentialed_actor.owned_actor.jrd;
        let insert_actor_data = InsertActor::builder()
            .actor_type(ActorDbType::from(
                &credentialed_actor.owned_actor.ap.ap_type,
            ))
            .and_webfinger_jrd(webfinger_jrd)
            .activity_pub(credentialed_actor.owned_actor.ap)
            .visibility(ActorVisibilityType::Visible)
            .build();

        // insert actor
        let result = insert_actor(&mut *exec, &insert_actor_data).await?;
        if !result {
            return Ok(None);
        }

        // link actor to owner
        insert_actor_owner(
            &mut *exec,
            &insert_actor_data.activity_pub.id,
            &credentialed_actor.creating_user_acct,
            default_actor,
        )
        .await?;

        create_standard_collections(&mut *exec, &insert_actor_data.activity_pub.id).await?;
        place_standard_collections_in_public(&mut *exec, &insert_actor_data.activity_pub.id)
            .await?;

        // create extra collections
        if let Some(extra_collections) = extra_collections {
            for name in extra_collections.iter() {
                create_collection(&mut *exec, &insert_actor_data.activity_pub.id, name).await?;
            }
        }

        // add log event for user
        add_user_log_event(
            &mut *exec,
            &credentialed_actor.creating_user_acct,
            &LogEvent::new(format!(
                "{} created by {}. Set to default actor: {}.",
                &insert_actor_data.activity_pub.id,
                &credentialed_actor.creating_user_acct,
                default_actor
            )),
        )
        .await?;

        // add log event for actor
        add_actor_log_event(
            &mut *exec,
            &insert_actor_data.activity_pub.id,
            &LogEvent::new(format!(
                "Created by {}. Set to default actor: {}.",
                &credentialed_actor.creating_user_acct, default_actor
            )),
        )
        .await?;

        if add_to_site_collections {
            let site_actors_id = create_actor_id(host_name, &SiteActors::Actors.to_string());
            let all_actors_id = create_collection_id(
                &site_actors_id,
                &ExtraCollectionNames::AllSiteActors.to_string(),
            );
            insert_into_collection(
                &mut *exec,
                &all_actors_id,
                &insert_actor_data.activity_pub.id,
            )
            .await?;
            let actor_type_id = match insert_actor_data.actor_type {
                ActorDbType::Application => create_collection_id(
                    &site_actors_id,
                    &ExtraCollectionNames::Applications.to_string(),
                ),
                ActorDbType::Group => {
                    create_collection_id(&site_actors_id, &ExtraCollectionNames::Groups.to_string())
                }
                ActorDbType::Organization => create_collection_id(
                    &site_actors_id,
                    &ExtraCollectionNames::Organizations.to_string(),
                ),
                ActorDbType::Person => create_collection_id(
                    &site_actors_id,
                    &ExtraCollectionNames::Persons.to_string(),
                ),
                ActorDbType::Service => create_collection_id(
                    &site_actors_id,
                    &ExtraCollectionNames::Services.to_string(),
                ),
            };
            insert_into_collection(
                &mut *exec,
                &actor_type_id,
                &insert_actor_data.activity_pub.id,
            )
            .await?;
        }

        Ok(Some(CredentialedActor {
            owned_actor: OwnedActor {
                ap: insert_actor_data.activity_pub,
                jrd: insert_actor_data.webfinger_jrd,
                ..credentialed_actor.owned_actor
            },
            ..credentialed_actor
        }))
    }
}
