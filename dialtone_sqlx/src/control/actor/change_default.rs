use crate::db::actor::add_event::add_log_event as add_actor_log_event;
use crate::db::actor_owner::update_default::{clear_default_actor, set_default_actor};
use crate::db::user::add_event::add_log_event as add_user_log_event;
use crate::DbError;
use dialtone_common::rest::event_log::LogEvent;
use sqlx::PgPool;

pub async fn change_default_actor(
    pool: &PgPool,
    owner_acct: &str,
    actor_id: &str,
) -> Result<Option<()>, DbError> {
    let mut tx = pool.begin().await?;
    clear_default_actor(&mut tx, owner_acct).await?;
    set_default_actor(&mut tx, actor_id, owner_acct).await?;
    add_actor_log_event(
        &mut tx,
        actor_id,
        &LogEvent::new(format!("{actor_id} set to default actor for {owner_acct}.")),
    )
    .await?;
    add_user_log_event(
        &mut tx,
        owner_acct,
        &LogEvent::new(format!("{actor_id} set to default actor.")),
    )
    .await?;
    tx.commit().await?;
    Ok(Some(()))
}
