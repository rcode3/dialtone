use dialtone_common::{
    ap::{actor::Actor, ap_object::ApObject},
    rest::{actors::actor_model::UpdateActorAp, ap_objects::ap_object_model::ApObjectSystemData},
};
use sqlx::PgPool;

use crate::{
    db::{
        actor::update_ap::update_actor_ap,
        ap_object::{insert::insert_ap_object, ApObjectDbType},
    },
    BadInput, DbError,
};

/// A transaction to update an actor and an insert an associated ap object.
pub async fn update_actor_with_image_attr_insert(
    pool: &PgPool,
    actor: Actor,
    ap_object: ApObject,
    host_name: &str,
    system_data: ApObjectSystemData,
) -> Result<Option<()>, DbError> {
    // begin transaction
    let mut tx = pool.begin().await?;
    let update_ap = UpdateActorAp {
        name: actor.name,
        summary: actor.summary,
        icon: actor.icon,
        image: actor.image,
    };

    update_actor_ap(&mut tx, update_ap, &actor.id)
        .await?
        .ok_or(BadInput::InvalidData(
            "attempt to update actor that does not exist",
        ))?;

    let db_type = ApObjectDbType::from(&ap_object.ap_type.clone().ok_or(BadInput::InvalidData(
        "Ap Object does not have an Ap Object Type",
    ))?);
    insert_ap_object(
        &mut tx,
        host_name,
        &ap_object,
        None,
        db_type,
        Some(&system_data),
        crate::db::ap_object::ApObjectVisibilityDbType::Visible,
    )
    .await?;

    // commit transaction
    tx.commit().await?;
    Ok(Some(()))
}
