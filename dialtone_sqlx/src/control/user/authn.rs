use crate::db::user::check_bcrypt;
use crate::logic::user::auth::bcrypted_pw;
use crate::DbError;
use sqlx::types::chrono::{DateTime, Utc};
use sqlx::PgPool;

pub async fn authn_user(
    pool: &PgPool,
    acct: &str,
    password: &str,
    login_from: &str,
    login_at: DateTime<Utc>,
) -> Result<bool, DbError> {
    let acct = acct.to_lowercase();
    let bcrypt_password = bcrypted_pw(&acct, password)?;
    let login_data =
        check_bcrypt::check_bcrypt(pool, &acct, &bcrypt_password, login_from, login_at).await?;
    if let Some(last_login) = login_data {
        let retval = last_login
            .first()
            .ok_or(DbError::Internal("no login data"))?
            .success;
        Ok(retval)
    } else {
        Ok(false)
    }
}
