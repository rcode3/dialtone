use std::future::Future;

use crate::db::user::insert::insert_user;
use sqlx::{Acquire, Postgres};

use crate::logic::user::auth::new_auth;

#[allow(clippy::manual_async_fn)]
pub fn create_user<'a, 'c, A>(
    db: A,
    acct: &'a str,
    password: &'a str,
) -> impl Future<Output = Result<(), crate::DbError>> + Send + 'a
where
    A: Acquire<'c, Database = Postgres> + Send + 'a,
{
    async move {
        let mut exec = db.acquire().await?;
        let auth_data = new_auth(acct, password)?;
        insert_user(&mut *exec, acct, auth_data).await?;
        Ok(())
    }
}
