use std::future::Future;

use crate::control::actor::create_owned::{
    create_credentialed_actor_with_options, CreateCredentialedOptions,
};
use crate::control::system_role::add::add_system_roles_tx;
use crate::db::user::add_event::add_log_event as add_user_log_event;
use crate::db::user::insert::insert_user;
use crate::logic::actor::new_actor::NewActor;
use crate::logic::user::auth::new_auth;
use crate::DbError;
use dialtone_common::ap::actor::ActorType;
use dialtone_common::ap::pun::create_preferred_user_name;
use dialtone_common::containers::credentialed_actor::CredentialedActor;
use dialtone_common::rest::event_log::LogEvent;
use dialtone_common::rest::users::web_user::SystemRoleType;
use dialtone_common::utils::make_acct::make_acct;
use sqlx::{Acquire, PgPool, Postgres};

pub async fn create_user_with_default_actor(
    pool: &PgPool,
    user_name: &str,
    host_name: &str,
    password: &str,
    system_roles: &[SystemRoleType],
) -> Result<CredentialedActor, DbError> {
    // begin transaction
    let mut tx = pool.begin().await?;

    let result =
        create_user_with_default_actor_tx(&mut tx, user_name, host_name, password, system_roles)
            .await?;

    //commit transaction
    tx.commit().await?;

    //return result
    Ok(result)
}

#[allow(clippy::manual_async_fn)]
pub fn create_user_with_default_actor_tx<'a, 'c, A>(
    db: A,
    user_name: &'a str,
    host_name: &'a str,
    password: &'a str,
    system_roles: &'a [SystemRoleType],
) -> impl Future<Output = Result<CredentialedActor, DbError>> + Send + 'a
where
    A: Acquire<'c, Database = Postgres> + Send + 'a,
{
    async move {
        let mut exec = db.acquire().await?;

        // create new user data
        let user_acct = make_acct(&user_name.to_lowercase(), host_name);
        let auth_data = new_auth(&user_acct, password)?;

        let preferred_user_name = create_preferred_user_name(user_name, &ActorType::Person);

        // insert the new user in the database
        insert_user(&mut *exec, &user_acct, auth_data).await?;

        // add system roles
        add_system_roles_tx(&mut *exec, system_roles, &user_acct, host_name).await?;

        let new_actor_info = NewActor::builder()
            .preferred_user_name(preferred_user_name)
            .actor_type(ActorType::Person)
            .owner(&user_acct)
            .build();
        let options = CreateCredentialedOptions {
            extra_collections: None,
            add_to_site_collections: true,
        };
        let result = create_credentialed_actor_with_options(
            &mut *exec,
            new_actor_info,
            true,
            host_name,
            options,
        )
        .await?
        .ok_or(DbError::Internal(
            "Unable to create default actor for user.",
        ))?;

        // add log event for user
        add_user_log_event(
            &mut *exec,
            &user_acct,
            &LogEvent::new(format!(
                "{user_acct} created with default actor {}.",
                &result.owned_actor.ap.id,
            )),
        )
        .await?;

        //return result
        Ok(result)
    }
}
