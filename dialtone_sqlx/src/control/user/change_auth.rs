use crate::logic::user::auth::new_auth;
use crate::{db::user::update_auth::update_user_auth, DbError};
use sqlx::{Executor, Postgres};

pub async fn change_user_auth(
    exec: impl Executor<'_, Database = Postgres>,
    acct: &str,
    password: &str,
) -> Result<bool, DbError> {
    let auth_data = new_auth(acct, password)?;
    let result = update_user_auth(exec, acct, auth_data).await?;
    match result {
        None => Ok(false),
        Some(_) => Ok(true),
    }
}
