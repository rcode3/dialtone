use crate::db::user::check_name_available::check_user_name_available;
use crate::DbError;
use dialtone_common::ap::actor::ActorType;
use dialtone_common::ap::id::create_actor_id;
use dialtone_common::ap::pun::create_preferred_user_name;
use dialtone_common::utils::make_acct::make_acct;
use sqlx::PgPool;

pub async fn check_user_name_availability(
    pool: &PgPool,
    user_name: &str,
    host_name: &str,
) -> Result<bool, DbError> {
    let user_name = user_name.to_lowercase();

    let person_pun = create_preferred_user_name(&user_name, &ActorType::Person);
    let group_pun = create_preferred_user_name(&user_name, &ActorType::Group);
    let service_pun = create_preferred_user_name(&user_name, &ActorType::Service);
    let organization_pun = create_preferred_user_name(&user_name, &ActorType::Organization);
    let application_pun = create_preferred_user_name(&user_name, &ActorType::Application);

    let person_id = create_actor_id(host_name, &person_pun);
    let group_id = create_actor_id(host_name, &group_pun);
    let service_id = create_actor_id(host_name, &service_pun);
    let organization_id = create_actor_id(host_name, &organization_pun);
    let application_id = create_actor_id(host_name, &application_pun);

    let actor_ids = &[
        person_id.as_str(),
        group_id.as_str(),
        service_id.as_str(),
        organization_id.as_str(),
        application_id.as_str(),
    ];
    let acct_name = make_acct(&user_name, host_name);
    let action = check_user_name_available(pool, &acct_name, actor_ids).await?;
    Ok(action)
}
