use crate::db::return_optional;
use crate::db::site_info::SITE_INFO_JSON_OBJECT;
use const_format::concatcp;
use dialtone_common::rest::sites::site_data::SiteInfo;
use sqlx::{Executor, Postgres};

pub async fn fetch_site(
    exec: impl Executor<'_, Database = Postgres>,
    host_name: &str,
) -> Result<Option<SiteInfo>, crate::DbError> {
    let sql = concatcp!(
        "select ",
        SITE_INFO_JSON_OBJECT,
        r#"
            as "site_info: Json<SiteInfo>" from site_info where host_name = $1
        "#
    );
    let result = sqlx::query(sql)
        .bind(host_name)
        .fetch_optional(exec)
        .await?;
    return_optional(&result)
}
