use sqlx::{Executor, Postgres};

pub async fn delete_site(
    exec: impl Executor<'_, Database = Postgres>,
    host_name: &str,
) -> Result<(), crate::DbError> {
    sqlx::query(
        r#"
        delete from site_info where host_name = $1
        "#,
    )
    .bind(host_name)
    .execute(exec)
    .await?;
    Ok(())
}
