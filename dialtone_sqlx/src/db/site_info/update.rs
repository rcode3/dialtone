use dialtone_common::rest::sites::site_data::SiteData;
use sqlx::{types::Json, Executor, Postgres};

pub async fn update_site(
    exec: impl Executor<'_, Database = Postgres>,
    host_name: &str,
    site_data: SiteData,
) -> Result<(), crate::DbError> {
    sqlx::query(
        r#"
            UPDATE site_info
            set
                site_data = $2
            where
                host_name = $1
            returning host_name
        "#,
    )
    .bind(host_name)
    .bind(Json(site_data))
    .fetch_one(exec)
    .await?;

    Ok(())
}
