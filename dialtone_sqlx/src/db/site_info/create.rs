use std::collections::HashMap;
use std::future::Future;

use dialtone_common::rest::sites::site_data::{
    ClientDefaults, DialtoneWebClient, PublicSiteInfo, SiteData, SiteNames,
};
use dialtone_common::rest::sites::theme::Theme;
use dialtone_common::rest::users::web_user::SystemRoleType;
use sqlx::types::Json;
use sqlx::{Acquire, Postgres};

#[allow(clippy::manual_async_fn)]
pub fn create_site<'a, 'c, A>(
    db: A,
    host_name: &'a str,
    short_name: String,
    long_name: Option<String>,
    default_theme: Option<Theme>,
    system_roles_sets: HashMap<String, Vec<SystemRoleType>>,
) -> impl Future<Output = Result<(), crate::DbError>> + Send + 'a
where
    A: Acquire<'c, Database = Postgres> + Send + 'a,
{
    async move {
        let mut exec = db.acquire().await?;
        let host_name = host_name.to_lowercase();
        let default_theme = match default_theme {
            None => Theme::GreenOnBlack,
            Some(t) => t,
        };
        let long_name = match long_name {
            None => short_name.clone(),
            Some(n) => n,
        };
        let site_data = SiteData {
            public: PublicSiteInfo {
                names: SiteNames {
                    short_name,
                    long_name,
                },
                client_defaults: ClientDefaults {
                    dialtone_webclient: DialtoneWebClient { default_theme },
                    ..ClientDefaults::default()
                },
                ..PublicSiteInfo::default()
            },
            system_roles_sets,
            ..SiteData::default()
        };
        sqlx::query(
            r#"
INSERT INTO site_info ( host_name, site_data )
VALUES ( $1, $2 )
returning host_name
        "#,
        )
        .bind(host_name)
        .bind(Json(site_data))
        .fetch_one(&mut *exec)
        .await?;

        Ok(())
    }
}
