pub use create::create_site;
pub use delete::delete_site;
pub use fetch::fetch_site;
pub use page_sites::page_sites;
pub mod create;
pub mod delete;
pub mod fetch;
pub mod page_sites;
pub mod update;

pub const SITE_INFO_JSON_OBJECT: &str = r#"
            json_build_object(
                'host_name',host_name,
                'site_data',site_data,
                'created_at',created_at,
                'modified_at',modified_at
            )
"#;
