use crate::db::user::UserDbStatusType;
use dialtone_common::rest::{event_log::LogEvent, users::web_user::UserStatus};
use sqlx::{types::Json, Executor, Postgres};

pub async fn change_user_status(
    exec: impl Executor<'_, Database = Postgres>,
    acct: &str,
    status: &UserStatus,
) -> Result<Option<()>, crate::DbError> {
    let log_event = LogEvent::new(format!("Status changed to {}.", &status));
    let user_status = UserDbStatusType::from(status);
    let result = sqlx::query(
        r#"
        update user_acct
        set 
            status = $1,
            event_log = event_log || $3
        where acct = $2
        returning acct
        "#,
    )
    .bind(user_status)
    .bind(acct)
    .bind(Json(log_event))
    .fetch_optional(exec)
    .await?;
    match result {
        None => Ok(None),
        Some(_) => Ok(Some(())),
    }
}
