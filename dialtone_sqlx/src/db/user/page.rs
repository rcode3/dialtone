use crate::db::user::WEB_USER_JSON_OBJECT;
use crate::db::{bind_params, date_range_condition, make_where_clause, return_optional, CondParam};
use const_format::concatcp;
use dialtone_common::rest::users::web_user::WebUser;
use sqlx::types::chrono::{DateTime, Utc};
use sqlx::{Executor, Postgres};

pub async fn page_users(
    exec: impl Executor<'_, Database = Postgres>,
    prev_date: Option<&DateTime<Utc>>,
    next_date: Option<&DateTime<Utc>>,
    limit: u16,
    host_name: Option<&str>,
) -> Result<Option<Vec<WebUser>>, crate::DbError> {
    let limit = i32::from(limit);
    let pre_select = concatcp!(
        "select json_agg(",
        WEB_USER_JSON_OBJECT,
        r#"
        ) as "web_user: Json<WebUser>" from (select * from user_acct
        "#
    );
    let post_select = concatcp!("order by created_at asc limit $1) as web_user");
    let mut conditions: Vec<String> = Vec::new();
    let mut params: Vec<CondParam> = Vec::new();
    if host_name.is_some() {
        params.push(CondParam::String(host_name.unwrap()));
        conditions.push(format!("acct like '%'||${}", params.len() + 1));
    }
    let (conditions, params) =
        date_range_condition(prev_date, next_date, "created_at", conditions, params);
    let where_clause = make_where_clause(&conditions);
    let sql = format!("{pre_select} {where_clause} {post_select}");
    let mut query = sqlx::query(&sql).bind(limit);
    query = bind_params(query, &params);
    // left here for future reference
    // println!("sql = {}", query.sql());
    let result = query.fetch_optional(exec).await?;
    return_optional(&result)
}
