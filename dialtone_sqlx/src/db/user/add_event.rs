use std::future::Future;

use dialtone_common::rest::event_log::LogEvent;
use sqlx::types::Json;
use sqlx::{Acquire, Postgres, Row};

#[allow(clippy::manual_async_fn)]
pub fn add_log_event<'a, 'c, A>(
    db: A,
    acct: &'a str,
    log_event: &'a LogEvent,
) -> impl Future<Output = Result<Option<()>, crate::DbError>> + Send + 'a
where
    A: Acquire<'c, Database = Postgres> + Send + 'a,
{
    async move {
        let mut exec = db.acquire().await?;

        let result = sqlx::query(
            r#"
        update user_acct
        set
            event_log = event_log || $2
        where acct = $1
        returning acct
    "#,
        )
        .bind(acct)
        .bind(Json(log_event))
        .fetch_optional(&mut *exec)
        .await?;
        match result {
            None => Ok(None),
            Some(row) => {
                row.try_get::<String, usize>(0)?;
                Ok(Some(()))
            }
        }
    }
}
