use std::future::Future;

use dialtone_common::rest::event_log::LogEvent;
use sqlx::types::Json;
use sqlx::{Acquire, Postgres};

use crate::logic::user::AuthData;

#[allow(clippy::manual_async_fn)]
pub fn insert_user<'a, 'c, A>(
    db: A,
    acct: &'a str,
    auth_data: AuthData,
) -> impl Future<Output = Result<(), crate::DbError>> + Send + 'a
where
    A: Acquire<'c, Database = Postgres> + Send + 'a,
{
    async move {
        let mut exec = db.acquire().await?;

        let event_log = vec![LogEvent::new(format!("{acct} created."))];
        sqlx::query(
            r#"
        insert into user_acct
        (acct, auth_data, event_log)
        values ($1, $2, $3)
        returning acct
        "#,
        )
        .bind(acct)
        .bind(Json(auth_data))
        .bind(Json(event_log))
        .fetch_one(&mut *exec)
        .await?;

        Ok(())
    }
}
