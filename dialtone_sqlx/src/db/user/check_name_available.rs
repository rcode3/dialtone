use sqlx::{Executor, Postgres};

pub async fn check_user_name_available(
    exec: impl Executor<'_, Database = Postgres>,
    acct: &str,
    actor_ids: &[&str],
) -> Result<bool, crate::DbError> {
    let rec = sqlx::query(
        r#"
        ( select 1 as found
        from user_acct
        where
            acct = $1
        limit 1 )
        union all
            select 1 as found
            from actor_owner
            where
                actor_id = any ($2::text[])
            limit 1
        "#,
    )
    .bind(acct)
    .bind(actor_ids)
    .fetch_optional(exec)
    .await?;

    match rec {
        Some(_) => Ok(false),
        _ => Ok(true),
    }
}
