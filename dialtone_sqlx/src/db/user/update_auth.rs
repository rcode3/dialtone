use dialtone_common::rest::event_log::LogEvent;
use sqlx::types::Json;
use sqlx::{Executor, Postgres, Row};

use crate::logic::user::AuthData;

pub async fn update_user_auth(
    exec: impl Executor<'_, Database = Postgres>,
    acct: &str,
    auth_data: AuthData,
) -> Result<Option<()>, crate::DbError> {
    let log_event = LogEvent::new(String::from("Authentication data changed."));
    let result = sqlx::query(
        r#"
        update user_acct
        set 
            auth_data = $1,
            event_log = event_log || $3
        where acct = $2
        returning acct
        "#,
    )
    .bind(Json(auth_data))
    .bind(acct)
    .bind(Json(log_event))
    .fetch_optional(exec)
    .await?;
    match result {
        None => Ok(None),
        Some(row) => {
            row.try_get::<String, usize>(0)?;
            Ok(Some(()))
        }
    }
}
