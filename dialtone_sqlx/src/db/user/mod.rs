use dialtone_common::rest::users::web_user::UserStatus;
use dialtone_sqlx_macros::SqlxEnumProxy;
use serde_variant::to_variant_name;
use std::str::FromStr;

pub use crate::control::user::create::create_user;

pub mod add_event;
pub mod change_status;
pub mod check_bcrypt;
pub mod check_name_available;
pub mod fetch_auth_and_mark_seen;
pub mod fetch_info;
pub mod fetch_sysinfo;
pub mod insert;
pub mod page;
pub mod update_auth;
pub mod update_prefs;
pub mod update_system_data;

#[derive(sqlx::Type, Debug, PartialEq, Eq, SqlxEnumProxy)]
#[sqlx(type_name = "user_status_type")]
#[proxy_for(UserStatus)]
pub enum UserDbStatusType {
    Active,

    Suspended,

    #[sqlx(rename = "Pending Approval")]
    PendingApproval,
}

pub const WEB_USER_JSON_OBJECT: &str = r#"
json_build_object(
    'user_authz', json_build_object(
        'acct', acct,
        'system_permissions', system_permissions
    ),
    'preferences', preferences,
    'last_seen_data', last_seen_data,
    'last_login_data', last_login_data,
    'actor_counts', actor_counts,
    'default_actor_data', default_actor_data,
    'status', status,
    'event_log', event_log,
    'created_at', created_at,
    'modified_at', modified_at 
)
"#;

pub const USER_SYSTEM_INFO_JSON_OBJECT: &str = r#"
json_build_object(
    'system_data', system_data,
    'created_at', created_at,
    'modified_at', modified_at 
)
"#;
