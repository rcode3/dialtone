use crate::db::user::USER_SYSTEM_INFO_JSON_OBJECT;
use const_format::concatcp;
use sqlx::{Executor, Postgres};

use crate::db::return_optional;
use dialtone_common::rest::users::web_user::UserSystemInfo;

pub async fn fetch_user_system_info(
    exec: impl Executor<'_, Database = Postgres>,
    acct: &str,
) -> Result<Option<UserSystemInfo>, crate::DbError> {
    let sql = concatcp!(
        "select ",
        USER_SYSTEM_INFO_JSON_OBJECT,
        r#" 
            as "web_user: Json<WebUser>" from user_acct where acct = $1
        "#
    );
    let result = sqlx::query(sql).bind(acct).fetch_optional(exec).await?;
    return_optional(&result)
}
