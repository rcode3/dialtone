use dialtone_common::rest::users::web_user::UserSystemData;
use sqlx::types::Json;
use sqlx::{Executor, Postgres, Row};

pub async fn update_user_system_data(
    exec: impl Executor<'_, Database = Postgres>,
    acct: &str,
    system_data: UserSystemData,
) -> Result<Option<()>, crate::DbError> {
    let result = sqlx::query(
        r#"
        update user_acct
        set system_data = $1
        where acct = $2
        returning acct
        "#,
    )
    .bind(Json(system_data))
    .bind(acct)
    .fetch_optional(exec)
    .await?;
    match result {
        None => Ok(None),
        Some(row) => {
            row.try_get::<String, usize>(0)?;
            Ok(Some(()))
        }
    }
}
