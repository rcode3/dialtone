use sqlx::types::chrono::{DateTime, Utc};
use sqlx::{Executor, Postgres};

use crate::db::return_optional;
use dialtone_common::rest::users::web_user::LastLoginData;

pub async fn check_bcrypt(
    exec: impl Executor<'_, Database = Postgres>,
    acct: &str,
    bcrypt_password: &str,
    login_from: &str,
    login_at: DateTime<Utc>,
) -> Result<Option<Vec<LastLoginData>>, crate::DbError> {
    let result = sqlx::query(
        r#"
        with pcheck as (
            select exists (
                select true from user_acct
                where
                    acct = $1
                and
                    status = 'Active'
                and
                    (auth_data->'password_auth'->'bcrypt_password'->>'crypted_password') = $2
            ) as success
        )
        update user_acct
        set last_login_data =
            jsonb_path_query_array(
                jsonb_build_object('from', $3::text, 'at', $4::text, 'success', pcheck.success)
                    || last_login_data, '$[0 to 10]')
        from pcheck
        where
            acct = $1
        returning last_login_data as "last_login_data: Json<Vec<LastLoginData>>"
        "#,
    )
    .bind(acct)
    .bind(bcrypt_password)
    .bind(login_from)
    .bind(login_at.to_string())
    .fetch_optional(exec)
    .await?;
    return_optional(&result)
}
