use dialtone_common::rest::event_log::LogEvent;
use dialtone_common::rest::users::web_user::UserPrefs;
use sqlx::types::Json;
use sqlx::{Executor, Postgres, Row};

pub async fn update_user_prefs(
    exec: impl Executor<'_, Database = Postgres>,
    acct: &str,
    preferences: UserPrefs,
) -> Result<Option<()>, crate::DbError> {
    let log_event = LogEvent::new("User preferences changed.".to_string());
    let result = sqlx::query(
        r#"
        update user_acct
        set 
            preferences = $1,
            event_log = event_log || $3
        where acct = $2
        returning acct
        "#,
    )
    .bind(Json(preferences))
    .bind(acct)
    .bind(Json(log_event))
    .fetch_optional(exec)
    .await?;
    match result {
        None => Ok(None),
        Some(row) => {
            row.try_get::<String, usize>(0)?;
            Ok(Some(()))
        }
    }
}
