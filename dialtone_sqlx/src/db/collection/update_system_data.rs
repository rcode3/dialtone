use dialtone_common::rest::collections::collection_model::CollectionSystemData;
use sqlx::types::Json;
use sqlx::{Executor, Postgres, Row};

pub async fn update_collection_system_data(
    exec: impl Executor<'_, Database = Postgres>,
    collection_id: &str,
    system_data: &CollectionSystemData,
) -> Result<Option<()>, crate::DbError> {
    let result = sqlx::query(
        r#"
        update collection
        set
            system_data = $1
        where
            id = $3
        returning id
        "#,
    )
    .bind(Json(system_data))
    .bind(collection_id)
    .fetch_optional(exec)
    .await?;
    match result {
        None => Ok(None),
        Some(row) => {
            row.try_get::<String, usize>(0)?;
            Ok(Some(()))
        }
    }
}
