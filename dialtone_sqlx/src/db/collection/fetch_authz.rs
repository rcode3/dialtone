use sqlx::{Executor, Postgres};

/// This function is usefult for authorization checks, but does not fetch any real information.
/// This function checks that the user is an owner of the actor to which the collection belongs.
/// Note that this function specifically does not check if the actor has 'Banned' visibility
/// or if the collection has a visibility of 'Invisible'.
pub async fn fetch_collection_authz(
    exec: impl Executor<'_, Database = Postgres>,
    collection_id: &str,
    user_acct: &str,
) -> Result<bool, crate::DbError> {
    let sql = r#"
        select
            1
        from collection, actor_owner
        where
            collection.id = $1
            and
            collection.actor_owner = actor_owner.actor_id
            and
            actor_owner.user_owner = $3
    "#;
    let result = sqlx::query(sql)
        .bind(collection_id)
        .bind(user_acct)
        .fetch_optional(exec)
        .await?;
    match result {
        Some(_) => Ok(true),
        None => Ok(false),
    }
}
