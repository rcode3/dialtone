use crate::db::{bind_params, date_range_condition, make_where_clause, CondParam};
use dialtone_common::ap::collection::{Collection, CollectionItem};
use dialtone_common::rest::collections::collection_model::CollectionVisibilityType;
use sqlx::types::chrono::{DateTime, Utc};
use sqlx::types::Json;
use sqlx::{Error, Executor, Postgres, Row};
// use sqlx::{Execute};

pub async fn page_by_id(
    exec: impl Executor<'_, Database = Postgres>,
    prev_date: Option<&DateTime<Utc>>,
    next_date: Option<&DateTime<Utc>>,
    limit: u16,
    collection_id: &str,
    visibility: Option<&CollectionVisibilityType>,
) -> Result<Option<Collection>, crate::DbError> {
    let limit = i32::from(limit);
    let visibility_condition = match visibility {
        None => "",
        Some(visibility) => match visibility {
            CollectionVisibilityType::PreVisible => "and visibility = 'PreVisible'",
            CollectionVisibilityType::Visible => "and visibility = 'Visible'",
            CollectionVisibilityType::Invisible => "and visibility = 'Invisible'",
        },
    };
    let (actor_sub_select, _params) = sub_select("actor", collection_id, prev_date, next_date);
    let (ap_object_sub_select, _params) =
        sub_select("ap_object", collection_id, prev_date, next_date);
    let (collection_sub_select, params) =
        sub_select("collection", collection_id, prev_date, next_date);
    let sql = format!(
        "
        select
            item_count as sort_col,
            null,
            null,
            activity_pub_json
        from (
            select 
                count(*) as item_count
            from
                collection_item
            where
                collection_item.id = $2
        ) as collection_item_count,
          collection
        where
            collection.id = $2
            {visibility_condition}    
        union
        select
            -1 as sort_col,
            min(item_created_at),
            max(item_created_at),
            jsonb_agg(item_json)
        from (
            ({actor_sub_select})
            union all
            ({ap_object_sub_select})
            union all
            ({collection_sub_select})
            limit $1
        ) as collection_union,
        collection
        where 
            collection.id = $2
            {visibility_condition}
        group by collection.id
        order by sort_col desc
    "
    );
    let mut query = sqlx::query(&sql).bind(limit);
    query = bind_params(query, &params);
    // println!("sql = {}", query.sql());
    let result = query.fetch_all(exec).await?;
    if result.is_empty() {
        Ok(None)
    } else {
        // first row has the collection json and total items
        // col 1 has the total_item count
        // col 2 and 3 should be null
        // col 4 has the collection as activity_pub_json
        let row1 = result.get(0).ok_or(Error::RowNotFound)?;
        let ap_json = row1.try_get::<Json<Collection>, usize>(3)?;
        let total_items = row1.try_get::<i64, usize>(0)?;
        let row2 = result.get(1);
        // if row2 is present, then the collection is not empty
        // col 1 should be -1
        // col 2 should be previous date
        // col 3 should be next date
        // col 4 should be items in the collection
        if let Some(row2) = row2 {
            let prev = row2.try_get::<DateTime<Utc>, usize>(1)?;
            let next = row2.try_get::<DateTime<Utc>, usize>(2)?;
            let items = row2.try_get::<Json<Vec<CollectionItem>>, usize>(3)?;
            Ok(Some(Collection {
                items: items.0,
                next: Some(next.to_rfc3339()),
                prev: Some(prev.to_rfc3339()),
                total_items: Some(total_items as u64),
                ..ap_json.0
            }))
        } else {
            Ok(Some(Collection {
                total_items: Some(total_items as u64),
                ..ap_json.0
            }))
        }
    }
}

fn sub_select<'a>(
    table_name: &'a str,
    id: &'a str,
    prev_date: Option<&'a DateTime<Utc>>,
    next_date: Option<&'a DateTime<Utc>>,
) -> (String, Vec<CondParam<'a>>) {
    let pre_select = format!(
        " select
            collection_item.created_at as item_created_at,
            {table_name}.activity_pub_json as item_json
        from
            collection_item, {table_name}
    "
    );
    let post_select = format!(
        "
        and
        {table_name}.id = collection_item.object_ap_id
        and
        {table_name}.visibility = 'Visible'
        order by item_created_at asc "
    );
    let mut conditions: Vec<String> = Vec::new();
    let params: Vec<CondParam> = vec![CondParam::String(id)];
    conditions.push(format!("collection_item.id = ${}", params.len() + 1));
    let (conditions, params) = date_range_condition(
        prev_date,
        next_date,
        "collection_item.created_at",
        conditions,
        params,
    );
    let where_clause = make_where_clause(&conditions);
    (format!("{pre_select} {where_clause} {post_select}"), params)
}

#[cfg(test)]
#[allow(non_snake_case)]
mod page_by_id_tests {
    use super::sub_select;

    #[test]
    fn GIVEN_table_name_and_no_dates_WHEN_create_sub_select_THEN_output_is_correct() {
        let (ss, params) = sub_select("foo", "bar", None, None);
        println!("{ss}");
        println!("{params:?}");
        assert_eq!(params.len(), 1);
    }
}
