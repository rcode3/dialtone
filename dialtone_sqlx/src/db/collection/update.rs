use dialtone_common::rest::collections::collection_model::UpdateOwnedCollectionData;
use dialtone_common::rest::event_log::LogEvent;
use sqlx::types::Json;
use sqlx::{Executor, Postgres, Row};

pub async fn update_owner_data(
    exec: impl Executor<'_, Database = Postgres>,
    owner_actor_id: &str,
    collection_id: &str,
    update: UpdateOwnedCollectionData,
) -> Result<Option<()>, crate::DbError> {
    let log_event = LogEvent::new("Collections's ActivityPub and owner data updated".to_string());
    let result = sqlx::query(
        r#"
        update collection
        set
            activity_pub_json = activity_pub_json || $1,
            owner_data = $2,
            event_log = event_log || $3
        where
            actor_owner = $4
            and
            id = $5
        "#,
    )
    .bind(Json(update.ap_data))
    .bind(Json(update.owner_data))
    .bind(Json(log_event))
    .bind(owner_actor_id)
    .bind(collection_id)
    .fetch_optional(exec)
    .await?;
    match result {
        None => Ok(None),
        Some(row) => {
            row.try_get::<String, usize>(0)?;
            Ok(Some(()))
        }
    }
}
