use sqlx::{Executor, Postgres, Row};

/// Returns a vector of all the collections a ActivityPub object is in
/// by owner.
pub async fn fetch_collection_names(
    exec: impl Executor<'_, Database = Postgres>,
    owner_actor_id: &str,
    object_ap_id: &str,
) -> Result<Vec<String>, crate::DbError> {
    let result = sqlx::query(
        r#"
        select
            id
        from collection_items
        where
            actor_owner = $1
            and
            object_ap_id = $2
        "#,
    )
    .bind(owner_actor_id)
    .bind(object_ap_id)
    .fetch_all(exec)
    .await?;
    let collection_ids = result
        .iter()
        .map(|row| row.get::<String, usize>(0))
        .collect();
    Ok(collection_ids)
}
