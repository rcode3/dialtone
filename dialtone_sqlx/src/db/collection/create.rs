use std::future::Future;

use dialtone_common::{ap::collection::Collection, rest::event_log::LogEvent};
use sqlx::{types::Json, Acquire, Postgres};

/// Inserts (creates) a collection.
#[allow(clippy::manual_async_fn)]
pub fn create_collection<'a, 'c, A>(
    db: A,
    owner_actor_id: &'a str,
    collection_id: &'a str,
    activity_pub_json: &'a Collection,
) -> impl Future<Output = Result<(), crate::DbError>> + Send + 'a
where
    A: Acquire<'c, Database = Postgres> + Send + 'a,
{
    async move {
        let mut exec = db.acquire().await?;
        let event_log = vec![LogEvent::new(format!("Created by {owner_actor_id}."))];
        sqlx::query(
            r#"
        insert into collection
            (actor_owner, id, activity_pub_json, event_log)
        values
            ($1,          $2, $3,                $4)
        returning id
        "#,
        )
        .bind(owner_actor_id)
        .bind(collection_id)
        .bind(Json(activity_pub_json))
        .bind(Json(event_log))
        .fetch_one(&mut *exec)
        .await?;
        Ok(())
    }
}
