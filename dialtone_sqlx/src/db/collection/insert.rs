use sqlx::{Executor, Postgres};

/// Inserts a reference to an item into the collection.
/// If the item already exists, this will NOOP as
/// "on conflict do nothing" is used.
pub async fn insert_into_collection(
    exec: impl Executor<'_, Database = Postgres>,
    collection_id: &str,
    object_ap_id: &str,
) -> Result<(), crate::DbError> {
    sqlx::query(
        r#"
        insert into collection_item
            (id, object_ap_id)
        values
            ($1, $2          )
        on conflict do nothing
        returning id, object_ap_id
        "#,
    )
    .bind(collection_id)
    .bind(object_ap_id)
    .fetch_optional(exec)
    .await?;
    Ok(())
}

/// This function insert an object into collections
/// based on the actor id being in a related collection.
pub async fn insert_into_related_collections(
    exec: impl Executor<'_, Database = Postgres>,
    actor_id: &str,
    related_collection_name: &str,
    target_collection_name: &str,
    object_ap_id: &str,
) -> Result<(), crate::DbError> {
    sqlx::query(
        r#"
        insert into collection_item
            (id, object_ap_id)
        select
            replace(id, $2, $3), $1
        from collection_item
        where
            id like '%'||$2
            and
            object_ap_id = $4
        on conflict do nothing
        returning id, object_ap_id
        "#,
    )
    .bind(object_ap_id)
    .bind(related_collection_name)
    .bind(target_collection_name)
    .bind(actor_id)
    .fetch_optional(exec)
    .await?;
    Ok(())
}
