use dialtone_common::ap::collection::Collection;
use dialtone_common::rest::collections::collection_model::CollectionPage;
use sqlx::types::chrono::{DateTime, Utc};
use sqlx::types::Json;
use sqlx::{Error, Executor, Postgres, Row};
// use sqlx::{Execute};

use crate::db::{bind_params, date_range_condition, make_where_clause, CondParam};

pub async fn page_by_actor(
    exec: impl Executor<'_, Database = Postgres>,
    prev_date: Option<&DateTime<Utc>>,
    next_date: Option<&DateTime<Utc>>,
    limit: u16,
    actor_id: &str,
) -> Result<Option<CollectionPage>, crate::DbError> {
    let limit = i32::from(limit);
    let pre_select = r#"
        select
            min(created_at),
            max(created_at),
            json_agg(activity_pub_json)
        from (
            select
                collection.activity_pub_json,
                collection.created_at
            from collection
    "#;
    let post_select = r#"
        and
        collection.visibility = 'Visible'
        order by collection.created_at asc
        limit $1) as collection
    "#;
    let mut conditions: Vec<String> = Vec::new();
    let params: Vec<CondParam> = vec![CondParam::String(actor_id)];
    conditions.push(format!("collection.actor_owner = ${}", params.len() + 1));
    let (conditions, params) = date_range_condition(
        prev_date,
        next_date,
        "collection.created_at",
        conditions,
        params,
    );
    let where_clause = make_where_clause(&conditions);
    let sql = format!("{pre_select} {where_clause} {post_select}");
    let mut query = sqlx::query(&sql).bind(limit);
    query = bind_params(query, &params);
    // left here for future reference
    // println!("sql = {}", query.sql());
    let result = query.fetch_optional(exec).await?;
    match result {
        None => Ok(None),
        Some(row) => {
            let page_values = row.try_get::<Json<Vec<Collection>>, usize>(2);
            match page_values {
                Ok(value) => {
                    let min_created_at = row.get::<DateTime<Utc>, usize>(0);
                    let max_created_at = row.get::<DateTime<Utc>, usize>(1);
                    Ok(Some(CollectionPage {
                        min_created_at,
                        max_created_at,
                        page_values: value.0,
                    }))
                }
                Err(err) => match err {
                    Error::ColumnDecode { .. } => Ok(None),
                    _ => Err(crate::DbError::Sqlx(err)),
                },
            }
        }
    }
}
