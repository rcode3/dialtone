use const_format::concatcp;
use dialtone_common::rest::collections::collection_model::CollectionSystemInfo;
use sqlx::{Executor, Postgres};

use crate::db::{collection::COLLECTION_SYSTEM_INFO_JSON_OBJECT, return_optional};

pub async fn fetch_collection_system_info(
    exec: impl Executor<'_, Database = Postgres>,
    collection_id: &str,
) -> Result<Option<CollectionSystemInfo>, crate::DbError> {
    let sql = concatcp!(
        "select",
        COLLECTION_SYSTEM_INFO_JSON_OBJECT,
        "from collection where id = $1"
    );
    let result = sqlx::query(sql)
        .bind(collection_id)
        .fetch_optional(exec)
        .await?;
    return_optional(&result)
}
