use dialtone_common::rest::{
    collections::collection_model::CollectionVisibilityType, event_log::LogEvent,
};
use sqlx::{types::Json, Executor, Postgres, Row};

use super::CollectionVisibilityDbType;

pub async fn update_collection_visibility(
    exec: impl Executor<'_, Database = Postgres>,
    collection_id: &str,
    visibility: &CollectionVisibilityType,
) -> Result<Option<()>, crate::DbError> {
    let visibility = CollectionVisibilityDbType::from(visibility);
    let log_event = LogEvent::new(format!("Visibility chagned to {}", visibility.to_string()));
    let result = sqlx::query(
        r#"
        update collection
        set
            visibility = $1,
            event_log = event_log || $2
        where
            id = $3
        returning id
        "#,
    )
    .bind(visibility)
    .bind(Json(log_event))
    .bind(collection_id)
    .fetch_optional(exec)
    .await?;
    match result {
        None => Ok(None),
        Some(row) => {
            row.try_get::<String, usize>(0)?;
            Ok(Some(()))
        }
    }
}
