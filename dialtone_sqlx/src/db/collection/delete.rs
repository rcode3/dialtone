use sqlx::{Executor, Postgres};

/// Removes an item for a collection.
pub async fn delete_from_collection(
    exec: impl Executor<'_, Database = Postgres>,
    collection_id: &str,
    object_ap_id: &str,
) -> Result<(), crate::DbError> {
    sqlx::query(
        r#"
        delete from collection_item
        where
            id = $1
            and
            object_ap_id = $2
        "#,
    )
    .bind(collection_id)
    .bind(object_ap_id)
    .execute(exec)
    .await?;
    Ok(())
}
