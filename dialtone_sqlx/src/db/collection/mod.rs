pub mod create;
pub mod delete;
pub mod fetch;
pub mod fetch_authz;
pub mod fetch_sysinfo;
pub mod insert;
pub mod page;
pub mod page_collection;
pub mod update;
pub mod update_system_data;
pub mod update_visibility;

use dialtone_common::rest::collections::collection_model::CollectionVisibilityType;
use dialtone_sqlx_macros::SqlxEnumProxy;
use serde_variant::to_variant_name;
use std::str::FromStr;

/// Defines collection visibility. Mirrors [CollectionVisibilityType]. See it for specifics.
#[derive(sqlx::Type, Debug, SqlxEnumProxy, PartialEq)]
#[sqlx(type_name = "collection_visibility_type")]
#[proxy_for(CollectionVisibilityType)]
pub enum CollectionVisibilityDbType {
    PreVisible,
    Visible,
    Invisible,
}

pub const PUBLIC_COLLECTION_JSON_OBJECT: &str = r#"
    json_build_object(
        'id', collection_id,
        'activity_pub_json', activity_pub_json,
        'items', items
    )
"#;

pub const OWNED_COLLECTION_JSON_OBJECT: &str = r#"
    json_build_object(
        'id', collection_id,
        'activity_pub_json', activity_pub_json,
        'items',items,
        'event_log',event_log
        'owner_data', owner_data,
        'created_at',created_at,
        'modified_at',modified_at
    )
"#;

pub const COLLECTION_SYSTEM_INFO_JSON_OBJECT: &str = r#"
json_build_object(
    'id', id,
    'actor_owner', owner_actor_id,
    'visibility', visibility,
    'system_data', system_data
)
"#;
