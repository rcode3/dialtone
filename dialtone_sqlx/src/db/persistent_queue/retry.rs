use sqlx::{Executor, Postgres};

pub async fn retry_job(
    exec: impl Executor<'_, Database = Postgres>,
    job_id: i32,
    attempts_remaining: i32,
) -> Result<bool, crate::DbError> {
    if attempts_remaining == 0 {
        return Ok(false);
    }
    let query = sqlx::query(
        r#"
            update job_queue
            set attempts_remaining = $1
            where id = $2
            returning id
        "#,
    )
    .bind(attempts_remaining - 1)
    .bind(job_id);
    let result = query.fetch_optional(exec).await?;
    match result {
        None => Ok(false),
        _ => Ok(true),
    }
}
