use crate::db::persistent_queue::JobDbType;
use sqlx::{Executor, Postgres};

pub async fn notify(
    exec: impl Executor<'_, Database = Postgres>,
    job_type: &JobDbType,
) -> Result<Option<()>, crate::DbError> {
    let result = sqlx::query("select pg_notify($1::text,null)")
        .bind(job_type)
        .fetch_optional(exec)
        .await?;
    match result {
        None => Ok(None),
        Some(_) => Ok(Some(())),
    }
}
