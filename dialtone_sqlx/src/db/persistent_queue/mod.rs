pub mod insert_job;
pub mod listener;
pub mod notifier;
pub mod pop_queue;
pub mod retry;

#[derive(sqlx::Type, Debug)]
#[sqlx(type_name = "job_type")]
pub enum JobDbType {
    #[sqlx(rename = "No Operation")]
    NoOperation,

    #[sqlx(rename = "Make Thumbnail")]
    MakeThumbnail,

    #[sqlx(rename = "Resize Actor Icon")]
    ResizeActorIcon,

    #[sqlx(rename = "Resize Actor Banner")]
    ResizeActorBanner,

    #[sqlx(rename = "Fetch Remote Icon")]
    FetchRemoteIcon,

    #[sqlx(rename = "Fetch Remote Banner")]
    FetchRemoteBanner,

    #[sqlx(rename = "Send Activity Pub")]
    SendActivityPub,
}

impl ToString for JobDbType {
    fn to_string(&self) -> String {
        match &self {
            JobDbType::NoOperation => "No Operation".to_string(),
            JobDbType::MakeThumbnail => "Make Thumbnail".to_string(),
            JobDbType::ResizeActorIcon => "Resize Actor Icon".to_string(),
            JobDbType::ResizeActorBanner => "Resize Actor Banner".to_string(),
            JobDbType::FetchRemoteIcon => "Fetch Remote Icon".to_string(),
            JobDbType::FetchRemoteBanner => "Fetch Remote Banner".to_string(),
            JobDbType::SendActivityPub => "Send Activity Pub".to_string(),
        }
    }
}
