use crate::db::persistent_queue::JobDbType;
use crate::DbError;
use sqlx::postgres::{PgListener, PgNotification};
use sqlx::PgPool;
use std::future::Future;

pub async fn listener<'a, F, Fut>(
    pool: &PgPool,
    job_type: &JobDbType,
    call_back: F,
    listen_indefinitely: bool,
) -> Result<(), DbError>
where
    F: Fn(PgPool, PgNotification) -> Fut,
    Fut: Future<Output = ()>,
{
    let mut pg_listener = PgListener::connect_with(pool).await?;
    pg_listener.listen(job_type.to_string().as_str()).await?;
    loop {
        let notification = pg_listener.recv().await?;
        call_back(pool.clone(), notification).await;
        if !listen_indefinitely {
            break;
        }
    }
    Ok(())
}
