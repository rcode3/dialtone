use const_format::concatcp;
use dialtone_common::ap::actor::ActorType;
use dialtone_common::rest::paging::Page;
use sqlx::types::chrono::{DateTime, Utc};
use sqlx::types::Json;
use sqlx::{Error, Executor, Postgres, Row};

use dialtone_common::pages::create_base_url;
use dialtone_common::rest::actors::actor_model::PublicActor;

use crate::db::actor::{ActorDbType, PUBLIC_ACTOR_JSON_OBJECT};
use crate::db::{bind_params, date_range_condition, make_where_clause, CondParam};

pub async fn page_public_actors_by_host(
    exec: impl Executor<'_, Database = Postgres>,
    prev_date: Option<&DateTime<Utc>>,
    next_date: Option<&DateTime<Utc>>,
    limit: u16,
    host_name: &str,
    actor_type: Option<&ActorType>,
) -> Result<Option<Page<PublicActor>>, crate::DbError> {
    let limit = i32::from(limit);
    let pre_select = concatcp!(
        r#"
        select
            min(modified_at),
            max(modified_at),
            json_agg("#,
        PUBLIC_ACTOR_JSON_OBJECT,
        r#")
        from (
            select
                actor.modified_at,
                actor.activity_pub_json,
                actor.webfinger_json
            from actor
    "#
    );
    let post_select = "and visibility != 'Banned' order by modified_at asc limit $1) as actor";
    let mut conditions: Vec<String> = Vec::new();
    let mut params: Vec<CondParam> = Vec::new();
    let id_start = create_base_url(host_name);
    params.push(CondParam::String(&id_start));
    conditions.push(format!("id like ${}||'%'", params.len() + 1));
    let a_type_string;
    if actor_type.is_some() {
        let a_type = ActorDbType::from(actor_type.unwrap());
        a_type_string = a_type.to_string();
        params.push(CondParam::String(&a_type_string));
        conditions.push(format!("type = ${}::actor_type", params.len() + 1));
    }
    let (conditions, params) =
        date_range_condition(prev_date, next_date, "modified_at", conditions, params);
    let where_clause = make_where_clause(&conditions);
    let sql = format!("{pre_select} {where_clause} {post_select}");
    let mut query = sqlx::query(&sql).bind(limit);
    query = bind_params(query, &params);
    // left here for future reference
    // println!("sql = {}", query.sql());
    let result = query.fetch_optional(exec).await?;
    match result {
        None => Ok(None),
        Some(row) => {
            let page_values = row.try_get::<Json<Vec<PublicActor>>, usize>(2);
            match page_values {
                Ok(value) => {
                    let min_modified_at = row.get::<DateTime<Utc>, usize>(0);
                    let max_modified_at = row.get::<DateTime<Utc>, usize>(1);
                    Ok(Some(Page {
                        prev: Some(min_modified_at),
                        next: Some(max_modified_at),
                        items: value.0,
                    }))
                }
                Err(err) => match err {
                    Error::ColumnDecode { .. } => Ok(None),
                    _ => Err(crate::DbError::Sqlx(err)),
                },
            }
        }
    }
}
