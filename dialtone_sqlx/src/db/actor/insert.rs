use std::future::Future;

use buildstructor::Builder;
use dialtone_common::ap::actor::Actor;
use sqlx::types::Json;
use sqlx::{Acquire, Postgres};

use dialtone_common::rest::actors::actor_model::{OwnerActorData, SystemActorData};
use dialtone_common::webfinger::Jrd;

use crate::db::actor::{ActorDbType, ActorVisibilityType};

#[derive(Debug, Builder)]
pub struct InsertActor {
    pub actor_type: ActorDbType,
    pub webfinger_jrd: Option<Jrd>,
    pub activity_pub: Actor,
    pub visibility: ActorVisibilityType,
    pub system_data: Option<SystemActorData>,
    pub owner_data: Option<OwnerActorData>,
}

/// Inserts an actor into the actor table.
/// Returns true if the insertion worked, otherwise false.
#[allow(clippy::manual_async_fn)]
pub fn insert_actor<'a, 'c, A>(
    db: A,
    InsertActor {
        actor_type,
        webfinger_jrd,
        activity_pub,
        visibility,
        system_data,
        owner_data,
    }: &'a InsertActor,
) -> impl Future<Output = Result<bool, crate::DbError>> + Send + 'a
where
    A: Acquire<'c, Database = Postgres> + Send + 'a,
{
    async move {
        let mut exec = db.acquire().await?;

        let result = sqlx::query(
            r#"
        insert into actor
            (id, type, acct, webfinger_json, activity_pub_json, visibility, system_data, owner_data)
        values
            ($1, $2,   $3,   $4,             $5,                $6,         $7,          $8        )
        on conflict do nothing
        returning id
        "#,
        )
        .bind(&activity_pub.id)
        .bind(actor_type)
        .bind(webfinger_jrd.as_ref().map(|jrd| &jrd.subject))
        .bind(Json(webfinger_jrd))
        .bind(Json(activity_pub))
        .bind(visibility)
        .bind(Json(system_data))
        .bind(Json(owner_data))
        .fetch_optional(&mut *exec)
        .await?;

        Ok(result.is_some())
    }
}
