use sqlx::types::Json;
use sqlx::{Executor, Postgres, Row};

use dialtone_common::rest::actors::actor_model::ActorSystemInfo;

use crate::db::actor::ActorVisibilityType;

pub async fn update_actor_system_info(
    exec: impl Executor<'_, Database = Postgres>,
    actor_system_info: ActorSystemInfo,
) -> Result<Option<()>, crate::DbError> {
    let result = sqlx::query(
        r#"
        update actor
        set visibility = $1, system_data = $2
        where id = $3
        returning id
        "#,
    )
    .bind(ActorVisibilityType::from(&actor_system_info.visibility))
    .bind(Json(actor_system_info.system_data))
    .bind(actor_system_info.actor_id)
    .fetch_optional(exec)
    .await?;
    match result {
        None => Ok(None),
        Some(row) => {
            row.try_get::<String, usize>(0)?;
            Ok(Some(()))
        }
    }
}
