use dialtone_common::{ap::actor::ActorType, pages::create_base_url};
use sqlx::{query, Executor, Postgres, Row};

use super::ActorDbType;

pub async fn count_actors_by_host(
    exec: impl Executor<'_, Database = Postgres>,
    host_name: &str,
    actor_type: Option<&ActorType>,
) -> Result<u64, crate::DbError> {
    let id_start = create_base_url(host_name);
    let actor_type_condition = if actor_type.is_some() {
        let a_type = ActorDbType::from(actor_type.unwrap());
        let a_type_string = a_type.to_string();
        format!("and type = '{a_type_string}'::actor_type")
    } else {
        String::default()
    };
    let sql = format!(
        r#"
        select
            count(*)
        from
            actor,
            actor_owner
        where
            actor.id = actor_owner.actor_id
            and
            actor.id like '{id_start}'||'%'
            and
            actor.visibility != 'Banned'
            {actor_type_condition}
    "#
    );
    let result = query(&sql).fetch_one(exec).await?;
    let count = result.try_get::<i64, usize>(0)?;
    Ok(count as u64)
}
