use crate::db::actor::PUBLIC_ACTOR_JSON_OBJECT;
use crate::db::return_optional;
use const_format::concatcp;
use dialtone_common::webfinger::Jrd;
use dialtone_common::{ap::actor::Actor, rest::actors::actor_model::PublicActor};
use sqlx::{Executor, Postgres};

pub async fn fetch_public_actor_by_id(
    exec: impl Executor<'_, Database = Postgres>,
    actor_id: &str,
) -> Result<Option<PublicActor>, crate::DbError> {
    let sql = concatcp!(
        "select",
        PUBLIC_ACTOR_JSON_OBJECT,
        r#"
        as "public_actor: Json<PublicActor>" from actor where id = $1 and visibility = 'Visible'
        "#
    );
    let result = sqlx::query(sql).bind(actor_id).fetch_optional(exec).await?;
    return_optional(&result)
}

pub async fn fetch_public_actor_by_wf_acct(
    exec: impl Executor<'_, Database = Postgres>,
    wf_acct: &str,
) -> Result<Option<PublicActor>, crate::DbError> {
    let sql = concatcp!(
        "select",
        PUBLIC_ACTOR_JSON_OBJECT,
        r#"
        as "public_actor: Json<PublicActor>" from actor where acct = $1 and visibility = 'Visible'
        "#
    );
    let result = sqlx::query(sql).bind(wf_acct).fetch_optional(exec).await?;
    return_optional(&result)
}

pub async fn fetch_ap_actor_by_id(
    exec: impl Executor<'_, Database = Postgres>,
    actor_id: &str,
) -> Result<Option<Actor>, crate::DbError> {
    let sql = r#"select activity_pub_json as "ap: Json<Actor>" from actor where id = $1 and visibility = 'Visible'"#;
    let result = sqlx::query(sql).bind(actor_id).fetch_optional(exec).await?;
    return_optional(&result)
}

pub async fn fetch_jrd_by_wf_acct(
    exec: impl Executor<'_, Database = Postgres>,
    wf_acct: &str,
) -> Result<Option<Jrd>, crate::DbError> {
    let sql = r#"select webfinger_json as "jrd: Json<Jrd>" from actor where acct = $1 and visibility = 'Visible'"#;
    let result = sqlx::query(sql).bind(wf_acct).fetch_optional(exec).await?;
    return_optional(&result)
}
