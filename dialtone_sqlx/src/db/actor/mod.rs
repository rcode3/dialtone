use std::str::FromStr;

use serde_variant::to_variant_name;

use dialtone_common::ap::actor::ActorType;
use dialtone_common::rest::actors::actor_model::ActorVisibility;
use dialtone_sqlx_macros::SqlxEnumProxy;

pub mod add_event;
pub mod count;
pub mod fetch_owned;
pub mod fetch_public;
pub mod fetch_sysinfo;
pub mod insert;
pub mod page_by_host;
pub mod page_owned;
pub mod page_owned_by_owner;
pub mod update_ap;
pub mod update_sysinfo;

#[derive(sqlx::Type, Debug, SqlxEnumProxy, PartialEq)]
#[sqlx(type_name = "actor_type")]
#[proxy_for(ActorType)]
pub enum ActorDbType {
    Application,
    Group,
    Organization,
    Person,
    Service,
}

#[derive(sqlx::Type, Debug, SqlxEnumProxy, PartialEq)]
#[sqlx(type_name = "actor_visibility_type")]
#[proxy_for(ActorVisibility)]
pub enum ActorVisibilityType {
    Visible,
    Invisible,
    Banned,
}

pub const PUBLIC_ACTOR_JSON_OBJECT: &str = r#"
json_build_object(
    'ap', actor.activity_pub_json,
    'jrd', actor.webfinger_json
)
"#;

pub const OWNER_ACTOR_JSON_OBJECT: &str = r#"
json_build_object(
    'ap', actor.activity_pub_json,
    'jrd', actor.webfinger_json,
    'collection_count', collection_count,
    'owner_count', owner_count,
    'owner_data', actor.owner_data,
    'event_log', event_log,
    'created_at', actor.created_at,
    'modified_at', actor.modified_at
)
"#;

pub const ACTOR_SYSTEM_INFO_JSON_OBJECT: &str = r#"
json_build_object(
    'actor_id', id,
    'visibility', visibility,
    'system_data', system_data
)
"#;
