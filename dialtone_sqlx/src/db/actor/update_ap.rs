use dialtone_common::rest::event_log::LogEvent;
use sqlx::types::Json;
use sqlx::{Executor, Postgres, Row};

use dialtone_common::rest::actors::actor_model::UpdateActorAp;

pub async fn update_actor_ap(
    exec: impl Executor<'_, Database = Postgres>,
    actor_ap: UpdateActorAp,
    actor_id: &str,
) -> Result<Option<()>, crate::DbError> {
    let log_event = LogEvent::new("Actor's ActivityPub updated".to_string());
    let result = sqlx::query(
        r#"
        update actor
        set 
            activity_pub_json = activity_pub_json || $1,
            event_log = event_log || $3
        where id = $2
        returning id
        "#,
    )
    .bind(Json(actor_ap))
    .bind(actor_id)
    .bind(Json(log_event))
    .fetch_optional(exec)
    .await?;
    match result {
        None => Ok(None),
        Some(row) => {
            row.try_get::<String, usize>(0)?;
            Ok(Some(()))
        }
    }
}

pub async fn update_actor_ap_by_owner(
    exec: impl Executor<'_, Database = Postgres>,
    actor_ap: UpdateActorAp,
    actor_id: &str,
    user_owner: &str,
) -> Result<Option<()>, crate::DbError> {
    let log_event = LogEvent::new("Actor's ActivityPub updated".to_string());
    let result = sqlx::query(
        r#"
        update actor
        set 
            activity_pub_json = activity_pub_json || $1,
            event_log = event_log || $4
        from actor_owner
        where actor.id = $2
        and actor.id = actor_owner.actor_id and actor_owner.user_owner = $3
        returning id
        "#,
    )
    .bind(Json(actor_ap))
    .bind(actor_id)
    .bind(user_owner)
    .bind(Json(log_event))
    .fetch_optional(exec)
    .await?;
    match result {
        None => Ok(None),
        Some(row) => {
            row.try_get::<String, usize>(0)?;
            Ok(Some(()))
        }
    }
}
