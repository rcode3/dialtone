use const_format::concatcp;
use dialtone_common::ap::actor::ActorType;
use sqlx::types::chrono::{DateTime, Utc};
use sqlx::{Executor, Postgres};

use dialtone_common::pages::create_base_url;
use dialtone_common::rest::actors::actor_model::{ActorVisibility, OwnedActor};

use crate::db::actor::{ActorDbType, ActorVisibilityType, OWNER_ACTOR_JSON_OBJECT};
use crate::db::{bind_params, date_range_condition, make_where_clause, return_optional, CondParam};

pub async fn page_owned_actors(
    exec: impl Executor<'_, Database = Postgres>,
    prev_date: Option<&DateTime<Utc>>,
    next_date: Option<&DateTime<Utc>>,
    limit: u16,
    host_name: Option<&str>,
    visibility: Option<&ActorVisibility>,
    actor_type: Option<&ActorType>,
) -> Result<Option<Vec<OwnedActor>>, crate::DbError> {
    let limit = i32::from(limit);
    let pre_select = concatcp!(
        "select json_agg(",
        OWNER_ACTOR_JSON_OBJECT,
        r#"
        ) as "owned_actor: Json<OwnedActor>" from (
          select 
            *
          from actor
        "#
    );
    let post_select = concatcp!("order by modified_at asc limit $1) as actor");
    let mut conditions: Vec<String> = Vec::new();
    let mut params: Vec<CondParam> = Vec::new();
    let id_start;
    if host_name.is_some() {
        id_start = create_base_url(host_name.unwrap());
        params.push(CondParam::String(&id_start));
        conditions.push(format!("id like ${}||'%'", params.len() + 1));
    }
    let v_type_string;
    match visibility {
        None => {
            conditions.push("visibility != 'Banned'".to_string());
        }
        Some(visibility) => {
            let v_type = ActorVisibilityType::from(visibility);
            v_type_string = v_type.to_string();
            params.push(CondParam::String(&v_type_string));
            conditions.push(format!(
                "visibility = ${}::actor_visibility_type",
                params.len() + 1
            ));
        }
    }
    let a_type_string;
    if actor_type.is_some() {
        let a_type = ActorDbType::from(actor_type.unwrap());
        a_type_string = a_type.to_string();
        params.push(CondParam::String(&a_type_string));
        conditions.push(format!("type = ${}::actor_type", params.len() + 1));
    }
    let (conditions, params) =
        date_range_condition(prev_date, next_date, "modified_at", conditions, params);
    let where_clause = make_where_clause(&conditions);
    let sql = format!("{pre_select} {where_clause} {post_select}");
    let mut query = sqlx::query(&sql).bind(limit);
    query = bind_params(query, &params);
    // left here for future reference
    // println!("sql = {}", query.sql());
    let result = query.fetch_optional(exec).await?;
    return_optional(&result)
}
