use const_format::concatcp;
use sqlx::{Executor, Postgres};

use dialtone_common::rest::actors::actor_model::OwnedActor;

use crate::db::actor::OWNER_ACTOR_JSON_OBJECT;
use crate::db::return_optional;

pub async fn fetch_owned_actor_by_id(
    exec: impl Executor<'_, Database = Postgres>,
    actor_id: &str,
) -> Result<Option<OwnedActor>, crate::DbError> {
    let sql = concatcp!(
        "select",
        OWNER_ACTOR_JSON_OBJECT,
        r#"
        as "owned_actor: Json<OwnedActor>" from actor where id = $1 and visibility != 'Banned'
        "#
    );
    let result = sqlx::query(sql).bind(actor_id).fetch_optional(exec).await?;
    return_optional(&result)
}

pub async fn fetch_owned_actor_by_acct(
    exec: impl Executor<'_, Database = Postgres>,
    acct: &str,
) -> Result<Option<OwnedActor>, crate::DbError> {
    let sql = concatcp!(
        "select",
        OWNER_ACTOR_JSON_OBJECT,
        r#"
        as "owned_actor: Json<OwnedActor>" from actor where acct = $1 and visibility != 'Banned'
        "#
    );
    let result = sqlx::query(sql).bind(acct).fetch_optional(exec).await?;
    return_optional(&result)
}

pub async fn fetch_default_owned_actor(
    exec: impl Executor<'_, Database = Postgres>,
    user_acct: &str,
) -> Result<Option<OwnedActor>, crate::DbError> {
    let sql = concatcp!(
        "select",
        OWNER_ACTOR_JSON_OBJECT,
        r#"
        as "owned_actor: Json<OwnedActor>" 
        from 
          actor, actor_owner 
        where 
          user_owner = $1 
        and
          id = actor_id
        and 
          visibility != 'Banned'
        and
          default_actor = true
        "#
    );
    let result = sqlx::query(sql)
        .bind(user_acct)
        .fetch_optional(exec)
        .await?;
    return_optional(&result)
}
