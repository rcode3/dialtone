use const_format::concatcp;
use sqlx::{Executor, Postgres};

use dialtone_common::rest::actors::actor_model::ActorSystemInfo;

use crate::db::actor::ACTOR_SYSTEM_INFO_JSON_OBJECT;
use crate::db::return_optional;

pub async fn fetch_actor_system_info(
    exec: impl Executor<'_, Database = Postgres>,
    actor_id: &str,
) -> Result<Option<ActorSystemInfo>, crate::DbError> {
    let sql = concatcp!(
        "select",
        ACTOR_SYSTEM_INFO_JSON_OBJECT,
        r#"
        as "actor_system_info: Json<ActorSystemInfo>" from actor where id = $1
        "#
    );
    let result = sqlx::query(sql).bind(actor_id).fetch_optional(exec).await?;
    return_optional(&result)
}
