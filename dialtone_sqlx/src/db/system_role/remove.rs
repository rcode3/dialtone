use crate::db::system_role::SystemRoleDbType;
use dialtone_common::rest::users::web_user::SystemRoleType;
use sqlx::{Executor, Postgres};

pub async fn remove_system_role(
    exec: impl Executor<'_, Database = Postgres>,
    system_role: &SystemRoleType,
    acct: &str,
    host_name: &str,
) -> Result<(), crate::DbError> {
    let system_role = SystemRoleDbType::from(system_role);
    sqlx::query(
        r#"
        delete from system_role
        where
            role_type = $1
            and
            member = $2
            and
            host_name = $3
        returning member
        "#,
    )
    .bind(&system_role)
    .bind(acct)
    .bind(host_name)
    .fetch_one(exec)
    .await?;
    Ok(())
}
