use crate::db::system_role::SystemRoleDbType;
use dialtone_common::rest::users::web_user::SystemRoleType;
use sqlx::{Executor, Postgres};

pub async fn add_system_role(
    exec: impl Executor<'_, Database = Postgres>,
    system_role: &SystemRoleType,
    acct: &str,
    host_name: &str,
) -> Result<(), crate::DbError> {
    let system_role = SystemRoleDbType::from(system_role);
    sqlx::query(
        r#"
        insert into system_role
        (role_type, member, host_name)
        values ($1, $2, $3)
        returning member
        "#,
    )
    .bind(&system_role)
    .bind(acct)
    .bind(host_name)
    .fetch_one(exec)
    .await?;

    Ok(())
}
