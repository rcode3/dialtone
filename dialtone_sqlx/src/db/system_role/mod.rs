use dialtone_common::rest::users::web_user::SystemRoleType;
use dialtone_sqlx_macros::SqlxEnumProxy;
use serde_variant::to_variant_name;
use std::str::FromStr;

pub mod add;
pub mod remove;

/// Permissions a user can have. Mirrors [SystemRole]. See it for details.
#[derive(sqlx::Type, Debug, SqlxEnumProxy)]
#[sqlx(type_name = "system_role_type")]
#[proxy_for(SystemRoleType)]
pub enum SystemRoleDbType {
    #[sqlx(rename = "User Administrator")]
    UserAdmin,
    #[sqlx(rename = "Actor Administrator")]
    ActorAdmin,
    #[sqlx(rename = "Content Administrator")]
    ContentAdmin,

    #[sqlx(rename = "Person Owner")]
    PersonOwner,
    #[sqlx(rename = "Organization Owner")]
    OrganizationOwner,
    #[sqlx(rename = "Group Owner")]
    GroupOwner,
    #[sqlx(rename = "Application Owner")]
    ApplicationOwner,
    #[sqlx(rename = "Service Owner")]
    ServiceOwner,
}
