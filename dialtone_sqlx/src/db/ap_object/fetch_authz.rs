use sqlx::{Executor, Postgres};

/// This function is useful for authorization checks, but does not fetch any real information.
/// This function checks that the user is an owner of the actor to which the ap_object belongs.
/// Note that this function specifically does not check if the actor has 'Banned' visibility
/// or if the ap_object has a visibility of 'Invisible'.
pub async fn fetch_ap_object_authz(
    exec: impl Executor<'_, Database = Postgres>,
    ap_object_id: &str,
    user_acct: &str,
) -> Result<bool, crate::DbError> {
    let sql = r#"
        select
            1
        from ap_object, actor_owner
        where
            ap_object.id = $1
            and
            ap_object.actor_owner = actor_owner.actor_id
            and
            actor_owner.user_owner = $2
    "#;
    let result = sqlx::query(sql)
        .bind(ap_object_id)
        .bind(user_acct)
        .fetch_optional(exec)
        .await?;
    match result {
        Some(_) => Ok(true),
        None => Ok(false),
    }
}
