use dialtone_common::ap::ap_object::ApObject;
use dialtone_common::rest::ap_objects::ap_object_model::ApObjectOwnerData;
use dialtone_common::rest::event_log::LogEvent;
use sqlx::types::Json;
use sqlx::{Executor, Postgres};

pub async fn update_ap_object(
    exec: impl Executor<'_, Database = Postgres>,
    ap_object: &ApObject,
    owner_data: Option<&ApObjectOwnerData>,
) -> Result<(), crate::DbError> {
    let log_event = LogEvent::new("ActivityPub changed.".to_string());
    sqlx::query(
        r#"
        update ap_object
        set
            activity_pub_json = $1,
            owner_data = $2,
            event_log = event_log || $4
        where
            id = $3
        returning id
        "#,
    )
    .bind(Json(ap_object))
    .bind(Json(owner_data))
    .bind(ap_object.id.as_ref())
    .bind(Json(log_event))
    .fetch_one(exec)
    .await?;

    Ok(())
}
