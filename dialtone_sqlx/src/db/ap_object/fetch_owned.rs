use crate::db::ap_object::OWNED_AP_OBJECT_JSON_OBJECT;
use crate::db::return_optional;
use const_format::concatcp;
use dialtone_common::rest::ap_objects::ap_object_model::OwnedApObject;
use sqlx::{Executor, Postgres};

pub async fn fetch_owned_ap_object(
    exec: impl Executor<'_, Database = Postgres>,
    ap_object_id: &str,
) -> Result<Option<OwnedApObject>, crate::DbError> {
    let sql = concatcp!(
        "select",
        OWNED_AP_OBJECT_JSON_OBJECT,
        "from ap_object where id = $1 and visibility = 'Visible'"
    );
    let result = sqlx::query(sql)
        .bind(ap_object_id)
        .fetch_optional(exec)
        .await?;
    return_optional(&result)
}
