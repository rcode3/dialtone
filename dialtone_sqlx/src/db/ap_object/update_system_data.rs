use sqlx::types::Json;
use sqlx::{Executor, Postgres, Row};

use dialtone_common::rest::ap_objects::ap_object_model::ApObjectSystemData;

pub async fn update_ap_object_system_data(
    exec: impl Executor<'_, Database = Postgres>,
    ap_object_id: &str,
    system_data: &ApObjectSystemData,
) -> Result<Option<()>, crate::DbError> {
    let result = sqlx::query(
        r#"
        update ap_object
        set
            system_data = $1
        where
            id = $2
        returning id
        "#,
    )
    .bind(Json(system_data))
    .bind(ap_object_id)
    .fetch_optional(exec)
    .await?;
    match result {
        None => Ok(None),
        Some(row) => {
            row.try_get::<String, usize>(0)?;
            Ok(Some(()))
        }
    }
}
