use chrono::{DateTime, Utc};
use dialtone_common::rest::paging::Page;
use sqlx::types::Json;
use sqlx::{Error, Executor, Postgres, Row};

use dialtone_common::ap::ap_object::ApObject;
use dialtone_common::rest::ap_objects::ap_object_model::ApObjectVisibilityType;

use crate::db::ap_object::ApObjectVisibilityDbType;
use crate::db::{bind_params, date_range_condition, make_where_clause, CondParam};

pub async fn page_ap_objects_by_actor(
    exec: impl Executor<'_, Database = Postgres>,
    prev_date: Option<&DateTime<Utc>>,
    next_date: Option<&DateTime<Utc>>,
    limit: u16,
    actor_id: &str,
    visibility: Option<&ApObjectVisibilityType>,
) -> Result<Option<Page<ApObject>>, crate::DbError> {
    let limit = i32::from(limit);
    let pre_select = r#"
        select
            min(modified_at),
            max(modified_at),
            json_agg(activity_pub_json)
        from (
            select
                ap_object.activity_pub_json,
                ap_object.modified_at
            from ap_object
    "#;
    let post_select = "order by modified_at asc limit $1) as ap_object";
    let mut conditions: Vec<String> = Vec::new();
    let mut params: Vec<CondParam> = vec![CondParam::String(actor_id)];
    conditions.push(format!("ap_object.actor_owner = ${}", params.len() + 1));
    let visibility_string;
    if visibility.is_some() {
        let visibility_type = ApObjectVisibilityDbType::from(visibility.unwrap());
        visibility_string = visibility_type.to_string();
        params.push(CondParam::String(&visibility_string));
        conditions.push(format!(
            "visibility = ${}::ap_object_visibility_type",
            params.len() + 1
        ))
    }
    let (conditions, params) =
        date_range_condition(prev_date, next_date, "modified_at", conditions, params);
    let where_clause = make_where_clause(&conditions);
    let sql = format!("{pre_select} {where_clause} {post_select}");
    let mut query = sqlx::query(&sql).bind(limit);
    query = bind_params(query, &params);
    // left here for future reference
    // println!("sql = {}", query.sql());
    let result = query.fetch_optional(exec).await?;
    match result {
        None => Ok(None),
        Some(row) => {
            let page_values = row.try_get::<Json<Vec<ApObject>>, usize>(2);
            match page_values {
                Ok(value) => {
                    let min_modified_at = row.get::<DateTime<Utc>, usize>(0);
                    let max_modified_at = row.get::<DateTime<Utc>, usize>(1);
                    Ok(Some(Page::<ApObject> {
                        prev: Some(min_modified_at),
                        next: Some(max_modified_at),
                        items: value.0,
                    }))
                }
                Err(err) => match err {
                    Error::ColumnDecode { .. } => Ok(None),
                    _ => Err(crate::DbError::Sqlx(err)),
                },
            }
        }
    }
}
