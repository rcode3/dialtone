use crate::db::ap_object::{ApObjectDbType, ApObjectVisibilityDbType};
use dialtone_common::ap::ap_object::ApObject;
use dialtone_common::rest::ap_objects::ap_object_model::{ApObjectOwnerData, ApObjectSystemData};
use dialtone_common::rest::event_log::LogEvent;
use sqlx::types::Json;
use sqlx::{Executor, Postgres};

pub async fn insert_ap_object(
    exec: impl Executor<'_, Database = Postgres>,
    received_for_host: &str,
    ap_object: &ApObject,
    owner_data: Option<&ApObjectOwnerData>,
    ap_object_type: ApObjectDbType,
    system_data: Option<&ApObjectSystemData>,
    visibility: ApObjectVisibilityDbType,
) -> Result<(), crate::DbError> {
    let event_log = vec![LogEvent::new(format!(
        "Created as {}.",
        visibility.to_string()
    ))];
    sqlx::query(
        r#"
        insert into ap_object
            (id, type, actor_owner, received_for_host, activity_pub_json, owner_data, system_data, visibility, event_log)
        values
            ($1, $2,   $3,          $4,                $5,                $6,         $7,          $8,         $9)
        returning id
        "#,
    )
    .bind(ap_object.id.as_ref())
    .bind(ap_object_type)
    .bind(ap_object.attributed_to.as_ref())
    .bind(received_for_host)
    .bind(Json(ap_object))
    .bind(Json(owner_data))
    .bind(Json(system_data))
    .bind(visibility)
    .bind(Json(event_log))
    .fetch_one(exec)
    .await?;

    Ok(())
}
