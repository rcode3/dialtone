use dialtone_common::rest::event_log::LogEvent;
use sqlx::types::Json;
use sqlx::{Executor, Postgres, Row};

pub async fn add_log_event(
    exec: impl Executor<'_, Database = Postgres>,
    ap_object_id: &str,
    log_event: &LogEvent,
) -> Result<Option<()>, crate::DbError> {
    let result = sqlx::query(
        r#"
        update ap_object
        set
            event_log = event_log || $2
        where id = $1
        returning id
    "#,
    )
    .bind(ap_object_id)
    .bind(Json(log_event))
    .fetch_optional(exec)
    .await?;
    match result {
        None => Ok(None),
        Some(row) => {
            row.try_get::<String, usize>(0)?;
            Ok(Some(()))
        }
    }
}
