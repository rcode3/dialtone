use crate::db::return_optional;
use dialtone_common::ap::ap_object::{ApObject, ApObjectType};
use sqlx::{Executor, Postgres};

use super::ApObjectDbType;

pub async fn fetch_random_ap_object_by_actor(
    exec: impl Executor<'_, Database = Postgres>,
    ap_object_type: &ApObjectType,
    actor_id: &str,
) -> Result<Option<ApObject>, crate::DbError> {
    let ap_object_type = ApObjectDbType::from(ap_object_type);
    let sql = r#"
        select activity_pub_json
        from ap_object 
        where 
            actor_owner = $1
            and
            type = $2
            and
            visibility = 'Visible'
        order by random()
        limit 1
    "#;
    let result = sqlx::query(sql)
        .bind(actor_id)
        .bind(ap_object_type)
        .fetch_optional(exec)
        .await?;
    return_optional(&result)
}

pub async fn fetch_random_ap_object_by_collection(
    exec: impl Executor<'_, Database = Postgres>,
    ap_object_type: &ApObjectType,
    collection_id: &str,
) -> Result<Option<ApObject>, crate::DbError> {
    let ap_object_type = ApObjectDbType::from(ap_object_type);
    let sql = r#"
        select 
            ap_object.activity_pub_json
        from ap_object, collection_item 
        where 
            collection_item.id = $1
            and
            ap_object.type = $2
            and
            collection_item.object_ap_id = ap_object.id
        order by random()
        limit 1
    "#;
    let result = sqlx::query(sql)
        .bind(collection_id)
        .bind(ap_object_type)
        .fetch_optional(exec)
        .await?;
    return_optional(&result)
}
