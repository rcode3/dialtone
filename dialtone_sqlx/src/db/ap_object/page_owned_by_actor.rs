use chrono::{DateTime, Utc};
use const_format::concatcp;
use sqlx::{Executor, Postgres};

use dialtone_common::rest::ap_objects::ap_object_model::{ApObjectVisibilityType, OwnedApObject};

use crate::db::ap_object::{ApObjectVisibilityDbType, OWNED_AP_OBJECT_JSON_OBJECT};
use crate::db::{bind_params, date_range_condition, make_where_clause, return_optional, CondParam};

pub async fn page_owned_ap_objects_by_actor(
    exec: impl Executor<'_, Database = Postgres>,
    prev_date: Option<&DateTime<Utc>>,
    next_date: Option<&DateTime<Utc>>,
    limit: u16,
    actor_id: &str,
    visibility: Option<&ApObjectVisibilityType>,
) -> Result<Option<Vec<OwnedApObject>>, crate::DbError> {
    let limit = i32::from(limit);
    let pre_select = concatcp!(
        "select json_agg(",
        OWNED_AP_OBJECT_JSON_OBJECT,
        r#"
        ) as "owned_ap_object: Json<OwnedApObject>" from (
            select
                ap_object.activity_pub_json,
                ap_object.owner_data,
                ap_object.event_log,
                ap_object.created_at,
                ap_object.modified_at
            from ap_object
        "#
    );
    let post_select = concatcp!("order by modified_at asc limit $1) as ap_object");
    let mut conditions: Vec<String> = Vec::new();
    let mut params: Vec<CondParam> = vec![CondParam::String(actor_id)];
    conditions.push(format!("ap_object.actor_owner = ${}", params.len() + 1));
    let visibility_string;
    if visibility.is_some() {
        let visibility_type = ApObjectVisibilityDbType::from(visibility.unwrap());
        visibility_string = visibility_type.to_string();
        params.push(CondParam::String(&visibility_string));
        conditions.push(format!(
            "visibility = ${}::ap_object_visibility_type",
            params.len() + 1
        ))
    }
    let (conditions, params) =
        date_range_condition(prev_date, next_date, "modified_at", conditions, params);
    let where_clause = make_where_clause(&conditions);
    let sql = format!("{pre_select} {where_clause} {post_select}");
    let mut query = sqlx::query(&sql).bind(limit);
    query = bind_params(query, &params);
    // left here for future reference
    // println!("sql = {}", query.sql());
    let result = query.fetch_optional(exec).await?;
    return_optional(&result)
}
