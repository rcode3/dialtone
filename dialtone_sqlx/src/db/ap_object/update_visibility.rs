use dialtone_common::rest::{
    ap_objects::ap_object_model::ApObjectVisibilityType, event_log::LogEvent,
};
use sqlx::{types::Json, Executor, Postgres, Row};

use crate::db::ap_object::ApObjectVisibilityDbType;

pub async fn update_ap_object_visibility(
    exec: impl Executor<'_, Database = Postgres>,
    ap_object_id: &str,
    visibility: &ApObjectVisibilityType,
) -> Result<Option<()>, crate::DbError> {
    let visibility = ApObjectVisibilityDbType::from(visibility);
    let log_event = LogEvent::new(format!("Visibility chagned to {}", visibility.to_string()));
    let result = sqlx::query(
        r#"
        update ap_object
        set
            visibility = $1,
            event_log = event_log || $3
        where
            id = $2
        returning id
        "#,
    )
    .bind(visibility)
    .bind(ap_object_id)
    .bind(Json(log_event))
    .fetch_optional(exec)
    .await?;
    match result {
        None => Ok(None),
        Some(row) => {
            row.try_get::<String, usize>(0)?;
            Ok(Some(()))
        }
    }
}
