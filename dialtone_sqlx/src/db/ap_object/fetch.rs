use crate::db::return_optional;
use const_format::concatcp;
use dialtone_common::ap::ap_object::ApObject;
use sqlx::{Executor, Postgres};

pub async fn fetch_ap_object(
    exec: impl Executor<'_, Database = Postgres>,
    ap_object_id: &str,
) -> Result<Option<ApObject>, crate::DbError> {
    let sql = concatcp!(
        "select activity_pub_json
        from ap_object where id = $1 and visibility = 'Visible'"
    );
    let result = sqlx::query(sql)
        .bind(ap_object_id)
        .fetch_optional(exec)
        .await?;
    return_optional(&result)
}
