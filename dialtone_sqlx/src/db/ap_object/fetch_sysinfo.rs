use crate::db::ap_object::AP_OBJECT_SYSTEM_INFO_JSON_OBJECT;
use crate::db::return_optional;
use const_format::concatcp;
use dialtone_common::rest::ap_objects::ap_object_model::ApObjectSystemInfo;
use sqlx::{Executor, Postgres};

pub async fn fetch_ap_object_system_info(
    exec: impl Executor<'_, Database = Postgres>,
    ap_object_id: &str,
) -> Result<Option<ApObjectSystemInfo>, crate::DbError> {
    let sql = concatcp!(
        "select",
        AP_OBJECT_SYSTEM_INFO_JSON_OBJECT,
        "from ap_object where id = $1"
    );
    let result = sqlx::query(sql)
        .bind(ap_object_id)
        .fetch_optional(exec)
        .await?;
    return_optional(&result)
}
