pub mod add_event;
pub mod fetch;
pub mod fetch_authz;
pub mod fetch_owned;
pub mod fetch_random;
pub mod fetch_sysinfo;
pub mod insert;
pub mod page_by_actor;
pub mod page_by_host;
pub mod page_owned_by_actor;
pub mod update;
pub mod update_system_data;
pub mod update_visibility;

use dialtone_common::ap::ap_object::ApObjectType;
use dialtone_common::rest::ap_objects::ap_object_model::ApObjectVisibilityType;
use dialtone_sqlx_macros::SqlxEnumProxy;
use serde_variant::to_variant_name;
use std::str::FromStr;

#[derive(sqlx::Type, Debug, SqlxEnumProxy, PartialEq)]
#[sqlx(type_name = "ap_object_type")]
#[proxy_for(ApObjectType)]
pub enum ApObjectDbType {
    Note,
    Article,
    Document,
    Image,
    Video,
    Audio,
    Page,
}

/// Defines AP object visibility. Mirrors [ApObjectVisibilityType]. See it for specifics.
#[derive(sqlx::Type, Debug, SqlxEnumProxy, PartialEq)]
#[sqlx(type_name = "ap_object_visibility_type")]
#[proxy_for(ApObjectVisibilityType)]
pub enum ApObjectVisibilityDbType {
    PreVisible,
    Visible,
    Invisible,
}

pub const OWNED_AP_OBJECT_JSON_OBJECT: &str = r#"
json_build_object(
    'ap', activity_pub_json,
    'owner_data', owner_data,
    'event_log', event_log,
    'created_at', created_at,
    'modified_at', modified_at
)
"#;

pub const AP_OBJECT_SYSTEM_INFO_JSON_OBJECT: &str = r#"
json_build_object(
    'ap_object_id', id,
    'ap_object_type', type,
    'actor_owner', actor_owner,
    'visibility', visibility,
    'system_data', system_data 
)
"#;
