use sqlx::{Executor, Postgres, Row};

pub async fn set_default_actor(
    exec: impl Executor<'_, Database = Postgres>,
    actor_id: &str,
    user_owner: &str,
) -> Result<Option<()>, crate::DbError> {
    let result = sqlx::query(
        r#"
        update actor_owner
        set default_actor = true
        where actor_id = $1 and user_owner = $2
        returning actor_id
        "#,
    )
    .bind(actor_id)
    .bind(user_owner)
    .fetch_optional(exec)
    .await?;
    match result {
        None => Ok(None),
        Some(row) => {
            row.try_get::<String, usize>(0)?;
            Ok(Some(()))
        }
    }
}

pub async fn clear_default_actor(
    exec: impl Executor<'_, Database = Postgres>,
    user_owner: &str,
) -> Result<Option<()>, crate::DbError> {
    let result = sqlx::query(
        r#"
        update actor_owner
        set default_actor = false
        where user_owner = $1
        returning actor_id
        "#,
    )
    .bind(user_owner)
    .fetch_optional(exec)
    .await?;
    match result {
        None => Ok(None),
        Some(row) => {
            row.try_get::<String, usize>(0)?;
            Ok(Some(()))
        }
    }
}
