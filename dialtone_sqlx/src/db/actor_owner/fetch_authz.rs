use sqlx::{Executor, Postgres};

/// This function is useful for authorization checks, but does not fetch any real information.
/// This function checks that the user is an owner of an actor.
/// Note that this function specifically does not check if the actor has 'Banned' visibility thus
/// allowing users to take actions against an actor when the actor is unseen. This allows actor admins
/// the ability to tell users to fix something.
pub async fn fetch_actor_owner_authz(
    exec: impl Executor<'_, Database = Postgres>,
    actor_id: &str,
    user_acct: &str,
) -> Result<bool, crate::DbError> {
    let sql = r#"
        select
            1
        from actor_owner, actor
        where
            actor_id = $1
            and
            user_owner = $2
            and
            actor_id = actor.id
    "#;
    let result = sqlx::query(sql)
        .bind(actor_id)
        .bind(user_acct)
        .fetch_optional(exec)
        .await?;
    match result {
        None => Ok(false),
        Some(_) => Ok(true),
    }
}
