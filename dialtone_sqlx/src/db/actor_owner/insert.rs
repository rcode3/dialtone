use std::future::Future;

use sqlx::{Acquire, Postgres};

#[allow(clippy::manual_async_fn)]
pub fn insert_actor_owner<'a, 'c, A>(
    db: A,
    actor_id: &'a str,
    user_acct: &'a str,
    default_actor: bool,
) -> impl Future<Output = Result<(), crate::DbError>> + Send + 'a
where
    A: Acquire<'c, Database = Postgres> + Send + 'a,
{
    async move {
        let mut exec = db.acquire().await?;

        sqlx::query(
            r#"
        insert into actor_owner
          (actor_id, user_owner, default_actor)
        values
          ($1,       $2,         $3)
        returning actor_id
        "#,
        )
        .bind(actor_id)
        .bind(user_acct)
        .bind(default_actor)
        .fetch_one(&mut *exec)
        .await?;

        Ok(())
    }
}
