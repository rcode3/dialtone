pub mod auth;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct AuthData {
    pub password_auth: PasswordAuth,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum PasswordAuth {
    #[serde(rename = "bcrypt_password")]
    BcryptPassword(BcryptPassword),
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct BcryptPassword {
    pub crypted_password: String,
}
