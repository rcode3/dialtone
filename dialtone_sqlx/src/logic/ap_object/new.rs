use crate::logic::ap_object::html::process_html;
use crate::logic::ap_object::local_id::make_local_id;
use crate::BadInput;
use dialtone_common::ap::ap_object::{ApObject, ApObjectMediaType, ApObjectType};
use dialtone_common::ap::id::create_ap_object_id;
use dialtone_common::rest::ap_objects::ap_object_model::CreateOwnedApObject;

pub fn new_html_ap_object(
    host_name: &str,
    pun: &str,
    actor_id: &str,
    new_ap_object: &CreateOwnedApObject,
) -> Result<ApObject, BadInput> {
    let content = &new_ap_object
        .content
        .as_ref()
        .ok_or(BadInput::InvalidData("ap_objects content required"))?;
    let (html, title) = process_html(content.as_bytes())?;
    let local_id = make_local_id(Some(pun), title.as_deref());
    let ap_object_id = create_ap_object_id(host_name, &new_ap_object.ap_type, &local_id);
    let html = String::from_utf8(html)?;
    // names and summary are cloned here because they must be cloned for creating the ApObject
    let name = new_ap_object.name.clone().map_or(title.clone(), Some);
    let summary = new_ap_object.summary.clone().map_or(title, Some);
    Ok(ApObject::builder()
        .and_name(name)
        .and_summary(summary)
        .id(ap_object_id)
        .media_type(ApObjectMediaType::TextHtml)
        .ap_type(new_ap_object.ap_type.to_owned())
        .content(html)
        .actor(actor_id)
        .attributed_to(actor_id)
        .and_to(new_ap_object.to.clone())
        .and_cc(new_ap_object.cc.clone())
        .and_bto(new_ap_object.bto.clone())
        .and_bcc(new_ap_object.bcc.clone())
        .build())
}

pub fn new_media_ap_object(
    host_name: &str,
    actor_id: &str,
    url: &str,
    file_name: &str,
    pun: &str,
    new_ap_object: &CreateOwnedApObject,
) -> ApObject {
    let local_id = make_local_id(Some(pun), Some(file_name));
    new_media_ap_object_with_local_id(host_name, actor_id, url, new_ap_object, &local_id)
}

pub fn new_media_ap_object_with_local_id(
    host_name: &str,
    actor_id: &str,
    url: &str,
    new_ap_object: &CreateOwnedApObject,
    local_id: &str,
) -> ApObject {
    let ap_object_id = create_ap_object_id(host_name, &new_ap_object.ap_type, local_id);
    ApObject::builder()
        .and_name(new_ap_object.name.clone())
        .and_summary(new_ap_object.summary.clone())
        .and_content(new_ap_object.content.clone())
        .id(ap_object_id)
        .url(url)
        .and_media_type(new_ap_object.media_type.clone())
        .ap_type(new_ap_object.ap_type.clone())
        .actor(actor_id)
        .attributed_to(actor_id)
        .and_to(new_ap_object.to.clone())
        .and_cc(new_ap_object.cc.clone())
        .and_bto(new_ap_object.bto.clone())
        .and_bcc(new_ap_object.bcc.clone())
        .build()
}

pub fn new_fetched_media_ap_object(
    host_name: &str,
    actor_id: &str,
    fetch_url: &str,
    local_url: &str,
    ap_type: ApObjectType,
    media_type: Option<ApObjectMediaType>,
) -> ApObject {
    let fetched_file_name = fetch_url.rsplit_once('/').map(|split| split.0);
    let local_id = make_local_id(None, fetched_file_name);
    let ap_object_id = create_ap_object_id(host_name, &ap_type, &local_id);
    ApObject::builder()
        .id(ap_object_id)
        .url(local_url)
        .and_media_type(media_type)
        .ap_type(ap_type)
        .actor(actor_id)
        .attributed_to(actor_id)
        .build()
}

#[cfg(test)]
#[allow(non_snake_case)]
mod new_ap_object_tests {
    use super::*;
    use dialtone_common::ap::ap_object::ApObjectType;
    use dialtone_common::rest::ap_objects::ap_object_model::CreateOwnedApObject;
    use dialtone_test_util::test_constants::{
        TEST_HOSTNAME, TEST_NOROLEUSER_ACCT, TEST_NOROLEUSER_PUN,
    };

    #[test]
    fn new_html_ap_object_test() {
        let create = CreateOwnedApObject {
            name: None,
            media_type: None,
            ap_type: ApObjectType::Article,
            content: Some("<p>this is a post</p>".to_string()),
            summary: None,
            owner_data: None,
            to: None,
            cc: None,
            bto: None,
            bcc: None,
        };
        let ap_object =
            new_html_ap_object("example.com", "foo", "http://example.com/p/foo", &create).unwrap();
        assert_eq!(ap_object.name, Some("this is a post".to_string()));
        assert_eq!(ap_object.summary, Some("this is a post".to_string()));
        assert_eq!(ap_object.content, Some("<p>this is a post</p>".to_string()));
        let id = ap_object.id.unwrap();
        assert!(id.starts_with("https://example.com/pub/article/foo,this-is-a-post"));
        assert_eq!(
            ap_object.actor,
            Some("http://example.com/p/foo".to_string())
        );
        assert_eq!(
            ap_object.attributed_to,
            Some("http://example.com/p/foo".to_string())
        );
    }

    #[test]
    fn GIVEN_media_params_WHEN_new_media_ap_object_THEN_result_has_correct_properties() {
        //GIVEN
        let url = "https://example.com/test";
        let file_name = "test.png";
        let new_ap_object = CreateOwnedApObject {
            name: Some("test".to_string()),
            media_type: Some(ApObjectMediaType::ImageJpeg),
            ap_type: ApObjectType::Image,
            content: Some("content".to_string()),
            summary: Some("summary".to_string()),
            owner_data: None,
            to: None,
            cc: None,
            bto: None,
            bcc: None,
        };

        // WHEN
        let ap_object = new_media_ap_object(
            TEST_HOSTNAME,
            TEST_NOROLEUSER_ACCT,
            url,
            file_name,
            TEST_NOROLEUSER_PUN,
            &new_ap_object,
        );

        // THEN
        assert_eq!(ap_object.name.unwrap(), "test");
        assert_eq!(ap_object.summary.unwrap(), "summary");
        assert_eq!(ap_object.content.unwrap(), "content");
        assert_eq!(ap_object.ap_type, Some(ApObjectType::Image));
        assert_eq!(ap_object.media_type, Some(ApObjectMediaType::ImageJpeg));
        assert_eq!(ap_object.url.unwrap(), url);
        assert!(ap_object.id.is_some());
        assert!(ap_object.actor.is_some());
        assert!(ap_object.attributed_to.is_some());
    }
}
