use dialtone_common::ap::{
    ap_object::{ApObject, ApObjectMediaType, ApObjectType},
    id::create_ap_object_id,
};

use crate::{BadInput, DbError};

use super::{html::process_html, local_id::make_local_id};

pub fn ap_object_from_owner(
    host_name: &str,
    pun: &str,
    actor_id: &str,
    ap_object: ApObject,
) -> Result<ApObject, DbError> {
    let content = &ap_object
        .content
        .as_ref()
        .ok_or(BadInput::InvalidData("ap_objects content required"))?;

    let ap_object_type = if let Some(ap_object_type) = ap_object.ap_type {
        if ap_object_type != ApObjectType::Note && ap_object_type != ApObjectType::Article {
            return Err(DbError::BadInput(BadInput::InvalidData(
                "only Note and Article types are supported",
            )));
        } else {
            ap_object_type
        }
    } else {
        return Err(DbError::BadInput(BadInput::InvalidData(
            "no activity pub object type specified",
        )));
    };

    let (html, title) = process_html(content.as_bytes()).map_err(BadInput::from)?;
    let local_id = make_local_id(Some(pun), title.as_deref());
    let ap_object_id = create_ap_object_id(host_name, &ap_object_type, &local_id);
    let html = String::from_utf8(html).map_err(BadInput::from)?;

    Ok(ApObject::builder()
        .and_name(ap_object.name.map_or_else(|| title.clone(), Some))
        .and_summary(ap_object.summary.map_or(title, Some))
        .id(ap_object_id)
        .media_type(ApObjectMediaType::TextHtml)
        .ap_type(ap_object_type)
        .content(html)
        .actor(actor_id)
        .attributed_to(actor_id)
        .and_to(ap_object.to)
        .and_cc(ap_object.cc)
        .and_bto(ap_object.bto)
        .and_bcc(ap_object.bcc)
        .build())
}
