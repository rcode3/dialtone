use dialtone_common::{
    ap::ap_object::ApObject, rest::ap_objects::ap_object_model::UpdateOwnedApObject,
};

pub fn update_ap_object(ap_object: ApObject, update: UpdateOwnedApObject) -> ApObject {
    ApObject {
        ap_context: ap_object.ap_context,
        name: update.name.or(ap_object.name),
        id: ap_object.id,
        url: ap_object.url,
        media_type: ap_object.media_type,
        ap_type: ap_object.ap_type,
        content: update.content.or(ap_object.content),
        summary: update.summary.or(ap_object.summary),
        actor: ap_object.actor,
        attributed_to: ap_object.attributed_to,
        to: ap_object.to,
        cc: ap_object.cc,
        bto: ap_object.bto,
        bcc: ap_object.bcc,
    }
}

#[cfg(test)]
mod update_ap_object_tests {
    use dialtone_common::{
        ap::ap_object::ApObject, rest::ap_objects::ap_object_model::UpdateOwnedApObject,
    };

    use super::update_ap_object;

    #[test]
    #[allow(non_snake_case)]
    fn GIVEN_an_ap_object_with_none_parts_WHEN_updated_THEN_update_parts_are_updated() {
        // GIVEN
        let ap_object = ApObject::builder().build();

        // WHEN
        let update = UpdateOwnedApObject {
            name: Some("some_name".to_string()),
            content: Some("some_content".to_string()),
            summary: Some("some_summary".to_string()),
            owner_data: None,
        };
        let updated_ap_object = update_ap_object(ap_object, update);

        // THEN
        assert_eq!(updated_ap_object.name, Some("some_name".to_string()));
        assert_eq!(updated_ap_object.content, Some("some_content".to_string()));
        assert_eq!(updated_ap_object.summary, Some("some_summary".to_string()));
    }

    #[test]
    #[allow(non_snake_case)]
    fn GIVEN_an_ap_object_with_some_parts_WHEN_updated_THEN_update_parts_are_updated() {
        // GIVEN
        let ap_object = ApObject::builder()
            .name("name")
            .content("content")
            .summary("summary")
            .build();

        // WHEN
        let update = UpdateOwnedApObject {
            name: Some("new_name".to_string()),
            content: Some("new_content".to_string()),
            summary: Some("new_summary".to_string()),
            owner_data: None,
        };
        let updated_ap_object = update_ap_object(ap_object, update);

        // THEN
        assert_eq!(updated_ap_object.name, Some("new_name".to_string()));
        assert_eq!(updated_ap_object.content, Some("new_content".to_string()));
        assert_eq!(updated_ap_object.summary, Some("new_summary".to_string()));
    }

    #[test]
    #[allow(non_snake_case)]
    fn GIVEN_an_ap_object_with_some_parts_WHEN_updated_with_none_parts_THEN_update_parts_are_not_changed(
    ) {
        // GIVEN
        let ap_object = ApObject::builder()
            .name("name")
            .content("content")
            .summary("summary")
            .build();

        // WHEN
        let update = UpdateOwnedApObject {
            name: None,
            content: None,
            summary: None,
            owner_data: None,
        };
        let updated_ap_object = update_ap_object(ap_object, update);

        // THEN
        assert_eq!(updated_ap_object.name, Some("name".to_string()));
        assert_eq!(updated_ap_object.content, Some("content".to_string()));
        assert_eq!(updated_ap_object.summary, Some("summary".to_string()));
    }
}
