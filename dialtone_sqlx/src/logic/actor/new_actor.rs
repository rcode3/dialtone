use buildstructor::Builder;
use dialtone_common::ap::actor::{Actor, ActorType, Endpoints, PublicKey};
use dialtone_common::ap::ap_object::{ApObject, ApObjectType};
use dialtone_common::ap::id::{
    create_actor_id, create_collection_id, create_shared_inbox, StandardCollectionNames,
};
use dialtone_common::rest::event_log::LogEvent;
use sqlx::types::chrono::Utc;

use dialtone_common::ap::image_attr::{ImageAttributeObj, ImageAttributeType, ImageAttributes};
use dialtone_common::containers::credentialed_actor::CredentialedActor;
use dialtone_common::pages::HTML_MEDIA_TYPE;
use dialtone_common::rest::actors::actor_model::{KeyPair, KeyType, OwnedActor, OwnerActorData};
use dialtone_common::utils::media_types::{ACTIVITY_PUB_MEDIA_TYPE, ACTIVITY_STREAMS_MEDIA_TYPE};
use dialtone_common::webfinger::{
    create_wf_acct, Jrd, JrdLink, AVATAR_REL_URL_ID, PROFILE_PAGE_URL_ID, SELF,
};
use serde_variant::to_variant_name;

#[derive(Debug, Builder)]
pub struct NewActor {
    pub preferred_user_name: String,
    pub actor_type: ActorType,
    pub owner: String,
    pub name: Option<String>,
    pub summary: Option<String>,
    pub icon: Option<ImageAttributes>,
    pub image: Option<ImageAttributes>,
}

pub fn new_actor(host_name: &str, new_actor: NewActor) -> CredentialedActor {
    // create actor ID
    let id = create_actor_id(host_name, &new_actor.preferred_user_name);

    // create private and public keys
    let rsa = openssl::rsa::Rsa::generate(2048).expect("unable to generated RSA 2048 key.");
    let pkey = openssl::pkey::PKey::from_rsa(rsa).expect("unable to create RSA private key.");
    let public_key: Vec<u8> = pkey
        .public_key_to_pem()
        .expect("unable to create public key from private key.");
    let public_key_pem = std::str::from_utf8(&public_key)
        .expect("unable to create public key PEM.")
        .to_string();
    let private_key = pkey
        .private_key_to_pem_pkcs8()
        .expect("unable to create PKC8 private key.");
    let private_key_pem = std::str::from_utf8(&private_key)
        .expect("unable to create private key PEM.")
        .to_string();
    let key_id = make_key_id(&id);

    let key_pair = KeyPair::builder()
        .key_id(&key_id)
        .key_type(KeyType::Rsa2048)
        .private_key_pem(private_key_pem)
        .public_key_pem(&public_key_pem)
        .created_at(Utc::now())
        .build();

    // create JRD
    let jrd = create_jrd(
        host_name,
        &new_actor.preferred_user_name,
        &id,
        &new_actor.icon,
    );

    let owner_actor_data = OwnerActorData::builder().key_pair(key_pair).build();

    // create back end actor
    CredentialedActor {
        owned_actor: OwnedActor {
            ap: Actor::builder()
                .ap_type(new_actor.actor_type)
                .and_name(new_actor.name)
                .and_summary(new_actor.summary)
                .and_icon(new_actor.icon)
                .and_image(new_actor.image)
                .following(create_collection_id(
                    &id,
                    &StandardCollectionNames::Following.to_string(),
                ))
                .followers(create_collection_id(
                    &id,
                    &StandardCollectionNames::Followers.to_string(),
                ))
                .liked(create_collection_id(
                    &id,
                    &StandardCollectionNames::Liked.to_string(),
                ))
                .likes(create_collection_id(
                    &id,
                    &StandardCollectionNames::Likes.to_string(),
                ))
                .preferred_user_name(new_actor.preferred_user_name)
                .inbox(create_collection_id(
                    &id,
                    &StandardCollectionNames::Inbox.to_string(),
                ))
                .outbox(create_collection_id(
                    &id,
                    &StandardCollectionNames::Outbox.to_string(),
                ))
                .public_key(PublicKey {
                    id: key_id,
                    owner: id.clone(),
                    public_key_pem,
                })
                .endpoints(Endpoints {
                    shared_inbox: Option::from(create_shared_inbox(host_name)),
                })
                .id(id)
                .build(),
            jrd: Option::from(jrd),
            collection_count: 0,
            owner_count: 0,
            owner_data: Some(owner_actor_data),
            event_log: vec![LogEvent::new("Actor created.".to_string())],
            created_at: Utc::now(),
            modified_at: Utc::now(),
        },
        creating_user_acct: new_actor.owner,
    }
}

fn create_jrd(
    host_name: &str,
    preferred_user_name: &str,
    id: &str,
    icon: &Option<ImageAttributes>,
) -> Jrd {
    let mut links: Vec<JrdLink> = vec![
        // add activity pub link
        JrdLink {
            rel: SELF.to_string(),
            href: Some(id.to_string()),
            media_type: Option::from(ACTIVITY_PUB_MEDIA_TYPE.to_string()),
        },
        // add activity streams link
        JrdLink {
            rel: SELF.to_string(),
            href: Some(id.to_string()),
            media_type: Option::from(ACTIVITY_STREAMS_MEDIA_TYPE.to_string()),
        },
        // add link to profile page
        JrdLink {
            rel: PROFILE_PAGE_URL_ID.to_string(),
            href: Some(id.to_string()),
            media_type: Option::from(HTML_MEDIA_TYPE.to_string()),
        },
    ];

    // add the first icon
    if let Some(i) = icon {
        if let Some(o) = i.first() {
            links.push(JrdLink {
                rel: AVATAR_REL_URL_ID.to_string(),
                href: Some(o.url),
                media_type: o.media_type,
            });
        }
    }

    Jrd {
        subject: create_wf_acct(host_name, preferred_user_name),
        links,
    }
}

pub fn banner_from_ap_object(banner: Option<ApObject>) -> Option<ImageAttributes> {
    if let Some(banner) = banner {
        if banner.ap_type == Some(ApObjectType::Image)
            && banner.url.is_some()
            && banner.media_type.is_some()
        {
            let media_type = Some(
                to_variant_name(&banner.media_type.unwrap())
                    .unwrap()
                    .to_string(),
            );
            let image_attributes = ImageAttributes::SingleObj(ImageAttributeObj {
                summary: Some("Banner Image".to_string()),
                url: banner.url.unwrap(),
                width: None,
                height: None,
                media_type,
                ap_type: Some(ImageAttributeType::Image),
            });
            Some(image_attributes)
        } else {
            None
        }
    } else {
        None
    }
}

pub fn icon_from_ap_object(icon: Option<ApObject>) -> Option<ImageAttributes> {
    if let Some(icon) = icon {
        if icon.ap_type == Some(ApObjectType::Image)
            && icon.url.is_some()
            && icon.media_type.is_some()
        {
            let media_type = Some(
                to_variant_name(&icon.media_type.unwrap())
                    .unwrap()
                    .to_string(),
            );
            let image_attributes = ImageAttributes::SingleObj(ImageAttributeObj {
                summary: Some("Note (400x400)".to_string()),
                url: icon.url.unwrap(),
                width: Some(400),
                height: Some(400),
                media_type,
                ap_type: Some(ImageAttributeType::Image),
            });
            Some(image_attributes)
        } else {
            None
        }
    } else {
        None
    }
}

pub fn new_actor_with_images(
    new_actor: NewActor,
    icon: Option<ImageAttributes>,
    banner: Option<ImageAttributes>,
) -> NewActor {
    NewActor {
        icon,
        image: banner,
        ..new_actor
    }
}

fn make_key_id(actor_id: &str) -> String {
    format!("{}/keys/{}", actor_id, Utc::now().timestamp())
}

#[cfg(test)]
#[allow(non_snake_case)]
mod new_actor_tests {
    use dialtone_common::ap::{actor::ActorType, ap_object::ApObjectMediaType};

    use super::*;

    #[test]
    fn GIVEN_new_actor_data_WHEN_new_THEN_no_panics() {
        // GIVEN
        let new_actor = NewActor::builder()
            .preferred_user_name("testuser")
            .actor_type(ActorType::Person)
            .owner("user@example.com")
            .build();

        // WHEN
        super::new_actor("example.com", new_actor);

        // THEN
    }

    #[test]
    fn GIVEN_new_actor_WHEN_serialized_THEN_no_panics() {
        // GIVEN
        let new_actor = NewActor::builder()
            .preferred_user_name("testuser")
            .actor_type(ActorType::Person)
            .owner("user@example.com")
            .build();
        let actor = super::new_actor("example.com", new_actor);

        // WHEN
        let json = serde_json::to_string(&actor.owned_actor);

        // THEN
        assert!(json.is_ok());
    }

    #[test]
    fn GIVEN_ap_object_WHEN_convert_to_banner_THEN_attributes_are_correct() {
        // GIVEN
        let ap_object = ApObject::builder()
            .id("https://example.com/foo")
            .url("https://example.com/media/foo.jpg")
            .media_type(ApObjectMediaType::ImageJpeg)
            .ap_type(ApObjectType::Image)
            .build();

        // WHEN
        let banner = banner_from_ap_object(Some(ap_object));

        // THEN
        assert!(banner.is_some());
        let banner = banner.unwrap();
        assert_eq!(
            banner.first().unwrap().ap_type,
            Some(ImageAttributeType::Image)
        );
        assert_eq!(
            banner.first().unwrap().url,
            "https://example.com/media/foo.jpg".to_string()
        );
        assert!(banner.first().unwrap().media_type.is_some());
    }

    #[test]
    fn GIVEN_ap_object_WHEN_convert_to_icon_THEN_attributes_are_correct() {
        // GIVEN
        let ap_object = ApObject::builder()
            .id("https://example.com/foo")
            .url("https://example.com/media/foo.jpg")
            .media_type(ApObjectMediaType::ImageJpeg)
            .ap_type(ApObjectType::Image)
            .build();

        // WHEN
        let icon = icon_from_ap_object(Some(ap_object));

        // THEN
        assert!(icon.is_some());
        let icon = icon.unwrap();
        assert_eq!(
            icon.first().unwrap().ap_type,
            Some(ImageAttributeType::Image)
        );
        assert_eq!(
            icon.first().unwrap().url,
            "https://example.com/media/foo.jpg".to_string()
        );
        assert_eq!(icon.first().unwrap().width, Some(400));
        assert_eq!(icon.first().unwrap().height, Some(400));
        assert!(icon.first().unwrap().media_type.is_some());
    }
}
