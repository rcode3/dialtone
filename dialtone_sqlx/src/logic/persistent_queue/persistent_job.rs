use crate::logic::persistent_queue::job_details::JobDetails;

#[derive(Debug)]
pub struct PersistentJob {
    pub job_id: i32,
    pub attempts_remaining: i32,
    pub job_details: JobDetails,
}
