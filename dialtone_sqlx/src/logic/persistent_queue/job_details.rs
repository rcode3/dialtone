use dialtone_common::ap::{activity::Activity, actor::Actor};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub enum JobDetails {
    NoOp(NoOpJobDetails),
    SendActivityPub(SendActivityPubJobDetails),
    FetchRemoteIcon(FetchRemoteIconJobDetails),
    FetchRemoteBanner(FetchRemoteBannerJobDetails),
}

/// NoOp means No Operation.
/// This is for testing purposes.
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct NoOpJobDetails {
    pub message: String,
}

/// Details for sending Activity Pub.
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct SendActivityPubJobDetails {
    pub public_key_id: String,
    pub private_key_pem: String,
    pub target_uri: String,
    pub activity: Activity,
}

/// Details for fetching an actor's icon.
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct FetchRemoteIconJobDetails {
    pub actor: Actor,
    pub fetched_for_user: Option<String>,
    pub host_name: String,
}

/// Details for fetching an actor's banner.
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct FetchRemoteBannerJobDetails {
    pub actor: Actor,
    pub fetched_for_user: Option<String>,
    pub host_name: String,
}

#[cfg(test)]
mod job_details_tests {
    use dialtone_common::ap::{activity::create_ap_object_activity, ap_object::ApObject};

    use super::*;

    #[test]
    fn roundtrip_noop_test() {
        let noop = NoOpJobDetails {
            message: "test".to_string(),
        };
        let details = JobDetails::NoOp(noop);
        let serialized = serde_json::to_string(&details).unwrap();
        let deserialized: JobDetails = serde_json::from_str(&serialized).unwrap();
        assert_eq!(details, deserialized);
    }

    #[test]
    fn roundtrip_send_activity_pub_test() {
        let activity =
            create_ap_object_activity("https://example.com/foo", &ApObject::builder().build());
        let send_activity_pub = SendActivityPubJobDetails {
            public_key_id: "blah".to_string(),
            private_key_pem: "blah".to_string(),
            target_uri: "http://foo".to_string(),
            activity,
        };
        let details = JobDetails::SendActivityPub(send_activity_pub);
        let serialized = serde_json::to_string(&details).unwrap();
        let deserialized: JobDetails = serde_json::from_str(&serialized).unwrap();
        assert_eq!(details, deserialized)
    }
}
