use std::collections::HashMap;

use dialtone_common::rest::sites::site_data::{PublicSiteInfo, SiteData, SiteNames};
use dialtone_sqlx::db::site_info::{create_site, fetch_site, update::update_site};
use dialtone_test_util::test_pg;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_site_WHEN_updated_THEN_fetch_shows_update() {
    let host_name = "test.example";
    let short_name = "test".to_string();
    let long_name = Some("testy testers".to_string());

    test_pg::test_pg(move |pool| async move {
        // GIVEN
        let action = create_site(
            &pool,
            host_name,
            short_name.clone(),
            long_name.clone(),
            None,
            HashMap::new(),
        )
        .await;
        assert!(action.is_ok());

        // fetch the sites
        let action = fetch_site(&pool, host_name).await;
        assert!(action.is_ok());
        let site_info = action.unwrap().unwrap();
        let site_data = site_info.site_data;
        let public_site_info = site_data.public;

        // WHEN
        let new_site_data = SiteData {
            public: PublicSiteInfo {
                names: SiteNames {
                    short_name: "new test".to_string(),
                    long_name: "new long name".to_string(),
                },
                ..public_site_info
            },
            ..SiteData::default()
        };
        let action = update_site(&pool, host_name, new_site_data).await;
        assert!(action.is_ok());

        // THEN
        let action = fetch_site(&pool, host_name).await;
        assert!(action.is_ok());
        let site_info = action.unwrap().unwrap();
        assert_eq!("new test", site_info.site_data.public.names.short_name);
        assert_eq!(
            "new long name",
            Some(site_info.site_data.public.names.long_name).unwrap()
        );
    })
    .await;
}
