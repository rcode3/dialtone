use std::collections::HashMap;

use dialtone_sqlx::db::site_info::{create_site, delete_site, fetch_site};
use dialtone_test_util::test_pg;

#[tokio::test]
async fn delete_site_test() {
    let host_name = "test.example";
    let short_name = "test".to_string();
    let long_name = Some("testy testers".to_string());

    test_pg::test_pg(move |pool| async move {
        // create the sites
        let action = create_site(
            &pool,
            host_name,
            short_name.clone(),
            long_name.clone(),
            None,
            HashMap::new(),
        )
        .await;
        assert!(action.is_ok());

        // fetch the sites
        let action = fetch_site(&pool, host_name).await;
        assert!(action.is_ok());
        let site_info = action.unwrap().unwrap();
        assert_eq!(short_name, site_info.site_data.public.names.short_name);
        assert_eq!(long_name, Some(site_info.site_data.public.names.long_name));

        // delete the sites
        let action = delete_site(&pool, host_name).await;
        assert!(action.is_ok());

        // try to refetch the sites
        let action = fetch_site(&pool, host_name).await;
        assert!(action.unwrap().is_none());
    })
    .await;
}
