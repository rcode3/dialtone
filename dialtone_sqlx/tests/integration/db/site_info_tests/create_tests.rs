use std::collections::HashMap;

use dialtone_sqlx::db::site_info::create_site;
use dialtone_test_util::test_pg;

#[tokio::test]
async fn create_site_test() {
    test_pg::test_pg(move |pool| async move {
        let action = create_site(
            &pool,
            "test.example",
            "test".to_string(),
            Some("testy testers".to_string()),
            None,
            HashMap::new(),
        )
        .await;
        assert!(action.is_ok());
    })
    .await;
}
