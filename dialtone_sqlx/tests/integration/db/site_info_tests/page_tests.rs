use dialtone_common::rest::sites::site_data::SiteInfo;
use dialtone_sqlx::{
    db::site_info::{create_site, page_sites},
    DbError,
};
use dialtone_test_util::test_pg;
use std::{collections::HashMap, thread, time::Duration};

#[tokio::test]
async fn page_site_test() {
    test_pg::test_pg(move |pool| async move {
        // create sites to page through
        for n in 1..=100 {
            let short_name = format!("test{n}");
            let host_name = format!("{short_name}.example");
            let long_name = Some(format!("test sites {n}"));

            // create the sites
            let action = create_site(
                &pool,
                &host_name,
                short_name.clone(),
                long_name.clone(),
                None,
                HashMap::new(),
            )
            .await;
            assert!(action.is_ok());
            // insure 1 millisecond separation
            thread::sleep(Duration::from_millis(1));
        }

        // get first page of 10
        let page1 = page_sites(&pool, None, None, 10).await;
        let page1_sites = assert_action_ok(page1);
        assert_eq!(page1_sites.len(), 10);
        (0..10).for_each(|n| {
            assert_eq!(page1_sites[n].host_name, format!("test{}.example", n + 1));
        });

        // get the second page
        let page2 = page_sites(
            &pool,
            None,
            Some(&page1_sites.last().unwrap().created_at),
            10,
        )
        .await;
        let page2_sites = assert_action_ok(page2);
        assert_eq!(page2_sites.len(), 10);
        (0..10).for_each(|n| {
            assert_eq!(page2_sites[n].host_name, format!("test{}.example", n + 11));
        });

        // go back to page 1 via page 2
        let page1_back = page_sites(
            &pool,
            Some(&page2_sites.first().unwrap().created_at),
            None,
            10,
        )
        .await;
        let page1_back_sites = assert_action_ok(page1_back);
        assert_eq!(page1_back_sites.len(), 10);
        (0..10).for_each(|n| {
            assert_eq!(
                page1_back_sites[n].host_name,
                format!("test{}.example", n + 1)
            );
        });

        // get a page based on prev and next date
        let page2_sub = page_sites(
            &pool,
            Some(&page2_sites.last().unwrap().created_at),
            Some(&page2_sites.first().unwrap().created_at),
            10,
        )
        .await;
        let page2_sub_sites = assert_action_ok(page2_sub);
        assert_eq!(page2_sub_sites.len(), 8);
        (0..8).for_each(|n| {
            assert_eq!(
                page2_sub_sites[n].host_name,
                format!("test{}.example", n + 12)
            );
        });
    })
    .await;
}

fn assert_action_ok(action: Result<Option<Vec<SiteInfo>>, DbError>) -> Vec<SiteInfo> {
    if action.is_err() {
        println!("{:?}", action.as_ref().err())
    }
    assert!(action.is_ok());
    action.unwrap().unwrap()
}
