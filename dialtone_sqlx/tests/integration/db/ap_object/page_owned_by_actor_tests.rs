use dialtone_common::rest::ap_objects::ap_object_model::{ApObjectVisibilityType, OwnedApObject};
use dialtone_sqlx::db::ap_object::page_owned_by_actor::page_owned_ap_objects_by_actor;
use dialtone_sqlx::DbError;
use dialtone_test_util::create_actor::create_actor_tst_utl;
use dialtone_test_util::create_ap_object::create_article_tst_utl;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_constants::TEST_HOSTNAME;
use dialtone_test_util::{test_action, test_pg};
use sqlx::{Pool, Postgres};

#[tokio::test]
async fn page_all_owned_ap_objects_by_actor_test() {
    test_pg::test_pg(move |pool| async move {
        let host_name = TEST_HOSTNAME;
        create_system_tst_utl(&pool).await;
        let created_actor = create_actor_tst_utl(&pool, "testymctestfase", host_name).await;
        create_many_articles(
            &pool,
            host_name,
            created_actor.owned_actor.ap.preferred_user_name.as_str(),
            50,
        )
        .await;

        let page1_result = page_owned_ap_objects_by_actor(
            &pool,
            None,
            None,
            10,
            created_actor.owned_actor.ap.id.as_str(),
            None,
        )
        .await;
        let page1_ap_objects = assert_action_ok(page1_result);
        assert_eq!(page1_ap_objects.len(), 10);

        let page2_result = page_owned_ap_objects_by_actor(
            &pool,
            None,
            Some(&page1_ap_objects.last().unwrap().modified_at),
            15,
            created_actor.owned_actor.ap.id.as_str(),
            None,
        )
        .await;
        let page2_ap_objects = assert_action_ok(page2_result);
        assert_eq!(page2_ap_objects.len(), 15);

        let all_page_result = page_owned_ap_objects_by_actor(
            &pool,
            None,
            None,
            50,
            created_actor.owned_actor.ap.id.as_str(),
            None,
        )
        .await;
        let all_page_ap_objects = assert_action_ok(all_page_result);
        assert_eq!(all_page_ap_objects.len(), 50);
    })
    .await;
}

#[tokio::test]
async fn page_owned_ap_objects_of_actor_test() {
    test_pg::test_pg(move |pool| async move {
        let host_name = TEST_HOSTNAME;
        create_system_tst_utl(&pool).await;
        let actor1 = create_actor_tst_utl(&pool, "testymctestfase", host_name).await;
        create_many_articles(
            &pool,
            host_name,
            actor1.owned_actor.ap.preferred_user_name.as_str(),
            20,
        )
        .await;
        let actor2 = create_actor_tst_utl(&pool, "othertesty", host_name).await;
        create_many_articles(
            &pool,
            host_name,
            actor2.owned_actor.ap.preferred_user_name.as_str(),
            30,
        )
        .await;

        let actor1_all_result = page_owned_ap_objects_by_actor(
            &pool,
            None,
            None,
            20,
            actor1.owned_actor.ap.id.as_str(),
            None,
        )
        .await;
        let actor1_all_ap_objects = assert_action_ok(actor1_all_result);
        assert_eq!(actor1_all_ap_objects.len(), 20);

        let actor2_all_result = page_owned_ap_objects_by_actor(
            &pool,
            None,
            None,
            30,
            actor2.owned_actor.ap.id.as_str(),
            None,
        )
        .await;
        let actor2_all_ap_objects = assert_action_ok(actor2_all_result);
        assert_eq!(actor2_all_ap_objects.len(), 30);
    })
    .await;
}

#[tokio::test]
async fn page_visible_owned_ap_objects_by_actor_test() {
    test_pg::test_pg(move |pool| async move {
        let host_name = TEST_HOSTNAME;
        create_system_tst_utl(&pool).await;
        let created_actor = create_actor_tst_utl(&pool, "testymctestfase", host_name).await;
        create_many_articles(
            &pool,
            host_name,
            created_actor.owned_actor.ap.preferred_user_name.as_str(),
            5,
        )
        .await;

        let all_page_result = page_owned_ap_objects_by_actor(
            &pool,
            None,
            None,
            50,
            created_actor.owned_actor.ap.id.as_str(),
            Some(&ApObjectVisibilityType::Visible),
        )
        .await;
        let all_page_ap_objects = assert_action_ok(all_page_result);
        assert_eq!(all_page_ap_objects.len(), 5);

        let none_page_result = page_owned_ap_objects_by_actor(
            &pool,
            None,
            None,
            50,
            created_actor.owned_actor.ap.id.as_str(),
            Some(&ApObjectVisibilityType::Invisible),
        )
        .await;
        let none_page_ap_objects = assert_action_ok(none_page_result);
        assert_eq!(none_page_ap_objects.len(), 0);
    })
    .await;
}

async fn create_many_articles(
    pool: &Pool<Postgres>,
    host_name: &str,
    actor_id: &str,
    num_articles: i32,
) {
    for n in 1..=num_articles {
        let title = format!(r#"test_article_{n}"#);
        create_article_tst_utl(pool, host_name, &title, Some(actor_id)).await;
    }
}

fn assert_action_ok(action: Result<Option<Vec<OwnedApObject>>, DbError>) -> Vec<OwnedApObject> {
    test_action!(action);
    match action.unwrap() {
        None => Vec::new(),
        Some(data) => data,
    }
}
