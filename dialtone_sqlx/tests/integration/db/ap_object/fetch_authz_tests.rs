use dialtone_sqlx::db::ap_object::fetch_authz::fetch_ap_object_authz;
use dialtone_test_util::{
    create_ap_object::create_article_for_actor_tst_utl, create_system::create_system_tst_utl,
    test_action, test_constants::TEST_NOROLEUSER_ACCT, test_pg,
};

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_ap_object_WHEN_authz_owning_user_THEN_true() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let actor_id = actors.no_role_actor.owned_actor.ap.id;
        let ap_object = create_article_for_actor_tst_utl(&pool, &actor_id, "foo").await;

        // WHEN
        let action =
            fetch_ap_object_authz(&pool, &ap_object.id.unwrap(), TEST_NOROLEUSER_ACCT).await;

        // THEN
        test_action!(action);
        assert!(action.unwrap());
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_ap_object_WHEN_authz_non_owning_user_THEN_true() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let actor_id = actors.all_role_actor.owned_actor.ap.id;
        let ap_object = create_article_for_actor_tst_utl(&pool, &actor_id, "foo").await;

        // WHEN
        let action =
            fetch_ap_object_authz(&pool, &ap_object.id.unwrap(), TEST_NOROLEUSER_ACCT).await;

        // THEN
        test_action!(action);
        assert!(!action.unwrap());
    })
    .await;
}
