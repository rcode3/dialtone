use dialtone_common::rest::ap_objects::ap_object_model::ApObjectVisibilityType;
use dialtone_sqlx::db::ap_object::page_by_actor::page_ap_objects_by_actor;
use dialtone_test_util::create_actor::create_actor_tst_utl;
use dialtone_test_util::create_ap_object::create_article_tst_utl;
use dialtone_test_util::create_system::create_bare_system_for_host_tst_utl;
use dialtone_test_util::{test_action, test_pg};
use sqlx::{Pool, Postgres};

#[tokio::test]
async fn page_all_ap_objects_by_actor_test() {
    test_pg::test_pg(move |pool| async move {
        let host_name = "example.net";
        create_bare_system_for_host_tst_utl(&pool, host_name).await;
        let created_actor = create_actor_tst_utl(&pool, "testymctestfase", host_name).await;
        create_many_articles(
            &pool,
            host_name,
            created_actor.owned_actor.ap.preferred_user_name.as_str(),
            50,
        )
        .await;

        let page1_result = page_ap_objects_by_actor(
            &pool,
            None,
            None,
            10,
            created_actor.owned_actor.ap.id.as_str(),
            None,
        )
        .await;
        test_action!(page1_result);
        let page1_ap_objects = page1_result.unwrap().unwrap();
        assert_eq!(page1_ap_objects.items.len(), 10);

        let page2_result = page_ap_objects_by_actor(
            &pool,
            None,
            Some(&page1_ap_objects.next.unwrap()),
            10,
            created_actor.owned_actor.ap.id.as_str(),
            None,
        )
        .await;
        test_action!(page2_result);
        let page2_ap_objects = page2_result.unwrap().unwrap();
        assert_eq!(page2_ap_objects.items.len(), 10);

        let all_page_result = page_ap_objects_by_actor(
            &pool,
            None,
            None,
            50,
            created_actor.owned_actor.ap.id.as_str(),
            None,
        )
        .await;
        test_action!(all_page_result);
        let all_page_ap_objects = all_page_result.unwrap().unwrap();
        assert_eq!(all_page_ap_objects.items.len(), 50);
    })
    .await;
}

#[tokio::test]
async fn page_ap_objects_of_actor_test() {
    test_pg::test_pg(move |pool| async move {
        let host_name = "example.net";
        create_bare_system_for_host_tst_utl(&pool, host_name).await;
        let actor1 = create_actor_tst_utl(&pool, "testymctestfase", host_name).await;
        create_many_articles(
            &pool,
            host_name,
            actor1.owned_actor.ap.preferred_user_name.as_str(),
            20,
        )
        .await;
        let actor2 = create_actor_tst_utl(&pool, "othertesty", host_name).await;
        create_many_articles(
            &pool,
            host_name,
            actor2.owned_actor.ap.preferred_user_name.as_str(),
            30,
        )
        .await;

        let actor1_all_result = page_ap_objects_by_actor(
            &pool,
            None,
            None,
            20,
            actor1.owned_actor.ap.id.as_str(),
            None,
        )
        .await;
        test_action!(actor1_all_result);
        let actor1_all_ap_objects = actor1_all_result.unwrap().unwrap();
        assert_eq!(actor1_all_ap_objects.items.len(), 20);

        let actor2_all_result = page_ap_objects_by_actor(
            &pool,
            None,
            None,
            30,
            actor2.owned_actor.ap.id.as_str(),
            None,
        )
        .await;
        test_action!(actor2_all_result);
        let actor2_all_ap_objects = actor2_all_result.unwrap().unwrap();
        assert_eq!(actor2_all_ap_objects.items.len(), 30);
    })
    .await;
}

#[tokio::test]
async fn page_visible_ap_objects_by_actor_test() {
    test_pg::test_pg(move |pool| async move {
        let host_name = "example.net";
        create_bare_system_for_host_tst_utl(&pool, host_name).await;
        let created_actor = create_actor_tst_utl(&pool, "testymctestfase", host_name).await;
        create_many_articles(
            &pool,
            host_name,
            created_actor.owned_actor.ap.preferred_user_name.as_str(),
            50,
        )
        .await;

        let all_page_result = page_ap_objects_by_actor(
            &pool,
            None,
            None,
            50,
            created_actor.owned_actor.ap.id.as_str(),
            Some(&ApObjectVisibilityType::Visible),
        )
        .await;
        test_action!(all_page_result);
        let all_page_ap_objects = all_page_result.unwrap().unwrap();
        assert_eq!(all_page_ap_objects.items.len(), 50);

        let none_page_result = page_ap_objects_by_actor(
            &pool,
            None,
            None,
            50,
            created_actor.owned_actor.ap.id.as_str(),
            Some(&ApObjectVisibilityType::Invisible),
        )
        .await;
        test_action!(none_page_result);
        let none_page_ap_objects = none_page_result.unwrap();
        assert!(none_page_ap_objects.is_none());
    })
    .await;
}

async fn create_many_articles(
    pool: &Pool<Postgres>,
    host_name: &str,
    actor_id: &str,
    num_articles: i32,
) {
    for n in 1..=num_articles {
        let title = format!("test_article_{n}");
        create_article_tst_utl(pool, host_name, &title, Some(actor_id)).await;
    }
}
