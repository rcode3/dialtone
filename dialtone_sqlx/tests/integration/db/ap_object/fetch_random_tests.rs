use dialtone_common::ap::ap_object::ApObjectType;
use dialtone_sqlx::db::ap_object::fetch_random::fetch_random_ap_object_by_actor;
use dialtone_test_util::create_actor::create_actor_tst_utl;
use dialtone_test_util::create_ap_object::create_article_for_actor_tst_utl;
use dialtone_test_util::create_system::create_bare_system_for_host_tst_utl;
use dialtone_test_util::test_constants::{TEST_HOSTNAME, TEST_NOROLEUSER_NAME};
use dialtone_test_util::{test_action, test_pg};

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_actor_with_ap_objects_WHEN_fetch_random_THEN_one_is_returned() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        let actor = create_actor_tst_utl(&pool, TEST_NOROLEUSER_NAME, TEST_HOSTNAME).await;
        create_article_for_actor_tst_utl(&pool, &actor.owned_actor.ap.id, "some content").await;

        // WHEN
        let action = fetch_random_ap_object_by_actor(
            &pool,
            &ApObjectType::Article,
            &actor.owned_actor.ap.id,
        )
        .await;

        // THEN
        test_action!(action);
        assert!(action.unwrap().is_some());
    })
    .await;
}
