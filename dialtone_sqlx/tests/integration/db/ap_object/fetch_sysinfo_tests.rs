use dialtone_common::ap::ap_object::{ApObject, ApObjectType};
use dialtone_common::rest::ap_objects::ap_object_model::ApObjectVisibilityType;
use dialtone_sqlx::db::ap_object::fetch_sysinfo::fetch_ap_object_system_info;
use dialtone_sqlx::db::ap_object::insert::insert_ap_object;
use dialtone_sqlx::db::ap_object::{ApObjectDbType, ApObjectVisibilityDbType};
use dialtone_test_util::create_site::create_site_tst_utl;
use dialtone_test_util::{test_action, test_pg};

#[tokio::test]
async fn fetch_ap_object_system_info_test() {
    test_pg::test_pg(move |pool| async move {
        let host_name = "example.net";
        create_site_tst_utl(&pool, host_name).await;
        let ap_object = ApObject::builder()
            .id("https://example.net/pub/bar/this_is_a_post")
            .ap_type(ApObjectType::Article)
            .content("<p>this is a post</p>")
            .build();
        let action = insert_ap_object(
            &pool,
            host_name,
            &ap_object,
            None,
            ApObjectDbType::Article,
            None,
            ApObjectVisibilityDbType::Visible,
        )
        .await;
        test_action!(action);

        let action =
            fetch_ap_object_system_info(&pool, ap_object.id.as_ref().unwrap().as_str()).await;
        test_action!(action);
        let fetched_result = action.unwrap();
        assert!(fetched_result.is_some());
        let fetched_ap_object_sysinfo = fetched_result.unwrap();
        assert_eq!(
            fetched_ap_object_sysinfo.visibility,
            ApObjectVisibilityType::Visible
        );
        assert!(fetched_ap_object_sysinfo.actor_owner.is_none());
    })
    .await;
}
