use dialtone_common::rest::ap_objects::ap_object_model::ApObjectSystemData;
use dialtone_sqlx::db::ap_object::fetch_sysinfo::fetch_ap_object_system_info;
use dialtone_sqlx::db::ap_object::update_system_data::update_ap_object_system_data;
use dialtone_test_util::create_ap_object::create_article_tst_utl;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_constants::TEST_HOSTNAME;
use dialtone_test_util::{test_action, test_pg};

#[tokio::test]
async fn update_ap_object_system_data_test() {
    test_pg::test_pg(move |pool| async move {
        let host_name = TEST_HOSTNAME;
        create_system_tst_utl(&pool).await;
        let ap_object = create_article_tst_utl(&pool, host_name, "this_is_a_post", None).await;
        let ap_object_id = ap_object.id.unwrap();

        let system_data = ApObjectSystemData {
            file_data: None,
            http_get_data: None,
            fetched_for_user: None,
        };
        let action = update_ap_object_system_data(&pool, &ap_object_id, &system_data).await;
        test_action!(action);

        let action = fetch_ap_object_system_info(&pool, &ap_object_id).await;
        test_action!(action);
        let fetched_result = action.unwrap();
        assert!(fetched_result.is_some());
        let fetched_ap_object = fetched_result.unwrap();
        assert_eq!(system_data, fetched_ap_object.system_data.unwrap());
    })
    .await;
}
