use dialtone_sqlx::db::ap_object::fetch_owned::fetch_owned_ap_object;
use dialtone_sqlx::db::ap_object::update::update_ap_object;
use dialtone_test_util::create_ap_object::create_article_tst_utl;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_constants::TEST_HOSTNAME;
use dialtone_test_util::{test_action, test_pg};

#[tokio::test]
async fn update_ap_object_test() {
    test_pg::test_pg(move |pool| async move {
        let host_name = TEST_HOSTNAME;
        create_system_tst_utl(&pool).await;
        let ap_object = create_article_tst_utl(&pool, host_name, "this_is_a_post", None).await;

        let mut new_ap_object = ap_object.clone();
        let content = "<p>updated content</p>".to_string();
        new_ap_object.content = Some(content);
        let action = update_ap_object(&pool, &new_ap_object, None).await;
        test_action!(action);

        let action = fetch_owned_ap_object(&pool, ap_object.id.as_ref().unwrap().as_str()).await;
        test_action!(action);
        let fetched_result = action.unwrap();
        assert!(fetched_result.is_some());
        let fetched_ap_object = fetched_result.unwrap();
        assert_eq!(&new_ap_object.id, &fetched_ap_object.ap.id);
        assert_eq!(new_ap_object.ap_type, fetched_ap_object.ap.ap_type);
        assert_eq!(new_ap_object.content, fetched_ap_object.ap.content);
    })
    .await;
}
