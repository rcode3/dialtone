use dialtone_common::ap::ap_object::{ApObject, ApObjectType};
use dialtone_sqlx::db::ap_object::fetch::fetch_ap_object;

use dialtone_sqlx::db::ap_object::insert::insert_ap_object;
use dialtone_sqlx::db::ap_object::{ApObjectDbType, ApObjectVisibilityDbType};
use dialtone_test_util::create_site::create_site_tst_utl;
use dialtone_test_util::{test_action, test_pg};

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_ap_object_id_WHEN_fetch_ap_object_by_id_THEN_success() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        let host_name = "example.net";
        create_site_tst_utl(&pool, host_name).await;
        let ap_object = ApObject::builder()
            .id("https://example.net/pub/bar/this_is_a_post")
            .ap_type(ApObjectType::Article)
            .content("<p>this is a post</p>")
            .build();
        let action = insert_ap_object(
            &pool,
            host_name,
            &ap_object,
            None,
            ApObjectDbType::Article,
            None,
            ApObjectVisibilityDbType::Visible,
        )
        .await;
        test_action!(action);

        // WHEN
        let action = fetch_ap_object(&pool, ap_object.id.as_ref().unwrap().as_str()).await;
        test_action!(action);

        // THEN
        let fetched_result = action.unwrap();
        assert!(fetched_result.is_some());
        let fetched_ap_object = fetched_result.unwrap();
        assert_eq!(&ap_object.id, &fetched_ap_object.id);
        assert_eq!(ap_object.ap_type, fetched_ap_object.ap_type);
        assert_eq!(ap_object.content, fetched_ap_object.content);
    })
    .await;
}
