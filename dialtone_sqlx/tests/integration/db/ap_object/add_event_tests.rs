use dialtone_common::rest::event_log::LogEvent;
use dialtone_sqlx::db::ap_object::add_event::add_log_event;
use dialtone_sqlx::db::ap_object::fetch_owned::fetch_owned_ap_object;
use dialtone_test_util::create_ap_object::create_article_tst_utl;
use dialtone_test_util::create_system::create_bare_system_for_host_tst_utl;
use dialtone_test_util::{test_action, test_pg};

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_ap_object_WHEN_add_event_THEN_event_log_is_one_greater() {
    test_pg::test_pg(move |pool| async move {
        let host_name = "example.net";
        create_bare_system_for_host_tst_utl(&pool, host_name).await;

        // GIVEN ap_object
        let ap_object = create_article_tst_utl(&pool, host_name, "this_is_a_post", None).await;
        let ap_object_id = ap_object.id.unwrap();

        // WHEN add event
        let log_event = LogEvent::new("new test event".to_string());
        let action = add_log_event(&pool, &ap_object_id, &log_event).await;
        test_action!(action);

        // THEN event log is one greater
        let action = fetch_owned_ap_object(&pool, &ap_object_id).await;
        test_action!(action);
        let fetched_result = action.unwrap();
        assert!(fetched_result.is_some());
        let fetched_ap_object = fetched_result.unwrap();
        assert_eq!(fetched_ap_object.event_log.len(), 2);
    })
    .await;
}
