use dialtone_common::rest::users::web_user::{SystemPermission, SystemRoleType};
use dialtone_sqlx::db::system_role::add::add_system_role;
use dialtone_sqlx::db::system_role::remove::remove_system_role;
use dialtone_sqlx::db::user::create_user;
use dialtone_sqlx::db::user::fetch_info::fetch_user_info;
use dialtone_test_util::create_site::create_site_tst_utl;
use dialtone_test_util::test_constants::{TEST_HOSTNAME, TEST_NOROLEUSER_ACCT, TEST_PASSWORD};
use dialtone_test_util::{test_action, test_pg};

#[tokio::test]
async fn remove_system_role_test() {
    test_pg::test_pg(move |pool| async move {
        create_site_tst_utl(&pool, TEST_HOSTNAME).await;
        create_user(&pool, TEST_NOROLEUSER_ACCT, TEST_PASSWORD)
            .await
            .unwrap();

        let action = add_system_role(
            &pool,
            &SystemRoleType::UserAdmin,
            TEST_NOROLEUSER_ACCT,
            TEST_HOSTNAME,
        )
        .await;
        test_action!(action);

        let web_user = fetch_user_info(&pool, TEST_NOROLEUSER_ACCT).await.unwrap();
        let perms = web_user.unwrap().user_authz.system_permissions;
        assert_eq!(perms.len(), 1);
        assert_eq!(
            perms[0],
            SystemPermission {
                role: SystemRoleType::UserAdmin,
                host_name: TEST_HOSTNAME.to_string()
            }
        );

        let action = remove_system_role(
            &pool,
            &SystemRoleType::UserAdmin,
            TEST_NOROLEUSER_ACCT,
            TEST_HOSTNAME,
        )
        .await;
        test_action!(action);

        let web_user = fetch_user_info(&pool, TEST_NOROLEUSER_ACCT).await.unwrap();
        let perms = web_user.unwrap().user_authz.system_permissions;
        assert_eq!(perms.len(), 0);
    })
    .await;
}
