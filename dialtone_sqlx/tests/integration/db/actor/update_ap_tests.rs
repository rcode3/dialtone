use dialtone_common::rest::actors::actor_model::UpdateActorAp;
use dialtone_sqlx::db::actor::fetch_owned::fetch_owned_actor_by_id;
use dialtone_sqlx::db::actor::update_ap::{update_actor_ap, update_actor_ap_by_owner};
use dialtone_test_util::create_actor::create_actor_tst_utl;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::TEST_HOSTNAME;
use dialtone_test_util::test_pg::test_pg;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_new_actor_THEN_fetch_from_db_WHEN_update_actors_ap_by_actor_id_THEN_database_is_updated(
) {
    test_pg(move |pool| async move {
        // GIVEN new actor
        create_system_tst_utl(&pool).await;
        let created_actor = create_actor_tst_utl(&pool, "testymctestfase", TEST_HOSTNAME).await;
        let actor_id = created_actor.owned_actor.ap.id;

        // THEN fetch from db
        let action = fetch_owned_actor_by_id(&pool, &actor_id).await;
        test_action!(action);
        assert!(action.unwrap().unwrap().ap.name.is_none());

        // WHEN update actors ap by actors id
        let update = UpdateActorAp {
            name: Some("foo".to_string()),
            summary: None,
            icon: None,
            image: None,
        };
        let action = update_actor_ap(&pool, update, &actor_id).await;
        test_action!(action);

        // THEN database is updated
        let action = fetch_owned_actor_by_id(&pool, &actor_id).await;
        test_action!(action);
        let owned_actor = action.unwrap().unwrap();
        let name = owned_actor.ap.name;
        assert!(name.is_some());
        assert_eq!(name.unwrap(), "foo");
        assert_eq!(owned_actor.event_log.len(), 2);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_new_actor_THEN_fetch_from_db_WHEN_update_actors_ap_by_owner_THEN_database_is_updated(
) {
    test_pg(move |pool| async move {
        // GIVEN new actor
        create_system_tst_utl(&pool).await;
        let created_actor = create_actor_tst_utl(&pool, "testymctestfase", TEST_HOSTNAME).await;
        let actor_id = created_actor.owned_actor.ap.id;
        let owner = created_actor.creating_user_acct;

        // THEN fetch from db
        let action = fetch_owned_actor_by_id(&pool, &actor_id).await;
        test_action!(action);
        assert!(action.unwrap().unwrap().ap.name.is_none());

        // THEN update actors ap by owner
        let update = UpdateActorAp {
            name: Some("foo".to_string()),
            summary: None,
            icon: None,
            image: None,
        };
        let action = update_actor_ap_by_owner(&pool, update, &actor_id, &owner).await;
        test_action!(action);

        // THEN database is updated
        let action = fetch_owned_actor_by_id(&pool, &actor_id).await;
        test_action!(action);
        let owned_actor = action.unwrap().unwrap();
        let name = owned_actor.ap.name;
        assert!(name.is_some());
        assert_eq!(name.unwrap(), "foo");
        assert_eq!(owned_actor.event_log.len(), 2);
    })
    .await;
}
