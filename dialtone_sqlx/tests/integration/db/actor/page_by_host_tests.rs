use dialtone_common::pages::create_base_url;
use dialtone_common::rest::actors::actor_model::PublicActor;
use dialtone_common::utils::make_acct::make_acct;
use dialtone_sqlx::db::actor::page_by_host::page_public_actors_by_host;
use dialtone_sqlx::db::user::create_user;
use dialtone_test_util::create_actor::create_actor_for_user_tst_utl;
use dialtone_test_util::create_system::create_bare_system_for_host_tst_utl;
use dialtone_test_util::{test_action, test_pg};
use sqlx::{Pool, Postgres};
use std::{thread, time::Duration};

#[tokio::test]
async fn page_all_actors_by_host_test() {
    test_pg::test_pg(move |pool| async move {
        let host1 = "example.net";
        let host2 = "example.com";
        create_many_actors(&pool, host1, 10).await;
        create_many_actors(&pool, host2, 15).await;

        let host1_page_result =
            page_public_actors_by_host(&pool, None, None, 20, host1, None).await;
        test_action!(host1_page_result);
        let host1_page = host1_page_result.unwrap().unwrap();
        assert_eq!(host1_page.items.len(), 13);
        assert_actor_ids_for_host(&host1_page.items, host1);

        let host2_page_result =
            page_public_actors_by_host(&pool, None, None, 20, host2, None).await;
        test_action!(host2_page_result);
        let host2_page = host2_page_result.unwrap().unwrap();
        assert_eq!(host2_page.items.len(), 18);
        assert_actor_ids_for_host(&host2_page.items, host2);
    })
    .await;
}

async fn create_many_actors(pool: &Pool<Postgres>, host_name: &str, num_actors: i32) {
    create_bare_system_for_host_tst_utl(pool, host_name).await;
    let user_name = "testuser";
    let acct = make_acct(user_name, host_name);
    let action = create_user(pool, &acct, "supersecret").await;
    assert!(action.is_ok());
    for n in 1..=num_actors {
        let pun = format!("testactor{n}");
        create_actor_for_user_tst_utl(pool, &pun, host_name, &acct, false).await;
        // insure 1 millisecond separation
        thread::sleep(Duration::from_millis(1));
    }
}

fn assert_actor_ids_for_host(actors: &[PublicActor], host_name: &str) {
    let base_url = create_base_url(host_name);
    actors
        .iter()
        .for_each(|actor| assert!(actor.ap.id.starts_with(base_url.as_str())))
}
