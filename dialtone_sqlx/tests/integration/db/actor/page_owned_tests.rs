use dialtone_common::ap::actor::ActorType;
use dialtone_common::pages::create_base_url;
use dialtone_common::rest::actors::actor_model::{ActorSystemInfo, ActorVisibility, OwnedActor};
use dialtone_common::utils::make_acct::make_acct;
use dialtone_sqlx::db::actor::fetch_sysinfo::fetch_actor_system_info;
use dialtone_sqlx::db::actor::page_owned::page_owned_actors;
use dialtone_sqlx::db::actor::update_sysinfo::update_actor_system_info;
use dialtone_sqlx::db::user::create_user;
use dialtone_sqlx::DbError;
use dialtone_test_util::create_actor::create_actor_for_user_tst_utl;
use dialtone_test_util::create_system::create_bare_system_for_host_tst_utl;
use dialtone_test_util::{test_action, test_pg};
use sqlx::{Pool, Postgres};
use std::{thread, time::Duration};

#[tokio::test]
async fn page_all_owned_actors() {
    test_pg::test_pg(move |pool| async move {
        create_many_actors(&pool, "example1.com", 5).await;
        create_many_actors(&pool, "example2.com", 5).await;

        // get first page of 8 actors
        let page1 = page_owned_actors(&pool, None, None, 8, None, None, None).await;
        let page1_actors = assert_action_ok(page1);
        assert_eq!(page1_actors.len(), 8);
        (0..8).for_each(|n| {
            assert!(page1_actors[n]
                .ap
                .id
                .as_str()
                .starts_with(&create_base_url("example1.com")));
        });

        // get the second page of 8 actors
        let page2 = page_owned_actors(
            &pool,
            None,
            Some(&page1_actors.last().unwrap().modified_at),
            8,
            None,
            None,
            None,
        )
        .await;
        let page2_actors = assert_action_ok(page2);
        assert_eq!(page2_actors.len(), 8);
        (0..8).for_each(|n| {
            assert!(page2_actors[n]
                .ap
                .id
                .as_str()
                .starts_with(&create_base_url("example2.com")));
        });

        // go back to page1 based on prev page
        let page1_back = page_owned_actors(
            &pool,
            Some(&page2_actors.first().unwrap().modified_at),
            None,
            8,
            None,
            None,
            None,
        )
        .await;
        let page1_back_actors = assert_action_ok(page1_back);
        assert_eq!(page1_back_actors.len(), 8);
        (0..8).for_each(|n| {
            assert!(page1_back_actors[n]
                .ap
                .id
                .as_str()
                .starts_with(&create_base_url("example1.com")));
        });

        // get a page based on prev and next date
        let page2_sub = page_owned_actors(
            &pool,
            Some(&page2_actors.last().unwrap().modified_at),
            Some(&page2_actors.first().unwrap().modified_at),
            10,
            None,
            None,
            None,
        )
        .await;
        let page2_sub_actors = assert_action_ok(page2_sub);
        assert_eq!(page2_sub_actors.len(), 6);
        (0..6).for_each(|n| {
            assert!(page2_actors[n]
                .ap
                .id
                .as_str()
                .starts_with(&create_base_url("example2.com")));
        });
    })
    .await;
}

#[tokio::test]
async fn page_host_owned_actors_test() {
    test_pg::test_pg(move |pool| async move {
        create_many_actors(&pool, "example1.com", 2).await;
        create_many_actors(&pool, "example2.com", 3).await;

        let example1_page =
            page_owned_actors(&pool, None, None, 10, Some("example1.com"), None, None).await;
        let example1_page_actors = assert_action_ok(example1_page);
        assert_eq!(example1_page_actors.len(), 5);

        let example2_page =
            page_owned_actors(&pool, None, None, 10, Some("example2.com"), None, None).await;
        let example2_page_actors = assert_action_ok(example2_page);
        assert_eq!(example2_page_actors.len(), 6);

        let example3_page =
            page_owned_actors(&pool, None, None, 10, Some("example3.com"), None, None).await;
        let example3_page_actor = assert_action_ok(example3_page);
        assert_eq!(example3_page_actor.len(), 0);
    })
    .await;
}

#[tokio::test]
async fn page_banned_owned_actors_test() {
    test_pg::test_pg(move |pool| async move {
        create_many_actors(&pool, "example1.com", 3).await;

        // verify all newly created actors are unbanned
        let unbanned_page = page_owned_actors(&pool, None, None, 10, None, None, None).await;
        let unbanned_page_actors = assert_action_ok(unbanned_page);
        assert_eq!(unbanned_page_actors.len(), 6);

        // go get the first actors and mark banned
        let action =
            fetch_actor_system_info(&pool, &unbanned_page_actors.first().unwrap().ap.id).await;
        test_action!(action);
        let option_asi = action.unwrap();
        assert!(option_asi.is_some());
        let asi = option_asi.unwrap();
        assert_eq!(asi.visibility, ActorVisibility::Visible);
        let mut new_asi: ActorSystemInfo = asi.clone();
        new_asi.visibility = ActorVisibility::Banned;
        let action = update_actor_system_info(&pool, new_asi).await;
        test_action!(action);
        assert!(action.unwrap().is_some());

        // refetch all unbanned and there should be one less
        let unbanned_page = page_owned_actors(&pool, None, None, 10, None, None, None).await;
        let unbanned_page_actors = assert_action_ok(unbanned_page);
        assert_eq!(unbanned_page_actors.len(), 5);

        // get all banned, there should be 1
        let banned_page = page_owned_actors(
            &pool,
            None,
            None,
            10,
            None,
            Some(&ActorVisibility::Banned),
            None,
        )
        .await;
        let banned_page_actors = assert_action_ok(banned_page);
        assert_eq!(banned_page_actors.len(), 1);
    })
    .await;
}

#[tokio::test]
async fn page_owned_actors_by_type_test() {
    test_pg::test_pg(move |pool| async move {
        create_many_actors(&pool, "example5.com", 2).await;

        let person_page =
            page_owned_actors(&pool, None, None, 10, None, None, Some(&ActorType::Person)).await;
        let person_page_actors = assert_action_ok(person_page);
        assert_eq!(person_page_actors.len(), 2);

        let group_page = page_owned_actors(
            &pool,
            None,
            None,
            10,
            Some("example2.com"),
            None,
            Some(&ActorType::Group),
        )
        .await;
        let group_page_actors = assert_action_ok(group_page);
        assert_eq!(group_page_actors.len(), 0);
    })
    .await;
}

async fn create_many_actors(pool: &Pool<Postgres>, host_name: &str, num_actors: i32) {
    create_bare_system_for_host_tst_utl(pool, host_name).await;
    let user_name = "testuser";
    let acct = make_acct(user_name, host_name);
    let action = create_user(pool, &acct, "supersecret").await;
    assert!(action.is_ok());
    for n in 1..=num_actors {
        let pun = format!("testactor{n}");
        create_actor_for_user_tst_utl(pool, &pun, host_name, &acct, false).await;
        // insure 1 millisecond separation
        thread::sleep(Duration::from_millis(1));
    }
}

fn assert_action_ok(action: Result<Option<Vec<OwnedActor>>, DbError>) -> Vec<OwnedActor> {
    test_action!(action);
    match action.unwrap() {
        None => Vec::new(),
        Some(data) => data,
    }
}
