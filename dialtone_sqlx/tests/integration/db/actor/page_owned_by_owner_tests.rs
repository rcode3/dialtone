use dialtone_common::ap::actor::ActorType;
use dialtone_common::rest::actors::actor_model::{ActorSystemInfo, ActorVisibility, OwnedActor};
use dialtone_common::utils::make_acct::make_acct;
use dialtone_sqlx::db::actor::fetch_sysinfo::fetch_actor_system_info;
use dialtone_sqlx::db::actor::page_owned::page_owned_actors;
use dialtone_sqlx::db::actor::page_owned_by_owner::page_owned_actors_by_owner;
use dialtone_sqlx::db::actor::update_sysinfo::update_actor_system_info;
use dialtone_sqlx::db::user::create_user;
use dialtone_sqlx::DbError;
use dialtone_test_util::create_actor::create_actor_for_user_tst_utl;
use dialtone_test_util::create_system::create_bare_system_for_host_tst_utl;
use dialtone_test_util::{test_action, test_pg};
use sqlx::{Pool, Postgres};
use std::{thread, time::Duration};

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_many_owners_WHEN_paged_separately_THEN_not_intermingled() {
    test_pg::test_pg(move |pool| async move {
        let acct1 = create_many_actors(&pool, "example1.com", 2).await;
        let acct2 = create_many_actors(&pool, "example2.com", 3).await;

        //get acct1 actors
        let acct1_page = page_owned_actors_by_owner(&pool, None, None, 10, &acct1, None).await;
        let acct1_page_actors = assert_action_ok(acct1_page);
        assert_eq!(acct1_page_actors.len(), 2);

        //get acct2 actors
        let acct2_page = page_owned_actors_by_owner(&pool, None, None, 10, &acct2, None).await;
        let acct2_page_actors = assert_action_ok(acct2_page);
        assert_eq!(acct2_page_actors.len(), 3);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_many_actors_WHEN_paged_forward_THEN_two_pages() {
    test_pg::test_pg(move |pool| async move {
        let acct = create_many_actors(&pool, "example1.com", 6).await;

        //get page1
        let page1_result = page_owned_actors_by_owner(&pool, None, None, 3, &acct, None).await;
        let page1_actors = assert_action_ok(page1_result);
        println!(
            "page1 actors {:?}",
            page1_actors
                .iter()
                .map(|a| a.ap.id.to_owned())
                .collect::<Vec<String>>()
        );
        assert_eq!(page1_actors.len(), 3);

        //get page2
        let page2_result = page_owned_actors_by_owner(
            &pool,
            None,
            Some(&page1_actors.last().unwrap().modified_at),
            3,
            &acct,
            None,
        )
        .await;
        let page2_actors = assert_action_ok(page2_result);
        println!(
            "page2 actors {:?}",
            page2_actors
                .iter()
                .map(|a| a.ap.id.to_owned())
                .collect::<Vec<String>>()
        );
        assert_eq!(page2_actors.len(), 3);
    })
    .await;
}

#[tokio::test]
async fn page_banned_owned_actors_test() {
    test_pg::test_pg(move |pool| async move {
        let acct = create_many_actors(&pool, "example1.com", 3).await;

        // verify all newly created actors are unbanned
        let unbanned_page = page_owned_actors_by_owner(&pool, None, None, 10, &acct, None).await;
        let unbanned_page_actors = assert_action_ok(unbanned_page);
        assert_eq!(unbanned_page_actors.len(), 3);

        // go get the first actors and mark banned
        let action =
            fetch_actor_system_info(&pool, &unbanned_page_actors.first().unwrap().ap.id).await;
        test_action!(action);
        let option_asi = action.unwrap();
        assert!(option_asi.is_some());
        let asi = option_asi.unwrap();
        assert_eq!(asi.visibility, ActorVisibility::Visible);
        let mut new_asi: ActorSystemInfo = asi.clone();
        new_asi.visibility = ActorVisibility::Banned;
        let action = update_actor_system_info(&pool, new_asi).await;
        test_action!(action);
        assert!(action.unwrap().is_some());

        // refetch all unbanned and there should be one less
        let unbanned_page = page_owned_actors_by_owner(&pool, None, None, 10, &acct, None).await;
        let unbanned_page_actors = assert_action_ok(unbanned_page);
        assert_eq!(unbanned_page_actors.len(), 2);

        // get all banned, there should be 1
        let banned_page = page_owned_actors(
            &pool,
            None,
            None,
            10,
            None,
            Some(&ActorVisibility::Banned),
            None,
        )
        .await;
        let banned_page_actors = assert_action_ok(banned_page);
        assert_eq!(banned_page_actors.len(), 1);
    })
    .await;
}

#[tokio::test]
async fn page_owned_actors_by_type_test() {
    test_pg::test_pg(move |pool| async move {
        let acct = create_many_actors(&pool, "example5.com", 2).await;

        let person_page =
            page_owned_actors_by_owner(&pool, None, None, 10, &acct, Some(&ActorType::Person))
                .await;
        let person_page_actors = assert_action_ok(person_page);
        assert_eq!(person_page_actors.len(), 2);

        let group_page =
            page_owned_actors_by_owner(&pool, None, None, 10, &acct, Some(&ActorType::Group)).await;
        let group_page_actors = assert_action_ok(group_page);
        assert_eq!(group_page_actors.len(), 0);
    })
    .await;
}

async fn create_many_actors(pool: &Pool<Postgres>, host_name: &str, num_actors: i32) -> String {
    create_bare_system_for_host_tst_utl(pool, host_name).await;
    let user_name = "testuser";
    let acct = make_acct(user_name, host_name);
    let action = create_user(pool, &acct, "supersecret").await;
    assert!(action.is_ok());
    for n in 1..=num_actors {
        let pun = format!("testactor{n}");
        let actor = create_actor_for_user_tst_utl(pool, &pun, host_name, &acct, false).await;
        println!(
            "created actor {} with modified time {}",
            actor.owned_actor.ap.id, actor.owned_actor.modified_at
        );
        // insure 1 millisecond separation
        thread::sleep(Duration::from_millis(1));
    }
    acct
}

fn assert_action_ok(action: Result<Option<Vec<OwnedActor>>, DbError>) -> Vec<OwnedActor> {
    if action.is_err() {
        println!("{:?}", action.as_ref().err())
    }
    assert!(action.is_ok());
    match action.unwrap() {
        None => Vec::new(),
        Some(data) => data,
    }
}
