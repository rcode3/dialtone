use dialtone_common::rest::actors::actor_model::{ActorSystemInfo, ActorVisibility};
use dialtone_sqlx::db::actor::fetch_sysinfo::fetch_actor_system_info;
use dialtone_sqlx::db::actor::update_sysinfo::update_actor_system_info;
use dialtone_test_util::create_actor::create_actor_tst_utl;
use dialtone_test_util::create_system::create_bare_system_for_host_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::TEST_HOSTNAME;
use dialtone_test_util::test_pg::test_pg;

#[tokio::test]
async fn update_actor_system_info_test() {
    test_pg(move |pool| async move {
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        let created_actor = create_actor_tst_utl(&pool, "testymctestfase", TEST_HOSTNAME).await;

        let action = fetch_actor_system_info(&pool, &created_actor.owned_actor.ap.id).await;
        test_action!(action);
        let option_asi = action.unwrap();
        assert!(option_asi.is_some());
        let asi = option_asi.unwrap();
        assert_eq!(asi.visibility, ActorVisibility::Visible);

        let mut new_asi: ActorSystemInfo = asi.clone();
        new_asi.visibility = ActorVisibility::Banned;
        let action = update_actor_system_info(&pool, new_asi).await;
        test_action!(action);
        assert!(action.unwrap().is_some());

        let action = fetch_actor_system_info(&pool, &created_actor.owned_actor.ap.id).await;
        test_action!(action);
        let option_asi = action.unwrap();
        assert!(option_asi.is_some());
        let asi = option_asi.unwrap();
        assert_eq!(asi.visibility, ActorVisibility::Banned);
    })
    .await;
}
