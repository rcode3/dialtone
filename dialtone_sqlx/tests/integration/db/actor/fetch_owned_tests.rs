use dialtone_common::rest::actors::actor_model::OwnedActor;
use dialtone_sqlx::db::actor::fetch_owned::{
    fetch_default_owned_actor, fetch_owned_actor_by_acct, fetch_owned_actor_by_id,
};
use dialtone_sqlx::DbError;

use dialtone_test_util::create_actor::create_actor_tst_utl;
use dialtone_test_util::create_system::create_bare_system_for_host_tst_utl;
use dialtone_test_util::test_constants::TEST_HOSTNAME;
use dialtone_test_util::test_pg::test_pg;

#[tokio::test]
async fn fetch_owned_actor_by_id_test() {
    test_pg(move |pool| async move {
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        let created_actor = create_actor_tst_utl(&pool, "testymctestfase", TEST_HOSTNAME).await;

        let action = fetch_owned_actor_by_id(&pool, &created_actor.owned_actor.ap.id).await;
        assert_action_ok(action);
    })
    .await;
}

#[tokio::test]
async fn fetch_owned_actor_by_acct_test() {
    test_pg(move |pool| async move {
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        let created_actor = create_actor_tst_utl(&pool, "testymctestfase", TEST_HOSTNAME).await;

        let webfinger_acct = created_actor.owned_actor.jrd.unwrap().subject;
        let action = fetch_owned_actor_by_acct(&pool, &webfinger_acct).await;
        assert_action_ok(action);
    })
    .await;
}

#[tokio::test]
async fn fetch_default_owned_actor_test() {
    test_pg(move |pool| async move {
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        let created_actor = create_actor_tst_utl(&pool, "testymctestfase", TEST_HOSTNAME).await;

        let user_acct = created_actor.creating_user_acct;
        let action = fetch_default_owned_actor(&pool, &user_acct).await;
        assert_action_ok(action);
    })
    .await;
}

fn assert_action_ok(action: Result<Option<OwnedActor>, DbError>) -> OwnedActor {
    if action.is_err() {
        println!("{:?}", action.as_ref().err())
    }
    assert!(action.is_ok());
    action.unwrap().unwrap()
}
