use dialtone_sqlx::db::actor::insert::{insert_actor, InsertActor};
use dialtone_test_util::create_actor::create_actor_tst_utl;
use dialtone_test_util::create_system::create_bare_system_for_host_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{TEST_HOSTNAME, TEST_NOROLEUSER_NAME};
use dialtone_test_util::test_pg::test_pg;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_actor_is_in_db_WHEN_insert_actor_again_THEN_false() {
    test_pg(move |pool| async move {
        // GIVEN
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        let created_actor = create_actor_tst_utl(&pool, TEST_NOROLEUSER_NAME, TEST_HOSTNAME).await;

        // WHEN
        let insertion = InsertActor::builder()
            .actor_type(dialtone_sqlx::db::actor::ActorDbType::Person)
            .activity_pub(created_actor.owned_actor.ap)
            .visibility(dialtone_sqlx::db::actor::ActorVisibilityType::Visible)
            .build();
        let action = insert_actor(&pool, &insertion).await;

        // THEN
        test_action!(action);
        let result = action.unwrap();
        assert!(!result);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_actor_is_in_db_WHEN_insert_actor_again_with_different_type_THEN_false() {
    test_pg(move |pool| async move {
        // GIVEN
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        let created_actor = create_actor_tst_utl(&pool, TEST_NOROLEUSER_NAME, TEST_HOSTNAME).await;

        // WHEN
        let insertion = InsertActor::builder()
            .actor_type(dialtone_sqlx::db::actor::ActorDbType::Person)
            .activity_pub(created_actor.owned_actor.ap)
            .visibility(dialtone_sqlx::db::actor::ActorVisibilityType::Visible)
            .build();
        let action = insert_actor(&pool, &insertion).await;

        // THEN
        test_action!(action);
        let result = action.unwrap();
        assert!(!result);
    })
    .await;
}
