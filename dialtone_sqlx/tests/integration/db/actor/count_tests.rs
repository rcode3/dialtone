use std::thread;
use std::time::Duration;

use dialtone_common::ap::actor::ActorType;
use dialtone_sqlx::db::actor::count::count_actors_by_host;
use dialtone_sqlx::db::user::create_user;
use dialtone_test_util::create_actor::create_actor_for_user_tst_utl;
use dialtone_test_util::create_system::create_bare_system_for_host_tst_utl;
use dialtone_test_util::test_constants::{TEST_HOSTNAME, TEST_NOROLEUSER_ACCT, TEST_PASSWORD};
use dialtone_test_util::test_pg;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_many_actors_WHEN_get_actor_count_without_type_THEN_count_is_correct() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        create_user(&pool, TEST_NOROLEUSER_ACCT, TEST_PASSWORD)
            .await
            .unwrap();
        for n in 1..=5 {
            let pun = format!("testactor-{n}");
            create_actor_for_user_tst_utl(&pool, &pun, TEST_HOSTNAME, TEST_NOROLEUSER_ACCT, false)
                .await;
            // insure 1 millisecond separation
            thread::sleep(Duration::from_millis(1));
        }

        // WHEN
        let count = count_actors_by_host(&pool, TEST_HOSTNAME, None).await;

        // THEN
        let count = count.unwrap();
        assert_eq!(count, 8);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_many_actors_WHEN_get_actor_count_with_person_type_THEN_count_is_correct() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        create_user(&pool, TEST_NOROLEUSER_ACCT, TEST_PASSWORD)
            .await
            .unwrap();
        for n in 1..=5 {
            let pun = format!("testactor-{n}");
            create_actor_for_user_tst_utl(&pool, &pun, TEST_HOSTNAME, TEST_NOROLEUSER_ACCT, false)
                .await;
            // insure 1 millisecond separation
            thread::sleep(Duration::from_millis(1));
        }

        // WHEN
        let count = count_actors_by_host(&pool, TEST_HOSTNAME, Some(&ActorType::Person)).await;

        // THEN
        let count = count.unwrap();
        assert_eq!(count, 5);
    })
    .await;
}
