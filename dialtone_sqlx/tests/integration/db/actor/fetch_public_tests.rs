use dialtone_common::rest::actors::actor_model::{ActorSystemInfo, ActorVisibility};
use dialtone_sqlx::db::actor::fetch_public::{
    fetch_ap_actor_by_id, fetch_jrd_by_wf_acct, fetch_public_actor_by_id,
    fetch_public_actor_by_wf_acct,
};
use dialtone_sqlx::db::actor::update_sysinfo::update_actor_system_info;
use dialtone_test_util::create_actor::create_actor_tst_utl;
use dialtone_test_util::create_system::create_bare_system_for_host_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{TEST_HOSTNAME, TEST_NOROLEUSER_NAME};
use dialtone_test_util::test_pg::test_pg;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_visible_actor_WHEN_fetch_public_actor_by_id_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        let created_actor = create_actor_tst_utl(&pool, TEST_NOROLEUSER_NAME, TEST_HOSTNAME).await;
        let actor_id = created_actor.owned_actor.ap.id;

        // WHEN
        let action = fetch_public_actor_by_id(&pool, &actor_id).await;

        // THEN
        test_action!(action);
        let action = action.unwrap();
        let actor = action.unwrap();
        assert_eq!(actor.ap.id, actor_id);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_visible_actor_WHEN_fetch_public_actor_by_wf_acct_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        let created_actor = create_actor_tst_utl(&pool, TEST_NOROLEUSER_NAME, TEST_HOSTNAME).await;
        let wf_acct = created_actor.owned_actor.jrd.unwrap().subject;

        // WHEN
        let action = fetch_public_actor_by_wf_acct(&pool, &wf_acct).await;

        // THEN
        test_action!(action);
        let action = action.unwrap();
        let actor = action.unwrap();
        assert_eq!(actor.jrd.unwrap().subject, wf_acct);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_visible_actor_WHEN_fetch_ap_actor_by_id_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        let created_actor = create_actor_tst_utl(&pool, TEST_NOROLEUSER_NAME, TEST_HOSTNAME).await;
        let actor_id = created_actor.owned_actor.ap.id;

        // WHEN
        let action = fetch_ap_actor_by_id(&pool, &actor_id).await;

        // THEN
        test_action!(action);
        let action = action.unwrap();
        let actor = action.unwrap();
        assert_eq!(actor.id, actor_id);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_visible_actor_WHEN_fetch_jrd_by_acct_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        let created_actor = create_actor_tst_utl(&pool, TEST_NOROLEUSER_NAME, TEST_HOSTNAME).await;
        let wf_acct = created_actor.owned_actor.jrd.unwrap().subject;

        // WHEN
        let action = fetch_jrd_by_wf_acct(&pool, &wf_acct).await;

        // THEN
        test_action!(action);
        let action = action.unwrap();
        let jrd = action.unwrap();
        assert_eq!(jrd.subject, wf_acct);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_invisible_actor_WHEN_fetch_actor_by_id_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        let created_actor = create_actor_tst_utl(&pool, TEST_NOROLEUSER_NAME, TEST_HOSTNAME).await;
        let actor_system_info = ActorSystemInfo {
            actor_id: created_actor.owned_actor.ap.id.clone(),
            visibility: ActorVisibility::Invisible,
            system_data: None,
        };
        let action = update_actor_system_info(&pool, actor_system_info).await;
        test_action!(action);

        // WHEN
        let action = fetch_public_actor_by_id(&pool, &created_actor.owned_actor.ap.id).await;

        // THEN
        test_action!(action);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_banned_actor_WHEN_fetch_actor_by_id_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        let created_actor = create_actor_tst_utl(&pool, TEST_NOROLEUSER_NAME, TEST_HOSTNAME).await;
        let actor_system_info = ActorSystemInfo {
            actor_id: created_actor.owned_actor.ap.id.clone(),
            visibility: ActorVisibility::Banned,
            system_data: None,
        };
        let action = update_actor_system_info(&pool, actor_system_info).await;
        test_action!(action);

        // WHEN
        let action = fetch_public_actor_by_id(&pool, &created_actor.owned_actor.ap.id).await;

        // THEN
        test_action!(action);
    })
    .await;
}
