use dialtone_common::{rest::users::web_user::WebUser, utils::make_acct::make_acct};
use dialtone_sqlx::db::user::page::page_users;
use dialtone_sqlx::{control::user::create::create_user, DbError};
use dialtone_test_util::test_pg;
use sqlx::{Pool, Postgres};
use std::{thread, time::Duration};

#[tokio::test]
async fn page_all_users_test() {
    test_pg::test_pg(move |pool| async move {
        create_many_users(&pool, "example1.com", 5).await;
        create_many_users(&pool, "example2.com", 5).await;

        // get first page of 5 users
        let page1 = page_users(&pool, None, None, 5, None).await;
        let page1_users = assert_action_ok(page1);
        assert_eq!(page1_users.len(), 5);
        (0..5).for_each(|n| {
            assert_eq!(
                page1_users[n].user_authz.acct,
                format!("users{}@example1.com", n + 1)
            )
        });

        // get second page of 5 users
        let page2 = page_users(
            &pool,
            None,
            Some(&page1_users.last().unwrap().created_at),
            5,
            None,
        )
        .await;
        let page2_users = assert_action_ok(page2);
        assert_eq!(page2_users.len(), 5);
        (0..5).for_each(|n| {
            assert_eq!(
                page2_users[n].user_authz.acct,
                format!("users{}@example2.com", n + 1)
            )
        });

        // go back to page1 based on prev page
        let page1_back = page_users(
            &pool,
            Some(&page2_users.first().unwrap().created_at),
            None,
            5,
            None,
        )
        .await;
        let page1_back_users = assert_action_ok(page1_back);
        assert_eq!(page1_back_users.len(), 5);
        (0..5).for_each(|n| {
            assert_eq!(
                page1_back_users[n].user_authz.acct,
                format!("users{}@example1.com", n + 1)
            )
        });

        // get a page based on prev and next date
        let page2_sub = page_users(
            &pool,
            Some(&page2_users.last().unwrap().created_at),
            Some(&page2_users.first().unwrap().created_at),
            10,
            None,
        )
        .await;
        let page2_sub_users = assert_action_ok(page2_sub);
        assert_eq!(page2_sub_users.len(), 3);
        (0..3).for_each(|n| {
            assert_eq!(
                page2_sub_users[n].user_authz.acct,
                format!("users{}@example2.com", n + 2)
            )
        });
    })
    .await;
}

#[tokio::test]
async fn page_host_users_test() {
    test_pg::test_pg(move |pool| async move {
        let host = Some("example1.com");
        create_many_users(&pool, "example1.com", 5).await;
        create_many_users(&pool, "example2.com", 5).await;

        // get first page of 5 users
        let page1 = page_users(&pool, None, None, 3, host).await;
        let page1_users = assert_action_ok(page1);
        assert_eq!(page1_users.len(), 3);
        (0..3).for_each(|n| {
            assert_eq!(
                page1_users[n].user_authz.acct,
                format!("users{}@example1.com", n + 1)
            )
        });

        // get second page of users
        let page2 = page_users(
            &pool,
            None,
            Some(&page1_users.last().unwrap().created_at),
            3,
            host,
        )
        .await;
        let page2_users = assert_action_ok(page2);
        assert_eq!(page2_users.len(), 2);
        (0..2).for_each(|n| {
            assert_eq!(
                page2_users[n].user_authz.acct,
                format!("users{}@example1.com", n + 4)
            )
        });

        // go back to page1 based on prev page
        let page1_back = page_users(
            &pool,
            Some(&page2_users.first().unwrap().created_at),
            None,
            3,
            host,
        )
        .await;
        let page1_back_users = assert_action_ok(page1_back);
        assert_eq!(page1_back_users.len(), 3);
        (0..3).for_each(|n| {
            assert_eq!(
                page1_back_users[n].user_authz.acct,
                format!("users{}@example1.com", n + 1)
            )
        });

        // get a page based on prev and next date
        let page1_sub = page_users(
            &pool,
            Some(&page1_users.last().unwrap().created_at),
            Some(&page1_users.first().unwrap().created_at),
            10,
            host,
        )
        .await;
        let page1_sub_users = assert_action_ok(page1_sub);
        assert_eq!(page1_sub_users.len(), 1);
        (0..1).for_each(|n| {
            assert_eq!(
                page1_sub_users[n].user_authz.acct,
                format!("users{}@example1.com", n + 2)
            )
        });
    })
    .await;
}

async fn create_many_users(pool: &Pool<Postgres>, host_name: &str, num_users: i32) {
    let password = "supersecret";
    for n in 1..=num_users {
        let user_acct = make_acct(&format!("users{n}"), host_name);
        let action = create_user(pool, &user_acct, password).await;
        assert!(action.is_ok());
        // insure 1 millisecond separation
        thread::sleep(Duration::from_millis(1));
    }
}

fn assert_action_ok(action: Result<Option<Vec<WebUser>>, DbError>) -> Vec<WebUser> {
    if action.is_err() {
        println!("{:?}", action.as_ref().err())
    }
    assert!(action.is_ok());
    action.unwrap().unwrap()
}
