use dialtone_common::rest::users::web_user::UserStatus;
use dialtone_sqlx::db::user::create_user;
use dialtone_sqlx::db::user::fetch_auth_and_mark_seen::fetch_auth_and_mark_seen;
use dialtone_sqlx::db::user::fetch_info::fetch_user_info;
use sqlx::types::chrono::Utc;

use dialtone_test_util::{test_action, test_pg};

#[tokio::test]
async fn fetch_auth_and_mark_seen_test() {
    test_pg::test_pg(move |pool| async move {
        let acct = "test@example.com";
        let password = "secretpassword";

        create_user(&pool, acct, password).await.unwrap();

        let action = fetch_auth_and_mark_seen(&pool, acct, "127.0.0.1", Utc::now()).await;
        test_action!(action);
        assert_eq!(action.unwrap().unwrap().status, UserStatus::PendingApproval);

        let action = fetch_user_info(&pool, acct).await;
        assert!(action.is_ok());
        let web_user = action.unwrap();
        assert_eq!(1, web_user.unwrap().last_seen_data.len());
    })
    .await;
}
