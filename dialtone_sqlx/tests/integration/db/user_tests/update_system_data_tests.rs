use dialtone_common::rest::users::web_user::UserSystemData;
use dialtone_sqlx::db::user::create_user;
use dialtone_sqlx::db::user::fetch_sysinfo::fetch_user_system_info;
use dialtone_sqlx::db::user::update_system_data::update_user_system_data;
use dialtone_test_util::test_pg;

#[tokio::test]
async fn update_user_system_data_test() {
    test_pg::test_pg(move |pool| async move {
        let acct = "test@example.com";
        let password = "secretpassword";

        create_user(&pool, acct, password).await.unwrap();

        let action = fetch_user_system_info(&pool, acct).await;
        assert!(action.is_ok());
        let web_user = action.unwrap().unwrap();
        assert!(web_user.system_data.is_none());

        let user_system_data = UserSystemData {};
        let action = update_user_system_data(&pool, acct, user_system_data).await;
        assert!(action.is_ok());
        assert!(action.unwrap().is_some());

        let action = fetch_user_system_info(&pool, acct).await;
        assert!(action.is_ok());
        let web_user = action.unwrap().unwrap();
        assert!(web_user.system_data.is_some());
    })
    .await;
}
