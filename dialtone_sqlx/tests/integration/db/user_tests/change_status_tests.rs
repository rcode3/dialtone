use dialtone_common::rest::users::web_user::UserStatus;
use dialtone_sqlx::db::user::change_status::change_user_status;
use dialtone_sqlx::db::user::create_user;
use dialtone_sqlx::db::user::fetch_info::fetch_user_info;

use dialtone_test_util::test_pg;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_new_user_THEN_status_is_pending_WHEN_change_status_THEN_status_is_changed_in_db() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN new user
        let acct = "test@example.com";
        let password = "secretpassword";

        create_user(&pool, acct, password).await.unwrap();

        // THEN status is pending
        let action = fetch_user_info(&pool, acct).await;
        assert!(action.is_ok());
        let webuser = action.unwrap().unwrap();
        assert_eq!(webuser.status, UserStatus::PendingApproval);
        assert_eq!(webuser.event_log.len(), 1);

        // WHEN change status
        let action = change_user_status(&pool, acct, &UserStatus::Active).await;
        println!("{action:?}");
        assert!(action.is_ok());
        assert!(action.unwrap().is_some());

        // THEN status is recorded
        let action = fetch_user_info(&pool, acct).await;
        assert!(action.is_ok());
        let webuser = action.unwrap().unwrap();
        assert_eq!(webuser.status, UserStatus::Active);
        assert_eq!(webuser.event_log.len(), 2);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_new_user_WHEN_status_changed_twice_THEN_event_log_has_three_events() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN new user
        let acct = "test@example.com";
        let password = "secretpassword";
        create_user(&pool, acct, password).await.unwrap();

        // WHEN status changed twice
        let action = change_user_status(&pool, acct, &UserStatus::Active).await;
        println!("{action:?}");
        assert!(action.is_ok());
        assert!(action.unwrap().is_some());

        let action = change_user_status(&pool, acct, &UserStatus::Suspended).await;
        println!("{action:?}");
        assert!(action.is_ok());
        assert!(action.unwrap().is_some());

        // THEN event log has 3 entries
        let action = fetch_user_info(&pool, acct).await;
        assert!(action.is_ok());
        let webuser = action.unwrap().unwrap();
        assert_eq!(webuser.event_log.len(), 3);
    })
    .await;
}
