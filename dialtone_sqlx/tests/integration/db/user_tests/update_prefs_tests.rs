use dialtone_common::rest::sites::theme::Theme;
use dialtone_common::rest::users::web_user::UserPrefs;
use dialtone_sqlx::db::user::create_user;
use dialtone_sqlx::db::user::fetch_info::fetch_user_info;
use dialtone_sqlx::db::user::update_prefs::update_user_prefs;
use dialtone_test_util::test_pg;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_new_user_WHEN_prefs_change_THEN_user_prefs_are_saved_in_database() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN new user
        let acct = "test@example.com";
        let password = "secretpassword";
        create_user(&pool, acct, password).await.unwrap();

        let action = fetch_user_info(&pool, acct).await;
        assert!(action.is_ok());
        let web_user = action.unwrap().unwrap();
        assert!(web_user.preferences.is_none());

        // WHEN prefs change
        let user_prefs = UserPrefs {
            theme: Theme::GreenOnBlack,
        };
        let action = update_user_prefs(&pool, acct, user_prefs).await;
        assert!(action.is_ok());
        assert!(action.unwrap().is_some());

        // THEN user prefs are saved in database
        let action = fetch_user_info(&pool, acct).await;
        assert!(action.is_ok());
        let web_user = action.unwrap().unwrap();
        assert!(web_user.preferences.is_some());
        assert_eq!(web_user.preferences.unwrap().theme, Theme::GreenOnBlack);
        assert_eq!(web_user.event_log.len(), 2);
    })
    .await;
}
