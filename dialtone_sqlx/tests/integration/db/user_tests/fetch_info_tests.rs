use dialtone_sqlx::db::user::create_user;
use dialtone_sqlx::db::user::fetch_info::fetch_user_info;
use dialtone_test_util::test_pg;

#[tokio::test]
async fn fetch_user_info_test() {
    test_pg::test_pg(move |pool| async move {
        let acct = "test@example.com";
        let password = "secretpassword";

        create_user(&pool, acct, password).await.unwrap();

        let action = fetch_user_info(&pool, acct).await;
        assert!(action.is_ok());
    })
    .await;
}
