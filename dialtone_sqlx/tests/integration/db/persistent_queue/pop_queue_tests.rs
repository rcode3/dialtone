use dialtone_sqlx::db::persistent_queue::insert_job::insert_job;
use dialtone_sqlx::db::persistent_queue::pop_queue::pop_queue;
use dialtone_sqlx::db::persistent_queue::JobDbType;
use dialtone_sqlx::logic::persistent_queue::job_details::{JobDetails, NoOpJobDetails};
use dialtone_test_util::{test_action, test_pg};

#[tokio::test]
async fn pop_queue_test() {
    test_pg::test_pg(move |pool| async move {
        let job_details = JobDetails::NoOp(NoOpJobDetails {
            message: "a test".to_string(),
        });
        let action = insert_job(&pool, &JobDbType::NoOperation, &job_details, None, None).await;
        test_action!(action);
        println!("job id = {}", action.unwrap());

        let action = pop_queue(&pool, &JobDbType::NoOperation).await;
        test_action!(action);
        let job = action.unwrap();
        assert!(job.is_some());
        println!("job id = {}", job.unwrap().job_id);
    })
    .await;
}
