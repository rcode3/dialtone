use dialtone_sqlx::db::persistent_queue::insert_job::insert_job;
use dialtone_sqlx::db::persistent_queue::retry::retry_job;
use dialtone_sqlx::db::persistent_queue::JobDbType;
use dialtone_sqlx::logic::persistent_queue::job_details::{JobDetails, NoOpJobDetails};
use dialtone_test_util::{test_action, test_pg};

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_job_with_more_attempts_WHEN_retry_THEN_true() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        let job_details = JobDetails::NoOp(NoOpJobDetails {
            message: "a test".to_string(),
        });
        let action = insert_job(&pool, &JobDbType::NoOperation, &job_details, None, None).await;
        test_action!(action);
        let job = action.unwrap();
        println!("job id = {job}");

        // WHEN
        let action = retry_job(&pool, job, 2).await;

        // THEN
        test_action!(action);
        let result = action.unwrap();
        assert!(result);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_job_with_no_more_attempts_WHEN_retry_THEN_true() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        let job_details = JobDetails::NoOp(NoOpJobDetails {
            message: "a test".to_string(),
        });
        let action = insert_job(&pool, &JobDbType::NoOperation, &job_details, None, None).await;
        test_action!(action);
        let job = action.unwrap();
        println!("job id = {job}");

        // WHEN
        let action = retry_job(&pool, job, 0).await;

        // THEN
        test_action!(action);
        let result = action.unwrap();
        assert!(!result);
    })
    .await;
}
