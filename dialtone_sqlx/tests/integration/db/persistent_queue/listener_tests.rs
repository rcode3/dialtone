use dialtone_sqlx::db::persistent_queue::listener::listener;
use dialtone_sqlx::db::persistent_queue::notifier::notify;
use dialtone_sqlx::db::persistent_queue::JobDbType;
use dialtone_test_util::test_pg;
use std::thread;
use tokio::runtime::Runtime;

#[tokio::test]
async fn listener_test() {
    test_pg::test_pg(move |pool| async move {
        let pg_pool = pool.clone();
        thread::spawn(|| {
            let tr = Runtime::new().unwrap();
            tr.spawn(async move {
                listener(
                    &pg_pool,
                    &JobDbType::NoOperation,
                    move |_pool, notification| async move {
                        println!("got notification {notification:?}");
                    },
                    false,
                )
                .await
                .unwrap();
            });
        });

        let action = notify(&pool, &JobDbType::NoOperation).await;
        if action.is_err() {
            println!("notify failure {:?}", action.as_ref().err());
        }
        assert!(action.is_ok())
    })
    .await;
}
