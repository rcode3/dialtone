use chrono::{DateTime, Utc};
use dialtone_common::ap::ap_object::ApObject;
use dialtone_common::ap::collection::{Collection, CollectionItem};
use dialtone_common::rest::collections::collection_model::CollectionVisibilityType;
use dialtone_sqlx::db::collection::insert::insert_into_collection;
use dialtone_sqlx::db::collection::page::page_by_id;
use dialtone_sqlx::db::collection::page_collection::page_by_actor;
use dialtone_test_util::create_ap_object::create_article_for_actor_tst_utl;
use dialtone_test_util::create_system::{create_system_tst_utl, CreateSystemTestActors};
use dialtone_test_util::{test_action, test_pg};
use sqlx::{Pool, Postgres};

#[tokio::test]
#[allow(non_snake_case)]
#[should_panic]
async fn GIVEN_unknown_collection_id_WHEN_page_no_options_THEN_failure() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        // this just ensures there are collections in the database
        let (_collection, _actors) = get_collection(&pool).await;

        // WHEN
        let action = page_by_id(
            &pool,
            None,
            None,
            10,
            "https://example.com/does_not_exist",
            None,
        )
        .await;

        // THEN
        test_action!(action);
        action.unwrap().unwrap();
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_an_empty_collection_WHEN_page_no_options_THEN_get_empty_collection() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        let (collection, _actors) = get_collection(&pool).await;

        // WHEN
        let action = page_by_id(&pool, None, None, 10, &collection.id, None).await;

        // THEN
        test_action!(action);
        let collection = action.unwrap().unwrap();
        assert_eq!(collection.total_items.unwrap(), 0);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_an_empty_collection_WHEN_page_with_visibility_THEN_get_empty_collection() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        let (collection, _actors) = get_collection(&pool).await;

        // WHEN
        let action = page_by_id(
            &pool,
            None,
            None,
            10,
            &collection.id,
            Some(&CollectionVisibilityType::Visible),
        )
        .await;

        // THEN
        test_action!(action);
        let collection = action.unwrap().unwrap();
        assert_eq!(collection.total_items.unwrap(), 0);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_a_collection_WHEN_page_with_invisibility_THEN_success() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        let (collection, _actors) = get_collection(&pool).await;

        // WHEN
        let action = page_by_id(
            &pool,
            None,
            None,
            10,
            &collection.id,
            Some(&CollectionVisibilityType::Invisible),
        )
        .await;

        // THEN
        test_action!(action);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_collection_with_ap_objects_WHEN_page_no_options_THEN_items_are_in_collection() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        let (collection, actors) = get_collection(&pool).await;
        let mut ap_objects: Vec<ApObject> = Vec::new();
        for n in 0..10 {
            let ap_object = create_article_for_actor_tst_utl(
                &pool,
                &actors.no_role_actor.owned_actor.ap.id,
                format!("test content {n}").as_str(),
            )
            .await;
            insert_into_collection(&pool, &collection.id, ap_object.id.as_ref().unwrap())
                .await
                .unwrap();
            ap_objects.push(ap_object);
        }

        // WHEN
        let action = page_by_id(&pool, None, None, 100, &collection.id, None).await;

        // THEN
        test_action!(action);
        let collection = action.unwrap().unwrap();
        println!("collection is {collection:#?}");
        assert_eq!(collection.total_items.unwrap(), 10);
        assert_eq!(collection.items.len(), 10);
        for ap_object in ap_objects {
            println!("looking for item in collection: {ap_object:#?}");
            assert!(collection
                .items
                .contains(&CollectionItem::ApObject(Box::new(ap_object))));
        }
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_collection_with_actors_and_ap_objects_WHEN_page_no_options_THEN_items_are_in_collection(
) {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        let (collection, actors) = get_collection(&pool).await;
        let mut collection_items: Vec<CollectionItem> = Vec::new();

        insert_into_collection(
            &pool,
            &collection.id,
            &actors.no_role_actor.owned_actor.ap.id,
        )
        .await
        .unwrap();
        collection_items.push(CollectionItem::Actor(Box::new(
            actors.no_role_actor.owned_actor.ap.clone(),
        )));

        insert_into_collection(
            &pool,
            &collection.id,
            &actors.all_role_actor.owned_actor.ap.id,
        )
        .await
        .unwrap();
        collection_items.push(CollectionItem::Actor(Box::new(
            actors.all_role_actor.owned_actor.ap,
        )));

        insert_into_collection(
            &pool,
            &collection.id,
            &actors.user_admin_actor.owned_actor.ap.id,
        )
        .await
        .unwrap();
        collection_items.push(CollectionItem::Actor(Box::new(
            actors.user_admin_actor.owned_actor.ap,
        )));

        insert_into_collection(
            &pool,
            &collection.id,
            &actors.actor_admin_actor.owned_actor.ap.id,
        )
        .await
        .unwrap();
        collection_items.push(CollectionItem::Actor(Box::new(
            actors.actor_admin_actor.owned_actor.ap,
        )));

        for n in 0..10 {
            let ap_object = create_article_for_actor_tst_utl(
                &pool,
                &actors.no_role_actor.owned_actor.ap.id,
                format!("test content {n}").as_str(),
            )
            .await;
            insert_into_collection(&pool, &collection.id, ap_object.id.as_ref().unwrap())
                .await
                .unwrap();
            collection_items.push(CollectionItem::ApObject(Box::new(ap_object)));
        }

        // WHEN
        let action = page_by_id(&pool, None, None, 100, &collection.id, None).await;

        // THEN
        test_action!(action);
        let collection = action.unwrap().unwrap();
        println!("collection is {collection:#?}");
        assert_eq!(collection.total_items.unwrap(), 14);
        assert_eq!(collection.items.len(), 14);
        for item in collection_items {
            println!("looking for item in collection: {item:#?}");
            assert!(collection.items.contains(&item));
        }
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_collection_with_actors_WHEN_page_no_options_THEN_item_count_is_correct() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        let (collection, actors) = get_collection(&pool).await;
        insert_into_collection(
            &pool,
            &collection.id,
            &actors.no_role_actor.owned_actor.ap.id,
        )
        .await
        .unwrap();
        insert_into_collection(
            &pool,
            &collection.id,
            &actors.all_role_actor.owned_actor.ap.id,
        )
        .await
        .unwrap();
        insert_into_collection(
            &pool,
            &collection.id,
            &actors.user_admin_actor.owned_actor.ap.id,
        )
        .await
        .unwrap();
        insert_into_collection(
            &pool,
            &collection.id,
            &actors.actor_admin_actor.owned_actor.ap.id,
        )
        .await
        .unwrap();

        // WHEN
        let action = page_by_id(&pool, None, None, 100, &collection.id, None).await;

        // THEN
        test_action!(action);
        assert_eq!(action.unwrap().unwrap().items.len(), 4);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_collection_with_items_WHEN_page_forward_and_backward_THEN_success() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        let (collection, actors) = get_collection(&pool).await;
        for n in 0..100 {
            let ap_object = create_article_for_actor_tst_utl(
                &pool,
                &actors.no_role_actor.owned_actor.ap.id,
                format!("test content {n}").as_str(),
            )
            .await;
            insert_into_collection(&pool, &collection.id, &ap_object.id.unwrap())
                .await
                .unwrap();
        }

        // WHEN page forward
        let action = page_by_id(&pool, None, None, 10, &collection.id, None).await;

        // THEN get ten items and next
        test_action!(action);
        let collection = action.unwrap().unwrap();
        assert_eq!(collection.total_items.unwrap(), 100);
        assert_eq!(collection.items.len(), 10);
        assert!(collection.next.is_some());

        // WHEN page next
        let next = DateTime::parse_from_rfc3339(&collection.next.unwrap()).unwrap();
        let action = page_by_id(
            &pool,
            None,
            Some(&next.with_timezone(&Utc)),
            10,
            &collection.id,
            None,
        )
        .await;

        // THEN get ten items and next and prev
        test_action!(action);
        let collection = action.unwrap().unwrap();
        assert_eq!(collection.total_items.unwrap(), 100);
        assert_eq!(collection.items.len(), 10);
        assert!(collection.next.is_some());
        assert!(collection.prev.is_some());

        // WHEN page prev
        let prev = DateTime::parse_from_rfc3339(&collection.prev.unwrap()).unwrap();
        let action = page_by_id(
            &pool,
            Some(&prev.with_timezone(&Utc)),
            None,
            10,
            &collection.id,
            None,
        )
        .await;

        // THEN get ten items and next and prev
        test_action!(action);
        let collection = action.unwrap().unwrap();
        assert_eq!(collection.total_items.unwrap(), 100);
        assert_eq!(collection.items.len(), 10);
        assert!(collection.next.is_some());
    })
    .await;
}

async fn get_collection(pool: &Pool<Postgres>) -> (Collection, CreateSystemTestActors) {
    let actors = create_system_tst_utl(pool).await;
    let actor = &actors.no_role_actor;
    let action = page_by_actor(pool, None, None, 10, &actor.owned_actor.ap.id).await;
    (
        action
            .unwrap()
            .unwrap()
            .page_values
            .first()
            .unwrap()
            .to_owned(),
        actors,
    )
}
