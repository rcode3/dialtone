use dialtone_common::ap::id::StandardCollectionNames;
use dialtone_sqlx::db::collection::insert::{
    insert_into_collection, insert_into_related_collections,
};
use dialtone_sqlx::db::collection::page::page_by_id;
use dialtone_test_util::create_ap_object::create_article_for_actor_tst_utl;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_pg;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_two_actors_who_follow_a_third_WHEN_insert_into_related_collection_THEN_item_in_followers_inbox_collection(
) {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        // content_admin and actor_admin following all_role_actor
        let actors = create_system_tst_utl(&pool).await;
        let ap_object = create_article_for_actor_tst_utl(
            &pool,
            &actors.all_role_actor.owned_actor.ap.id,
            "test content",
        )
        .await;
        insert_into_collection(
            &pool,
            &actors.content_admin_actor.owned_actor.ap.following.unwrap(),
            &actors.all_role_actor.owned_actor.ap.id,
        )
        .await
        .unwrap();
        insert_into_collection(
            &pool,
            &actors.actor_admin_actor.owned_actor.ap.following.unwrap(),
            &actors.all_role_actor.owned_actor.ap.id,
        )
        .await
        .unwrap();

        // WHEN
        insert_into_related_collections(
            &pool,
            &actors.all_role_actor.owned_actor.ap.id,
            &StandardCollectionNames::Following.to_string(),
            &StandardCollectionNames::Inbox.to_string(),
            &ap_object.id.unwrap(),
        )
        .await
        .unwrap();

        // THEN
        let page1 = page_by_id(
            &pool,
            None,
            None,
            10,
            &actors.content_admin_actor.owned_actor.ap.inbox.unwrap(),
            None,
        )
        .await
        .unwrap();
        let page2 = page_by_id(
            &pool,
            None,
            None,
            10,
            &actors.actor_admin_actor.owned_actor.ap.inbox.unwrap(),
            None,
        )
        .await
        .unwrap();
        let page1 = page1.unwrap();
        let page2 = page2.unwrap();
        assert_eq!(page1.total_items.unwrap(), 1);
        assert_eq!(page2.total_items.unwrap(), 1);
    })
    .await;
}
