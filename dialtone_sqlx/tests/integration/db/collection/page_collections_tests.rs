use dialtone_common::ap::id::StandardCollectionNames;
use dialtone_sqlx::db::collection::page_collection::page_by_actor;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::{test_action, test_pg};
use strum::IntoEnumIterator;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_new_actor_WHEN_page_collections_THEN_get_four() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let actor = actors.no_role_actor;

        // WHEN
        let action = page_by_actor(&pool, None, None, 20, &actor.owned_actor.ap.id).await;
        test_action!(action);

        // THEN
        let collections = action.unwrap().unwrap();
        assert_eq!(
            collections.page_values.len(),
            StandardCollectionNames::iter().len()
        );
        println!("{collections:?}");
    })
    .await;
}
