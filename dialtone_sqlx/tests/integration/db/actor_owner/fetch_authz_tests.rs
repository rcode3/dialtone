use dialtone_common::ap::actor::ActorType;
use dialtone_sqlx::control::actor::create_owned::create_credentialed_actor;
use dialtone_sqlx::control::user::create::create_user;
use dialtone_sqlx::db::actor_owner::fetch_authz::fetch_actor_owner_authz;
use dialtone_sqlx::logic::actor::new_actor::NewActor;
use dialtone_test_util::{
    create_system::create_bare_system_for_host_tst_utl, test_action, test_pg,
};

#[tokio::test]
async fn fetch_actor_owner_authz_test() {
    test_pg::test_pg(move |pool| async move {
        create_bare_system_for_host_tst_utl(&pool, "example.com").await;
        let acct = "test@example.com";
        let action = create_user(&pool, acct, "secretpassword").await;
        assert!(action.is_ok());

        let new_actor = NewActor::builder()
            .preferred_user_name("test_user")
            .actor_type(ActorType::Person)
            .owner(acct)
            .build();
        let action = create_credentialed_actor(&pool, new_actor, true, "example.com").await;
        test_action!(action);
        let actor = action.unwrap().unwrap();

        let action = fetch_actor_owner_authz(&pool, &actor.owned_actor.ap.id, acct).await;
        test_action!(action);
        let authorized = action.unwrap();
        assert!(authorized);
    })
    .await;
}
