use dialtone_sqlx::control::actor::change_default::change_default_actor;
use dialtone_sqlx::control::user::create_with_actor::create_user_with_default_actor;
use dialtone_sqlx::db::actor::fetch_owned::{fetch_default_owned_actor, fetch_owned_actor_by_id};
use dialtone_sqlx::db::user::fetch_info::fetch_user_info;
use dialtone_test_util::create_actor::create_actor_for_user_tst_utl;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_constants::TEST_HOSTNAME;
use dialtone_test_util::{test_action, test_pg};

#[tokio::test]
#[allow(non_snake_case)]
async fn WHEN_create_user_THEN_default_actor_WHEN_create_actor_THEN_old_default_WHEN_change_default_THEN_new_default(
) {
    test_pg::test_pg(move |pool| async move {
        // WHEN create user with default actor
        // create a users with a default actors
        create_system_tst_utl(&pool).await;
        let action = create_user_with_default_actor(
            &pool,
            "test_user",
            TEST_HOSTNAME,
            "secretpassword",
            &[],
        )
        .await;
        test_action!(action);
        let credentialed_actor = action.unwrap();

        // THEN default actor is recorded
        // verify the default actors
        let action = fetch_default_owned_actor(&pool, &credentialed_actor.creating_user_acct).await;
        test_action!(action);
        assert_eq!(
            action.unwrap().unwrap().ap.id,
            credentialed_actor.owned_actor.ap.id
        );

        // WHEN create actor for user
        // create a new actors for the users
        let action = create_actor_for_user_tst_utl(
            &pool,
            "another_actor",
            TEST_HOSTNAME,
            &credentialed_actor.creating_user_acct,
            false,
        )
        .await;
        let new_actor = action.owned_actor;

        // THEN old default is still active
        // verify the default actors did not change
        let action = fetch_default_owned_actor(&pool, &credentialed_actor.creating_user_acct).await;
        test_action!(action);
        assert_eq!(
            action.unwrap().unwrap().ap.id,
            credentialed_actor.owned_actor.ap.id
        );

        // WHEN change default actor
        // change the default actors
        let action = change_default_actor(
            &pool,
            &credentialed_actor.creating_user_acct,
            &new_actor.ap.id,
        )
        .await;
        test_action!(action);

        // THEN new default actor
        // verify the default actors is now the new actors
        let action = fetch_default_owned_actor(&pool, &credentialed_actor.creating_user_acct).await;
        test_action!(action);
        assert_eq!(action.unwrap().unwrap().ap.id, new_actor.ap.id);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_user_with_default_and_spare_actors_WHEN_change_default_actor_THEN_event_log_is_recorded(
) {
    test_pg::test_pg(move |pool| async move {
        // GIVEN user with default actor and spare actors
        create_system_tst_utl(&pool).await;
        let action = create_user_with_default_actor(
            &pool,
            "test_user",
            TEST_HOSTNAME,
            "secretpassword",
            &[],
        )
        .await;
        test_action!(action);
        let original_actor = action.unwrap();
        let user_acct = original_actor.creating_user_acct;

        let action =
            create_actor_for_user_tst_utl(&pool, "another_actor", TEST_HOSTNAME, &user_acct, false)
                .await;
        let new_actor = action.owned_actor;

        // WHEN change default actor
        let action = change_default_actor(&pool, &user_acct, &new_actor.ap.id).await;
        test_action!(action);

        // THEN event log is recorded
        let actor = fetch_owned_actor_by_id(&pool, &original_actor.owned_actor.ap.id)
            .await
            .unwrap()
            .unwrap();
        println!("original actor event log {:?}", actor.event_log);
        assert_eq!(actor.event_log.len(), 1);

        let actor = fetch_owned_actor_by_id(&pool, &new_actor.ap.id)
            .await
            .unwrap()
            .unwrap();
        println!("new actor event log {:?}", actor.event_log);
        assert_eq!(actor.event_log.len(), 2);

        let user = fetch_user_info(&pool, &user_acct).await.unwrap().unwrap();
        println!("user event log {:?}", user.event_log);
        assert_eq!(user.event_log.len(), 5);
    })
    .await;
}
