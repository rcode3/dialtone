use dialtone_common::ap::ap_object::{ApObjectMediaType, ApObjectType};
use dialtone_common::rest::ap_objects::ap_object_model::ApObjectSystemData;
use dialtone_sqlx::control::actor::update_image::update_actor_with_image_attr_insert;
use dialtone_sqlx::logic::ap_object::new::new_fetched_media_ap_object;

use dialtone_test_util::create_system::create_bare_system_for_host_tst_utl;
use dialtone_test_util::{
    create_actor::create_actor_tst_utl,
    test_action,
    test_constants::{TEST_HOSTNAME, TEST_NOROLEUSER_NAME},
    test_pg,
};

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_actor_WHEN_update_with_image_THEN_success() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        let actor = create_actor_tst_utl(&pool, TEST_NOROLEUSER_NAME, TEST_HOSTNAME).await;

        // WHEN
        let ap_object = new_fetched_media_ap_object(
            TEST_HOSTNAME,
            &actor.owned_actor.ap.id,
            "https://example.com/foo",
            "https://example.net/bar",
            ApObjectType::Image,
            Some(ApObjectMediaType::ImageJpeg),
        );
        let action = update_actor_with_image_attr_insert(
            &pool,
            actor.owned_actor.ap,
            ap_object,
            TEST_HOSTNAME,
            ApObjectSystemData {
                file_data: None,
                http_get_data: None,
                fetched_for_user: None,
            },
        )
        .await;

        // THEN
        test_action!(action);
    })
    .await;
}
