use dialtone_common::ap::actor::{Actor, ActorType, PublicKey};
use dialtone_common::rest::actors::actor_model::PublicActor;
use dialtone_common::utils::media_types::ACTIVITY_STREAMS_PROFILE_PARAMETER;
use dialtone_sqlx::control::actor::create::create_actor;
use dialtone_test_util::{test_action, test_pg};
use serde_json::Value;

#[tokio::test]
#[allow(non_snake_case)]
async fn WHEN_create_actor_THEN_success() {
    test_pg::test_pg(move |pool| async move {
        // WHEN
        let actor = Actor {
            ap_type: ActorType::Person,
            id: "https://example.com/bob".to_string(),
            preferred_user_name: "bobby joe".to_string(),
            name: None,
            summary: None,
            icon: None,
            image: None,
            following: None,
            followers: None,
            liked: None,
            likes: None,
            inbox: None,
            outbox: None,
            public_key: PublicKey {
                id: "https://example.com/bob#key".to_string(),
                owner: "https://example.com/bob".to_string(),
                public_key_pem: "---- BEGIN PUBLIC KEY ----- blah blah blah".to_string(),
            },
            endpoints: None,
            ap_context: Some(Value::String(
                ACTIVITY_STREAMS_PROFILE_PARAMETER.to_string(),
            )),
        };
        let actor_info = PublicActor {
            ap: actor,
            jrd: None,
        };
        let action = create_actor(&pool, actor_info).await;

        // THEN
        test_action!(action);
    })
    .await;
}
