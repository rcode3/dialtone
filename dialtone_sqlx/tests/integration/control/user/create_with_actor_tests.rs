use dialtone_sqlx::control::user::create_with_actor::create_user_with_default_actor;
use dialtone_test_util::{
    create_system::create_bare_system_for_host_tst_utl, test_action, test_pg,
};

#[tokio::test]
#[allow(non_snake_case)]
async fn WHEN_create_user_with_default_actor_THEN_success() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN system
        create_bare_system_for_host_tst_utl(&pool, "example.com").await;

        // WHEN
        let action = create_user_with_default_actor(
            &pool,
            "test_user",
            "example.com",
            "secretpassword",
            &[],
        )
        .await;

        // THEN
        test_action!(action);
    })
    .await;
}
