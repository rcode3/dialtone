use dialtone_common::ap::actor::ActorType;
use dialtone_sqlx::{
    control::{
        actor::create_owned::create_credentialed_actor,
        user::check_name_availability::check_user_name_availability,
    },
    db::user::create_user,
    logic::actor::new_actor::NewActor,
};
use dialtone_test_util::{
    create_system::create_bare_system_for_host_tst_utl, test_action, test_pg,
};

#[tokio::test]
async fn check_user_name_availability_test() {
    test_pg::test_pg(move |pool| async move {
        let action = check_user_name_availability(&pool, "test", "example.com").await;
        assert!(action.is_ok())
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_user_WHEN_check_name_available_THEN_not_available() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        let acct = "test@example.com";
        let action = create_user(&pool, acct, "secretpassword").await;
        assert!(action.is_ok());

        // WHEN
        let action = check_user_name_availability(&pool, "test", "example.com").await;

        // THEN
        assert!(action.is_ok());
        let result = action.unwrap();
        assert!(!result);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_actor_WHEN_check_name_available_THEN_not_available() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        create_bare_system_for_host_tst_utl(&pool, "example.com").await;
        let acct = "test@example.com";
        let action = create_user(&pool, acct, "secretpassword").await;
        assert!(action.is_ok());

        let new_actor = NewActor::builder()
            .preferred_user_name("test_user")
            .actor_type(ActorType::Person)
            .owner(acct)
            .build();
        let action = create_credentialed_actor(&pool, new_actor, true, "example.com").await;
        test_action!(action);

        // WHEN
        let action = check_user_name_availability(&pool, "test_user", "example.com").await;

        // THEN
        assert!(action.is_ok());
        let result = action.unwrap();
        assert!(!result);
    })
    .await;
}
