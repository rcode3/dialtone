use dialtone_common::rest::users::web_user::UserStatus;
use dialtone_sqlx::control::user::change_auth::change_user_auth;
use dialtone_sqlx::db::user::change_status::change_user_status;
use dialtone_sqlx::db::user::create_user;
use dialtone_sqlx::{control::user::authn::authn_user, db::user::fetch_info::fetch_user_info};
use dialtone_test_util::{test_action, test_pg};
use sqlx::types::chrono::Utc;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_new_user_WHEN_auth_with_current_data_THEN_sucess_WHEN_auth_data_changes_THEN_next_auth_must_use_new_data(
) {
    test_pg::test_pg(move |pool| async move {
        // GIVE new user
        let acct = "test@example.com";
        let password = "secretpassword";
        let new_password = "new_password";
        create_user(&pool, acct, password).await.unwrap();

        // Users must be active to authenticate
        change_user_status(&pool, acct, &UserStatus::Active)
            .await
            .unwrap();

        // WHEN auth with current data
        let action = authn_user(&pool, acct, password, "127.0.0.1", Utc::now())
            .await
            .unwrap();

        // THEN success
        assert!(action);

        // WHEN change auth data
        let action = change_user_auth(&pool, acct, new_password).await;
        test_action!(action);
        assert!(action.unwrap());

        // THEN next auth must use new data
        let action = authn_user(&pool, acct, new_password, "127.0.0.1", Utc::now())
            .await
            .unwrap();
        assert!(action);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_non_existent_user_WHEN_auth_THEN_no_success() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN non existent user
        let acct = "test@example.com";
        let password = "secretpassword";

        // WHEN auth
        let action = change_user_auth(&pool, acct, password).await;
        test_action!(action);

        // THEN no success
        assert!(!action.unwrap());
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_new_user_WHEN_change_auth_twice_THEN_three_event_logs() {
    test_pg::test_pg(move |pool| async move {
        // GIVE new user
        let acct = "test@example.com";
        let password = "secretpassword";
        let new_password = "new_password";
        create_user(&pool, acct, password).await.unwrap();

        // WHEN change auth twice
        let action = change_user_auth(&pool, acct, new_password).await;
        test_action!(action);
        assert!(action.unwrap());
        let action = change_user_auth(&pool, acct, new_password).await;
        test_action!(action);
        assert!(action.unwrap());

        // THEN three event logs
        let action = fetch_user_info(&pool, acct).await;
        assert!(action.is_ok());
        let webuser = action.unwrap().unwrap();
        assert_eq!(webuser.event_log.len(), 3);
    })
    .await;
}
