use dialtone_sqlx::control::user::create::create_user;
use dialtone_test_util::test_pg;

#[tokio::test]
async fn create_user_test() {
    test_pg::test_pg(move |pool| async move {
        let action = create_user(&pool, "test@example.com", "secretpassword").await;
        assert!(action.is_ok())
    })
    .await;
}
