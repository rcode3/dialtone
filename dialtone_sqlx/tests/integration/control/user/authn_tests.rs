use dialtone_common::rest::users::web_user::UserStatus;
use dialtone_sqlx::control::user::authn::authn_user;
use dialtone_sqlx::control::user::create::create_user;
use dialtone_sqlx::db::user::change_status::change_user_status;
use dialtone_sqlx::db::user::fetch_info::fetch_user_info;
use dialtone_test_util::test_pg;
use sqlx::types::chrono::Utc;

#[tokio::test]
async fn authn_user_test() {
    test_pg::test_pg(move |pool| async move {
        let acct = "test@example.com";
        let password = "secretpassword";
        create_user(&pool, acct, password).await.unwrap();

        change_user_status(&pool, acct, &UserStatus::Active)
            .await
            .unwrap();

        let action = authn_user(&pool, acct, password, "127.0.0.1", Utc::now())
            .await
            .unwrap();
        assert!(action);

        let web_user = fetch_user_info(&pool, acct).await.unwrap().unwrap();
        assert_eq!(1, web_user.last_login_data.len());

        let action = authn_user(&pool, acct, "wrongpassword", "127.0.0.1", Utc::now())
            .await
            .unwrap();
        assert!(!action);

        let web_user = fetch_user_info(&pool, acct).await.unwrap().unwrap();
        assert_eq!(2, web_user.last_login_data.len());
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_non_existent_user_WHEN_authn_THEN_return_false() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        let acct = "test@example.com";
        let password = "secretpassword";

        // WHEN
        let action = authn_user(&pool, acct, password, "127.0.0.1", Utc::now())
            .await
            .unwrap();

        // THEN
        assert!(!action);
    })
    .await;
}
