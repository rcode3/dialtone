use dialtone_common::rest::ap_objects::ap_object_model::UpdateOwnedApObject;
use dialtone_sqlx::{
    control::ap_object::update::update_owned_ap_object,
    db::ap_object::fetch_owned::fetch_owned_ap_object,
};
use dialtone_test_util::{
    create_actor::create_actor_tst_utl,
    create_ap_object::create_article_for_actor_tst_utl,
    create_system::create_bare_system_for_host_tst_utl,
    test_action,
    test_constants::{TEST_HOSTNAME, TEST_NOROLEUSER_NAME},
    test_pg,
};

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_ap_object_WHEN_updated_THEN_fetch_shows_update() {
    test_pg::test_pg(move |pool| async move {
        // GIVEN
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        let actor = create_actor_tst_utl(&pool, TEST_NOROLEUSER_NAME, TEST_HOSTNAME).await;
        let ap_object =
            create_article_for_actor_tst_utl(&pool, &actor.owned_actor.ap.id, "foo").await;
        let ap_object_id = ap_object.id.unwrap().to_owned();

        // WHEN
        let update = UpdateOwnedApObject {
            name: Some("new_name".to_string()),
            content: Some("new_content".to_string()),
            summary: None,
            owner_data: None,
        };
        let action = update_owned_ap_object(&pool, &ap_object_id, update).await;
        test_action!(action);

        // THEN
        let fetched = fetch_owned_ap_object(&pool, &ap_object_id).await;
        test_action!(fetched);
        assert!(&fetched.as_ref().unwrap().is_some());
        assert!(&fetched
            .as_ref()
            .unwrap()
            .as_ref()
            .unwrap()
            .ap
            .content
            .is_some());
        assert_eq!(
            fetched
                .as_ref()
                .unwrap()
                .as_ref()
                .unwrap()
                .ap
                .content
                .as_ref()
                .unwrap()
                .as_ref(),
            "new_content".to_string()
        );
    })
    .await;
}
