use dialtone_common::ap::ap_object::ApObjectType;
use dialtone_common::rest::ap_objects::ap_object_model::CreateOwnedApObject;
use dialtone_sqlx::control::ap_object::create_owned::create_owned_ap_object;
use dialtone_test_util::create_actor::create_actor_tst_utl;
use dialtone_test_util::create_system::create_bare_system_for_host_tst_utl;
use dialtone_test_util::{test_action, test_pg};

#[tokio::test]
async fn create_owned_ap_object_test() {
    test_pg::test_pg(move |pool| async move {
        let host_name = "example.com";
        create_bare_system_for_host_tst_utl(&pool, host_name).await;
        let user_name = "foo";
        let actor = create_actor_tst_utl(&pool, user_name, host_name).await;
        let create_ap_object = CreateOwnedApObject {
            name: None,
            media_type: None,
            ap_type: ApObjectType::Article,
            content: Some("<p>this is a post</p>".to_string()),
            summary: None,
            owner_data: None,
            to: None,
            cc: None,
            bto: None,
            bcc: None,
        };
        let action = create_owned_ap_object(
            &pool,
            host_name,
            user_name,
            &actor.owned_actor.ap.id,
            &create_ap_object,
        )
        .await;
        test_action!(action);
        println!("ap_objects = {:?}", action.unwrap());
    })
    .await;
}
