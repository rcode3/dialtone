use dialtone_common::ap::ap_object::{ApObject, ApObjectType};
use dialtone_sqlx::control::ap_object::create::create_ap_object;
use dialtone_test_util::create_site::create_site_tst_utl;
use dialtone_test_util::{test_action, test_pg};

#[tokio::test]
async fn create_ap_object_test() {
    test_pg::test_pg(move |pool| async move {
        let host_name = "example.net";
        create_site_tst_utl(&pool, host_name).await;
        let ap_object = ApObject::builder()
            .id("https://example.net/pub/bar/this_is_a_post")
            .ap_type(ApObjectType::Article)
            .content("<p>this is a post</p>")
            .build();
        let action = create_ap_object(&pool, &ap_object, host_name).await;
        test_action!(action);
        println!("ap_objects id = {}", action.unwrap());
    })
    .await;
}
