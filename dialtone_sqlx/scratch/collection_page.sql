select
  item_count as sort_col,
  null,
  null,
  activity_pub_json
from
  (
    select
      count(*) as item_count
    from
      collection_item
    where
      collection_item.id = 'https://localhost/collection/_system_icons*s/none'
  ) as collection_item_count,
  collection
where
  collection.id = 'https://localhost/collection/_system_icons*s/none'
  and visibility = 'Visible'
union
select
  -1 as sort_col,
  min(item_modified_at),
  max(item_modified_at),
  jsonb_agg(item_json)
from
  (
    (
      select
        collection_item.modified_at as item_modified_at,
        actor.activity_pub_json as item_json
      from
        collection_item,
        actor
      where
        collection_item.id = 'https://localhost/collection/_system_icons*s/none'
        and actor.id = collection_item.object_ap_id
        and actor.visibility = 'Visible'
      order by
        item_modified_at asc
    )
    union
    all (
      select
        collection_item.modified_at as item_modified_at,
        ap_object.activity_pub_json as item_json
      from
        collection_item,
        ap_object
      where
        collection_item.id = 'https://localhost/collection/_system_icons*s/none'
        and ap_object.id = collection_item.object_ap_id
        and ap_object.visibility = 'Visible'
      order by
        item_modified_at asc
    )
    union
    all (
      select
        collection_item.modified_at as item_modified_at,
        collection.activity_pub_json as item_json
      from
        collection_item,
        collection
      where
        collection_item.id = 'https://localhost/collection/_system_icons*s/none'
        and collection.id = collection_item.object_ap_id
        and collection.visibility = 'Visible'
      order by
        item_modified_at asc
    )
    limit
      10
  ) as collection,
  collection
where
  collection.id = 'https://localhost/collection/_system_icons*s/none'
  and visibility = 'Visible'
group by
  collection.id
order by sort_col desc