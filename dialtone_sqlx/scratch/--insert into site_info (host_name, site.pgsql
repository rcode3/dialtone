--insert into site_info (host_name, site_data) values ('example.com', '{}'::jsonb);
--insert into user (acct, auth_data, last_seen_data, last_login_data, status) values ('user@example.com', '{}'::jsonb, '[]'::jsonb, '[]'::jsonb, 'Active');
--insert into system_role (role_type, member, host_name) values ('System Operator', 'user@example.com', 'example.com');
insert into system_role (role_type, member, host_name) values ('System Moderator', 'user@example.com', 'example.com');
select * from user where acct = 'user@example.com';