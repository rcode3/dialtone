pub(crate) mod commands;
pub(crate) mod row_obj;

use anyhow::Context;
use clap::Command;
use commands::{
    account::{account_command, do_account, ACCOUNT},
    actor::{actor_command, do_actor, ACTOR},
    create::{create_command, do_create, CREATE},
    register::{do_register, register_command, REGISTER},
    site::{do_site, site_command, SITE},
};
use dialtone_ccli_util::config::{dirs::init, DialtoneConfig};
use dialtone_common::utils::version::DT_VERSION;
use simplelog::*;

#[tokio::main]
async fn main() {
    init(&[]).unwrap();
    TermLogger::init(
        LevelFilter::Info,
        Config::default(),
        TerminalMode::Stderr,
        ColorChoice::Auto,
    )
    .unwrap();
    let config = DialtoneConfig::from_config_dir();

    let mut command = Command::new("dialtone")
        .about("Dialtone command line client.")
        .version(DT_VERSION)
        .propagate_version(true)
        .subcommand(account_command())
        .subcommand(site_command())
        .subcommand(register_command())
        .subcommand(create_command())
        .subcommand(actor_command());

    let matches = command.clone().get_matches();

    let action = match matches.subcommand() {
        Some((SITE, sub_matches)) => do_site(config, sub_matches).await,
        Some((ACCOUNT, sub_matches)) => do_account(config, sub_matches).await,
        Some((REGISTER, sub_matches)) => do_register(sub_matches).await,
        Some((CREATE, sub_matches)) => do_create(config, sub_matches).await,
        Some((ACTOR, sub_matchers)) => do_actor(config, sub_matchers).await,
        _ => command.print_help().with_context(|| "unable to write help"),
    };

    if let Err(err) = action {
        error!("dialtone command execution ended in error: {err}");

        #[cfg(debug_assertions)]
        panic!("{err:?}");
    }
}
