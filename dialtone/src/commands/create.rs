use clap::{ArgMatches, Command};
use dialtone_ccli_util::config::DialtoneConfig;
use dialtone_ccli_util::input::actor_summary::actor_summary_arg;
use dialtone_ccli_util::input::actor_summary::fake_actor_summary_arg;
use dialtone_ccli_util::input::actor_summary::get_actor_summary;
use dialtone_ccli_util::input::actor_summary::get_fake_actor_summary;
use dialtone_ccli_util::input::actor_type::actor_type_arg;
use dialtone_ccli_util::input::actor_type::get_actor_type;
use dialtone_ccli_util::input::display_name::display_name_arg;
use dialtone_ccli_util::input::display_name::fake_display_name_arg;
use dialtone_ccli_util::input::display_name::get_display_name;
use dialtone_ccli_util::input::display_name::get_fake_display_name;
use dialtone_ccli_util::input::json::get_json_arg;
use dialtone_ccli_util::input::json::json_arg;
use dialtone_ccli_util::input::json::JsonOutput;
use dialtone_ccli_util::input::pun::get_pun;
use dialtone_ccli_util::input::pun::pun_arg;
use dialtone_ccli_util::input::user_acct::get_user_acct_with_default;
use dialtone_ccli_util::input::user_acct::user_acct_arg;
use dialtone_ccli_util::sc::get_sc;
use dialtone_ccli_util::sc::load_sc;
use dialtone_common::ap::actor::ActorType;
use dialtone_common::rest::actors::actor_exchanges::NewActorRequest;
use dialtone_reqwest::api_dt::actors::create::create_actor;
use dialtone_reqwest::api_dt::names::check_name_available::check_name_available;
use simplelog::*;

pub(crate) const CREATE: &str = "create";

pub(crate) fn create_command() -> Command<'static> {
    Command::new(CREATE)
        .about("Create actors")
        .arg(json_arg())
        .arg(user_acct_arg())
        .arg(actor_type_arg())
        .arg(pun_arg())
        .arg(display_name_arg())
        .arg(actor_summary_arg())
        .arg(fake_display_name_arg())
        .arg(fake_actor_summary_arg())
}

pub(crate) async fn do_create(
    config: DialtoneConfig,
    sub_matches: &ArgMatches,
) -> anyhow::Result<()> {
    let json_output = get_json_arg(sub_matches);
    let user_account = get_user_acct_with_default(sub_matches, config.default_acct);
    let sc = get_sc(&user_account.host_name);

    let pun = get_pun(sub_matches);
    info!("Checking if '{pun}' is available.");
    let name_available = check_name_available(&sc, &pun).await?;
    if !name_available {
        error!("'{pun}' is not available.");
        return Ok(());
    }

    let sc = load_sc(&user_account.to_string()).map_err(|_| {
        anyhow::anyhow!(format!(
            "Authenticaiton required. Login as '{user_account}' to continue."
        ))
    })?;

    let actor_type = if let Some(actor_type) = get_actor_type(sub_matches)? {
        actor_type
    } else {
        ActorType::Person
    };

    let actor_name = get_display_name(sub_matches);
    let generate_fake_name = get_fake_display_name(sub_matches);
    if actor_name.is_some() && generate_fake_name {
        error!("'name' and 'fake-name' options may not be used together.");
        return Ok(());
    }
    let actor_name = if generate_fake_name {
        let rng = rnglib::RNG::new(&rnglib::Language::Roman).unwrap();
        let first_name = rng.generate_name();
        let last_name = rng.generate_name();
        Some(format!("{first_name} {last_name}"))
    } else {
        actor_name
    };

    let actor_summary = get_actor_summary(sub_matches);
    let generate_fake_summary = get_fake_actor_summary(sub_matches);
    if actor_summary.is_some() && generate_fake_summary {
        error!("'summary' and 'fake-summary' options may not be used together.");
        return Ok(());
    }
    let actor_summary = if generate_fake_summary {
        Some(lipsum::lipsum_words(25))
    } else {
        actor_summary
    };

    let request = NewActorRequest {
        preferred_user_name: pun.to_owned(),
        actor_type,
        name: actor_name,
        summary: actor_summary,
    };
    info!("Creating '{pun}'");
    let action = create_actor(&sc, &request).await?;
    match json_output {
        JsonOutput::OneLine => {
            let s = serde_json::to_string(&action);
            println!("{}", s.unwrap());
        }
        JsonOutput::Pretty => {
            let s = serde_json::to_string_pretty(&action);
            println!("{}", s.unwrap());
        }
        JsonOutput::NoJson => {
            info!("Actor '{pun}' created.");
        }
    }
    Ok(())
}
