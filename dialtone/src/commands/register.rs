use clap::{ArgMatches, Command};
use dialtone_ccli_util::{
    input::{
        code::{code_arg, get_code},
        host_name::{get_host_name, host_name_arg},
        password::{get_password, password_arg},
        user_name::{get_user_name, user_name_arg},
    },
    sc::get_sc,
};
use dialtone_common::{
    rest::{
        sites::site_data::RegistrationMethod,
        users::{
            user_exchanges::{PostUser, PostUserResponse},
            user_login::UserCredential,
        },
    },
    utils::make_acct::make_acct,
};
use dialtone_reqwest::{
    api_dt::{
        names::check_name_available::check_name_available, sites::get_site_info::get_site_info,
        users::register_user::register_user,
    },
    site_connection::SiteConnection,
};
use simplelog::*;

pub(crate) const REGISTER: &str = "register";

pub(crate) fn register_command() -> Command<'static> {
    Command::new(REGISTER)
        .about("Register user account on a site")
        .arg(host_name_arg())
        .arg(user_name_arg())
        .arg(password_arg())
        .arg(code_arg())
}

pub(crate) async fn do_register(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches).unwrap();
    let sc = get_sc(&host_name);

    info!("Getting list of registrations methods from {host_name}");
    let site_info = get_site_info(&sc).await?;
    if site_info.registration_methods.is_empty() {
        error!("Account registration is closed for {host_name}");
    } else if site_info
        .registration_methods
        .contains(&RegistrationMethod::Open)
    {
        do_register_open(&sc, sub_matches).await?;
    } else if site_info
        .registration_methods
        .contains(&RegistrationMethod::SimpleCode)
    {
        do_register_simple(&sc, sub_matches).await?;
    } else {
        error!("No recognized registration methods available.");
    }

    Ok(())
}

async fn do_register_open(sc: &SiteConnection, sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let check_name_result = check_name(sc, sub_matches).await?;
    if check_name_result.available {
        let user_acct = make_acct(&check_name_result.user_name, sc.host_name.as_ref().unwrap());
        let post_user = PostUser::OpenRegistration(UserCredential {
            user_acct,
            password: check_name_result.password.to_string(),
        });
        handle_registration(sc, post_user).await?;
    }
    Ok(())
}

async fn do_register_simple(sc: &SiteConnection, sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let check_name_result = check_name(sc, sub_matches).await?;
    if check_name_result.available {
        let code = get_code(sub_matches);
        let user_acct = make_acct(&check_name_result.user_name, sc.host_name.as_ref().unwrap());
        let post_user = PostUser::SimpleCode {
            user_creds: UserCredential {
                user_acct,
                password: check_name_result.password.to_string(),
            },
            simple_code_for_registration: code,
        };
        handle_registration(sc, post_user).await?;
    }
    Ok(())
}

struct CheckNameResult {
    pub user_name: String,
    pub password: String,
    pub available: bool,
}

async fn check_name(
    sc: &SiteConnection,
    sub_matches: &ArgMatches,
) -> anyhow::Result<CheckNameResult> {
    let user_name = get_user_name(sub_matches);
    let available = check_name_available(sc, &user_name).await?;
    if !available {
        error!("{user_name} is not available for registration");
    }
    let password = get_password(sub_matches);
    Ok(CheckNameResult {
        user_name,
        password,
        available,
    })
}

async fn handle_registration(sc: &SiteConnection, post_user: PostUser) -> anyhow::Result<()> {
    let registration_result = register_user(sc, &post_user).await?;
    match registration_result {
        PostUserResponse::AccountRegistered(_) => info!("Account registred."),
        PostUserResponse::AccountNotRegistered(_) => error!("Account registration failed."),
        PostUserResponse::AccountNeedsApproval(_) => {
            info!("Account registration awaiting approval.")
        }
    }
    Ok(())
}
