use anyhow::Context;
use clap::{ArgMatches, Command};
use dialtone_ccli_util::{
    config::DialtoneConfig,
    input::{
        host_name::{get_host_name_with_default, host_name_arg},
        json::{get_json_arg, json_arg, JsonOutput},
    },
    output::site_info::{basic_public_info, registration_methods},
    sc::get_sc,
};
use dialtone_reqwest::api_dt::sites::get_site_info::get_site_info;
use simplelog::info;

pub(crate) const SITE: &str = "site";

const INFO: &str = "info";

pub(crate) fn site_command() -> Command<'static> {
    Command::new(SITE)
        .about("Commands to interrogate sites")
        .subcommand(
            Command::new(INFO)
                .about("Get site info")
                .arg(host_name_arg())
                .arg(json_arg()),
        )
}

pub(crate) async fn do_site(
    config: DialtoneConfig,
    sub_matches: &ArgMatches,
) -> anyhow::Result<()> {
    match sub_matches.subcommand() {
        Some((INFO, sub_matches)) => do_info(config, sub_matches).await,
        _ => site_command()
            .print_help()
            .with_context(|| "Unable to pring help"),
    }
}

async fn do_info(config: DialtoneConfig, sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name_with_default(sub_matches, config.default_host_name())?;
    let json_output = get_json_arg(sub_matches);

    info!("Getting site info for <b>{host_name}</b>");

    let sc = get_sc(&host_name);
    let site_info = get_site_info(&sc).await?;

    match json_output {
        JsonOutput::OneLine => {
            let s = serde_json::to_string(&site_info);
            println!("{}", s.unwrap());
        }
        JsonOutput::Pretty => {
            let s = serde_json::to_string_pretty(&site_info);
            println!("{}", s.unwrap());
        }
        JsonOutput::NoJson => {
            basic_public_info(&host_name, &site_info);
            registration_methods(&site_info);
        }
    }
    Ok(())
}
