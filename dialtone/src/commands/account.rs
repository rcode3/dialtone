use anyhow::Context;
use clap::{ArgMatches, Command};
use dialtone_ccli_util::config::DialtoneConfig;
use dialtone_ccli_util::input::json::{get_json_arg, json_arg, JsonOutput};
use dialtone_ccli_util::input::password::{get_password, password_arg};
use dialtone_ccli_util::input::user_acct::{get_user_acct_with_default, user_acct_arg};
use dialtone_ccli_util::sc::remove_sc;
use dialtone_ccli_util::{config::dirs::create_user_dir, sc::get_sc, sc::save_sc};
use dialtone_reqwest::api_dt::users::authenticate::authenticate;
use simplelog::*;

pub(crate) const ACCOUNT: &str = "account";

const LOGIN: &str = "login";
const LOGOUT: &str = "logout";

pub(crate) fn account_command() -> Command<'static> {
    Command::new(ACCOUNT)
        .about("Manage user accounts")
        .subcommand(
            Command::new(LOGIN)
                .about("Login into a site")
                .arg(user_acct_arg())
                .arg(password_arg())
                .arg(json_arg())
                .after_help("The default account will be used if none is provided."),
        )
        .subcommand(
            Command::new(LOGOUT)
                .about("Logout of a site")
                .arg(user_acct_arg())
                .after_help("The default account will be used if none is provided."),
        )
}

pub(crate) async fn do_account(
    config: DialtoneConfig,
    sub_matches: &ArgMatches,
) -> anyhow::Result<()> {
    match sub_matches.subcommand() {
        Some((LOGIN, sub_matches)) => do_login(config, sub_matches).await,
        Some((LOGOUT, sub_matches)) => do_logout(config, sub_matches),
        _ => account_command()
            .print_help()
            .with_context(|| "Unable to pring help"),
    }
}

pub async fn do_login(config: DialtoneConfig, sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let user_acct = get_user_acct_with_default(sub_matches, config.default_acct);
    let password = get_password(sub_matches);
    let json_output = get_json_arg(sub_matches);

    let acct = user_acct.to_string();

    let sc = get_sc(&user_acct.host_name);
    info!(
        "Authenticating to {} as {}",
        user_acct.host_name, user_acct.user_name
    );
    let login_result = authenticate(&sc, &user_acct.user_name, &password).await;
    match login_result {
        Ok(sc) => {
            create_user_dir(&acct).expect("error creating user directory");
            save_sc(&acct, &sc).expect("error saving site connection");
            match json_output {
                JsonOutput::OneLine => {
                    let s = serde_json::to_string(&sc);
                    println!("{}", s.unwrap());
                }
                JsonOutput::Pretty => {
                    let s = serde_json::to_string_pretty(&sc);
                    println!("{}", s.unwrap());
                }
                JsonOutput::NoJson => {
                    info!(
                        "Logged in to {} as {}",
                        user_acct.host_name, user_acct.user_name
                    );
                }
            }
            Ok(())
        }
        Err(err) => {
            let msg = format!("Login error: {err}");
            Err(anyhow::anyhow!(msg))
        }
    }
}

pub fn do_logout(config: DialtoneConfig, sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let user_acct = get_user_acct_with_default(sub_matches, config.default_acct);
    let _ = remove_sc(&user_acct.to_string());
    info!("Logged out of {}", user_acct.host_name);
    Ok(())
}
