use clap::{ArgMatches, Command};
use dialtone_ccli_util::{
    config::DialtoneConfig,
    input::{
        actor_ref::{actor_ref_arg, get_actor_ref},
        json::{get_json_arg, json_arg, JsonOutput},
        user_acct::{get_user_acct_with_default, user_acct_arg},
    },
    output::actor::actor_base_info,
    sc::load_sc,
};
use dialtone_common::rest::actors::actor_exchanges::GetActorRequest;
use dialtone_reqwest::api_dt::actors::get_actor::get_public_actor;

pub(crate) const ACTOR: &str = "actor";

pub(crate) fn actor_command() -> Command<'static> {
    Command::new(ACTOR)
        .about("Lookup an actor")
        .arg(json_arg())
        .arg(user_acct_arg())
        .arg(actor_ref_arg())
}

pub(crate) async fn do_actor(
    config: DialtoneConfig,
    sub_matches: &ArgMatches,
) -> anyhow::Result<()> {
    let json_output = get_json_arg(sub_matches);
    let user_account = get_user_acct_with_default(sub_matches, config.default_acct);

    let actor_ref = get_actor_ref(sub_matches, Some(&user_account.host_name))?;

    let sc = load_sc(&user_account.to_string()).map_err(|_| {
        anyhow::anyhow!(format!(
            "Authenticaiton required. Login as '{user_account}' to continue."
        ))
    })?;

    let public_actor = get_public_actor(&sc, &GetActorRequest::new(actor_ref)).await?;
    match json_output {
        JsonOutput::OneLine => {
            let s = serde_json::to_string(&public_actor);
            println!("{}", s.unwrap());
        }
        JsonOutput::Pretty => {
            let s = serde_json::to_string_pretty(&public_actor);
            println!("{}", s.unwrap());
        }
        JsonOutput::NoJson => {
            actor_base_info(&public_actor);
        }
    }

    Ok(())
}
