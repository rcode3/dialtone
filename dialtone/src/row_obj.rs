use tabled::Tabled;

#[derive(Tabled)]
pub struct RowObj {
    pub name: String,
    pub value: String,
}
