use assert_cmd::Command;
use dialtone_common::rest::actors::actor_model::OwnedActor;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_constants::*;
use dialtone_test_util::test_pg::test_pg_with_env;

use crate::util::{create_config_with_default_acct, start_test_server_with_env};

#[tokio::test(flavor = "multi_thread")]
#[allow(non_snake_case)]
async fn GIVEN_system_WHEN_create_actor_THEN_parsable() {
    test_pg_with_env(move |pool, env| async move {
        // GIVEN
        create_system_tst_utl(&pool).await;
        let env =
            start_test_server_with_env(pool, env, Some((TEST_NOROLEUSER_ACCT, TEST_PASSWORD)))
                .await;

        // WHEN
        let mut cmd = Command::cargo_bin("dialtone").unwrap();
        let assert = cmd
            .env_clear()
            .envs(env)
            .args([
                "create",
                "-a",
                TEST_NOROLEUSER_ACCT,
                "-P",
                "test_actor",
                "-J",
                "one-line",
            ])
            .assert();

        // THEN
        let output = assert.get_output();
        let _: OwnedActor = serde_json::from_slice(&output.stdout).unwrap();
        assert.success();
    })
    .await;
}

#[tokio::test(flavor = "multi_thread")]
#[allow(non_snake_case)]
async fn GIVEN_system_WHEN_create_group_THEN_parsable() {
    test_pg_with_env(move |pool, env| async move {
        // GIVEN
        create_system_tst_utl(&pool).await;
        let env =
            start_test_server_with_env(pool, env, Some((TEST_NOROLEUSER_ACCT, TEST_PASSWORD)))
                .await;

        // WHEN
        let mut cmd = Command::cargo_bin("dialtone").unwrap();
        let assert = cmd
            .env_clear()
            .envs(env)
            .args([
                "create",
                "-a",
                TEST_NOROLEUSER_ACCT,
                "-P",
                "test_group",
                "-t",
                "Group",
                "-J",
                "one-line",
            ])
            .assert();

        // THEN
        let output = assert.get_output();
        let _: OwnedActor = serde_json::from_slice(&output.stdout).unwrap();
        assert.success();
    })
    .await;
}

#[tokio::test(flavor = "multi_thread")]
#[allow(non_snake_case)]
async fn GIVEN_system_WHEN_create_actor_with_default_acct_THEN_parsable() {
    test_pg_with_env(move |pool, env| async move {
        // GIVEN
        create_config_with_default_acct(TEST_NOROLEUSER_ACCT, &env);
        create_system_tst_utl(&pool).await;
        let env =
            start_test_server_with_env(pool, env, Some((TEST_NOROLEUSER_ACCT, TEST_PASSWORD)))
                .await;

        // WHEN
        let mut cmd = Command::cargo_bin("dialtone").unwrap();
        let assert = cmd
            .env_clear()
            .envs(env)
            .args(["create", "-P", "test_actor", "-J", "one-line"])
            .assert();

        // THEN
        let output = assert.get_output();
        println!("{}", String::from_utf8_lossy(&output.stderr));
        println!("{}", String::from_utf8_lossy(&output.stdout));
        let _: OwnedActor = serde_json::from_slice(&output.stdout).unwrap();
        assert.success();
    })
    .await;
}
