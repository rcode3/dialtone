use assert_cmd::Command;
use dialtone_ccli_util::config::{
    dirs::{config_path, PROJECT_DIRS},
    DialtoneConfig, DIALTONE_CONFIG_FILE,
};
use dialtone_reqwest::site_connection::SiteConnection;
use dialtone_test_util::test_server::start_test_server;
use sqlx::{Pool, Postgres};
use std::{collections::HashMap, ffi::OsStr, fs};

pub fn auth_cmd<I, K, V>(user_acct: &str, password: &str, env: I)
where
    I: IntoIterator<Item = (K, V)>,
    K: AsRef<OsStr>,
    V: AsRef<OsStr>,
{
    let mut cmd = Command::cargo_bin("dialtone").unwrap();
    let assert = cmd
        .env_clear()
        .envs(env)
        .args([
            "account", "login", "-a", user_acct, "-p", password, "-J", "one-line",
        ])
        .assert();
    let output = assert.get_output();
    let _sc: SiteConnection = serde_json::from_slice(&output.stdout).unwrap();
    assert.success();
}

/// Starts a test server with the given environment.
///
/// * `pool` - connection pool to Postgres
/// * `env` - the environment to use
/// * `creds` - an optional tuple of account and password to authenticate as
///
/// Returns the passed in HashMap with the "TEST_PORT" variable set.
pub async fn start_test_server_with_env(
    pool: Pool<Postgres>,
    mut env: HashMap<String, String>,
    creds: Option<(&str, &str)>,
) -> HashMap<String, String> {
    let addr = start_test_server(pool).await;
    env.insert("TEST_PORT".to_string(), addr.port().to_string());
    if let Some(user_creds) = creds {
        auth_cmd(user_creds.0, user_creds.1, &env);
    };
    env
}

pub fn create_config_with_default_acct(acct: &str, env: &HashMap<String, String>) {
    env.iter()
        .filter(|(k, _v)| k.starts_with("XDG"))
        .for_each(|(k, v)| std::env::set_var(k, v));
    let dialtone_config = DialtoneConfig {
        default_acct: Some(acct.to_owned()),
    };
    fs::create_dir_all(PROJECT_DIRS.config_dir()).unwrap();
    let mut f = fs::File::create(config_path(DIALTONE_CONFIG_FILE)).unwrap();
    std::io::Write::write_all(&mut f, dialtone_config.to_toml_string().as_bytes()).unwrap();
    f.sync_all().unwrap();
}
