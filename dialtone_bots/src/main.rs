mod commands;
mod config;

use clap::Command;
use commands::{list::do_list, run::do_run};
use config::{BotsConfig, BOTS_CONFIG_FILE};
use dialtone_ccli_util::config::dirs::init;
use dialtone_common::utils::version::DT_VERSION;
use simplelog::{ColorChoice, Config, LevelFilter, TermLogger, TerminalMode};

#[allow(unused_variables)]
fn main() {
    init(&[(BOTS_CONFIG_FILE, &BotsConfig::default().to_toml_string())]).unwrap();
    TermLogger::init(
        LevelFilter::Info,
        Config::default(),
        TerminalMode::Stdout,
        ColorChoice::Never,
    )
    .unwrap();
    let config = BotsConfig::from_config_dir();

    let matches = Command::new("dialtone")
        .about("Dialtone bots.")
        .version(DT_VERSION)
        .propagate_version(true)
        .subcommand_required(true)
        .arg_required_else_help(true)
        .subcommand(
            Command::new("run")
                .about("Run bots")
                .arg_required_else_help(true),
        )
        .subcommand(Command::new("list").about("List botsx"))
        .get_matches();

    match matches.subcommand() {
        Some(("run", sub_matches)) => do_run(),
        Some(("list", sub_matches)) => do_list(),
        _ => unreachable!("Exhausted list of subcommands and subcommand_required prevents `None`"),
    }
}
