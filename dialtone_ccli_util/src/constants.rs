use lazy_static::lazy_static;
use regex::Regex;

lazy_static! {
    pub static ref HOST_NAME_RE: Regex =
        Regex::new(r"(^localhost$|^[a-z0-9-]*([\.a-z0-9-])*$)").unwrap();
}

#[cfg(test)]
mod constants_tests {
    use super::HOST_NAME_RE;

    #[test]
    fn host_name_re_test() {
        assert!(HOST_NAME_RE.is_match("test.example"));
        assert!(HOST_NAME_RE.is_match("test-foo.example"));
        assert!(HOST_NAME_RE.is_match("localhost"));
        assert!(!HOST_NAME_RE.is_match("test_foo.example"));
        assert!(HOST_NAME_RE.is_match("dt-dev.snark.fail"));
    }
}
