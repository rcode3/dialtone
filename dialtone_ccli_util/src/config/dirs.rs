use std::fs;
use std::path::PathBuf;

use directories::ProjectDirs;
use lazy_static::lazy_static;

use super::{DialtoneConfig, DIALTONE_CONFIG_FILE};

pub const QUALIFIER: &str = "dev";
pub const ORGANIZATION: &str = "dialtone";
pub const APPLICATION: &str = "dialtone";

const USERS: &str = "users";
const ACTORS: &str = "actors";

lazy_static! {
    pub static ref PROJECT_DIRS: ProjectDirs =
        ProjectDirs::from(QUALIFIER, ORGANIZATION, APPLICATION).unwrap();
    pub static ref USERS_PATH: PathBuf = {
        let mut path = PathBuf::new();
        path.push(PROJECT_DIRS.data_dir());
        path.push(USERS);
        path
    };
    pub static ref ACTORS_PATH: PathBuf = {
        let mut path = PathBuf::new();
        path.push(PROJECT_DIRS.data_dir());
        path.push(ACTORS);
        path
    };
}

/// Initializes the configuration directory.
///
/// # Arguments
///
/// * `default_config_files` - an array of tuples, where each tuple is a file name
/// and the default value of the file.
pub fn init(default_config_files: &[(&str, &str)]) -> std::io::Result<()> {
    fs::create_dir_all(PROJECT_DIRS.config_dir())?;
    fs::create_dir_all(PROJECT_DIRS.data_dir())?;
    fs::create_dir_all(USERS_PATH.as_path())?;
    fs::create_dir_all(ACTORS_PATH.as_path())?;
    for config_set in default_config_files {
        let path = config_path(config_set.0);
        if !path.exists() {
            fs::write(path, config_set.1).unwrap();
        }
    }
    let path = config_path(DIALTONE_CONFIG_FILE);
    if !path.exists() {
        fs::write(path, DialtoneConfig::default().to_toml_string()).unwrap();
    }
    Ok(())
}

pub fn config_path(config_file: &str) -> PathBuf {
    let mut path = PathBuf::new();
    path.push(PROJECT_DIRS.config_dir());
    path.push(config_file);
    path
}

pub fn create_user_dir(acct: &str) -> std::io::Result<()> {
    let path = get_user_path(acct);
    fs::create_dir_all(path.as_path())?;
    Ok(())
}

pub fn get_user_path(acct: &str) -> PathBuf {
    let mut path = PathBuf::new();
    path.push(USERS_PATH.as_path());
    path.push(acct);
    path
}
