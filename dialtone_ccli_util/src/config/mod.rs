use self::dirs::config_path;
use dialtone_common::utils::parse_acct::parse_user_acct;
use serde::{Deserialize, Serialize};
use std::fs;

pub mod dirs;

pub const DIALTONE_CONFIG_FILE: &str = "dialtone.toml";

#[derive(Serialize, Deserialize, Default, Debug)]
pub struct DialtoneConfig {
    pub default_acct: Option<String>,
}

impl DialtoneConfig {
    pub fn from_config_dir() -> Self {
        let path = config_path(DIALTONE_CONFIG_FILE);
        let contents: String = fs::read_to_string(path).unwrap();
        toml::from_str(&contents).unwrap()
    }

    pub fn to_toml_string(&self) -> String {
        toml::to_string_pretty(self).unwrap()
    }

    pub fn default_host_name(&self) -> Option<String> {
        if let Some(default_acct) = &self.default_acct {
            let name_host = parse_user_acct(default_acct).unwrap();
            Some(name_host.host_name)
        } else {
            None
        }
    }
}
