use clap::{Arg, ArgMatches};
use dialtone_common::ap::actor::ActorType;
use std::str::FromStr;
use strum::VariantNames;

use super::{get_optional_arg, long_args, short_args};

pub const ACTOR_TYPE: &str = "actor-type";

pub fn actor_type_arg() -> Arg<'static> {
    Arg::new(ACTOR_TYPE)
        .help("One of Person, Organization, Group, Application or Service")
        .long(long_args::ACTOR_TYPE)
        .short(short_args::ACTOR_TYPE)
        .required(false)
        .takes_value(true)
        .value_parser(validate_actor_type)
}

fn validate_actor_type(actor_type: &str) -> Result<String, String> {
    if ActorType::VARIANTS.contains(&actor_type) {
        Ok(actor_type.to_owned())
    } else {
        Err(format!("{actor_type} is not a valid ActorType."))
    }
}

pub fn get_actor_type(sub_matches: &ArgMatches) -> anyhow::Result<Option<ActorType>> {
    let value = get_optional_arg::<String>(ACTOR_TYPE, sub_matches);
    if let Some(value) = value {
        Ok(Some(ActorType::from_str(value)?))
    } else {
        Ok(None)
    }
}
