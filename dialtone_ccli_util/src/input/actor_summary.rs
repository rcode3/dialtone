use clap::{Arg, ArgAction, ArgMatches};

use super::{get_arg_as_bool, get_optional_arg, long_args, short_args};

pub const ACTOR_SUMMARY: &str = "actor-summary";
pub const FAKE_ACTOR_SUMMARY: &str = "fake-actor-summary";

pub fn actor_summary_arg() -> Arg<'static> {
    Arg::new(ACTOR_SUMMARY)
        .long(long_args::ACTOR_SUMMARY)
        .short(short_args::ACTOR_SUMMARY)
        .help("Actor display name.")
        .required(false)
        .takes_value(true)
        .value_parser(clap::builder::NonEmptyStringValueParser::new())
}

pub fn get_actor_summary(sub_matches: &ArgMatches) -> Option<String> {
    get_optional_arg::<String>(ACTOR_SUMMARY, sub_matches).map(|s| s.to_owned())
}

pub fn fake_actor_summary_arg() -> Arg<'static> {
    Arg::new(FAKE_ACTOR_SUMMARY)
        .long(long_args::FAKE_ACTOR_SUMMARY)
        .help("Generate a fake actor summary.")
        .required(false)
        .takes_value(true)
        .action(ArgAction::SetTrue)
}

pub fn get_fake_actor_summary(sub_matches: &ArgMatches) -> bool {
    get_arg_as_bool(FAKE_ACTOR_SUMMARY, sub_matches)
}
