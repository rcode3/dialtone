use anyhow::anyhow;
use clap::{Arg, ArgMatches};
use dialtone_common::{
    ap::{id::is_generic_actor_id_valid, pun::is_valid_pun},
    webfinger::is_valid_wf_acct,
};

use super::{get_arg_or_input_as_string, long_args, short_args};

pub const ACTOR_REF: &str = "ACTOR_REF";

pub fn actor_ref_arg() -> Arg<'static> {
    Arg::new(ACTOR_REF)
        .long(long_args::ACTOR_REFERENCE)
        .short(short_args::ACTOR_REFERENCE)
        .help("actor id, webfinger account, or preferred user name (PUN)")
        .required(false)
        .takes_value(true)
        .value_parser(validate_actor_ref)
}

pub fn get_actor_ref(sub_matches: &ArgMatches, host_name: Option<&str>) -> anyhow::Result<String> {
    let actor_ref =
        get_arg_or_input_as_string(ACTOR_REF, "Actor ID, Webfinger Acct, or PUN?", sub_matches);
    if let Some(wf_acct) = is_valid_wf_acct(&actor_ref) {
        Ok(wf_acct)
    } else if is_generic_actor_id_valid(&actor_ref) {
        Ok(actor_ref)
    } else if is_valid_pun(&actor_ref, true) {
        if let Some(host_name) = host_name {
            Ok(format!("acct:{actor_ref}@{host_name}"))
        } else {
            Err(anyhow!(format!("'{actor_ref}' does not appear to be a valid actor ID, Webfinger account, or preferred user name")))
        }
    } else {
        Err(anyhow!(format!("'{actor_ref}' does not appear to be a valid actor ID, Webfinger account, or preferred user name")))
    }
}

fn validate_actor_ref(actor_ref: &str) -> Result<String, String> {
    if let Some(wf_acct) = is_valid_wf_acct(actor_ref) {
        Ok(wf_acct)
    } else if is_generic_actor_id_valid(actor_ref) || is_valid_pun(actor_ref, true) {
        Ok(actor_ref.to_string())
    } else {
        Err(format!("'{actor_ref}' does not appear to be a valid actor ID, Webfinger account, or preferred user name"))
    }
}
