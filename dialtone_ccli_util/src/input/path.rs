use std::path::PathBuf;

use clap::{Arg, ArgMatches};

use super::{get_optional_arg, long_args, short_args};

pub const PATH: &str = "PATH";

pub fn path_arg(help: &'static str) -> Arg<'static> {
    Arg::new(PATH)
        .long(long_args::PATH)
        .short(short_args::PATH)
        .help(help)
        .required(false)
        .takes_value(true)
}

pub fn get_pathbuf_arg(sub_matches: &ArgMatches) -> Option<PathBuf> {
    let path_string = get_optional_arg::<String>(PATH, sub_matches);
    if let Some(path_string) = path_string {
        let mut path_buf = PathBuf::new();
        path_buf.push(path_string);
        Some(path_buf)
    } else {
        None
    }
}
