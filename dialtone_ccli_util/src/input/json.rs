use clap::{Arg, ArgMatches, ValueSource};

use super::{get_optional_arg, long_args, short_args};

pub const JSON: &str = "JSON";

const PRETTY: &str = "pretty";
const ONE_LINE: &str = "one-line";

pub fn json_arg() -> Arg<'static> {
    Arg::new(JSON)
        .long(long_args::JSON)
        .short(short_args::JSON)
        .help("json output")
        .value_parser([PRETTY, ONE_LINE])
        .default_value(PRETTY)
        .required(false)
}

pub enum JsonOutput {
    OneLine,
    Pretty,
    NoJson,
}

pub fn get_json_arg(sub_matches: &ArgMatches) -> JsonOutput {
    let value_source = sub_matches.value_source(JSON).unwrap();
    if let ValueSource::CommandLine = value_source {
        let arg_value = get_optional_arg::<String>(JSON, sub_matches).unwrap();
        match arg_value.as_str() {
            PRETTY => JsonOutput::Pretty,
            ONE_LINE => JsonOutput::OneLine,
            _ => panic!("cannot determine JSON output"),
        }
    } else {
        JsonOutput::NoJson
    }
}
