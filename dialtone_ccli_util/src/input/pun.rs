use clap::{Arg, ArgMatches};
use dialtone_common::ap::pun::is_valid_pun;

use super::{get_arg_or_input_as_string, long_args, short_args};

pub const PUN: &str = "PUN";

pub fn pun_arg() -> Arg<'static> {
    Arg::new(PUN)
        .long(long_args::PUN)
        .short(short_args::PUN)
        .help("Preferred user name")
        .required(false)
        .takes_value(true)
        .value_parser(validate_pun)
        .long_help(
            "A Preferred User Name (PUN) must be one word and cannot contain 
the characters '*', '!', '&', '\\', '(', ')', '+' ',', ';', or '=' ",
        )
}

pub fn get_pun(sub_matches: &ArgMatches) -> String {
    get_arg_or_input_as_string(PUN, "Preferred User Name of actor?", sub_matches)
}

fn validate_pun(pun: &str) -> Result<String, String> {
    if is_valid_pun(pun, false) {
        Ok(pun.to_owned())
    } else {
        Err(format!("'{pun}' is not a valid Preferred User Name (PUN)."))
    }
}
