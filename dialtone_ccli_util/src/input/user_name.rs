use clap::{Arg, ArgMatches};
use dialtone_common::ap::pun::is_valid_pun;

use super::{get_arg_or_input_as_string, long_args, short_args};

pub const USER_NAME: &str = "USER_NAME";

pub fn user_name_arg() -> Arg<'static> {
    Arg::new(USER_NAME)
        .long(long_args::USER_NAME)
        .short(short_args::USER_NAME)
        .help("Name of user (the part before the @ symbol)")
        .required(false)
        .takes_value(true)
        .value_parser(validate_user_name)
}

pub fn get_user_name(sub_matches: &ArgMatches) -> String {
    get_arg_or_input_as_string(USER_NAME, "Name of user?", sub_matches)
}

pub fn get_sysop_name(sub_matches: &ArgMatches) -> anyhow::Result<String> {
    let user_name = get_arg_or_input_as_string(USER_NAME, "Name of SYSOP user?", sub_matches);
    match validate_user_name(&user_name) {
        Ok(user_name) => Ok(user_name),
        Err(err) => Err(anyhow::anyhow!(err)),
    }
}

fn validate_user_name(user_name: &str) -> Result<String, String> {
    if is_valid_pun(user_name, false) {
        Ok(user_name.to_owned())
    } else {
        Err(format!("'{user_name}' is not a valid user name."))
    }
}
