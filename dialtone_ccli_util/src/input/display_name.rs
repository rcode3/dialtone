use clap::{Arg, ArgAction, ArgMatches};

use super::{get_arg_as_bool, get_optional_arg, long_args, short_args};

pub const DISPLAY_NAME: &str = "display-name";
pub const FAKE_DISPLAY_NAME: &str = "fake-display-name";

pub fn display_name_arg() -> Arg<'static> {
    Arg::new(DISPLAY_NAME)
        .long(long_args::DISPLAY_NAME)
        .short(short_args::DISPLAY_NAME)
        .help("Actor display name.")
        .required(false)
        .takes_value(true)
        .value_parser(clap::builder::NonEmptyStringValueParser::new())
}

pub fn get_display_name(sub_matches: &ArgMatches) -> Option<String> {
    get_optional_arg::<String>(DISPLAY_NAME, sub_matches).map(|s| s.to_owned())
}

pub fn fake_display_name_arg() -> Arg<'static> {
    Arg::new(FAKE_DISPLAY_NAME)
        .long(long_args::FAKE_DISPLAY_NAME)
        .help("Generate a fake actor display name.")
        .required(false)
        .takes_value(true)
        .action(ArgAction::SetTrue)
}

pub fn get_fake_display_name(sub_matches: &ArgMatches) -> bool {
    get_arg_as_bool(FAKE_DISPLAY_NAME, sub_matches)
}
