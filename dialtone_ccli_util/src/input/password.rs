use clap::{Arg, ArgMatches};

use super::{get_arg_or_input_as_password, long_args, short_args};

pub const PASSWORD: &str = "PASSWORD";

pub fn password_arg() -> Arg<'static> {
    Arg::new(PASSWORD)
        .long(long_args::PASSWORD)
        .short(short_args::PASSWORD)
        .help("password of user")
        .required(false)
        .takes_value(true)
        .value_parser(clap::builder::NonEmptyStringValueParser::new())
}

pub fn get_password(sub_matches: &ArgMatches) -> String {
    get_arg_or_input_as_password(PASSWORD, "Password of user?", sub_matches)
}

pub fn get_sysop_password(sub_matches: &ArgMatches) -> String {
    get_arg_or_input_as_password(PASSWORD, "Password of SYSOP user?", sub_matches)
}
