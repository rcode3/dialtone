use crate::constants::HOST_NAME_RE;
use clap::{Arg, ArgMatches};

use super::{
    get_arg_or_input_as_string, get_arg_or_input_as_string_with_default, long_args, short_args,
};

pub const HOST_NAME: &str = "HOST_NAME";

pub fn host_name_arg() -> Arg<'static> {
    Arg::new(HOST_NAME)
        .long(long_args::HOST_NAME)
        .short(short_args::HOST_NAME)
        .help("host name of site")
        .required(false)
        .takes_value(true)
        .value_parser(validate_host_name)
}

pub fn get_host_name(sub_matches: &ArgMatches) -> anyhow::Result<String> {
    let host_name = get_arg_or_input_as_string(HOST_NAME, "Host name of site?", sub_matches);
    match validate_host_name(&host_name) {
        Ok(host_name) => Ok(host_name),
        Err(err) => Err(anyhow::anyhow!(err)),
    }
}

pub fn get_host_name_with_default(
    sub_matches: &ArgMatches,
    default: Option<String>,
) -> anyhow::Result<String> {
    let host_name = get_arg_or_input_as_string_with_default(
        HOST_NAME,
        "Host name of site?",
        sub_matches,
        default,
    );
    match validate_host_name(&host_name) {
        Ok(host_name) => Ok(host_name),
        Err(err) => Err(anyhow::anyhow!(err)),
    }
}

fn validate_host_name(host_name: &str) -> Result<String, String> {
    if HOST_NAME_RE.is_match(host_name) {
        Ok(host_name.to_owned())
    } else {
        Err(format!("'{host_name}' is not a valid host name."))
    }
}
