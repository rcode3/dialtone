use clap::{Arg, ArgMatches};

use super::{get_arg_or_input_as_bool, long_args, short_args};

pub const YES: &str = "YES";

pub fn yes_arg(help: &'static str) -> Arg<'static> {
    Arg::new(YES)
        .long(long_args::YES)
        .short(short_args::YES)
        .help(help)
        .required(false)
        .takes_value(false)
}

pub fn get_yes_with_prompt(sub_matches: &ArgMatches, prompt: &str) -> bool {
    get_arg_or_input_as_bool(YES, prompt, sub_matches)
}
