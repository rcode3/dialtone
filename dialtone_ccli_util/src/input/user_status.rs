use std::str::FromStr;

use clap::{Arg, ArgMatches};
use dialtone_common::rest::users::web_user::UserStatus;
use strum::VariantNames;

use super::{get_arg_or_input_as_string, long_args, short_args};

pub const USER_STATUS: &str = "user-status";

pub fn user_status_arg() -> Arg<'static> {
    Arg::new(USER_STATUS)
        .help("One of Active, Suspended, or Pending Approval")
        .long(long_args::USER_STATUS)
        .short(short_args::USER_STATUS)
        .required(false)
        .takes_value(true)
        .value_parser(validate_user_status)
}

fn validate_user_status(status: &str) -> Result<String, String> {
    if UserStatus::VARIANTS.contains(&status) {
        Ok(status.to_owned())
    } else {
        Err(format!("{status} is not a valid UserStatus."))
    }
}

pub fn get_user_status(sub_matches: &ArgMatches) -> anyhow::Result<UserStatus> {
    let status_string = get_arg_or_input_as_string(USER_STATUS, "User status?", sub_matches);
    if let Ok(user_status) = UserStatus::from_str(&status_string) {
        Ok(user_status)
    } else {
        Err(anyhow::anyhow!(
            "{status_string} is not a valid User Status."
        ))
    }
}
