use clap::{Arg, ArgMatches};

use super::{get_arg_or_input_as_bool, long_args, short_args};

pub const AUTO_APPROVE: &str = "auto-approve";

pub fn auto_approve_arg() -> Arg<'static> {
    Arg::new(AUTO_APPROVE)
        .long(long_args::AUTO_APPROVE)
        .short(short_args::AUTO_APPROVE)
        .help("Set accounts to be automatically approved.")
        .required(false)
        .takes_value(true)
}

pub fn get_auto_approve(sub_matches: &ArgMatches) -> bool {
    get_arg_or_input_as_bool(
        AUTO_APPROVE,
        "Automatically approve registrations?",
        sub_matches,
    )
}
