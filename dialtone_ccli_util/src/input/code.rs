use clap::{Arg, ArgMatches};

use super::{get_arg_or_input_as_string, long_args, short_args};

pub const CODE: &str = "code";

pub fn code_arg() -> Arg<'static> {
    Arg::new(CODE)
        .long(long_args::CODE)
        .short(short_args::CODE)
        .help("A registration code.")
        .required(false)
        .takes_value(true)
        .value_parser(clap::builder::NonEmptyStringValueParser::new())
}

pub fn get_code(sub_matches: &ArgMatches) -> String {
    get_arg_or_input_as_string(CODE, "Registration code?", sub_matches)
}
