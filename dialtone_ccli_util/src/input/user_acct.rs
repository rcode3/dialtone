use clap::{Arg, ArgMatches};
use dialtone_common::{rest::users::user_login::NameHostPair, utils::parse_acct::parse_user_acct};

use super::{get_arg_or_input_as_string_with_default, long_args, short_args};

pub const USER_ACCT: &str = "USER_ACCT";

pub fn user_acct_arg() -> Arg<'static> {
    Arg::new(USER_ACCT)
        .long(long_args::USER_ACCOUNT)
        .short(short_args::USER_ACCOUNT)
        .help("User account (e.g. foo@example.com)")
        .required(false)
        .takes_value(true)
        .value_parser(validate_user_acct)
}

pub fn get_user_acct_with_default(
    sub_matches: &ArgMatches,
    default: Option<String>,
) -> NameHostPair {
    let acct =
        get_arg_or_input_as_string_with_default(USER_ACCT, "User account?", sub_matches, default);
    parse_user_acct(&acct).unwrap()
}

fn validate_user_acct(user_acct: &str) -> Result<String, String> {
    let validation = parse_user_acct(user_acct);
    match validation {
        Ok(_) => Ok(user_acct.to_string()),
        Err(_) => Err(format!("Invalid user account: {user_acct}")),
    }
}
