use clap::{Arg, ArgMatches};

use super::{get_arg_or_input_as_string, long_args, short_args};

pub const LONG_SITE_NAME: &str = "LONG_SITE_NAME";
pub const SHORT_SITE_NAME: &str = "SHORT_SITE_NAME";

pub fn long_site_name_arg() -> Arg<'static> {
    Arg::new(LONG_SITE_NAME)
        .long(long_args::LONG_SITE_NAME)
        .short(short_args::LONG_SITE_NAME)
        .help("long name of site")
        .required(false)
        .takes_value(true)
        .value_parser(clap::builder::NonEmptyStringValueParser::new())
}

pub fn get_long_site_name(sub_matches: &ArgMatches) -> String {
    get_arg_or_input_as_string(LONG_SITE_NAME, "Long name of site?", sub_matches)
}

pub fn short_site_name_arg() -> Arg<'static> {
    Arg::new(SHORT_SITE_NAME)
        .long(long_args::SHORT_SITE_NAME)
        .short(short_args::SHORT_SITE_NAME)
        .help("short name of site")
        .required(false)
        .takes_value(true)
}

pub fn get_short_site_name(sub_matches: &ArgMatches) -> String {
    get_arg_or_input_as_string(SHORT_SITE_NAME, "Short name of site?", sub_matches)
}
