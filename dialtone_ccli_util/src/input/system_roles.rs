use std::str::FromStr;

use clap::{Arg, ArgMatches, ValueSource};
use dialtone_common::rest::users::web_user::SystemRoleType;
use requestty::Question;
use strum::VariantNames;

use super::{get_arg_or_input_as_string, long_args, short_args};

pub const SYSTEM_ROLES: &str = "system-roles";
pub const ROLE: &str = "role";

pub fn system_roles_arg() -> Arg<'static> {
    Arg::new(SYSTEM_ROLES)
        .long(long_args::SYSTEM_ROLES)
        .short(short_args::SYSTEM_ROLES)
        .help("Name of system roles set.")
        .required(false)
        .takes_value(true)
        .value_parser(clap::builder::NonEmptyStringValueParser::new())
}

pub fn get_system_roles(sub_matches: &ArgMatches) -> String {
    get_arg_or_input_as_string(SYSTEM_ROLES, "Name of system roles set?", sub_matches)
}

pub fn roles_arg() -> Arg<'static> {
    Arg::new(ROLE)
        .long(long_args::ROLE)
        .short(short_args::ROLE)
        .help("A system role. (Repeatable)")
        .required(false)
        .takes_value(true)
        .action(clap::ArgAction::Append)
        .value_parser(validate_roles)
}

fn validate_roles(role: &str) -> Result<String, String> {
    if SystemRoleType::VARIANTS.contains(&role) {
        Ok(role.to_owned())
    } else {
        Err(format!("{role} is not a valid SystemRoleType."))
    }
}

pub fn get_roles(sub_matches: &ArgMatches) -> anyhow::Result<Vec<SystemRoleType>> {
    let value_source = sub_matches.value_source(ROLE);
    if let Some(ValueSource::CommandLine) = value_source {
        let roles = sub_matches
            .get_many::<String>(ROLE)
            .map(|vals| {
                vals.into_iter()
                    .map(|val| SystemRoleType::from_str(val).unwrap())
                    // unwrap ok because of clap validation
                    .collect::<Vec<SystemRoleType>>()
            })
            .unwrap_or_default();
        Ok(roles)
    } else {
        ask_for_roles()
    }
}

fn ask_for_roles() -> anyhow::Result<Vec<SystemRoleType>> {
    let mut roles = Vec::new();
    loop {
        let role = requestty::prompt_one(Question::input(ROLE).message("Role name?"))?
            .try_into_string()
            .expect("unable to convert role name to a string");
        let role = SystemRoleType::from_str(&role)?;
        roles.push(role);

        let ask_again = requestty::Question::confirm("ask_again")
            .message("Add another role (just hit enter for YES)?")
            .default(true);

        if !requestty::prompt_one(ask_again)?
            .as_bool()
            .expect("unable to convert question::confirm to true")
        {
            break;
        }
    }
    Ok(roles)
}
