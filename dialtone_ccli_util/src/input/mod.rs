use clap::ArgMatches;
use requestty::Question;

pub mod actor_ref;
pub mod actor_summary;
pub mod actor_type;
pub mod auto_approve;
pub mod code;
pub mod display_name;
pub mod host_name;
pub mod json;
pub mod password;
pub mod path;
pub mod pun;
pub mod site_name;
pub mod system_roles;
pub mod user_acct;
pub mod user_name;
pub mod user_status;
pub mod yes;

#[rustfmt::skip]
pub mod short_args {
    pub const ACTOR_REFERENCE: char = 'i';
    pub const ACTOR_SUMMARY: char   = 'Y';
    pub const ACTOR_TYPE: char      = 't';
    pub const AUTO_APPROVE: char    = 'A';
    pub const CODE: char            = 'C';
    pub const DISPLAY_NAME: char    = 'D';
    pub const HOST_NAME: char       = 'H';
    pub const JSON: char            = 'J';
    pub const LONG_SITE_NAME: char  = 'L';
    pub const PASSWORD: char        = 'p';
    pub const PATH: char            = 'd';
    pub const PUN: char             = 'P';
    pub const ROLE: char            = 'r';
    pub const SHORT_SITE_NAME: char = 'S';
    pub const SYSTEM_ROLES: char    = 's';
    pub const USER_ACCOUNT: char    = 'a';
    pub const USER_NAME: char       = 'u';
    pub const USER_STATUS: char     = 'U';
    pub const YES: char             = 'y';
}

#[rustfmt::skip]
pub mod long_args {
    pub const ACTOR_REFERENCE: &str    = "actor-reference";
    pub const ACTOR_SUMMARY: &str      = "actor-summary";
    pub const ACTOR_TYPE: &str         = "actor-type";
    pub const AUTO_APPROVE: &str       = "auto-approve";
    pub const CODE: &str               = "code";
    pub const DISPLAY_NAME: &str       = "display-name";
    pub const FAKE_DISPLAY_NAME: &str  = "fake-display-name";
    pub const FAKE_ACTOR_SUMMARY: &str = "fake-actor-summary";
    pub const HOST_NAME: &str          = "host-name";
    pub const JSON: &str               = "json";
    pub const LONG_SITE_NAME: &str     = "long-site-name";
    pub const PASSWORD: &str           = "password";
    pub const PATH: &str               = "directory";
    pub const PUN: &str                = "pun";
    pub const ROLE: &str               = "role";
    pub const SHORT_SITE_NAME: &str    = "short-site-name";
    pub const SYSTEM_ROLES: &str       = "system-roles";
    pub const USER_ACCOUNT: &str       = "user-account";
    pub const USER_NAME: &str          = "user-name";
    pub const USER_STATUS: &str        = "user-status";
    pub const YES: &str                = "yes";
}

pub fn get_arg_or_input_as_string(key: &str, message: &str, sub_matches: &ArgMatches) -> String {
    let arg_value = sub_matches.get_one::<String>(key);
    if let Some(value) = arg_value {
        value.to_owned()
    } else {
        let questions = vec![Question::input(key).message(message).build()];
        let answers = requestty::prompt(questions).unwrap();
        answers.get(key).unwrap().as_string().unwrap().to_owned()
    }
}

pub fn get_arg_or_input_as_string_with_default(
    key: &str,
    message: &str,
    sub_matches: &ArgMatches,
    default: Option<String>,
) -> String {
    let arg_value = sub_matches.get_one::<String>(key);
    if let Some(value) = arg_value {
        value.to_owned()
    } else if let Some(default_value) = default {
        default_value
    } else {
        let questions = vec![Question::input(key).message(message).build()];
        let answers = requestty::prompt(questions).unwrap();
        answers.get(key).unwrap().as_string().unwrap().to_owned()
    }
}

pub fn get_arg_or_input_as_password(key: &str, message: &str, sub_matches: &ArgMatches) -> String {
    let arg_value = sub_matches.get_one::<String>(key);
    if let Some(value) = arg_value {
        value.to_owned()
    } else {
        let questions = vec![Question::password(key).message(message).mask('*').build()];
        let answers = requestty::prompt(questions).unwrap();
        answers.get(key).unwrap().as_string().unwrap().to_owned()
    }
}

pub fn get_arg_or_input_as_bool(key: &str, message: &str, sub_matches: &ArgMatches) -> bool {
    let arg_value = sub_matches.get_one::<bool>(key);
    if let Some(value) = arg_value {
        value.to_owned()
    } else {
        let questions = vec![Question::confirm(key).message(message).build()];
        let answers = requestty::prompt(questions).unwrap();
        answers.get(key).unwrap().as_bool().unwrap().to_owned()
    }
}

pub fn get_arg_as_bool(key: &str, sub_matches: &ArgMatches) -> bool {
    sub_matches
        .get_one::<bool>(key)
        .map(|b| b.to_owned())
        .unwrap_or(false)
}

pub fn get_optional_arg<'a, T>(key: &'a str, sub_matches: &'a ArgMatches) -> Option<&'a T>
where
    T: Clone + Send + Sync + 'static,
{
    sub_matches.get_one::<T>(key)
}
