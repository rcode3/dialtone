use crate::config::dirs::get_user_path;
use dialtone_reqwest::site_connection::SiteConnection;
use std::fs;

pub fn get_sc(host_name: &str) -> SiteConnection {
    if host_name.eq_ignore_ascii_case("localhost") {
        let port = match std::env::var("TEST_PORT") {
            Ok(val) => val,
            Err(_e) => "3000".to_string(),
        };
        SiteConnection {
            host_addr: format!("localhost:{port}"),
            secure: false,
            auth_data: None,
            host_name: Some(host_name.to_string()),
            user_name: None,
        }
    } else {
        SiteConnection::new(host_name)
    }
}

const SC_FILE_NAME: &str = "sc.json";

pub fn save_sc(acct: &str, sc: &SiteConnection) -> std::io::Result<()> {
    let mut path = get_user_path(acct);
    path.push(SC_FILE_NAME);
    let json = serde_json::to_string(sc).unwrap();
    fs::write(path, json)?;
    Ok(())
}

pub fn load_sc(acct: &str) -> std::io::Result<SiteConnection> {
    let mut path = get_user_path(acct);
    path.push(SC_FILE_NAME);
    let string = fs::read_to_string(path)?;
    let site_connection = serde_json::from_str(&string)?;
    Ok(site_connection)
}

pub fn remove_sc(acct: &str) -> std::io::Result<()> {
    let mut path = get_user_path(acct);
    path.push(SC_FILE_NAME);
    if path.as_path().exists() {
        fs::remove_file(path).unwrap();
    }
    Ok(())
}
