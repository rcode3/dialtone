pub mod actor;
pub mod media;
pub mod site_info;
pub mod web_user;

use termimad::MadSkin;

pub fn skin() -> MadSkin {
    MadSkin::default()
}
