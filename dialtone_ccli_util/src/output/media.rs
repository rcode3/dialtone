use std::{collections::HashMap, fmt::Display};

#[derive(Default, Clone, Copy)]
pub struct Report {
    pub added: u32,
    pub existed: u32,
}

impl Display for Report {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "Added: {}, Pre-existed: {}",
            self.added, self.existed
        ))
    }
}

impl std::ops::Add<Report> for Report {
    type Output = Report;

    fn add(self, rhs: Report) -> Self::Output {
        Report {
            added: self.added + rhs.added,
            existed: self.existed + rhs.existed,
        }
    }
}

pub fn output_media_reports(reports: &HashMap<String, Report>) {
    reports
        .iter()
        .for_each(|(pun, report)| println!("Media processed for {pun}: {report}"))
}
