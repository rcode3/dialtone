use std::collections::HashMap;

use dialtone_common::rest::sites::site_data::{PublicSiteInfo, SiteInfo};
use termimad::minimad::TextTemplate;

use super::skin;

pub fn basic_info(site_info: &SiteInfo) {
    let skin = skin();

    let text_template = TextTemplate::from(
        r#"
            ## ${host_name}

            |-:|:-|
            |short name|${short_name}|
            |long name|${long_name}|
            |created|${created_at}|
            |last modified|${modified_at}|
            |-:|:-|
    "#,
    );
    let created_at = site_info.created_at.to_rfc3339();
    let modified_at = site_info.modified_at.to_rfc3339();
    let mut expander = text_template.expander();
    expander.set("host_name", &site_info.host_name);
    expander.set("short_name", &site_info.site_data.public.names.short_name);
    expander.set("long_name", &site_info.site_data.public.names.long_name);
    expander.set("created_at", &created_at);
    expander.set("modified_at", &modified_at);
    skin.print_expander(expander);
}

pub fn basic_public_info(host_name: &str, public_info: &PublicSiteInfo) {
    let skin = skin();

    let text_template = TextTemplate::from(
        r#"
            ## ${host_name}

            |-:|:-|
            |short name|${short_name}|
            |long name|${long_name}|
            |-:|:-|
    "#,
    );
    let mut expander = text_template.expander();
    expander.set("host_name", host_name);
    expander.set("short_name", &public_info.names.short_name);
    expander.set("long_name", &public_info.names.long_name);
    skin.print_expander(expander);
}

pub fn registration_methods(public_info: &PublicSiteInfo) {
    let skin = skin();

    let available = &public_info.registration_methods;
    if available.is_empty() {
        let text = r#"
## Available Registration Methods

**No account registration methods available.**

"#;
        skin.print_text(text);
    } else {
        let text_template = TextTemplate::from(
            r#"

## Available Registration Methods

${registrations
* ${registration_name}
}

        "#,
        );

        let mut expander = text_template.expander();
        let strs = available.iter().map(|r| r.to_string()).collect::<Vec<_>>();
        strs.iter().for_each(|v| {
            expander.sub("registrations").set("registration_name", v);
        });
        skin.print_expander(expander);
    }
}

pub fn system_roles(site_info: &SiteInfo) {
    let skin = skin();

    let roles = &site_info.site_data.system_roles_sets;
    if roles.is_empty() {
        let text = r#"

## System Roles Sets

**No system roles sets defined.**

"#;
        skin.print_text(text);
    } else {
        let text_template = TextTemplate::from(
            r#"
## System Roles Sets

|-:|:-|
${roles
|${role_name}|${role_set}|
}
|-:|:-|

"#,
        );
        let role_values = roles
            .iter()
            .map(|(k, v)| {
                (
                    k.to_string(),
                    v.iter()
                        .map(|v| v.to_string())
                        .collect::<Vec<String>>()
                        .join(", "),
                )
            })
            .collect::<HashMap<String, String>>();
        let mut expander = text_template.expander();
        role_values.iter().for_each(|(k, v)| {
            expander.sub("roles").set("role_name", k).set("role_set", v);
        });
        skin.print_expander(expander);
    }
}

pub fn open_registration(site_info: &SiteInfo) {
    let skin = skin();

    if site_info.site_data.is_open_registration_setup() {
        let text_template = TextTemplate::from(
            r#"

## Open Account Registration

|-:|:-|
|automatically approve accounts|${auto_approve}|
|system roles set name|${set_name}|
|-:|:-|

"#,
        );
        let auto_approve = site_info
            .site_data
            .open_registration
            .auto_approve
            .to_string();
        let mut expander = text_template.expander();
        expander.set("auto_approve", &auto_approve);
        expander.set(
            "set_name",
            site_info
                .site_data
                .open_registration
                .system_roles_set_name
                .as_ref()
                .unwrap(),
        );
        skin.print_expander(expander);
    } else {
        let text = r#"

## Open Account Registration

**Open account registration is not setup.**

"#;
        skin.print_text(text);
    }
}

pub fn code_registration(site_info: &SiteInfo) {
    let skin = skin();

    let codes = &site_info.site_data.simple_code_registration.codes;
    if codes.is_empty() {
        let text = r#"

## Account Registration With Codes

**No account registrations with codes are setup.**

"#;
        skin.print_text(text);
    } else {
        let text_template = TextTemplate::from(
            r#"

## Account Registration With Codes

|:-:|:-:|:-:|
|Code|Automatically Approved|System Roles Set|
|-:|:-:|:-|
${registrations
|${codes}|${auto_approve}|${set_name}|
}
|-:|:-|:-|

            "#,
        );
        let registrations = codes
            .iter()
            .map(|(k, v)| {
                (
                    k.to_string(),
                    v.auto_approve.to_string(),
                    v.system_roles_set_name.to_string(),
                )
            })
            .collect::<Vec<(String, String, String)>>();
        let mut expander = text_template.expander();
        registrations
            .iter()
            .for_each(|(code, auto_approve, set_name)| {
                expander
                    .sub("registrations")
                    .set("codes", code)
                    .set("auto_approve", auto_approve)
                    .set("set_name", set_name);
            });
        skin.print_expander(expander);
    }
}
