use dialtone_common::rest::actors::actor_model::PublicActor;
use termimad::minimad::TextTemplate;

use super::skin;

const ACTOR_BASE_INFO_MD: &str = r#"
    
# ${actor_id}

*            Actor Id: ${actor_id}
*      WebFinger Acct: ${actor_acct}
* Preferred User Name: ${pun}
*                Type: ${actor_type}
*          Banner URL: ${banner_url}
*            Icon URL: ${icon_url}

"#;

pub fn actor_base_info(public_actor: &PublicActor) {
    let skin = skin();
    let no_value = "".to_string();

    let actor_id = &public_actor.ap.id;
    let wf_acct = public_actor.jrd.as_ref().map_or("", |jrd| &jrd.subject);
    let pun = &public_actor.ap.preferred_user_name;
    let actor_type = &public_actor.ap.ap_type.to_string();
    let banner_url = public_actor
        .ap
        .image
        .as_ref()
        .map_or_else(|| Some(no_value.clone()), |image| image.first_url())
        .unwrap_or_else(|| no_value.clone());
    let icon_url = public_actor
        .ap
        .icon
        .as_ref()
        .map_or_else(|| Some(no_value.clone()), |image| image.first_url())
        .unwrap_or_else(|| no_value.clone());

    let base_template = TextTemplate::from(ACTOR_BASE_INFO_MD);
    let mut expander = base_template.expander();
    expander.set("actor_id", actor_id);
    expander.set("actor_acct", wf_acct);
    expander.set("pun", pun);
    expander.set("actor_type", actor_type);
    expander.set("banner_url", &banner_url);
    expander.set("icon_url", &icon_url);

    skin.print_expander(expander);
}
