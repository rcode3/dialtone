use dialtone_common::rest::{
    actors::actor_model::PublicActor,
    event_log::LogEvent,
    users::web_user::{LastLoginData, LastSeenData, SystemPermission, WebUser},
};
use termimad::minimad::TextTemplate;

use super::skin;

const BASE_INFO_MD: &str = r#"

# ${user_acct}

### Basic Information
*       Created: ${created_at}
*  Last Updated: ${modified_at}
*        Status: ${status}
*       Persons: ${person_count}
* Organizations: ${organization_count}
*        Groups: ${group_count}
*  Applications: ${application_count}
*      Services: ${service_count}


"#;

pub fn web_user_base_info(web_user: &WebUser) {
    let skin = skin();

    let created_at = web_user.created_at.to_rfc2822();
    let modified_at = web_user.modified_at.to_rfc2822();
    let status = web_user.status.to_string();
    let person_count = web_user.actor_counts.person.to_string();
    let organization_count = web_user.actor_counts.organization.to_string();
    let group_count = web_user.actor_counts.group.to_string();
    let application_count = web_user.actor_counts.application.to_string();
    let service_count = web_user.actor_counts.service.to_string();

    let base_template = TextTemplate::from(BASE_INFO_MD);
    let mut expander = base_template.expander();
    expander.set("user_acct", &web_user.user_authz.acct);
    expander.set("created_at", &created_at);
    expander.set("modified_at", &modified_at);
    expander.set("status", &status);
    expander.set("person_count", &person_count);
    expander.set("organization_count", &organization_count);
    expander.set("group_count", &group_count);
    expander.set("application_count", &application_count);
    expander.set("service_count", &service_count);

    skin.print_expander(expander);
}

const DEFAULT_ACTOR_NONE_MD: &str = r#"

### Default Actor

This user has no default actor.


"#;

const DEFAULT_ACTOR_MD: &str = r#"

### Default Actor
*       Actor Id: ${actor_id}
* WebFinger Acct: ${actor_acct}


"#;

pub fn web_user_default_actor(default_actor: &Option<PublicActor>) {
    let skin = skin();

    if let Some(public_actor) = default_actor {
        let actor_id = &public_actor.ap.id;
        let actor_acct = &public_actor.jrd.as_ref().unwrap().subject;

        let template = TextTemplate::from(DEFAULT_ACTOR_MD);
        let mut expander = template.expander();

        expander.set("actor_id", actor_id);
        expander.set("actor_acct", actor_acct);
        skin.print_expander(expander);
    } else {
        skin.print_text(DEFAULT_ACTOR_NONE_MD);
    }
}

const LAST_LOGIN_MD: &str = r#"

### Last Logins
|:-:|:-:|:-:|
|From|At|Success|
|-:|:-:|:-|
${logins
|${from}|${at}|${success}|
}
|-:|:-:|:-|


"#;

pub fn web_user_last_login(last_logins: &[LastLoginData]) {
    let skin = skin();

    let logins = last_logins
        .iter()
        .map(|ll| {
            (
                ll.from.to_owned(),
                ll.at.to_rfc2822(),
                ll.success.to_string(),
            )
        })
        .collect::<Vec<_>>();

    let template = TextTemplate::from(LAST_LOGIN_MD);
    let mut expander = template.expander();
    logins.iter().for_each(|(from, at, success)| {
        expander
            .sub("logins")
            .set("from", from)
            .set("at", at)
            .set("success", success);
    });

    skin.print_expander(expander);
}

const LAST_SEEN_MD: &str = r#"

### Last Seen
|:-:|:-:|
|From|At|
|-:|:-|
${seens
|${from}|${at}|
}
|-:|:-|


"#;

pub fn web_user_last_seen(last_seen: &[LastSeenData]) {
    let skin = skin();

    let seens = last_seen
        .iter()
        .map(|ls| (ls.from.to_owned(), ls.at.to_rfc2822()))
        .collect::<Vec<(String, String)>>();

    let template = TextTemplate::from(LAST_SEEN_MD);
    let mut expander = template.expander();
    seens.iter().for_each(|(from, at)| {
        expander.sub("seens").set("from", from).set("at", at);
    });

    skin.print_expander(expander);
}

const EVENT_LOG_MD: &str = r#"

### Event Log
|:-:|:-:|
|When|Event|
|-:|:-|
${events
|${when}|${message}|
}
|-:|:-|

"#;

pub fn web_user_event_log(event_log: &[LogEvent]) {
    let skin = skin();

    let events = event_log
        .iter()
        .map(|event| (event.when.to_rfc2822(), event.message.to_owned()))
        .collect::<Vec<(String, String)>>();

    let template = TextTemplate::from(EVENT_LOG_MD);
    let mut expander = template.expander();
    events.iter().for_each(|(when, message)| {
        expander
            .sub("events")
            .set("when", when)
            .set("message", message);
    });

    skin.print_expander(expander);
}

const WEB_USER_PERMISSIONS_MD: &str = r#"

### System Permissions
|:-:|:-:|
|Role|Host|
|-:|:-|
${permissions
|${role}|${host_name}|
}
|-:|:-|

"#;

pub fn web_user_permissions(permissions: &[SystemPermission]) {
    let skin = skin();

    let permissions = permissions
        .iter()
        .map(|p| (p.role.to_string(), p.host_name.to_owned()))
        .collect::<Vec<(String, String)>>();

    let template = TextTemplate::from(WEB_USER_PERMISSIONS_MD);
    let mut expander = template.expander();
    permissions.iter().for_each(|(role, host_name)| {
        expander
            .sub("permissions")
            .set("role", role)
            .set("host_name", host_name);
    });

    skin.print_expander(expander);
}
