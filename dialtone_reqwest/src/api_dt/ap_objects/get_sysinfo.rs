use dialtone_common::rest::{
    ap_objects::{ap_object_exchanges::GetApObject, ap_object_model::ApObjectSystemInfo},
    api_paths::full_path::AP_OBJECT__SYSINFO,
};

use crate::{dt_reqwest_error::DtReqwestError, site_connection::SiteConnection};

pub async fn get_ap_object_sysinfo(
    sc: &SiteConnection,
    ap_object_id: &str,
) -> Result<ApObjectSystemInfo, DtReqwestError> {
    sc.must_be_logged_in()?;
    let request = GetApObject {
        ap_object_id: ap_object_id.to_owned(),
    };
    let sysinfo = sc
        .get(AP_OBJECT__SYSINFO, &[])
        .query(&request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(sysinfo)
}
