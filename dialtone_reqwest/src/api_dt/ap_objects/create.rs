use dialtone_common::rest::{
    ap_objects::{
        ap_object_exchanges::{ApObjectIdResponse, PostApObject},
        ap_object_model::CreateOwnedApObject,
    },
    api_paths::full_path::AP_OBJECT__OWNED,
};

use crate::{dt_reqwest_error::DtReqwestError, site_connection::SiteConnection};

pub async fn create_ap_object(
    sc: &SiteConnection,
    actor_id: &str,
    creat_ap_object: &CreateOwnedApObject,
) -> Result<String, DtReqwestError> {
    sc.must_be_logged_in()?;
    let request = PostApObject {
        actor_id: actor_id.to_string(),
        create_ap_object: creat_ap_object.clone(),
    };
    let response: ApObjectIdResponse = sc
        .post(AP_OBJECT__OWNED, &[])
        .json(&request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(response.ap_object_id)
}
