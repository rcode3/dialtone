use dialtone_common::rest::{
    ap_objects::{
        ap_object_exchanges::{ApObjectIdResponse, PutApObject},
        ap_object_model::UpdateOwnedApObject,
    },
    api_paths::full_path::AP_OBJECT__OWNED,
};

use crate::{dt_reqwest_error::DtReqwestError, site_connection::SiteConnection};

pub async fn update_ap_object(
    sc: &SiteConnection,
    ap_object_id: &str,
    update_ap_object: &UpdateOwnedApObject,
) -> Result<String, DtReqwestError> {
    sc.must_be_logged_in()?;
    let request = PutApObject {
        ap_object_id: ap_object_id.to_owned(),
        update_ap_object: update_ap_object.to_owned(),
    };
    let response: ApObjectIdResponse = sc
        .put(AP_OBJECT__OWNED, &[])
        .json(&request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(response.ap_object_id)
}
