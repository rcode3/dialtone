use dialtone_common::rest::{
    ap_objects::{ap_object_exchanges::PutApObjectSysData, ap_object_model::ApObjectSystemData},
    api_paths::full_path::AP_OBJECT__SYSDATA,
    simple_exchanges::SimpleResponse,
};

use crate::{dt_reqwest_error::DtReqwestError, site_connection::SiteConnection};

pub async fn change_ap_object_sysdata(
    sc: &SiteConnection,
    ap_object_id: &str,
    sys_data: ApObjectSystemData,
) -> Result<bool, DtReqwestError> {
    sc.must_be_logged_in()?;
    let request = PutApObjectSysData {
        ap_object_id: ap_object_id.to_owned(),
        system_data: sys_data,
    };
    let response: SimpleResponse = sc
        .put(AP_OBJECT__SYSDATA, &[])
        .json(&request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(response.result)
}
