use dialtone_common::rest::{
    ap_objects::{ap_object_exchanges::GetApObject, ap_object_model::OwnedApObject},
    api_paths::full_path::AP_OBJECT__OWNED,
};

use crate::{dt_reqwest_error::DtReqwestError, site_connection::SiteConnection};

pub async fn get_owned_ap_object(
    sc: &SiteConnection,
    ap_object_id: &str,
) -> Result<OwnedApObject, DtReqwestError> {
    sc.must_be_logged_in()?;
    let request = GetApObject {
        ap_object_id: ap_object_id.to_owned(),
    };
    let ap_object = sc
        .get(AP_OBJECT__OWNED, &[])
        .query(&request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(ap_object)
}
