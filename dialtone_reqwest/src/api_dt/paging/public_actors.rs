use dialtone_common::rest::{
    actors::actor_model::PublicActor,
    paging::{public_actor::PublicActorsByHost, Page},
};

use crate::{dt_reqwest_error::DtReqwestError, site_connection::SiteConnection};

use super::page;

pub type PublicActorPage = Result<Page<PublicActor>, DtReqwestError>;

pub async fn get_public_actors_by_host(
    sc: &SiteConnection,
    props: &PublicActorsByHost,
) -> PublicActorPage {
    page(sc, props).await
}
