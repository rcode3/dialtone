use dialtone_common::{
    ap::ap_object::ApObject,
    rest::paging::{
        public_ap_object::{PublicApObjectsByActor, PublicApObjectsByHost},
        Page,
    },
};

use crate::{dt_reqwest_error::DtReqwestError, site_connection::SiteConnection};

use super::page;

pub type PublicApObjectPage = Result<Page<ApObject>, DtReqwestError>;

pub async fn get_public_ap_objects_by_host(
    sc: &SiteConnection,
    props: &PublicApObjectsByHost,
) -> PublicApObjectPage {
    page(sc, props).await
}

pub async fn get_public_ap_objects_by_actor(
    sc: &SiteConnection,
    props: &PublicApObjectsByActor,
) -> PublicApObjectPage {
    page(sc, props).await
}
