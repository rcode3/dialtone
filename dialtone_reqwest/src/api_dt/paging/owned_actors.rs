use dialtone_common::rest::{
    actors::actor_model::OwnedActor,
    paging::{
        owned_actor::{OwnedActorsByHost, OwnedActorsByOwner},
        Page,
    },
};

use crate::{dt_reqwest_error::DtReqwestError, site_connection::SiteConnection};

use super::page;

pub type OwnedActorPage = Result<Page<OwnedActor>, DtReqwestError>;

pub async fn get_owned_actors_by_owner(
    sc: &SiteConnection,
    props: &OwnedActorsByOwner,
) -> OwnedActorPage {
    page(sc, props).await
}

pub async fn get_owned_actors_by_host(
    sc: &SiteConnection,
    props: &OwnedActorsByHost,
) -> OwnedActorPage {
    page(sc, props).await
}
