use dialtone_common::rest::paging::PagingProps;
use serde::{de::DeserializeOwned, Serialize};

use crate::{dt_reqwest_error::DtReqwestError, site_connection::SiteConnection};

pub mod owned_actors;
pub mod owned_ap_objects;
pub mod public_actors;
pub mod public_ap_objects;
pub mod users;

pub async fn page<T, R, P>(sc: &SiteConnection, props: &T) -> Result<R, DtReqwestError>
where
    T: Serialize + PagingProps<P>,
    R: DeserializeOwned,
{
    if T::AUTHN_REQUIRED {
        sc.must_be_logged_in()?;
    }
    let response: R = sc
        .get(&T::full_path(), &[])
        .query(props)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(response)
}
