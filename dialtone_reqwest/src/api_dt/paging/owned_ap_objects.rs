use dialtone_common::rest::{
    ap_objects::ap_object_model::OwnedApObject,
    paging::{owned_ap_object::OwnedApObjectsByActor, Page},
};

use crate::{dt_reqwest_error::DtReqwestError, site_connection::SiteConnection};

use super::page;

pub type OwnedApObjectPage = Result<Page<OwnedApObject>, DtReqwestError>;

pub async fn get_owned_ap_objects_by_actor(
    sc: &SiteConnection,
    props: &OwnedApObjectsByActor,
) -> OwnedApObjectPage {
    page(sc, props).await
}
