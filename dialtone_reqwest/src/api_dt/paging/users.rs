use dialtone_common::rest::{
    paging::{user::UsersByHost, Page},
    users::web_user::WebUser,
};

use crate::{dt_reqwest_error::DtReqwestError, site_connection::SiteConnection};

use super::page;

pub type UsersPage = Result<Page<WebUser>, DtReqwestError>;

pub async fn get_users_by_host(sc: &SiteConnection, props: &UsersByHost) -> UsersPage {
    page(sc, props).await
}
