use dialtone_common::rest::users::user_exchanges::{PostUser, PostUserResponse};

use crate::dt_reqwest_error::DtReqwestError;
use dialtone_common::rest::api_paths::full_path::USER;

use crate::site_connection::SiteConnection;

pub async fn register_user(
    sc: &SiteConnection,
    register_user: &PostUser,
) -> Result<PostUserResponse, DtReqwestError> {
    let response = sc
        .post(USER, &[])
        .json(&register_user)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(response)
}
