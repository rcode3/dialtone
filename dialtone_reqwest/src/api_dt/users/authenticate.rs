use crate::dt_reqwest_error::DtReqwestError;
use crate::site_connection::SiteConnection;
use dialtone_common::rest::api_paths::full_path::AUTHENTICATE;
use dialtone_common::rest::users::user_login::{AuthBody, UserCredential};

pub async fn authenticate(
    sc: &SiteConnection,
    username: &str,
    password: &str,
) -> Result<SiteConnection, DtReqwestError> {
    let user_login = UserCredential {
        user_acct: format!("{}@{}", username, sc.host_name()),
        password: password.to_string(),
    };
    let auth_body: AuthBody = sc
        .post(AUTHENTICATE, &[])
        .json(&user_login)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    let new_sc = SiteConnection {
        host_addr: sc.host_addr.clone(),
        secure: sc.secure,
        auth_data: Some(auth_body),
        host_name: sc.host_name.clone(),
        user_name: Some(username.to_string()),
    };
    Ok(new_sc)
}
