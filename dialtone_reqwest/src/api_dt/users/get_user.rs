use crate::dt_reqwest_error::DtReqwestError;
use crate::site_connection::SiteConnection;
use dialtone_common::rest::api_paths::full_path::USER;
use dialtone_common::rest::users::user_exchanges::GetUser;
use dialtone_common::rest::users::web_user::WebUser;
use dialtone_common::utils::make_acct::make_acct;

pub async fn get_user(sc: &SiteConnection, acct: Option<&str>) -> Result<WebUser, DtReqwestError> {
    sc.must_be_logged_in()?;
    let request = if let Some(a) = acct {
        GetUser { acct: a.to_owned() }
    } else {
        GetUser {
            acct: make_acct(sc.user_name.as_ref().unwrap(), sc.host_name()),
        }
    };
    let user: WebUser = sc
        .get(USER, &[])
        .query(&request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(user)
}
