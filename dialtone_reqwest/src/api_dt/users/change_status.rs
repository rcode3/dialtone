use dialtone_common::rest::api_paths::full_path::USER__STATUS;
use dialtone_common::rest::simple_exchanges::SimpleResponse;
use dialtone_common::rest::users::user_exchanges::PutUserStatus;

use crate::dt_reqwest_error::DtReqwestError;
use crate::site_connection::SiteConnection;

pub async fn change_status(
    sc: &SiteConnection,
    request: &PutUserStatus,
) -> Result<bool, DtReqwestError> {
    sc.must_be_logged_in()?;
    let response: SimpleResponse = sc
        .put(USER__STATUS, &[])
        .json(&request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    if response.result {
        Ok(true)
    } else {
        Ok(false)
    }
}
