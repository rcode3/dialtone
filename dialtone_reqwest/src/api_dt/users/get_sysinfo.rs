use crate::dt_reqwest_error::DtReqwestError;
use crate::site_connection::SiteConnection;
use dialtone_common::rest::api_paths::full_path::USER__SYSINFO;
use dialtone_common::rest::users::user_exchanges::GetUser;
use dialtone_common::rest::users::web_user::UserSystemInfo;

pub async fn get_user_sysinfo(
    sc: &SiteConnection,
    acct: &str,
) -> Result<UserSystemInfo, DtReqwestError> {
    sc.must_be_logged_in()?;
    let request = GetUser {
        acct: acct.to_owned(),
    };
    let sysinfo = sc
        .get(USER__SYSINFO, &[])
        .query(&request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(sysinfo)
}
