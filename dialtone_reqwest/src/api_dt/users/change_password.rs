use crate::dt_reqwest_error::DtReqwestError;
use crate::site_connection::SiteConnection;
use dialtone_common::rest::api_paths::full_path::USER__PASSWORD;
use dialtone_common::rest::simple_exchanges::SimpleResponse;
use dialtone_common::rest::users::user_exchanges::PutUserPassword;

pub async fn change_password(
    sc: &SiteConnection,
    request: &PutUserPassword,
) -> Result<bool, DtReqwestError> {
    sc.must_be_logged_in()?;
    let response: SimpleResponse = sc
        .put(USER__PASSWORD, &[])
        .json(&request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    if response.result {
        Ok(true)
    } else {
        Ok(false)
    }
}
