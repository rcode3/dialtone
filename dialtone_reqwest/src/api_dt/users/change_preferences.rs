use dialtone_common::rest::api_paths::full_path::USER__PREFS;
use dialtone_common::rest::simple_exchanges::SimpleResponse;
use dialtone_common::rest::users::user_exchanges::PutUserPreferences;

use crate::dt_reqwest_error::DtReqwestError;
use crate::site_connection::SiteConnection;

pub async fn change_preferences(
    sc: &SiteConnection,
    request: &PutUserPreferences,
) -> Result<bool, DtReqwestError> {
    sc.must_be_logged_in()?;
    let response: SimpleResponse = sc
        .put(USER__PREFS, &[])
        .json(&request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    if response.result {
        Ok(true)
    } else {
        Ok(false)
    }
}
