use crate::dt_reqwest_error::DtReqwestError;
use crate::site_connection::{PathArgument, SiteConnection};
use dialtone_common::rest::api_paths::full_path::SITES;
use dialtone_common::rest::sites::site_data::PublicSiteInfo;

pub async fn get_site_info(sc: &SiteConnection) -> Result<PublicSiteInfo, DtReqwestError> {
    let site_info: PublicSiteInfo = sc
        .get(SITES, &[PathArgument::new(":host_name", sc.host_name())])
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(site_info)
}
