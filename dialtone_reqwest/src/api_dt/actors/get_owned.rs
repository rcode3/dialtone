use crate::dt_reqwest_error::DtReqwestError;
use dialtone_common::rest::actors::actor_exchanges::GetActorRequest;
use dialtone_common::rest::actors::actor_model::OwnedActor;
use dialtone_common::rest::api_paths::full_path::ACTOR__OWNED;

use crate::site_connection::SiteConnection;

pub async fn get_owned_actor(
    sc: &SiteConnection,
    request: &GetActorRequest,
) -> Result<OwnedActor, DtReqwestError> {
    let response = sc
        .get(ACTOR__OWNED, &[])
        .query(&request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(response)
}
