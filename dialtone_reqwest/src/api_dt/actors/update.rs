use dialtone_common::rest::{
    actors::actor_exchanges::PutUpdateActorRequest, api_paths::full_path::ACTOR__OWNED,
    simple_exchanges::SimpleResponse,
};

use crate::{dt_reqwest_error::DtReqwestError, site_connection::SiteConnection};

pub async fn update_owned_actor(
    sc: &SiteConnection,
    update_request: &PutUpdateActorRequest,
) -> Result<bool, DtReqwestError> {
    sc.must_be_logged_in()?;
    let response: SimpleResponse = sc
        .put(ACTOR__OWNED, &[])
        .json(&update_request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(response.result)
}
