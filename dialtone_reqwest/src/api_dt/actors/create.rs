use crate::dt_reqwest_error::DtReqwestError;
use dialtone_common::rest::actors::actor_exchanges::NewActorRequest;
use dialtone_common::rest::actors::actor_model::OwnedActor;
use dialtone_common::rest::api_paths::full_path::ACTOR__OWNED;

use crate::site_connection::SiteConnection;

/// Creates an owned actor.
pub async fn create_actor(
    sc: &SiteConnection,
    new_actor_request: &NewActorRequest,
) -> Result<OwnedActor, DtReqwestError> {
    let response = sc
        .post(ACTOR__OWNED, &[])
        .json(&new_actor_request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(response)
}
