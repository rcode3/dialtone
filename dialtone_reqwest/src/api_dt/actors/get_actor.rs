use crate::dt_reqwest_error::DtReqwestError;
use dialtone_common::rest::actors::actor_exchanges::GetActorRequest;
use dialtone_common::rest::actors::actor_model::PublicActor;
use dialtone_common::rest::api_paths::full_path::ACTOR;

use crate::site_connection::SiteConnection;

pub async fn get_public_actor(
    sc: &SiteConnection,
    request: &GetActorRequest,
) -> Result<PublicActor, DtReqwestError> {
    let response = sc
        .get(ACTOR, &[])
        .query(request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(response)
}
