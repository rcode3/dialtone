use dialtone_common::rest::{
    actors::actor_exchanges::PutActorRequest, api_paths::full_path::ACTOR__DEFAULT,
    simple_exchanges::SimpleResponse,
};

use crate::{dt_reqwest_error::DtReqwestError, site_connection::SiteConnection};

pub async fn change_default_actor(
    sc: &SiteConnection,
    actor_id: &str,
) -> Result<bool, DtReqwestError> {
    sc.must_be_logged_in()?;
    let request = PutActorRequest {
        actor_id: actor_id.to_owned(),
    };
    let response: SimpleResponse = sc
        .put(ACTOR__DEFAULT, &[])
        .json(&request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(response.result)
}
