use dialtone_common::rest::{
    actors::{actor_exchanges::GetActorRequest, actor_model::ActorSystemInfo},
    api_paths::full_path::ACTOR__SYSINFO,
};

use crate::{dt_reqwest_error::DtReqwestError, site_connection::SiteConnection};

pub async fn get_actor_sysinfo(
    sc: &SiteConnection,
    request: &GetActorRequest,
) -> Result<ActorSystemInfo, DtReqwestError> {
    sc.must_be_logged_in()?;
    let sysinfo = sc
        .get(ACTOR__SYSINFO, &[])
        .query(request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(sysinfo)
}
