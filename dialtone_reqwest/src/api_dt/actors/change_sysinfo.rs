use dialtone_common::rest::{
    actors::actor_model::ActorSystemInfo, api_paths::full_path::ACTOR__SYSINFO,
    simple_exchanges::SimpleResponse,
};

use crate::{dt_reqwest_error::DtReqwestError, site_connection::SiteConnection};

pub async fn change_actor_sysinfo(
    sc: &SiteConnection,
    request: &ActorSystemInfo,
) -> Result<bool, DtReqwestError> {
    sc.must_be_logged_in()?;
    let response: SimpleResponse = sc
        .put(ACTOR__SYSINFO, &[])
        .json(&request)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;
    Ok(response.result)
}
