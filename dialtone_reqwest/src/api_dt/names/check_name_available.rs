use reqwest::StatusCode;

use crate::dt_reqwest_error::DtReqwestError;
use dialtone_common::rest::api_paths::full_path::NAME;
use dialtone_common::rest::users::user_login::NameHostPair;

use crate::site_connection::SiteConnection;

pub async fn check_name_available(sc: &SiteConnection, name: &str) -> Result<bool, DtReqwestError> {
    let name_check = NameHostPair {
        user_name: name.to_string(),
        host_name: sc.host_name().to_string(),
    };
    let response = sc.get(NAME, &[]).query(&name_check).send().await?;
    if response.status() == StatusCode::OK {
        Ok(true)
    } else {
        Ok(false)
    }
}
