use dialtone_common::ap::{
    activity::Activity,
    id::{create_collection_id, StandardCollectionNames},
};
use reqwest::{header::LOCATION, StatusCode};

use crate::{dt_reqwest_error::DtReqwestError, site_connection::SiteConnection};

pub async fn send_to_collection(
    sc: &SiteConnection,
    activity: &Activity,
    collection_name: &str,
) -> Result<String, DtReqwestError> {
    sc.must_be_logged_in()?;
    let collection_id = create_collection_id(&activity.actor_id, collection_name);
    let collection_path = format!(
        "/{}",
        collection_id
            .splitn(4, '/')
            .last()
            .ok_or(DtReqwestError::InvalidUrl)?,
    );
    let response = sc
        .post(&collection_path, &[])
        .json(activity)
        .send()
        .await?
        .error_for_status()?;
    if response.status() != StatusCode::CREATED {
        return Err(DtReqwestError::ActivityNotCreated(response.status()));
    };
    let location = if let Some(location) = response.headers().get(LOCATION) {
        location
    } else {
        return Err(DtReqwestError::MissingOrBadLocationHeader);
    };
    let location = if let Ok(location) = location.to_str() {
        location
    } else {
        return Err(DtReqwestError::MissingOrBadLocationHeader);
    };
    Ok(location.to_owned())
}

pub async fn send(sc: &SiteConnection, activity: &Activity) -> Result<String, DtReqwestError> {
    send_to_collection(sc, activity, StandardCollectionNames::Outbox.into()).await
}
