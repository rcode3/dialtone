use reqwest::StatusCode;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum DtReqwestError {
    #[error("authentication is required first")]
    AuthenticationRequired,
    #[error("request parameters or input is nonsensical")]
    BadRequest,
    #[error(transparent)]
    ReqwestError(#[from] reqwest::Error),
    #[error("Did not receive 201 indicating activity was created.")]
    ActivityNotCreated(StatusCode),
    #[error("Missing Location header.")]
    MissingOrBadLocationHeader,
    #[error("Invalid URL")]
    InvalidUrl,
}

impl DtReqwestError {
    pub fn reqwest_error(self) -> Option<reqwest::Error> {
        match self {
            DtReqwestError::ReqwestError(e) => Some(e),
            _ => None,
        }
    }

    pub fn status_code(self) -> StatusCode {
        match self {
            DtReqwestError::AuthenticationRequired => StatusCode::UNAUTHORIZED,
            DtReqwestError::BadRequest => StatusCode::BAD_REQUEST,
            DtReqwestError::InvalidUrl => StatusCode::BAD_REQUEST,
            DtReqwestError::ReqwestError(e) => e.status().unwrap(),
            DtReqwestError::ActivityNotCreated(e) => e,
            DtReqwestError::MissingOrBadLocationHeader => StatusCode::NO_CONTENT,
        }
    }
}
