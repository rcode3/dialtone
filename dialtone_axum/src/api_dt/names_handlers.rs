use crate::response::response_error::ResponseError;
use crate::start_server::AppState;
use crate::{err_bad_request, err_not_found, ok_success};
use axum::extract::{Query, State};
use axum::response::IntoResponse;
use dialtone_common::rest::users::user_login::NameHostPair;
use dialtone_sqlx::control::user::check_name_availability::check_user_name_availability;

pub async fn get_name_available(
    Query(name_check): Query<NameHostPair>,
    State(AppState { pg_pool }): State<AppState>,
) -> Result<impl IntoResponse, ResponseError> {
    if name_check.user_name.len() > 2 {
        let name_available =
            check_user_name_availability(&pg_pool, &name_check.user_name, &name_check.host_name)
                .await?;
        if name_available {
            ok_success!()
        } else {
            err_not_found!()
        }
    } else {
        err_bad_request!()
    }
}
