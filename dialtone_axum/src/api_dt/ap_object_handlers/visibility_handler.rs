use axum::{extract::State, response::IntoResponse, Json};
use dialtone_common::rest::{
    ap_objects::ap_object_exchanges::PutApObjectVisibility, simple_exchanges::SimpleResponse,
};
use dialtone_sqlx::db::ap_object::update_visibility::update_ap_object_visibility;

use crate::{
    authz::authz_ap_object::authz_contentadmin_for_ap_object,
    response::response_error::ResponseError,
    start_server::{AppState, Authnz},
};

pub async fn put_ap_object_visibility_handler(
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
    Json(request): Json<PutApObjectVisibility>,
) -> Result<impl IntoResponse, ResponseError> {
    authz_contentadmin_for_ap_object(&authnz, &request.ap_object_id).await?;
    update_ap_object_visibility(&pg_pool, &request.ap_object_id, &request.visibility)
        .await?
        .map(|_| Json(SimpleResponse::success()))
        .ok_or_else(ResponseError::not_found)
}
