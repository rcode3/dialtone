use axum::{
    extract::{Query, State},
    response::IntoResponse,
    Json,
};
use dialtone_common::rest::ap_objects::ap_object_exchanges::GetApObject;
use dialtone_sqlx::db::ap_object::fetch::fetch_ap_object;

use crate::{
    authz::authz_ap_object::authz_owner_or_contentadmin_for_ap_object,
    err_not_found,
    response::response_error::ResponseError,
    start_server::{AppState, Authnz},
};

pub async fn get_ap_object_handler(
    Query(params): Query<GetApObject>,
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
) -> Result<impl IntoResponse, ResponseError> {
    authz_owner_or_contentadmin_for_ap_object(&pg_pool, &authnz, &params.ap_object_id).await?;
    fetch_ap_object(&pg_pool, &params.ap_object_id)
        .await?
        .map_or_else(|| err_not_found!(), |v| Ok(Json(v)))
}
