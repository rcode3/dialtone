use axum::{
    extract::{Query, State},
    response::IntoResponse,
    Json,
};
use dialtone_common::{
    ap::id::{parse_actors_host, parse_actors_pun},
    rest::ap_objects::ap_object_exchanges::{
        ApObjectIdResponse, GetApObject, PostApObject, PutApObject,
    },
};
use dialtone_sqlx::{
    control::ap_object::{create_owned::create_owned_ap_object, update::update_owned_ap_object},
    db::ap_object::fetch_owned::fetch_owned_ap_object,
};

use crate::{
    authz::authz_ap_object::{
        authz_owner_or_contentadmin_for_actor, authz_owner_or_contentadmin_for_ap_object,
    },
    err_not_found,
    response::response_error::ResponseError,
    start_server::{AppState, Authnz},
};

// TODO deprecate with C2S AP handler
pub async fn post_owned_ap_object_handler(
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
    Json(request): Json<PostApObject>,
) -> Result<impl IntoResponse, ResponseError> {
    authz_owner_or_contentadmin_for_actor(&pg_pool, &authnz, &request.actor_id).await?;
    let host_name = parse_actors_host(&request.actor_id)?;
    let pun = parse_actors_pun(&request.actor_id)?;
    let ap_object = create_owned_ap_object(
        &pg_pool,
        &host_name,
        &pun,
        &request.actor_id,
        &request.create_ap_object,
    )
    .await?;
    let ap_object_id = ap_object
        .id
        .ok_or(ResponseError::InternalServerError(None))?;
    Ok(Json(ApObjectIdResponse { ap_object_id }))
}

pub async fn put_owned_ap_object_handler(
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
    Json(request): Json<PutApObject>,
) -> Result<impl IntoResponse, ResponseError> {
    authz_owner_or_contentadmin_for_ap_object(&pg_pool, &authnz, &request.ap_object_id).await?;
    let ap_object_id =
        update_owned_ap_object(&pg_pool, &request.ap_object_id, request.update_ap_object).await?;
    ap_object_id.map_or_else(
        || err_not_found!(),
        |id| Ok(Json(ApObjectIdResponse { ap_object_id: id })),
    )
}

pub async fn get_owned_ap_object_handler(
    Query(params): Query<GetApObject>,
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
) -> Result<impl IntoResponse, ResponseError> {
    authz_owner_or_contentadmin_for_ap_object(&pg_pool, &authnz, &params.ap_object_id).await?;
    fetch_owned_ap_object(&pg_pool, &params.ap_object_id)
        .await?
        .map_or_else(|| err_not_found!(), |v| Ok(Json(v)))
}
