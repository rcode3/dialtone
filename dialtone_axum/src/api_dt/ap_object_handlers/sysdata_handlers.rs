use axum::{extract::State, response::IntoResponse, Json};
use dialtone_common::rest::{
    ap_objects::ap_object_exchanges::PutApObjectSysData, simple_exchanges::SimpleResponse,
};
use dialtone_sqlx::db::ap_object::update_system_data::update_ap_object_system_data;

use crate::{
    authz::authz_ap_object::authz_contentadmin_for_ap_object,
    response::response_error::ResponseError,
    start_server::{AppState, Authnz},
};

pub async fn put_ap_object_sysdata_handler(
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
    Json(request): Json<PutApObjectSysData>,
) -> Result<impl IntoResponse, ResponseError> {
    authz_contentadmin_for_ap_object(&authnz, &request.ap_object_id).await?;
    update_ap_object_system_data(&pg_pool, &request.ap_object_id, &request.system_data)
        .await?
        .map(|_| Json(SimpleResponse::success()))
        .ok_or_else(ResponseError::not_found)
}
