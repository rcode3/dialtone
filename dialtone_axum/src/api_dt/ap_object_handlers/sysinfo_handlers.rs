use axum::{
    extract::{Query, State},
    response::IntoResponse,
    Json,
};
use dialtone_common::rest::ap_objects::ap_object_exchanges::GetApObject;
use dialtone_sqlx::db::ap_object::fetch_sysinfo::fetch_ap_object_system_info;

use crate::{
    authz::authz_ap_object::authz_contentadmin_for_ap_object,
    response::response_error::ResponseError,
    start_server::{AppState, Authnz},
};

pub async fn get_ap_object_sysinfo_handler(
    Query(params): Query<GetApObject>,
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
) -> Result<impl IntoResponse, ResponseError> {
    authz_contentadmin_for_ap_object(&authnz, &params.ap_object_id).await?;
    fetch_ap_object_system_info(&pg_pool, &params.ap_object_id)
        .await?
        .map(Json)
        .ok_or_else(ResponseError::not_found)
}
