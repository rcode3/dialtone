use crate::err_not_found;
use crate::response::response_error::ResponseError;
use crate::start_server::AppState;
use axum::extract::{Path, State};
use axum::response::IntoResponse;
use axum::Json;
use dialtone_sqlx::db::site_info::fetch_site;

pub async fn get_site_handler(
    Path(host_name): Path<String>,
    State(AppState { pg_pool }): State<AppState>,
) -> Result<impl IntoResponse, ResponseError> {
    let site_info = fetch_site(&pg_pool, &host_name).await?;
    site_info.map_or_else(|| err_not_found!(), |v| Ok(Json(v.site_data.public)))
}
