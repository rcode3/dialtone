use axum::{
    routing::{get, post, put},
    Router,
};

use dialtone_common::rest::{
    api_paths::API_DT,
    paging::{
        owned_actor::{OwnedActorsByHost, OwnedActorsByOwner},
        owned_ap_object::OwnedApObjectsByActor,
        public_actor::PublicActorsByHost,
        public_ap_object::{PublicApObjectsByActor, PublicApObjectsByHost},
        user::UsersByHost,
        PagingProps,
    },
};

use dialtone_common::rest::api_paths::path::*;

use crate::api_dt::names_handlers::get_name_available;
use crate::api_dt::sites_handlers::get_site_handler;
use crate::{api_dt::authenticate_handlers::post_authenticate_handler, start_server::AppState};

use super::{
    actor_handlers::{
        default_handlers::put_default_actor_handler,
        owned_handlers::{
            get_owned_actor_handler, post_owned_actor_handler, put_owned_actor_handler,
        },
        public_handlers::get_public_actor_handler,
        sysinfo_handlers::{get_actor_sysinfo_handler, put_actor_sysinfo_handler},
    },
    ap_object_handlers::{
        owned::{
            get_owned_ap_object_handler, post_owned_ap_object_handler, put_owned_ap_object_handler,
        },
        public_handlers::get_ap_object_handler,
        sysdata_handlers::put_ap_object_sysdata_handler,
        sysinfo_handlers::get_ap_object_sysinfo_handler,
        visibility_handler::put_ap_object_visibility_handler,
    },
    page_handlers::{
        owned_actors::{get_all_owned_actors_handler, get_owned_actors_by_owner_handler},
        owned_ap_objects::get_owned_ap_objects_by_actor_handler,
        public_actors::get_public_actors_by_host_handler,
        public_ap_objects::{
            get_public_ap_objects_by_actor_handler, get_public_ap_objects_by_host_handler,
        },
        users::get_users_handler,
    },
    user_handlers::{
        password_handlers::put_user_password_handler,
        prefs_handlers::put_user_prefs_handler,
        status_handlers::put_user_status_handler,
        sysdata_handlers::put_user_sysdata_handler,
        sysinfo_handlers::get_user_sysinfo_handler,
        webuser_handlers::{get_user_handler, post_user_handler},
    },
};

pub fn api_dt_routes() -> (String, Router<AppState>) {
    let router = Router::new()
        // sites
        .route(SITES, get(get_site_handler))
        // users
        .route(USER, get(get_user_handler))
        .route(USER, post(post_user_handler))
        .route(USER__PASSWORD, put(put_user_password_handler))
        .route(USER__STATUS, put(put_user_status_handler))
        .route(USER__PREFS, put(put_user_prefs_handler))
        .route(USER__SYSINFO, get(get_user_sysinfo_handler))
        .route(USER__SYSDATA, put(put_user_sysdata_handler))
        // authenticate
        .route(AUTHENTICATE, post(post_authenticate_handler))
        // actors
        .route(ACTOR, get(get_public_actor_handler))
        .route(ACTOR__OWNED, post(post_owned_actor_handler))
        .route(ACTOR__OWNED, get(get_owned_actor_handler))
        .route(ACTOR__OWNED, put(put_owned_actor_handler))
        .route(ACTOR__DEFAULT, put(put_default_actor_handler))
        .route(ACTOR__SYSINFO, get(get_actor_sysinfo_handler))
        .route(ACTOR__SYSINFO, put(put_actor_sysinfo_handler))
        // names
        .route(NAME, get(get_name_available))
        // ap objects
        .route(AP_OBJECT, get(get_ap_object_handler))
        .route(AP_OBJECT__OWNED, post(post_owned_ap_object_handler))
        .route(AP_OBJECT__OWNED, put(put_owned_ap_object_handler))
        .route(AP_OBJECT__OWNED, get(get_owned_ap_object_handler))
        .route(AP_OBJECT__SYSINFO, get(get_ap_object_sysinfo_handler))
        .route(AP_OBJECT__SYSDATA, put(put_ap_object_sysdata_handler))
        .route(AP_OBJECT__VISIBILITY, put(put_ap_object_visibility_handler))
        // pages
        .route(&UsersByHost::api_dt_path(), get(get_users_handler))
        .route(
            &OwnedActorsByHost::api_dt_path(),
            get(get_all_owned_actors_handler),
        )
        .route(
            &OwnedActorsByOwner::api_dt_path(),
            get(get_owned_actors_by_owner_handler),
        )
        .route(
            &PublicActorsByHost::api_dt_path(),
            get(get_public_actors_by_host_handler),
        )
        .route(
            &PublicApObjectsByActor::api_dt_path(),
            get(get_public_ap_objects_by_actor_handler),
        )
        .route(
            &PublicApObjectsByHost::api_dt_path(),
            get(get_public_ap_objects_by_host_handler),
        )
        .route(
            &OwnedApObjectsByActor::api_dt_path(),
            get(get_owned_ap_objects_by_actor_handler),
        );
    (API_DT.to_string(), router)
}
