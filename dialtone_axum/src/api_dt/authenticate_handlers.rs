use axum::extract::State;
use axum::Json;
use axum_client_ip::ClientIp;
use chrono::Utc;
use jsonwebtoken::{encode, Header};

use dialtone_common::rest::users::user_login::{AuthBody, UserCredential};
use dialtone_sqlx::control::user::authn::authn_user;

use crate::response::response_error::ResponseError;
use crate::start_server::{AppState, Claims, KEYS, UN_KNOWN};

pub async fn post_authenticate_handler(
    State(AppState { pg_pool }): State<AppState>,
    client_ip: Option<ClientIp>,
    Json(login): Json<UserCredential>,
) -> Result<Json<AuthBody>, ResponseError> {
    // Check if the users sent the credentials
    if login.user_acct.is_empty() || login.password.is_empty() {
        return Err(ResponseError::MissingCredentials(None));
    }

    let ip_string = if let Some(ip) = client_ip {
        ip.0.to_string()
    } else {
        UN_KNOWN.to_string()
    };
    tracing::debug!("forwarded_for = {:?}", ip_string);
    let auth_result = authn_user(
        &pg_pool,
        &login.user_acct,
        &login.password,
        &ip_string,
        Utc::now(),
    )
    .await?;
    if !auth_result {
        return Err(ResponseError::WrongCredentials(Some(format!(
            "user {} failed to authenticate",
            login.user_acct
        ))));
    }

    let claims = Claims {
        sub: login.user_acct.to_owned(),
        // TODO make this configurable at some point
        exp: 2019331688,
    };
    // Create the authorization token
    let token = encode(&Header::default(), &claims, &KEYS.encoding).map_err(|err| {
        ResponseError::TokenCreation(Some(format!(
            "encode in post_authenticate_handler: {err:?}",
        )))
    })?;
    tracing::info!("user {} authenticated", login.user_acct);

    // Send the authorized token
    Ok(Json(AuthBody::new(token)))
}
