use axum::extract::{Query, State};
use axum::Json;
use dialtone_common::rest::actors::actor_model::OwnedActor;
use dialtone_common::rest::paging::owned_actor::{OwnedActorsByHost, OwnedActorsByOwner};
use dialtone_common::rest::paging::Page;
use dialtone_sqlx::db::actor::page_owned::page_owned_actors;

use dialtone_sqlx::db::actor::page_owned_by_owner::page_owned_actors_by_owner;

use crate::authz::authz_sysrole::authz_actoradmin;

use crate::authz::authz_user::authz_acct_or_actoradmin;
use crate::err_not_found;
use crate::response::response_error::ResponseError;
use crate::start_server::{AppState, Authnz};

use super::FromVec;

type OwnedActorResponse = Result<Json<Page<OwnedActor>>, ResponseError>;

impl FromVec<OwnedActor> for OwnedActorResponse {
    fn from_vec(page: Option<Vec<OwnedActor>>) -> Self {
        match page {
            Some(vec) => Ok(Json(Page::<OwnedActor>::from(vec))),
            None => err_not_found!(),
        }
    }
}

pub async fn get_all_owned_actors_handler(
    Query(params): Query<OwnedActorsByHost>,
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
) -> OwnedActorResponse {
    authz_actoradmin(&authnz, &params.host_name).await?;
    let page = page_owned_actors(
        &pg_pool,
        params.prev_date.as_ref(),
        params.next_date.as_ref(),
        params.limit,
        Some(&params.host_name),
        params.visibility.as_ref(),
        params.actor_type.as_ref(),
    )
    .await?;
    OwnedActorResponse::from_vec(page)
}

pub async fn get_owned_actors_by_owner_handler(
    Query(params): Query<OwnedActorsByOwner>,
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
) -> OwnedActorResponse {
    authz_acct_or_actoradmin(&params.user_acct_owner, &authnz).await?;
    let page = page_owned_actors_by_owner(
        &pg_pool,
        params.prev_date.as_ref(),
        params.next_date.as_ref(),
        params.limit,
        &params.user_acct_owner,
        params.actor_type.as_ref(),
    )
    .await?;

    #[cfg(debug_assertions)]
    let page: Option<Vec<dialtone_common::rest::actors::actor_model::OwnedActor>> =
        page.map(|v| v.iter().map(crate::dev_util::local_owned_actor).collect());

    OwnedActorResponse::from_vec(page)
}
