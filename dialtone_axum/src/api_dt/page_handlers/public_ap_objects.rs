use axum::{
    extract::{Query, State},
    Json,
};
use dialtone_common::{
    ap::ap_object::ApObject,
    rest::paging::{
        public_ap_object::{PublicApObjectsByActor, PublicApObjectsByHost},
        Page,
    },
};
use dialtone_sqlx::db::ap_object::{
    page_by_actor::page_ap_objects_by_actor, page_by_host::page_ap_objects_by_host,
};

use crate::{err_not_found, response::response_error::ResponseError, start_server::AppState};

type PublicApObjectsResponse = Result<Json<Page<ApObject>>, ResponseError>;

pub async fn get_public_ap_objects_by_actor_handler(
    Query(params): Query<PublicApObjectsByActor>,
    State(AppState { pg_pool }): State<AppState>,
) -> PublicApObjectsResponse {
    let page = page_ap_objects_by_actor(
        &pg_pool,
        params.prev_date.as_ref(),
        params.next_date.as_ref(),
        params.limit,
        &params.actor_id,
        None,
    )
    .await?;
    match page {
        Some(vec) => Ok(Json(vec)),
        None => err_not_found!(),
    }
}

pub async fn get_public_ap_objects_by_host_handler(
    Query(params): Query<PublicApObjectsByHost>,
    State(AppState { pg_pool }): State<AppState>,
) -> PublicApObjectsResponse {
    let page = page_ap_objects_by_host(
        &pg_pool,
        params.prev_date.as_ref(),
        params.next_date.as_ref(),
        params.limit,
        &params.host_name,
    )
    .await?;
    match page {
        Some(vec) => Ok(Json(vec)),
        None => err_not_found!(),
    }
}
