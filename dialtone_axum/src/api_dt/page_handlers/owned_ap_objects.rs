use axum::{
    extract::{Query, State},
    Json,
};
use dialtone_common::rest::{
    ap_objects::ap_object_model::OwnedApObject,
    paging::{owned_ap_object::OwnedApObjectsByActor, Page},
};
use dialtone_sqlx::db::ap_object::page_owned_by_actor::page_owned_ap_objects_by_actor;

use crate::{
    authz::authz_ap_object::authz_owner_or_contentadmin_for_actor,
    err_not_found,
    response::response_error::ResponseError,
    start_server::{AppState, Authnz},
};

use super::FromVec;

type OwnedApObjectsResponse = Result<Json<Page<OwnedApObject>>, ResponseError>;

impl FromVec<OwnedApObject> for OwnedApObjectsResponse {
    fn from_vec(page: Option<Vec<OwnedApObject>>) -> Self {
        match page {
            Some(vec) => Ok(Json(Page::<OwnedApObject>::from(vec))),
            None => err_not_found!(),
        }
    }
}

pub async fn get_owned_ap_objects_by_actor_handler(
    Query(params): Query<OwnedApObjectsByActor>,
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
) -> OwnedApObjectsResponse {
    authz_owner_or_contentadmin_for_actor(&pg_pool, &authnz, &params.actor_id).await?;
    let page = page_owned_ap_objects_by_actor(
        &pg_pool,
        params.prev_date.as_ref(),
        params.next_date.as_ref(),
        params.limit,
        &params.actor_id,
        params.visibility.as_ref(),
    )
    .await?;
    OwnedApObjectsResponse::from_vec(page)
}
