use axum::extract::{Query, State};
use axum::response::IntoResponse;
use axum::Json;

use dialtone_common::rest::paging::user::UsersByHost;
use dialtone_common::rest::paging::Page;
use dialtone_common::rest::users::web_user::WebUser;
use dialtone_sqlx::db::user::page::page_users;

use crate::authz::authz_sysrole::authz_useradmin;
use crate::err_not_found;
use crate::response::response_error::ResponseError;
use crate::start_server::{AppState, Authnz};

use super::FromVec;

type UsersResponse = Result<Json<Page<WebUser>>, ResponseError>;

impl FromVec<WebUser> for UsersResponse {
    fn from_vec(page: Option<Vec<WebUser>>) -> Self {
        match page {
            Some(vec) => Ok(Json(Page::<WebUser>::from(vec))),
            None => err_not_found!(),
        }
    }
}

pub async fn get_users_handler(
    authnz: Authnz,
    Query(params): Query<UsersByHost>,
    State(AppState { pg_pool }): State<AppState>,
) -> Result<impl IntoResponse, ResponseError> {
    authz_useradmin(&authnz, &params.host_name).await?;
    let page = page_users(
        &pg_pool,
        params.prev_date.as_ref(),
        params.next_date.as_ref(),
        params.limit,
        Some(&params.host_name),
    )
    .await?;
    UsersResponse::from_vec(page)
}
