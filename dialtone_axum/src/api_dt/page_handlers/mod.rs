pub mod owned_actors;
pub mod owned_ap_objects;
pub mod public_actors;
pub mod public_ap_objects;
pub mod users;

pub trait FromVec<T> {
    fn from_vec(page: Option<Vec<T>>) -> Self;
}
