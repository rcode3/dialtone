use axum::{
    extract::{Query, State},
    Json,
};
use dialtone_common::rest::{
    actors::actor_model::PublicActor,
    paging::{public_actor::PublicActorsByHost, Page},
};
use dialtone_sqlx::db::actor::page_by_host::page_public_actors_by_host;

use crate::{err_not_found, response::response_error::ResponseError, start_server::AppState};

type PublicActorsResponse = Result<Json<Page<PublicActor>>, ResponseError>;

pub async fn get_public_actors_by_host_handler(
    Query(params): Query<PublicActorsByHost>,
    State(AppState { pg_pool }): State<AppState>,
) -> PublicActorsResponse {
    let page = page_public_actors_by_host(
        &pg_pool,
        params.prev_date.as_ref(),
        params.next_date.as_ref(),
        params.limit,
        &params.host_name,
        params.actor_type.as_ref(),
    )
    .await?;
    match page {
        Some(page) => Ok(Json(page)),
        None => err_not_found!(),
    }
}
