use axum::extract::State;
use axum::response::IntoResponse;
use axum::Json;

use dialtone_common::rest::users::user_exchanges::PutUserPreferences;
use dialtone_sqlx::db::user::update_prefs::update_user_prefs;

use crate::authz::authz_user::authz_acct_or_useradmin;
use crate::response::response_error::ResponseError;
use crate::start_server::{AppState, Authnz};
use crate::{ok_failure, ok_success};

pub async fn put_user_prefs_handler(
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
    Json(request): Json<PutUserPreferences>,
) -> Result<impl IntoResponse, ResponseError> {
    authz_acct_or_useradmin(&request.acct, &authnz).await?;
    let changed = update_user_prefs(&pg_pool, &request.acct, request.preferences).await?;
    if changed.is_some() {
        ok_success!()
    } else {
        ok_failure!()
    }
}
