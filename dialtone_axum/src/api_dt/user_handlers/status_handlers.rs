use axum::extract::State;
use axum::response::IntoResponse;
use axum::Json;

use dialtone_common::rest::users::user_exchanges::PutUserStatus;
use dialtone_common::utils::parse_acct::parse_user_acct;
use dialtone_sqlx::db::user::change_status::change_user_status;

use crate::authz::authz_sysrole::authz_useradmin;
use crate::response::response_error::ResponseError;
use crate::start_server::{AppState, Authnz};
use crate::{ok_failure, ok_success};

pub async fn put_user_status_handler(
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
    Json(request): Json<PutUserStatus>,
) -> Result<impl IntoResponse, ResponseError> {
    let user_host = parse_user_acct(&request.acct)?;
    authz_useradmin(&authnz, &user_host.host_name).await?;
    let changed = change_user_status(&pg_pool, &request.acct, &request.status).await?;
    if changed.is_some() {
        ok_success!()
    } else {
        ok_failure!()
    }
}
