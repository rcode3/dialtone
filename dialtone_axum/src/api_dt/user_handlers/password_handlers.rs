use axum::extract::State;
use axum::response::IntoResponse;
use axum::Json;

use dialtone_common::rest::users::user_exchanges::PutUserPassword;
use dialtone_sqlx::control::user::change_auth::change_user_auth;

use crate::authz::authz_user::authz_acct_or_useradmin;
use crate::response::response_error::ResponseError;
use crate::start_server::{AppState, Authnz};
use crate::{ok_failure, ok_success};

pub async fn put_user_password_handler(
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
    Json(request): Json<PutUserPassword>,
) -> Result<impl IntoResponse, ResponseError> {
    authz_acct_or_useradmin(&request.acct, &authnz).await?;
    let changed = change_user_auth(&pg_pool, &request.acct, &request.password).await?;
    if changed {
        ok_success!()
    } else {
        ok_failure!()
    }
}
