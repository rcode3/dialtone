use crate::authz::authz_user::authz_acct_or_useradmin;
use crate::response::response_error::ResponseError;
use crate::start_server::{AppState, Authnz};
use crate::{err_bad_request, err_not_found, err_unauthorized};
use axum::extract::{Query, State};
use axum::response::IntoResponse;
use axum::Json;
use dialtone_common::ap::pun::is_valid_pun;
use dialtone_common::rest::sites::site_data::RegistrationMethod;
use dialtone_common::rest::users::user_exchanges::{GetUser, PostUser, PostUserResponse};
use dialtone_common::rest::users::web_user::{SystemRoleType, UserStatus};
use dialtone_common::utils::make_acct::make_acct;
use dialtone_common::utils::parse_acct::parse_user_acct;
use dialtone_sqlx::control::user::check_name_availability::check_user_name_availability;
use dialtone_sqlx::control::user::create_with_actor::create_user_with_default_actor;
use dialtone_sqlx::db::site_info::fetch_site;
use dialtone_sqlx::db::user::change_status::change_user_status;
use dialtone_sqlx::db::user::fetch_info::fetch_user_info;
use sqlx::{Pool, Postgres};
use tokio::time::{sleep, Duration};

pub async fn get_user_handler(
    authnz: Authnz,
    Query(params): Query<GetUser>,
    State(AppState { pg_pool }): State<AppState>,
) -> impl IntoResponse {
    authz_acct_or_useradmin(&params.acct, &authnz).await?;
    let user = fetch_user_info(&pg_pool, &params.acct).await?;

    #[cfg(debug_assertions)]
    let user = user.map(|u| crate::dev_util::local_user(&u));

    user.map_or_else(|| err_unauthorized!(), |v| Ok(Json(v)))
}

pub async fn post_user_handler(
    State(AppState { pg_pool }): State<AppState>,
    Json(request): Json<PostUser>,
) -> impl IntoResponse {
    // a human will not care about this. a spam bot would.
    sleep(Duration::from_millis(750)).await;
    let user_host = match request {
        PostUser::OpenRegistration(ref user_creds) => parse_user_acct(&user_creds.user_acct),
        PostUser::SimpleCode {
            ref user_creds,
            simple_code_for_registration: _,
        } => parse_user_acct(&user_creds.user_acct),
    };
    if let Ok(acct_parts) = user_host {
        // since this code path creates a default actors,
        // the names needs to be checked.
        if !is_valid_pun(&acct_parts.user_name, false) {
            return err_bad_request!();
        }

        // check that the sites allows registration
        let site_info = fetch_site(&pg_pool, &acct_parts.host_name).await?;
        if let Some(site_info) = site_info {
            match request {
                PostUser::OpenRegistration(user_creds) => {
                    if site_info
                        .site_data
                        .public
                        .registration_methods
                        .contains(&RegistrationMethod::Open)
                    {
                        let system_roles = site_info
                            .site_data
                            .open_registration_system_roles_set()
                            .map_err(|_| {
                                crate::response::response_error::ResponseError::InternalServerError(
                                    None,
                                )
                            })?;
                        let auto_approve = site_info.site_data.is_open_registration_auto_approved();
                        do_registration(
                            &pg_pool,
                            &acct_parts.user_name,
                            &acct_parts.host_name,
                            &user_creds.password,
                            system_roles,
                            auto_approve,
                        )
                        .await
                    } else {
                        err_bad_request!()
                    }
                }
                PostUser::SimpleCode {
                    user_creds,
                    simple_code_for_registration,
                } => {
                    if site_info
                        .site_data
                        .public
                        .registration_methods
                        .contains(&RegistrationMethod::SimpleCode)
                    {
                        let system_roles = site_info
                            .site_data
                            .simple_code_registration_system_roles_set(
                                &simple_code_for_registration,
                            )
                            .map_err(|_| {
                                crate::response::response_error::ResponseError::UnableToAuthorize(
                                    None,
                                )
                            })?;
                        let auto_approve = site_info
                            .site_data
                            .is_simple_code_registration_auto_approved(
                                &simple_code_for_registration,
                            );
                        do_registration(
                            &pg_pool,
                            &acct_parts.user_name,
                            &acct_parts.host_name,
                            &user_creds.password,
                            system_roles,
                            auto_approve,
                        )
                        .await
                    } else {
                        err_bad_request!()
                    }
                }
            }
        } else {
            err_not_found!()
        }
    } else {
        err_bad_request!()
    }
}

async fn do_registration(
    pool: &Pool<Postgres>,
    user_name: &str,
    host_name: &str,
    password: &str,
    system_roles: &[SystemRoleType],
    auto_approve: bool,
) -> Result<impl IntoResponse, ResponseError> {
    // make sure the user is not already registered
    let name_available = check_user_name_availability(pool, user_name, host_name).await?;
    if !name_available {
        return err_unauthorized!();
    }

    create_user_with_default_actor(pool, user_name, host_name, password, system_roles).await?;
    if auto_approve {
        change_user_status(pool, &make_acct(user_name, host_name), &UserStatus::Active).await?;
        Ok(Json(PostUserResponse::AccountRegistered(None)))
    } else {
        Ok(Json(PostUserResponse::AccountNeedsApproval(None)))
    }
}
