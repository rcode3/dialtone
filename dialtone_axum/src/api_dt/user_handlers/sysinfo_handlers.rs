use axum::extract::{Query, State};
use axum::response::IntoResponse;
use axum::Json;

use dialtone_common::rest::users::user_exchanges::GetUser;
use dialtone_common::utils::parse_acct::parse_user_acct;
use dialtone_sqlx::db::user::fetch_sysinfo::fetch_user_system_info;

use crate::authz::authz_sysrole::authz_useradmin;
use crate::err_not_found;
use crate::response::response_error::ResponseError;
use crate::start_server::{AppState, Authnz};

pub async fn get_user_sysinfo_handler(
    Query(params): Query<GetUser>,
    State(AppState { pg_pool }): State<AppState>,
    authnz: Authnz,
) -> Result<impl IntoResponse, ResponseError> {
    let user_host = parse_user_acct(&params.acct)?;
    authz_useradmin(&authnz, &user_host.host_name).await?;
    let sysinfo = fetch_user_system_info(&pg_pool, &params.acct).await?;
    match sysinfo {
        None => err_not_found!(),
        Some(v) => Ok(Json(v)),
    }
}
