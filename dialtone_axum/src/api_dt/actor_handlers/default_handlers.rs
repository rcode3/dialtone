use axum::{extract::State, response::IntoResponse, Json};
use dialtone_common::rest::{
    actors::actor_exchanges::PutActorRequest, simple_exchanges::SimpleResponse,
};
use dialtone_sqlx::control::actor::change_default::change_default_actor;

use crate::{
    authz::authz_actor::authz_owner_or_actoradmin_for_actor,
    response::response_error::ResponseError,
    start_server::{AppState, Authnz},
};

pub async fn put_default_actor_handler(
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
    Json(request): Json<PutActorRequest>,
) -> Result<impl IntoResponse, ResponseError> {
    authz_owner_or_actoradmin_for_actor(&pg_pool, &authnz, &request.actor_id).await?;
    change_default_actor(&pg_pool, &authnz.claims.sub, &request.actor_id)
        .await?
        .map(|_| Json(SimpleResponse::success()))
        .ok_or_else(ResponseError::not_found)
}
