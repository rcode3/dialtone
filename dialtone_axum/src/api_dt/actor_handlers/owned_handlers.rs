use axum::extract::{Query, State};
use axum::response::IntoResponse;
use axum::Json;

use dialtone_common::rest::simple_exchanges::SimpleResponse;
use dialtone_common::utils::parse_acct::parse_user_acct;
use dialtone_sqlx::db::actor::fetch_public::fetch_public_actor_by_wf_acct;
use dialtone_sqlx::db::actor::update_ap::update_actor_ap;

use dialtone_common::ap::pun::is_valid_pun;
use dialtone_common::rest::actors::actor_exchanges::{
    GetActorRequest, GetActorType, NewActorRequest, PutUpdateActorRequest,
};

use dialtone_sqlx::control::actor::create_owned::create_credentialed_actor;
use dialtone_sqlx::db::actor::fetch_owned::fetch_owned_actor_by_id;
use dialtone_sqlx::logic::actor::new_actor::NewActor;

use crate::authz::authz_actor::authz_owner_or_actoradmin_for_actor;

use crate::response::response_error::ResponseError;
use crate::start_server::{AppState, Authnz};
use crate::{err_bad_request, err_not_found};

/// Creates an actors.
pub async fn post_owned_actor_handler(
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
    Json(new_actor_request): Json<NewActorRequest>,
) -> Result<impl IntoResponse, ResponseError> {
    let user_host = parse_user_acct(&authnz.claims.sub)?;
    if !is_valid_pun(&new_actor_request.preferred_user_name, false) {
        return err_bad_request!();
    }

    let create_new_actor = NewActor::builder()
        .preferred_user_name(new_actor_request.preferred_user_name)
        .actor_type(new_actor_request.actor_type)
        .owner(authnz.claims.sub)
        .and_name(new_actor_request.name)
        .and_summary(new_actor_request.summary)
        .build();
    let actor = create_credentialed_actor(&pg_pool, create_new_actor, false, &user_host.host_name)
        .await?
        .ok_or(ResponseError::BadRequest(None))?;
    Ok(Json(actor.owned_actor))
}

/// Update an actor.
pub async fn put_owned_actor_handler(
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
    Json(request): Json<PutUpdateActorRequest>,
) -> Result<impl IntoResponse, ResponseError> {
    authz_owner_or_actoradmin_for_actor(&pg_pool, &authnz, &request.actor_id).await?;
    update_actor_ap(&pg_pool, request.update, &request.actor_id)
        .await?
        .map(|_| Json(SimpleResponse::success()))
        .ok_or_else(ResponseError::not_found)
}

pub async fn get_owned_actor_handler(
    Query(params): Query<GetActorRequest>,
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
) -> Result<impl IntoResponse, ResponseError> {
    let actor_id = match params.get_type()? {
        GetActorType::ActorId(actor_id) => actor_id,
        GetActorType::WfAcct(wf_acct) => {
            let actor = fetch_public_actor_by_wf_acct(&pg_pool, &wf_acct).await?;
            if let Some(actor) = actor {
                actor.ap.id
            } else {
                return err_not_found!();
            }
        }
    };
    authz_owner_or_actoradmin_for_actor(&pg_pool, &authnz, &actor_id).await?;
    let actor = fetch_owned_actor_by_id(&pg_pool, &actor_id).await?;

    #[cfg(debug_assertions)]
    let actor = actor.map(|a| crate::dev_util::local_owned_actor(&a));

    match actor {
        None => err_not_found!(),
        Some(_) => Ok(Json(actor)),
    }
}
