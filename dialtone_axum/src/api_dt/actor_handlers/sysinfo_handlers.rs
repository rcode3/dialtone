use axum::{
    extract::{Query, State},
    response::IntoResponse,
    Json,
};
use dialtone_common::rest::{
    actors::{
        actor_exchanges::{GetActorRequest, GetActorType},
        actor_model::ActorSystemInfo,
    },
    simple_exchanges::SimpleResponse,
};
use dialtone_sqlx::db::actor::{
    fetch_public::fetch_public_actor_by_wf_acct, fetch_sysinfo::fetch_actor_system_info,
    update_sysinfo::update_actor_system_info,
};

use crate::{
    authz::authz_actor::authz_actoradmin_for_actor,
    err_not_found,
    response::response_error::ResponseError,
    start_server::{AppState, Authnz},
};

pub async fn get_actor_sysinfo_handler(
    authnz: Authnz,
    Query(params): Query<GetActorRequest>,
    State(AppState { pg_pool }): State<AppState>,
) -> Result<impl IntoResponse, ResponseError> {
    let actor_id = match params.get_type()? {
        GetActorType::ActorId(actor_id) => actor_id,
        GetActorType::WfAcct(wf_acct) => {
            let actor = fetch_public_actor_by_wf_acct(&pg_pool, &wf_acct).await?;
            if let Some(actor) = actor {
                actor.ap.id
            } else {
                return err_not_found!();
            }
        }
    };
    authz_actoradmin_for_actor(&authnz, &actor_id).await?;
    fetch_actor_system_info(&pg_pool, &actor_id)
        .await?
        .map(Json)
        .ok_or_else(ResponseError::not_found)
}

pub async fn put_actor_sysinfo_handler(
    authnz: Authnz,
    State(AppState { pg_pool }): State<AppState>,
    Json(request): Json<ActorSystemInfo>,
) -> Result<impl IntoResponse, ResponseError> {
    authz_actoradmin_for_actor(&authnz, &request.actor_id).await?;
    update_actor_system_info(&pg_pool, request)
        .await?
        .map(|_| Json(SimpleResponse::success()))
        .ok_or_else(ResponseError::not_found)
}
