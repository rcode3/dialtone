use axum::{
    extract::{Host, Query, State},
    Json,
};
use dialtone_common::{
    rest::actors::{
        actor_exchanges::{GetActorRequest, GetActorType},
        actor_model::PublicActor,
    },
    webfinger::normalize_acct,
    webfinger::Jrd,
};
use dialtone_sqlx::{
    control::actor::create::create_actor,
    db::{
        actor::fetch_public::{fetch_public_actor_by_id, fetch_public_actor_by_wf_acct},
        persistent_queue::{insert_job::insert_job, JobDbType},
    },
    logic::persistent_queue::job_details::{FetchRemoteIconJobDetails, JobDetails},
};
use sqlx::{Pool, Postgres};
use tracing::debug;

use crate::{
    client::{ap::fetch::fetch_ap_actor, webfinger::fetch::fetch_wf_acct},
    err_not_found,
    response::response_error::ResponseError,
    start_server::{AppState, Authnz},
};

type PublicActorResponse = Result<Json<Option<PublicActor>>, ResponseError>;

/// This function acts a search for an actor.
/// It attempts to look it up in the database.
/// If the actor is not found in the database, then it will attempt to find the actor
/// on another host. Because this reaches out to other hosts, it requires
/// a user to be logged in (hence requiring Claims).
pub async fn get_public_actor_handler(
    Query(params): Query<GetActorRequest>,
    Host(host): Host,
    State(AppState { pg_pool }): State<AppState>,
    authnz: Authnz,
) -> PublicActorResponse {
    match params.get_type()? {
        GetActorType::ActorId(actor_id) => {
            get_public_actor_by_actor_id(actor_id, pg_pool, host, authnz).await
        }
        GetActorType::WfAcct(wf_acct) => {
            get_public_actor_by_wf_acct(wf_acct, pg_pool, host, authnz).await
        }
    }
}

async fn get_public_actor_by_actor_id(
    actor_id: String,
    pool: Pool<Postgres>,
    host_name: String,
    authnz: Authnz,
) -> PublicActorResponse {
    let actor = fetch_public_actor_by_id(&pool, &actor_id).await?;
    if let Some(actor) = actor {
        Ok(Json(Some(actor)))
    } else {
        fetch_remote_ap_actor(pool, &actor_id, None, host_name, authnz).await
    }
}

async fn get_public_actor_by_wf_acct(
    wf_acct: String,
    pool: Pool<Postgres>,
    host_name: String,
    authnz: Authnz,
) -> PublicActorResponse {
    let acct = normalize_acct(&wf_acct);
    let actor = fetch_public_actor_by_wf_acct(&pool, &acct).await?;
    if let Some(actor) = actor {
        Ok(Json(Some(actor)))
    } else {
        let wf = fetch_wf_acct(&acct, None).await;
        debug!("Webfinger Fetch: {wf:?}");
        if let Ok(wf) = wf {
            if let Some(ap_url) = wf.ap_url() {
                fetch_remote_ap_actor(pool, &ap_url, Some(wf), host_name, authnz).await
            } else {
                err_not_found!()
            }
        } else {
            err_not_found!()
        }
    }
}

async fn fetch_remote_ap_actor(
    pool: Pool<Postgres>,
    ap_url: &str,
    jrd: Option<Jrd>,
    host_name: String,
    authnz: Authnz,
) -> PublicActorResponse {
    let fetch = fetch_ap_actor(ap_url).await;
    debug!("AP Actor fetch: {fetch:?}");
    if let Ok(actor) = fetch {
        let public_actor = PublicActor {
            ap: actor.clone(),
            jrd,
        };
        let public_actor = create_actor(&pool, public_actor).await?;
        let job_details = JobDetails::FetchRemoteIcon(FetchRemoteIconJobDetails {
            actor,
            fetched_for_user: Some(authnz.claims.sub.clone()),
            host_name,
        });
        insert_job(
            &pool,
            &JobDbType::FetchRemoteIcon,
            &job_details,
            Some(3),
            None,
        )
        .await?;
        Ok(Json(Some(public_actor)))
    } else {
        err_not_found!()
    }
}
