use dialtone_common::utils::media_types::{ACTIVITY_PUB_MEDIA_TYPE, JSON_TYPE};
use http::HeaderMap;

use crate::{err_unsupported_media, response::response_error::ResponseError};

pub mod actor_handlers;
pub mod collection_handlers;
pub mod fetch_actor;
pub mod routes;
pub mod shared_inbox_handlers;

pub fn content_is_activity_pub(headers: &HeaderMap) -> Result<(), ResponseError> {
    if let Some(accepts) = headers.get(http::header::CONTENT_TYPE) {
        if let Ok(accepted) = accepts.to_str() {
            if accepted.contains(ACTIVITY_PUB_MEDIA_TYPE) || accepted.contains(JSON_TYPE) {
                return Ok(());
            }
        }
    }
    // else
    err_unsupported_media!()
}
