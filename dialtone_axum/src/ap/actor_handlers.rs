use axum::{
    extract::{Host, OriginalUri, Path, State},
    response::IntoResponse,
    Json,
};
use dialtone_common::{ap::pun::is_valid_pun, utils::media_types::ACTIVITY_PUB_MEDIA_TYPE};
use dialtone_sqlx::db::actor::fetch_public::fetch_ap_actor_by_id;
use hyper::header;

use crate::{err_bad_request, err_not_found, start_server::AppState};

pub async fn get_ap_actor_handler(
    Path(pun): Path<String>,
    OriginalUri(uri): OriginalUri,
    Host(host): Host,
    State(AppState { pg_pool }): State<AppState>,
) -> impl IntoResponse {
    if !is_valid_pun(&pun, true) {
        return err_bad_request!();
    }
    let actor_id = format!("https://{host}{uri}");

    #[cfg(debug_assertions)]
    let actor_id = if host.contains("0.0.0.0") {
        format!("https://localhost{uri}")
    } else {
        actor_id
    };

    let actor = fetch_ap_actor_by_id(&pg_pool, &actor_id).await?;
    match actor {
        None => err_not_found!(),
        Some(actor) => Ok((
            [(header::CONTENT_TYPE, ACTIVITY_PUB_MEDIA_TYPE)],
            Json(actor),
        )),
    }
}
