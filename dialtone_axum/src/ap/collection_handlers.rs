use axum::{
    body::Bytes,
    extract::{Host, OriginalUri, Path, State},
    response::IntoResponse,
    Json,
};
use dialtone_common::{
    ap::{
        activity::{Activity, ActivityObjectType},
        id::{create_actor_id, is_valid_collection_name, StandardCollectionNames},
        pun::is_valid_pun,
        ActivityPubError,
    },
    rest::collections::collection_model::CollectionVisibilityType,
    utils::media_types::ACTIVITY_PUB_MEDIA_TYPE,
};
use dialtone_sqlx::{
    control::ap_object::{deliver::deliver, send::send},
    db::{actor::fetch_owned::fetch_owned_actor_by_id, collection::page::page_by_id},
    logic::ap_object::from_owner::ap_object_from_owner,
};
use http::{HeaderMap, Method, StatusCode};
use hyper::header;

use crate::{
    authz::authz_ap_object::authz_owner_or_contentadmin_for_actor,
    err_bad_request, err_not_found, err_unauthorized,
    http_signature::verify::verify_draft12,
    response::response_error::ResponseError,
    start_server::{AppState, Authnz},
};

use super::{content_is_activity_pub, fetch_actor::fetch_public_actor_by_actor_id};

pub async fn get_collection_handler(
    Path((pun, collection_name)): Path<(String, String)>,
    OriginalUri(uri): OriginalUri,
    Host(host): Host,
    State(AppState { pg_pool }): State<AppState>,
    authnz: Option<Authnz>,
) -> impl IntoResponse {
    if !is_valid_pun(&pun, true) {
        return err_bad_request!(ActivityPubError::InvalidPreferredUserName);
    }
    if !is_valid_collection_name(&collection_name, true) {
        return err_bad_request!(ActivityPubError::InvalidCollectionName);
    }
    // TODO need to be able to get any public collection (collection in _public_collections)
    if collection_name.eq(StandardCollectionNames::Inbox.into()) {
        if let Some(authnz) = authnz {
            let actor_id = create_actor_id(&host, &pun);
            authz_owner_or_contentadmin_for_actor(&pg_pool, &authnz, &actor_id).await?;
        } else {
            return err_unauthorized!();
        }
    }

    let collection_id = format!("https://{host}{uri}");

    #[cfg(debug_assertions)]
    let collection_id = if host.contains("0.0.0.0") {
        format!("https://localhost{uri}")
    } else {
        collection_id
    };

    let collection = page_by_id(
        &pg_pool,
        None,
        None,
        1000, // TODO this should be configurable or something.
        &collection_id,
        Some(&CollectionVisibilityType::Visible),
    )
    .await?;
    match collection {
        None => err_not_found!(),
        Some(collection) => Ok((
            [(header::CONTENT_TYPE, ACTIVITY_PUB_MEDIA_TYPE)],
            Json(collection),
        )),
    }
}

pub async fn post_collection_handler(
    Host(host): Host,
    Path((pun, collection_name)): Path<(String, String)>,
    OriginalUri(uri): OriginalUri,
    State(AppState { pg_pool }): State<AppState>,
    authnz: Option<Authnz>,
    headers: HeaderMap,
    body: Bytes,
) -> Result<impl IntoResponse, ResponseError> {
    #[cfg(debug_assertions)]
    let host = if host.contains("0.0.0.0") {
        "localhost".to_string()
    } else {
        host
    };

    if !is_valid_pun(&pun, true) {
        return err_bad_request!(ActivityPubError::InvalidPreferredUserName);
    }
    if !is_valid_collection_name(&collection_name, true) {
        return err_bad_request!(ActivityPubError::InvalidCollectionName);
    }
    if let Some(authnz) = authnz.as_ref() {
        let actor_id = create_actor_id(&host, &pun);
        authz_owner_or_contentadmin_for_actor(&pg_pool, authnz, &actor_id).await?;
    }
    if !collection_name.eq(StandardCollectionNames::Inbox.into()) && authnz.is_none() {
        // return unauthorized if somebody else is delivering to anything but Inbox
        return err_unauthorized!();
    }

    content_is_activity_pub(&headers)?;
    let activity = serde_json::from_slice::<Activity>(&body)?;

    let ap_object = if let ActivityObjectType::ApObject(object) = activity.object {
        object
    } else {
        return err_bad_request!(ActivityPubError::UnsupportedObjectInActivity);
    };
    let ap_object_id = if let Some(id) = ap_object.id.as_ref() {
        id
    } else {
        return err_bad_request!(ActivityPubError::MissingActivityPubId);
    };

    let collection_id = format!("https://{host}{uri}");

    if authnz.is_none() {
        // if somebody else delivering into inbox
        let sender =
            fetch_public_actor_by_actor_id(&pg_pool, activity.actor_id, host.clone()).await?;
        if let Some(sender) = sender {
            verify_draft12(
                &Method::POST,
                &uri,
                &headers,
                &sender.public_key.public_key_pem,
                Some(&body),
            )?;
            deliver(&pg_pool, &ap_object, &sender, &host, Some(&collection_id)).await?;
            Ok((StatusCode::OK, [(header::LOCATION, ap_object_id)]).into_response())
        } else {
            err_unauthorized!()
        }
    } else {
        let actor_id = create_actor_id(&host, &pun);
        let ap_object = ap_object_from_owner(&host, &pun, &actor_id, *ap_object)?;
        if collection_name.eq(StandardCollectionNames::Outbox.into()) {
            // TODO maybe changes this so the actor is fetched when the activity is queued (one db call)
            let actor = fetch_owned_actor_by_id(&pg_pool, &actor_id)
                .await?
                .ok_or_else(|| {
                    ResponseError::InternalServerError(Some("cannot get actor".to_string()))
                })?;
            send(&pg_pool, &ap_object, &actor.ap).await?;
        } else {
            todo!()
        }
        Ok((
            StatusCode::CREATED,
            [(header::LOCATION, ap_object.id.unwrap())],
        )
            .into_response())
    }
}
