use axum::{
    routing::{get, post},
    Router,
};

use crate::start_server::AppState;

use super::{
    actor_handlers::get_ap_actor_handler,
    collection_handlers::{get_collection_handler, post_collection_handler},
    shared_inbox_handlers::post_shared_inbox_handler,
};
pub fn ap_routes() -> (String, Router<AppState>) {
    let router = Router::new()
        .route("/p/:pun", get(get_ap_actor_handler))
        .route("/g/:pun", get(get_ap_actor_handler))
        .route("/o/:pun", get(get_ap_actor_handler))
        .route("/s/:pun", get(get_ap_actor_handler))
        .route("/a/:pun", get(get_ap_actor_handler))
        .route(
            "/p/:pun/collection/:collection_name",
            get(get_collection_handler),
        )
        .route(
            "/g/:pun/collection/:collection_name",
            get(get_collection_handler),
        )
        .route(
            "/o/:pun/collection/:collection_name",
            get(get_collection_handler),
        )
        .route(
            "/s/:pun/collection/:collection_name",
            get(get_collection_handler),
        )
        .route(
            "/a/:pun/collection/:collection_name",
            get(get_collection_handler),
        )
        .route(
            "/p/:pun/collection/:collection_name",
            post(post_collection_handler),
        )
        .route(
            "/g/:pun/collection/:collection_name",
            post(post_collection_handler),
        )
        .route(
            "/o/:pun/collection/:collection_name",
            post(post_collection_handler),
        )
        .route(
            "/s/:pun/collection/:collection_name",
            post(post_collection_handler),
        )
        .route(
            "/a/:pun/collection/:collection_name",
            post(post_collection_handler),
        )
        .route("/sharedInbox", post(post_shared_inbox_handler));
    ("/pub/".to_string(), router)
}
