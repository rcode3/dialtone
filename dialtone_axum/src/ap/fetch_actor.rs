use dialtone_common::{ap::actor::Actor, rest::actors::actor_model::PublicActor};
use dialtone_sqlx::{
    control::actor::create::create_actor,
    db::{
        actor::fetch_public::fetch_public_actor_by_id,
        persistent_queue::{insert_job::insert_job, JobDbType},
    },
    logic::persistent_queue::job_details::{FetchRemoteIconJobDetails, JobDetails},
};
use sqlx::{Pool, Postgres};
use tracing::debug;

use crate::{
    client::ap::fetch::fetch_ap_actor, err_not_found, response::response_error::ResponseError,
};

type ActorResponse = Result<Option<Actor>, ResponseError>;

pub async fn fetch_public_actor_by_actor_id(
    pool: &Pool<Postgres>,
    actor_id: String,
    host_name: String,
) -> ActorResponse {
    let actor = fetch_public_actor_by_id(pool, &actor_id).await?;
    if let Some(actor) = actor {
        Ok(Some(actor.ap))
    } else {
        fetch_remote_ap_actor(pool, &actor_id, host_name).await
    }
}

async fn fetch_remote_ap_actor(
    pool: &Pool<Postgres>,
    ap_url: &str,
    host_name: String,
) -> ActorResponse {
    let fetch = fetch_ap_actor(ap_url).await;
    debug!("AP Actor fetch: {fetch:?}");
    if let Ok(actor) = fetch {
        let public_actor = PublicActor {
            ap: actor.clone(),
            jrd: None,
        };
        let public_actor = create_actor(pool, public_actor).await?;
        let job_details = JobDetails::FetchRemoteIcon(FetchRemoteIconJobDetails {
            actor,
            fetched_for_user: None,
            host_name,
        });
        insert_job(
            pool,
            &JobDbType::FetchRemoteIcon,
            &job_details,
            Some(3),
            None,
        )
        .await?;
        Ok(Some(public_actor.ap))
    } else {
        err_not_found!()
    }
}
