use axum::{
    body::Bytes,
    extract::{Host, OriginalUri, State},
    response::IntoResponse,
};
use dialtone_common::ap::{
    activity::{Activity, ActivityObjectType},
    ActivityPubError,
};
use dialtone_sqlx::control::ap_object::deliver::deliver;
use http::{HeaderMap, Method, StatusCode};

use crate::{
    err_bad_request, err_unauthorized, http_signature::verify::verify_draft12,
    start_server::AppState,
};

use super::{content_is_activity_pub, fetch_actor::fetch_public_actor_by_actor_id};

pub async fn post_shared_inbox_handler(
    Host(host): Host,
    OriginalUri(uri): OriginalUri,
    State(AppState { pg_pool }): State<AppState>,
    headers: HeaderMap,
    body: Bytes,
) -> impl IntoResponse {
    content_is_activity_pub(&headers)?;
    let activity = serde_json::from_slice::<Activity>(&body)?;

    let ap_object = if let ActivityObjectType::ApObject(object) = activity.object {
        object
    } else {
        return err_bad_request!(ActivityPubError::UnsupportedObjectInActivity);
    };
    let _ap_object_id = if let Some(id) = ap_object.id.as_ref() {
        id
    } else {
        return err_bad_request!(ActivityPubError::MissingActivityPubId);
    };

    #[cfg(debug_assertions)]
    let host = if host.contains("0.0.0.0") {
        "localhost".to_string()
    } else {
        host
    };

    let sender = fetch_public_actor_by_actor_id(&pg_pool, activity.actor_id, host.clone()).await?;
    if let Some(sender) = sender {
        verify_draft12(
            &Method::POST,
            &uri,
            &headers,
            &sender.public_key.public_key_pem,
            Some(&body),
        )?;
        deliver(&pg_pool, &ap_object, &sender, &host, None).await?;
        Ok(StatusCode::OK)
    } else {
        err_unauthorized!()
    }
}
