use std::{
    env::{self, VarError},
    error::Error,
    fmt::Display,
    future::Future,
    ops::Add,
    path::{Path, PathBuf},
};

use dialtone_common::ap::ap_object::ApObjectMediaType;
use dialtone_sqlx::DbError;
use lazy_static::lazy_static;
use percent_encoding::*;
use ril::prelude::*;
use tokio::fs::{create_dir_all, metadata, read_dir, DirEntry};

const MEDIA_DIR_VAR: &str = "MEDIA";
const LOCAL_DIR: &str = "_local";
const REMOTE_DIR: &str = "_remote";

lazy_static! {
    pub static ref MEDIA_DIR: Result<String, VarError> = env::var(MEDIA_DIR_VAR);
}

#[derive(Debug)]
pub enum MediaError {
    MediaEnvironmentVariableNotResolved,
    MediaPathInvalid,
    MediaIoError,
    DatabaseError,
    ErrorWhileVisiting,
}

impl Error for MediaError {}

impl Display for MediaError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            MediaError::MediaEnvironmentVariableNotResolved => {
                f.write_str("Media Error: MEDIA environment variable not set")
            }
            MediaError::MediaPathInvalid => f.write_str("Media Error: Invalid Path to Media"),
            MediaError::MediaIoError => f.write_str("Media Error: I/O Error"),
            MediaError::DatabaseError => f.write_str("Media Error: Database Error"),
            MediaError::ErrorWhileVisiting => f.write_str("Media Error: Closure Visitation Error"),
        }
    }
}

impl From<std::io::Error> for MediaError {
    fn from(_: std::io::Error) -> Self {
        MediaError::MediaIoError
    }
}

impl From<DbError> for MediaError {
    fn from(_: DbError) -> Self {
        MediaError::DatabaseError
    }
}

impl From<sqlx::Error> for MediaError {
    fn from(_: sqlx::Error) -> Self {
        MediaError::DatabaseError
    }
}

impl From<anyhow::Error> for MediaError {
    fn from(_: anyhow::Error) -> Self {
        MediaError::ErrorWhileVisiting
    }
}

fn local_media_path(host_name: &str, pun: &str) -> Result<String, MediaError> {
    let media_dir = MEDIA_DIR
        .as_deref()
        .map_err(|_| MediaError::MediaEnvironmentVariableNotResolved)?;
    let pathbuf: PathBuf = [media_dir, LOCAL_DIR, host_name, pun].iter().collect();
    let path = pathbuf.to_str().ok_or(MediaError::MediaPathInvalid)?;
    Ok(path.to_string())
}

fn remote_media_path(actor_id: &str) -> Result<PathBuf, MediaError> {
    let media_dir = MEDIA_DIR
        .as_deref()
        .map_err(|_| MediaError::MediaEnvironmentVariableNotResolved)?;
    let actor_id = utf8_percent_encode(actor_id, NON_ALPHANUMERIC).to_string();
    let pathbuf: PathBuf = [media_dir, REMOTE_DIR, &actor_id].iter().collect();
    Ok(pathbuf)
}

pub async fn create_local_media_dir(host_name: &str, pun: &str) -> Result<(), MediaError> {
    let path = local_media_path(host_name, pun)?;
    create_dir_all(path).await?;
    Ok(())
}

pub async fn create_remote_media_dir(actor_id: &str) -> Result<PathBuf, MediaError> {
    let path = remote_media_path(actor_id)?;
    create_dir_all(&path).await?;
    Ok(path)
}

pub async fn local_media_dir_exists(host_name: &str, pun: &str) -> Result<bool, MediaError> {
    let path = local_media_path(host_name, pun)?;
    let result = metadata(path).await;
    if result.is_err() {
        Ok(false)
    } else {
        Ok(true)
    }
}

pub async fn remote_media_dir_exists(actor_id: &str) -> Result<bool, MediaError> {
    let path = remote_media_path(actor_id)?;
    let result = metadata(path).await;
    if result.is_err() {
        Ok(false)
    } else {
        Ok(true)
    }
}

pub async fn visit_local_media_dir<'a, CbResult, F, Fut>(
    host_name: &str,
    pun: &str,
    mut cb: F,
) -> Result<CbResult, MediaError>
where
    F: FnMut(DirEntry) -> Fut,
    Fut: Future<Output = Result<CbResult, MediaError>>,
    CbResult: Add<Output = CbResult> + Default,
{
    let path = local_media_path(host_name, pun)?;
    let mut read_dir = read_dir(path).await?;
    let mut cb_result = CbResult::default();
    while let Some(entry) = read_dir.next_entry().await? {
        let result = cb(entry).await?;
        cb_result = cb_result + result;
    }
    Ok(cb_result)
}

pub async fn local_media_file_names(host_name: &str, pun: &str) -> Result<Vec<String>, MediaError> {
    let path = local_media_path(host_name, pun)?;
    let mut read_dir = read_dir(path).await?;
    let mut result = Vec::new();
    while let Some(entry) = read_dir.next_entry().await? {
        result.push(entry.file_name().to_string_lossy().to_string());
    }
    Ok(result)
}

pub fn media_type_from_file_name(file_name: &str) -> Option<ApObjectMediaType> {
    let extension = Path::new(file_name).extension();
    if let Some(extension) = extension {
        let extension = extension.to_string_lossy().to_lowercase();
        match extension.as_str() {
            "jpeg" | "jpg" => Some(ApObjectMediaType::ImageJpeg),
            "gif" => Some(ApObjectMediaType::ImageGif),
            "png" => Some(ApObjectMediaType::ImagePng),
            _ => None,
        }
    } else {
        None
    }
}

pub fn media_type_from_image_format(image_format: &ImageFormat) -> Option<ApObjectMediaType> {
    match image_format {
        ImageFormat::Gif => Some(ApObjectMediaType::ImageGif),
        ImageFormat::Png => Some(ApObjectMediaType::ImagePng),
        ImageFormat::Jpeg => Some(ApObjectMediaType::ImageJpeg),
        _ => None,
    }
}

pub fn owned_media_url(host_name: &str, pun: &str, file_name: &str) -> Result<String, MediaError> {
    Ok(format!(
        "https://{host_name}/media/{LOCAL_DIR}/{host_name}/{pun}/{file_name}"
    ))
}

pub fn fetched_media_url(
    host_name: &str,
    actor_id: &str,
    file_name: &str,
) -> Result<String, MediaError> {
    let actor_id = utf8_percent_encode(actor_id, NON_ALPHANUMERIC).to_string();
    Ok(format!(
        "https://{host_name}/media/{REMOTE_DIR}/{host_name}/{actor_id}/{file_name}"
    ))
}
