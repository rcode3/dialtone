use super::HttpSignatureError;

pub fn get_digest_from_header<'a>(
    algorithm: &'a str,
    header_value: &'a str,
) -> Result<&'a str, HttpSignatureError> {
    let mut digests = header_value.split(',');
    let found = digests.find_map(|d| {
        if d.starts_with(format!("{algorithm}=").as_str()) {
            Some(d.trim_start_matches(format!("{algorithm}=").as_str()))
        } else if d.starts_with(format!("{}=", algorithm.to_ascii_lowercase()).as_str()) {
            Some(d.trim_start_matches(format!("{}=", algorithm.to_ascii_lowercase()).as_str()))
        } else if d.starts_with(format!("{}=", algorithm.to_ascii_uppercase()).as_str()) {
            Some(d.trim_start_matches(format!("{}=", algorithm.to_ascii_uppercase()).as_str()))
        } else {
            None
        }
    });
    if let Some(digest) = found {
        Ok(digest)
    } else {
        Err(HttpSignatureError::UnsupportedAlgorithm(
            algorithm.to_string(),
        ))
    }
}

#[cfg(test)]
#[allow(non_snake_case)]
mod tests {
    use super::get_digest_from_header;

    #[test]
    fn GIVEN_upper_case_alg_WHEN_get_digest_THEN_success() {
        // GIVEN
        let header_value = "sha-256=abcxyz";

        // WHEN
        let actual = get_digest_from_header("SHA-256", header_value);

        // THEN
        assert_eq!(actual.unwrap(), "abcxyz");
    }

    #[test]
    fn GIVEN_lower_case_alg_WHEN_get_digest_THEN_success() {
        // GIVEN
        let header_value = "sha-256=abcxyz";

        // WHEN
        let actual = get_digest_from_header("sha-256", header_value);

        // THEN
        assert_eq!(actual.unwrap(), "abcxyz");
    }

    #[test]
    fn GIVEN_mixed_case_alg_WHEN_get_digest_THEN_success() {
        // GIVEN
        let header_value = "sha-256=abcxyz";

        // WHEN
        let actual = get_digest_from_header("Sha-256", header_value);

        // THEN
        assert_eq!(actual.unwrap(), "abcxyz");
    }

    #[test]
    fn GIVEN_multiple_algs_WHEN_get_digest_THEN_get_one_being_sought() {
        // GIVEN
        let header_value = "sha-256=abcxyz,unixsum=0";

        // WHEN
        let actual = get_digest_from_header("Sha-256", header_value);

        // THEN
        assert_eq!(actual.unwrap(), "abcxyz");
    }

    #[test]
    #[should_panic]
    fn GIVEN_alg_not_present_WHEN_get_digest_THEN_get_one_being_sought() {
        // GIVEN
        let header_value = "sha-256=abcxyz,unixsum=0";

        // WHEN
        let actual = get_digest_from_header("Sha-512", header_value);

        // THEN
        actual.unwrap();
    }
}
