use base64ct::Encoding;
use chrono::Utc;
use headers::HeaderName;
use http::{HeaderMap, HeaderValue, Method, Uri};
use reqwest::header::HeaderMap as SendHeaderMap;

use super::{
    draft12::{
        create_draft12_to_be_signed, make_draft12_signature_header_value, SignatureHeaderParts,
    },
    HttpSignatureError, DATE_SIGNED_HEADER, DIGEST, HOST_SIGNED_HEADER, HS2019_ALGORITHM,
    PH_REQUEST_TARGET, SHA256_ALGORITHM, SIGNATURE_HEADER_NAME,
};

pub fn sign_draft12(
    method: &Method,
    uri: &Uri,
    key_id: &str,
    private_key_pem: &str,
    body: Option<&[u8]>,
) -> Result<SendHeaderMap, HttpSignatureError> {
    let mut send_headers = SendHeaderMap::new();
    let mut signing_headers = HeaderMap::new();

    // date header
    let now = Utc::now().to_rfc2822();
    signing_headers.insert(
        HeaderName::from_static(DATE_SIGNED_HEADER),
        HeaderValue::from_str(&now)?,
    );
    send_headers.insert::<&str>(DATE_SIGNED_HEADER, now.parse()?);

    // host header
    let host = uri.host().ok_or(HttpSignatureError::InvalidTargetUri)?;
    signing_headers.insert(
        HeaderName::from_static(HOST_SIGNED_HEADER),
        HeaderValue::from_str(host)?,
    );
    send_headers.insert::<&str>(HOST_SIGNED_HEADER, host.parse()?);

    let mut header_names = vec![DATE_SIGNED_HEADER, HOST_SIGNED_HEADER, PH_REQUEST_TARGET];

    // optional digest header
    if let Some(body) = body {
        let mut hasher = openssl::sha::Sha256::new();
        hasher.update(body);
        let digest = hasher.finish();
        let digest_hash_b64 = base64ct::Base64::encode_string(&digest);
        let value = format!("{SHA256_ALGORITHM}={digest_hash_b64}");
        signing_headers.insert(
            HeaderName::from_static(DIGEST),
            HeaderValue::from_str(&value)?,
        );
        header_names.push(DIGEST);
        send_headers.insert::<&str>(DIGEST, value.parse()?);
    };

    // create signature header
    let tbs_parts = SignatureHeaderParts {
        key_id,
        algorithm: Some(HS2019_ALGORITHM),
        created: None,
        expires: None,
        header_names,
        signature: "",
    };
    let to_be_signed = create_draft12_to_be_signed(&tbs_parts, method, uri, &signing_headers)?;
    let private_key = openssl::rsa::Rsa::private_key_from_pem(private_key_pem.as_bytes())?;
    let pkey = openssl::pkey::PKey::from_rsa(private_key)?;
    let mut signer = openssl::sign::Signer::new(openssl::hash::MessageDigest::sha256(), &pkey)?;
    signer.update(to_be_signed.as_bytes())?;
    let signature = signer.sign_to_vec()?;
    let signature_bin = base64ct::Base64::encode_string(&signature);
    let signing_parts = SignatureHeaderParts {
        signature: &signature_bin,
        ..tbs_parts
    };
    let signature_header_value = make_draft12_signature_header_value(&signing_parts);
    send_headers.insert::<&str>(SIGNATURE_HEADER_NAME, signature_header_value.parse()?);

    Ok(send_headers)
}
