use base64ct::Encoding;
use chrono::{DateTime, Utc};
use headers::HeaderName;
use http::{HeaderMap, Method, Uri};

use crate::http_signature::{digest::get_digest_from_header, DIGEST, SHA256_ALGORITHM};

use super::{
    draft12::{create_draft12_to_be_signed, get_draft12_signature_header_parts},
    HttpSignatureError, DATE_SIGNED_HEADER, HOST_SIGNED_HEADER, HS2019_ALGORITHM,
    RSA_SHA256_ALGORITHM, SIGNATURE_HEADER_NAME,
};

pub fn verify_draft12(
    method: &Method,
    uri: &Uri,
    headers: &HeaderMap,
    public_key_pem: &str,
    body: Option<&[u8]>,
) -> Result<(), HttpSignatureError> {
    let signature_headers = headers
        .get_all(HeaderName::from_static(SIGNATURE_HEADER_NAME))
        .iter()
        .map(|header_value| {
            header_value
                .to_str()
                .map_err(HttpSignatureError::SignatureStringFormat)
        })
        .collect::<Result<Vec<&str>, HttpSignatureError>>()?;
    // take the first one because draft 12 doesn't do multiples.
    let signature_header = if let Some(header) = signature_headers.first() {
        header
    } else {
        return Err(HttpSignatureError::MissingHeader(SIGNATURE_HEADER_NAME));
    };
    let parts = get_draft12_signature_header_parts(signature_header)?;
    let algorithm = parts
        .algorithm
        .ok_or(HttpSignatureError::NoAlgorithmSpecified)?;
    if !(algorithm.eq_ignore_ascii_case(RSA_SHA256_ALGORITHM)
        || algorithm.eq_ignore_ascii_case(HS2019_ALGORITHM))
    {
        return Err(HttpSignatureError::UnsupportedAlgorithm(
            algorithm.to_string(),
        ));
    }
    // else

    // verify necessary headers are signed
    parts
        .header_names
        .iter()
        .find(|name| (**name).eq(HOST_SIGNED_HEADER))
        .ok_or(HttpSignatureError::MissingSignedHeader(HOST_SIGNED_HEADER))?;
    parts
        .header_names
        .iter()
        .find(|name| (**name).eq(DATE_SIGNED_HEADER))
        .ok_or(HttpSignatureError::MissingSignedHeader(DATE_SIGNED_HEADER))?;

    // if a body is supplied, check the digest header
    let digest_header = parts.header_names.iter().find(|name| (**name).eq(DIGEST));
    if let Some(body) = body {
        if let Some(_digest_header) = digest_header {
            let digest_header_value = headers
                .get(HeaderName::from_static(DIGEST))
                .ok_or(HttpSignatureError::MissingHeader(DIGEST))?
                .to_str()?;
            let digest = get_digest_from_header(SHA256_ALGORITHM, digest_header_value)?;
            let mut hasher = openssl::sha::Sha256::new();
            hasher.update(body);
            let hash = hasher.finish();
            let digest_hash_b64 = base64ct::Base64::encode_string(&hash);
            if !digest_hash_b64.eq(digest) {
                return Err(HttpSignatureError::BodyDoesNotMatchDigest);
            }
        } else {
            return Err(HttpSignatureError::MissingSignedHeader(DIGEST));
        }
    };

    // check that signature is not too old
    let date_header_value = headers
        .get(HeaderName::from_static(DATE_SIGNED_HEADER))
        .ok_or(HttpSignatureError::MissingHeader(DATE_SIGNED_HEADER))?
        .to_str()?;
    let date_time = DateTime::parse_from_rfc2822(date_header_value)?.with_timezone(&Utc);
    let now = Utc::now();
    if (now - date_time).num_minutes() > 15 {
        return Err(HttpSignatureError::SignatureIsTooOld);
    }
    // else

    let to_be_signed = create_draft12_to_be_signed(&parts, method, uri, headers)?;
    let signature_bin = base64ct::Base64::decode_vec(parts.signature)
        .map_err(|_| HttpSignatureError::InvalidBase64())?;
    let public_key_pem = normalize_pem(public_key_pem);
    let public_key = openssl::rsa::Rsa::public_key_from_pem(public_key_pem.as_bytes())?;
    let pkey = openssl::pkey::PKey::from_rsa(public_key)?;
    let mut verifier =
        openssl::sign::Verifier::new(openssl::hash::MessageDigest::sha256(), &pkey).unwrap();
    verifier.update(to_be_signed.as_bytes())?;
    verifier.verify(&signature_bin)?;
    Ok(())
}

fn normalize_pem(orig: &str) -> String {
    orig.replace("\\n", "\n").trim().to_string()
}

#[cfg(test)]
#[allow(non_snake_case)]
mod tests {
    use super::normalize_pem;

    #[test]
    fn GIVEN_pem_with_escaped_newline_WHEN_normalize_THEN_has_new_lines() {
        // GIVEN
        let orig = r#"-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0yuLaCFm2BzMAWdCgShu\nSm4SJo0VcRbPyy1qnzs5Yg3qcB6Em0eEUDxGoSunZ7nC+3ZGKRU9GfdsYVNNTyuW\nSBlgLkxoyNn0riQUZCOkQlhq3Dt9gFl84gB1q81FQXWYZvDc/6ySw+LHU87L2LwB\nLAAHM5EnbqoO1ntIUpoaSQKoac8B3l526z4XmjwtXAboUAtr8NoJ3goPGOfAHS/q\ntceS5HHMo2XWOWJXpqCk8vNo1+/iRHM8bU0lH0+NE7zQ1cT44OydvXXmkA+BnVXk\nf1wd96W7Ufg4jKaYHIE0Ll415RN0LVHb8wpJH0rDSV6UqzsnuCusy9U+o4U/lmnn\nxwIDAQAB\n-----END PUBLIC KEY-----\n\n"#;

        // WHEN
        let actual = normalize_pem(orig);

        // THEN
        let expected = r#"-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0yuLaCFm2BzMAWdCgShu
Sm4SJo0VcRbPyy1qnzs5Yg3qcB6Em0eEUDxGoSunZ7nC+3ZGKRU9GfdsYVNNTyuW
SBlgLkxoyNn0riQUZCOkQlhq3Dt9gFl84gB1q81FQXWYZvDc/6ySw+LHU87L2LwB
LAAHM5EnbqoO1ntIUpoaSQKoac8B3l526z4XmjwtXAboUAtr8NoJ3goPGOfAHS/q
tceS5HHMo2XWOWJXpqCk8vNo1+/iRHM8bU0lH0+NE7zQ1cT44OydvXXmkA+BnVXk
f1wd96W7Ufg4jKaYHIE0Ll415RN0LVHb8wpJH0rDSV6UqzsnuCusy9U+o4U/lmnn
xwIDAQAB
-----END PUBLIC KEY-----"#;
        assert_eq!(expected, actual);
    }
}
