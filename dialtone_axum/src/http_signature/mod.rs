use chrono::ParseError;
use http::header::ToStrError;
use thiserror::Error;

pub mod digest;
pub mod draft12;
pub mod sign;
pub mod verify;

#[derive(Error, Debug)]
pub enum HttpSignatureError {
    #[error("Missing header '{0}'.")]
    MissingHeader(&'static str),
    #[error("One of the elements of the Signature header is missing.")]
    MissingSignatureHeaderPart,
    #[error("Malformed signature part '{0}'")]
    MalformedSignaturePart(String),
    #[error(transparent)]
    SignatureStringFormat(#[from] ToStrError),
    #[error("Unable to create To Be Signed. Components missing.")]
    UnableToCreateToBeSigned,
    #[error("Unknown component in the To Be Signed set.")]
    UnknownComponentToBeSigned,
    #[error("Missing component in the To Be Signed set.")]
    MissingComponentToBeSigned,
    #[error("The cryptographic algorithm {0} is not supported.")]
    UnsupportedAlgorithm(String),
    #[error("No algorithm was specified.")]
    NoAlgorithmSpecified,
    #[error("Missing signed header '{0}'.")]
    MissingSignedHeader(&'static str),
    #[error(transparent)]
    InvalidDateFormat(#[from] ParseError),
    #[error("Signature is too old.")]
    SignatureIsTooOld,
    #[error("Base64 encoded data is invalid.")]
    InvalidBase64(),
    #[error(transparent)]
    InvalidHeaderValue(#[from] http::header::InvalidHeaderValue),
    #[error("The target URI is invalid.")]
    InvalidTargetUri,
    #[error(transparent)]
    OpenSsl(#[from] openssl::error::ErrorStack),
    #[error("Body does not match digest.")]
    BodyDoesNotMatchDigest,
}

pub const SIGNATURE_HEADER_NAME: &str = "signature";
pub const RSA_SHA256_ALGORITHM: &str = "rsa-sha256";
pub const SHA256_ALGORITHM: &str = "SHA-256";
pub const HS2019_ALGORITHM: &str = "hs2019";
pub const DATE_SIGNED_HEADER: &str = "date";
pub const HOST_SIGNED_HEADER: &str = "host";
pub const SIGNATURE_PART: &str = "signature";
pub const HEADERS: &str = "headers";
pub const ALGORITHM: &str = "algorithm";
pub const KEY_ID: &str = "keyID";
pub const CREATED: &str = "created";
pub const EXPIRES: &str = "expires";
pub const DIGEST: &str = "digest";
pub const PH_REQUEST_TARGET: &str = "(request-target)";
pub const PH_CREATED: &str = "(created)";
pub const PH_EXPIRES: &str = "(expires)";
