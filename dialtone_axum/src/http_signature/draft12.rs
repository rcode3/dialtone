use headers::HeaderName;
use http::{HeaderMap, Method, Uri};

use super::{
    HttpSignatureError, ALGORITHM, CREATED, EXPIRES, HEADERS, KEY_ID, PH_CREATED, PH_EXPIRES,
    PH_REQUEST_TARGET, SIGNATURE_PART,
};

#[derive(Debug)]
pub struct SignatureHeaderParts<'a> {
    pub key_id: &'a str,
    pub algorithm: Option<&'a str>,
    pub created: Option<u32>,
    pub expires: Option<u32>,
    pub header_names: Vec<&'a str>,
    pub signature: &'a str,
}

pub fn get_draft12_signature_header_parts(
    signature_header: &str,
) -> Result<SignatureHeaderParts, HttpSignatureError> {
    // parse into key value pairs
    let key_values = signature_header
        .split(',')
        .map(|kv| -> Result<(&str, &str), HttpSignatureError> {
            kv.split_once('=')
                .map(|split| (split.0.trim(), split.1.trim_matches('"')))
                .ok_or_else(|| HttpSignatureError::MalformedSignaturePart(kv.to_string()))
        })
        .collect::<Vec<_>>();

    // return err if one of the parts is malformed
    let mut signature_parts = Vec::new();
    for result in key_values {
        if let Ok(part) = result {
            signature_parts.push(part);
        } else {
            result?;
        }
    }

    // all the manditory parts must be there, else return error
    let key_id = signature_parts
        .iter()
        .find(|part| part.0.eq_ignore_ascii_case(KEY_ID));

    let signed_headers = signature_parts
        .iter()
        .find(|part| part.0.eq_ignore_ascii_case(HEADERS));

    let signature = signature_parts
        .iter()
        .find(|part| part.0.eq_ignore_ascii_case(SIGNATURE_PART));

    if key_id.is_none() || signed_headers.is_none() || signature.is_none() {
        return Err(HttpSignatureError::MissingSignatureHeaderPart);
    }

    // keyId
    let key_id = key_id.unwrap().1;

    // headers
    let signed_headers = signed_headers
        .unwrap()
        .1
        .split_ascii_whitespace()
        .collect::<Vec<&str>>();

    // signature
    let signature = signature.unwrap().1;

    // algorithm
    let algorithm = signature_parts
        .iter()
        .find(|part| part.0.eq_ignore_ascii_case(ALGORITHM))
        .map(|part| part.1);

    // created
    let created = signature_parts
        .iter()
        .find(|part| part.0.eq_ignore_ascii_case(CREATED))
        .map(|part| {
            part.1
                .parse::<u32>()
                .map_err(|_| HttpSignatureError::MalformedSignaturePart(CREATED.to_string()))
        });
    let created = if let Some(created) = created {
        Some(created?)
    } else {
        None
    };

    // expires
    let expires = signature_parts
        .iter()
        .find(|part| part.0.eq_ignore_ascii_case(EXPIRES))
        .map(|part| {
            part.1
                .parse::<u32>()
                .map_err(|_| HttpSignatureError::MalformedSignaturePart(EXPIRES.to_string()))
        });
    let expires = if let Some(expires) = expires {
        Some(expires?)
    } else {
        None
    };

    Ok(SignatureHeaderParts {
        key_id,
        algorithm,
        created,
        expires,
        header_names: signed_headers,
        signature,
    })
}

pub fn make_draft12_signature_header_value(parts: &SignatureHeaderParts) -> String {
    let mut key_values: Vec<String> = Vec::new();

    // keyId
    key_values.push(format!(r#"{KEY_ID}="{}""#, parts.key_id));

    // headers
    let signed_headers = parts.header_names.join(" ");
    key_values.push(format!(r#"{HEADERS}="{signed_headers}""#));

    // signature
    key_values.push(format!(r#"{SIGNATURE_PART}="{}""#, parts.signature));

    // algorithm
    if let Some(algorithm) = parts.algorithm {
        key_values.push(format!(r#"{ALGORITHM}="{algorithm}""#));
    }

    // created
    if let Some(created) = parts.created {
        key_values.push(format!(r#"{CREATED}="{created}""#));
    }

    // expires
    if let Some(expires) = parts.expires {
        key_values.push(format!(r#"{EXPIRES}="{expires}""#));
    }

    key_values.join(",")
}

pub fn create_draft12_to_be_signed(
    parts: &SignatureHeaderParts,
    method: &Method,
    uri: &Uri,
    headers: &HeaderMap,
) -> Result<String, HttpSignatureError> {
    let mut to_be_signed: Vec<String> = Vec::new();
    for i in 0..parts.header_names.len() {
        let header_name = parts.header_names.get(i).unwrap();
        match *header_name {
            PH_REQUEST_TARGET => to_be_signed.push(format!(
                "{PH_REQUEST_TARGET}: {} {}",
                method.to_string().to_lowercase().as_str(),
                uri.path()
            )),
            PH_CREATED => {
                if let Some(created) = parts.created {
                    to_be_signed.push(format!("{PH_CREATED}: {created}"))
                } else {
                    return Err(HttpSignatureError::UnableToCreateToBeSigned);
                }
            }
            PH_EXPIRES => {
                if let Some(expires) = parts.expires {
                    to_be_signed.push(format!("{PH_EXPIRES}: {expires}"))
                } else {
                    return Err(HttpSignatureError::UnableToCreateToBeSigned);
                }
            }
            _ => {
                let header_name =
                    if let Ok(header_name) = HeaderName::from_lowercase(header_name.as_bytes()) {
                        header_name
                    } else {
                        return Err(HttpSignatureError::UnknownComponentToBeSigned);
                    };
                let header_value = headers.get(&header_name);
                if let Some(header_value) = header_value {
                    to_be_signed.push(format!("{}: {}", header_name, header_value.to_str()?));
                } else {
                    return Err(HttpSignatureError::MissingComponentToBeSigned);
                };
            }
        }
    }
    Ok(to_be_signed.join("\n"))
}

#[cfg(test)]
#[allow(non_snake_case)]
mod tests {
    use crate::http_signature::{sign::sign_draft12, verify::verify_draft12};

    use super::*;

    #[test]
    fn GIVEN_draft12_signature_from_make_WHEN_parse_THEN_get_all_parts() {
        // GIVEN
        let expected = SignatureHeaderParts {
            key_id: "foo-key-id",
            algorithm: Some("rsa1"),
            created: Some(1),
            expires: Some(2),
            header_names: vec!["digest", "host"],
            signature: "abc123",
        };
        let signature_header_value = make_draft12_signature_header_value(&expected);

        // WHEN
        let actual = get_draft12_signature_header_parts(&signature_header_value);

        // THEN
        let actual = actual.unwrap();
        assert_eq!(expected.key_id, actual.key_id);
        assert_eq!(expected.algorithm, actual.algorithm);
        assert_eq!(expected.created, actual.created);
        assert_eq!(expected.expires, actual.expires);
        assert_eq!(expected.header_names, actual.header_names);
        assert_eq!(expected.signature, actual.signature);
    }

    #[test]
    fn GIVEN_draft12_signature_with_optional_parts_WHEN_parse_THEN_get_all_parts() {
        // GIVEN
        let expected = SignatureHeaderParts {
            key_id: "foo-key-id",
            algorithm: None,
            created: None,
            expires: None,
            header_names: vec!["digest", "host"],
            signature: "abc123",
        };
        let signature_header_value = make_draft12_signature_header_value(&expected);

        // WHEN
        let actual = get_draft12_signature_header_parts(&signature_header_value);

        // THEN
        let actual = actual.unwrap();
        assert_eq!(expected.key_id, actual.key_id);
        assert_eq!(expected.algorithm, actual.algorithm);
        assert_eq!(expected.created, actual.created);
        assert_eq!(expected.expires, actual.expires);
        assert_eq!(expected.header_names, actual.header_names);
        assert_eq!(expected.signature, actual.signature);
    }

    #[test]
    fn GIVEN_draft12_signature_from_str_WHEN_parse_THEN_get_all_parts() {
        // GIVEN
        let signature_header_value = r#"keyId="foo-key-id",algorithm="rsa1",created="1",expires="2",headers="digest host",signature="abc123""#;

        // WHEN
        let actual = get_draft12_signature_header_parts(signature_header_value);

        // THEN
        let actual = actual.unwrap();
        assert_eq!("foo-key-id", actual.key_id);
        assert_eq!(Some("rsa1"), actual.algorithm);
        assert_eq!(Some(1), actual.created);
        assert_eq!(Some(2), actual.expires);
        assert_eq!(vec!["digest", "host"], actual.header_names);
        assert_eq!("abc123", actual.signature);
    }

    #[test]
    #[should_panic]
    fn GIVEN_draft12_signature_missing_parts_WHEN_parse_THEN_error() {
        // GIVEN
        let signature_header_value =
            r#"algorithm="rsa1",created="1",expires="2",headers="digest host",signature="abc123""#;

        // WHEN
        let actual = get_draft12_signature_header_parts(signature_header_value);

        // THEN
        actual.unwrap();
    }

    #[test]
    fn GIVEN_parameters_WHEN_draft_12_to_be_sigend_THEN_success() {
        // GIVEN
        let uri = Uri::builder().path_and_query("/foo").build().unwrap();
        let method = Method::from_bytes("POST".as_bytes()).unwrap();
        let mut headers = HeaderMap::new();
        headers.append(
            HeaderName::from_static("host"),
            "localhost".parse().unwrap(),
        );
        headers.append(HeaderName::from_static("digest"), "abc123".parse().unwrap());
        let parts = SignatureHeaderParts {
            key_id: "foo-key-id",
            algorithm: None,
            created: Some(1),
            expires: Some(2),
            header_names: vec![
                "digest",
                "host",
                "(created)",
                "(expires)",
                "(request-target)",
            ],
            signature: "abc123",
        };

        // WHEN
        let actual = create_draft12_to_be_signed(&parts, &method, &uri, &headers);

        // THEN
        let actual = actual.unwrap();
        let expected = "\
            digest: abc123\n\
            host: localhost\n\
            (created): 1\n\
            (expires): 2\n\
            (request-target): post /foo";
        assert_eq!(actual, expected);
    }

    #[test]
    fn GIVEN_signed_headers_WHEN_verified_THEN_success() {
        // GIVEN
        let uri = "https://example.com/foo".parse::<Uri>().unwrap();
        let method = Method::POST;
        let key_id = "http://example.com/key1";
        // create key pair
        let rsa = openssl::rsa::Rsa::generate(2048).expect("unable to generated RSA 2048 key.");
        let pkey = openssl::pkey::PKey::from_rsa(rsa).expect("unable to create RSA private key.");
        let public_key: Vec<u8> = pkey
            .public_key_to_pem()
            .expect("unable to create public key from private key.");
        let public_key_pem = std::str::from_utf8(&public_key)
            .expect("unable to create public key PEM.")
            .to_string();
        let private_key = pkey
            .private_key_to_pem_pkcs8()
            .expect("unable to create PKC8 private key.");
        let private_key_pem = std::str::from_utf8(&private_key)
            .expect("unable to create private key PEM.")
            .to_string();
        println!("private key\n{private_key_pem}");
        println!("public key\n{public_key_pem}");
        let fake_body = "abc123".as_bytes();
        let send_header_map =
            sign_draft12(&method, &uri, key_id, &private_key_pem, Some(fake_body)).unwrap();

        // WHEN
        //   convert reqwest headermap to http crate headermap
        let mut header_map = HeaderMap::new();
        send_header_map
            .iter()
            .for_each(|(header_name, header_value)| {
                header_map.insert(header_name, header_value.to_owned());
                println!("{header_name}: {header_value:?}");
            });
        let actual = verify_draft12(&method, &uri, &header_map, &public_key_pem, Some(fake_body));

        // THEN
        actual.unwrap();
    }
}
