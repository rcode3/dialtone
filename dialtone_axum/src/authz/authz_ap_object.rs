use dialtone_common::{
    authz::{actor::authz_actor_on_host, ap_object::authz_ap_object_on_host},
    rest::users::web_user::SystemRoleType,
};
use sqlx::{Executor, Postgres};

use crate::{err_unauthorized, response::response_error::ResponseError, start_server::Authnz};

use super::user_authz::{authz_actor_for_ap_object, authz_for_ap_object};

pub async fn authz_owner_or_contentadmin_for_ap_object(
    exec: impl Executor<'_, Database = Postgres>,
    authnz: &Authnz,
    ap_object_id: &str,
) -> Result<(), ResponseError> {
    let authz = authz_for_ap_object(exec, &authnz.user_authz, ap_object_id).await?;
    if !authz {
        err_unauthorized!()
    } else {
        Ok(())
    }
}

pub async fn authz_contentadmin_for_ap_object(
    authnz: &Authnz,
    ap_object_id: &str,
) -> Result<(), ResponseError> {
    let authz = authz_ap_object_on_host(
        &authnz.user_authz,
        &SystemRoleType::ContentAdmin,
        ap_object_id,
    );
    if !authz {
        err_unauthorized!()
    } else {
        Ok(())
    }
}

pub async fn authz_owner_or_contentadmin_for_actor(
    exec: impl Executor<'_, Database = Postgres>,
    authnz: &Authnz,
    actor_id: &str,
) -> Result<(), ResponseError> {
    let authz = authz_actor_for_ap_object(exec, &authnz.user_authz, actor_id).await?;
    if !authz {
        err_unauthorized!()
    } else {
        Ok(())
    }
}

pub async fn authz_contentadmin_for_actor(
    authnz: &Authnz,
    actor_id: &str,
) -> Result<(), ResponseError> {
    let authz = authz_actor_on_host(&authnz.user_authz, &SystemRoleType::ContentAdmin, actor_id);
    if !authz {
        err_unauthorized!()
    } else {
        Ok(())
    }
}
