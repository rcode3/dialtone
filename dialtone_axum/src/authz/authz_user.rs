use dialtone_common::{
    rest::users::{user_login::NameHostPair, web_user::SystemRoleType},
    utils::parse_acct::parse_user_acct,
};

use crate::{err_unauthorized, response::response_error::ResponseError, start_server::Authnz};

use super::authz_sysrole::authz_sysrole;

pub async fn authz_acct_or_sysrole(
    acct: &str,
    authnz: &Authnz,
    role: &SystemRoleType,
) -> Result<NameHostPair, ResponseError> {
    let owner_user_host = parse_user_acct(acct)?;
    if authnz.claims.sub != acct {
        authz_sysrole(authnz, role, &owner_user_host.host_name)
            .await
            .map_or_else(|_| err_unauthorized!(), |_| Ok(owner_user_host))
    } else {
        Ok(owner_user_host)
    }
}

pub async fn authz_acct_or_contentadmin(
    acct: &str,
    authnz: &Authnz,
) -> Result<NameHostPair, ResponseError> {
    authz_acct_or_sysrole(acct, authnz, &SystemRoleType::ContentAdmin).await
}

pub async fn authz_acct_or_actoradmin(
    acct: &str,
    authnz: &Authnz,
) -> Result<NameHostPair, ResponseError> {
    authz_acct_or_sysrole(acct, authnz, &SystemRoleType::ActorAdmin).await
}

pub async fn authz_acct_or_useradmin(
    acct: &str,
    authnz: &Authnz,
) -> Result<NameHostPair, ResponseError> {
    authz_acct_or_sysrole(acct, authnz, &SystemRoleType::UserAdmin).await
}
