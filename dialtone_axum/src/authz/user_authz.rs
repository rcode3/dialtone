use crate::response::response_error::ResponseError;

use dialtone_common::authz::actor::authz_actor_on_host;
use dialtone_common::authz::ap_object::authz_ap_object_on_host;

use dialtone_common::authz::user_authz_info::UserAuthzInfo;
use dialtone_common::rest::users::web_user::SystemRoleType;

use dialtone_sqlx::db::actor_owner::fetch_authz::fetch_actor_owner_authz;
use dialtone_sqlx::db::ap_object::fetch_authz::fetch_ap_object_authz;
use sqlx::{Executor, Postgres};

pub async fn authz_for_actor(
    exec: impl Executor<'_, Database = Postgres>,
    user_authz: &UserAuthzInfo,
    actor_id: &str,
) -> Result<bool, ResponseError> {
    if authz_actor_on_host(user_authz, &SystemRoleType::ActorAdmin, actor_id) {
        return Ok(true);
    }
    let actor_authz = fetch_actor_owner_authz(exec, actor_id, &user_authz.acct).await?;
    Ok(actor_authz)
}

pub async fn authz_actor_for_ap_object(
    exec: impl Executor<'_, Database = Postgres>,
    user_authz: &UserAuthzInfo,
    actor_id: &str,
) -> Result<bool, ResponseError> {
    if authz_actor_on_host(user_authz, &SystemRoleType::ContentAdmin, actor_id) {
        return Ok(true);
    }
    let ap_object_authz = fetch_actor_owner_authz(exec, actor_id, &user_authz.acct).await?;
    Ok(ap_object_authz)
}

pub async fn authz_for_ap_object(
    exec: impl Executor<'_, Database = Postgres>,
    user_authz: &UserAuthzInfo,
    ap_object_id: &str,
) -> Result<bool, ResponseError> {
    if authz_ap_object_on_host(user_authz, &SystemRoleType::ContentAdmin, ap_object_id) {
        return Ok(true);
    }
    let ap_object_authz = fetch_ap_object_authz(exec, ap_object_id, &user_authz.acct).await?;
    Ok(ap_object_authz)
}
