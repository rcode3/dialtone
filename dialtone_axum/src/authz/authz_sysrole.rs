use dialtone_common::{authz::host::authz_on_host, rest::users::web_user::SystemRoleType};

use crate::{err_unauthorized, response::response_error::ResponseError, start_server::Authnz};

pub async fn authz_sysrole(
    claims: &Authnz,
    role: &SystemRoleType,
    host_name: &str,
) -> Result<(), ResponseError> {
    if !authz_on_host(&claims.user_authz, role, host_name) {
        err_unauthorized!()
    } else {
        Ok(())
    }
}

pub async fn authz_contentadmin(claims: &Authnz, host_name: &str) -> Result<(), ResponseError> {
    authz_sysrole(claims, &SystemRoleType::ContentAdmin, host_name).await
}

pub async fn authz_actoradmin(claims: &Authnz, host_name: &str) -> Result<(), ResponseError> {
    authz_sysrole(claims, &SystemRoleType::ActorAdmin, host_name).await
}

pub async fn authz_useradmin(claims: &Authnz, host_name: &str) -> Result<(), ResponseError> {
    authz_sysrole(claims, &SystemRoleType::UserAdmin, host_name).await
}
