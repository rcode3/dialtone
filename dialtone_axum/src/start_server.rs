use std::fmt::Display;
use std::net::SocketAddr;
use std::time::Duration;

use axum::error_handling::HandleErrorLayer;
use axum::extract::{FromRef, FromRequestParts};
use axum::http::request::Parts;
use axum::http::Method;
use axum::RequestPartsExt;
use axum::{async_trait, extract::TypedHeader, http::StatusCode, Router};
use axum_client_ip::ClientIp;
use chrono::Utc;
use dialtone_common::authz::user_authz_info::UserAuthzInfo;
use headers::{authorization::Bearer, Authorization};
use jsonwebtoken::{decode, DecodingKey, EncodingKey, Validation};
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use sqlx::{Pool, Postgres};
use tokio_cron_scheduler::{Job, JobScheduler};
use tower::{BoxError, ServiceBuilder};
use tower_http::cors::{Any, CorsLayer};
use tower_http::trace::TraceLayer;

use dialtone_common::rest::users::web_user::UserStatus;
use dialtone_common::utils::version::DT_VERSION;
use dialtone_sqlx::db::persistent_queue::listener::listener;
use dialtone_sqlx::db::persistent_queue::notifier::notify;
use dialtone_sqlx::db::persistent_queue::pop_queue::pop_queue;
use dialtone_sqlx::db::persistent_queue::JobDbType;
use dialtone_sqlx::db::user::fetch_auth_and_mark_seen::fetch_auth_and_mark_seen;
use tracing::error;

use crate::ap::routes::ap_routes;
use crate::api_dt::routes::api_dt_routes;
use crate::queue::{
    process_remote_banner_queue, process_remote_icon_queue, process_send_activity_pub,
};
use crate::response::response_error::ResponseError;
use crate::well_known::routes::well_known_routes;

lazy_static! {
    pub static ref KEYS: Keys = {
        let secret = std::env::var("JWT_SECRET").expect("JWT_SECRET must be set");
        Keys::new(secret.as_bytes())
    };
}

pub const UN_KNOWN: &str = "UN.KNOWN";

pub async fn start_server(pg_pool: Pool<Postgres>) -> anyhow::Result<()> {
    tracing_subscriber::fmt::init();
    tracing::info!("dialtone version {}", DT_VERSION);

    start_queues(&pg_pool)?;
    start_jobs(&pg_pool).await?;

    let app = app_router(pg_pool);

    // run it
    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    tracing::debug!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service_with_connect_info::<SocketAddr>())
        .await?;
    Ok(())
}

/// Starts Postgres based Queue listeners.
fn start_queues(pg_pool: &Pool<Postgres>) -> anyhow::Result<()> {
    // Start the listener for the No-Operation queue.
    let no_op_pg_pool = pg_pool.clone();
    tokio::spawn(async move {
        listener(
            &no_op_pg_pool,
            &JobDbType::NoOperation,
            move |pool, notification| async move {
                tracing::info!("queue notification {:?}", notification);
                pop_queue(&pool, &JobDbType::NoOperation).await.unwrap();
            },
            true,
        )
        .await
    });

    // Start the listener for the Fetch Remote Icon queue.
    let fetch_icon_pg_pool = pg_pool.clone();
    tokio::spawn(async move {
        listener(
            &fetch_icon_pg_pool,
            &JobDbType::FetchRemoteIcon,
            move |pool, notification| async move {
                let _ = process_remote_icon_queue(&pool, notification)
                    .await
                    .map_err(|err| error!("remote icon queue error: {err:?}"));
            },
            true,
        )
        .await
    });

    // Start the listener for the Fetch Remote Banner queue.
    let fetch_banner_pg_pool = pg_pool.clone();
    tokio::spawn(async move {
        listener(
            &fetch_banner_pg_pool,
            &JobDbType::FetchRemoteBanner,
            move |pool, notification| async move {
                let _ = process_remote_banner_queue(&pool, notification)
                    .await
                    .map_err(|err| error!("remote banner queue error: {err:?}"));
            },
            true,
        )
        .await
    });

    // Start the listener for the Send Activity Pub queue.
    let send_activity_pub_pg_pool = pg_pool.clone();
    tokio::spawn(async move {
        listener(
            &send_activity_pub_pg_pool,
            &JobDbType::SendActivityPub,
            move |pool, notification| async move {
                let _ = process_send_activity_pub(&pool, notification)
                    .await
                    .map_err(|err| error!("send activity pub queue error: {err:?}"));
            },
            true,
        )
        .await
    });

    Ok(())
}

/// Starts CRON style scheduled jobs.
async fn start_jobs(pg_pool: &Pool<Postgres>) -> anyhow::Result<()> {
    let pg_pool = pg_pool.clone();
    let sched = JobScheduler::new().await.expect("cron schedulaer error");

    // Job to Notify the Postgres No Operation queue... basically a keep alive.
    let no_op_pg_pool = pg_pool.clone();
    let job = Job::new_repeated(Duration::from_secs(60), move |_uuid, _l| {
        let no_op_pg_pool = no_op_pg_pool.clone();
        tokio::spawn(async move {
            notify(&no_op_pg_pool, &JobDbType::NoOperation)
                .await
                .unwrap()
        });
    })
    .unwrap();
    sched.add(job).await.expect("adding job");

    // Job to Notify the Postgres Send Activity Pub queue.
    let send_ap_pg_pool = pg_pool.clone();
    let job = Job::new_repeated(Duration::from_secs(600), move |_uuid, _l| {
        let send_ap_pg_pool = send_ap_pg_pool.clone();
        tokio::spawn(async move {
            notify(&send_ap_pg_pool, &JobDbType::SendActivityPub)
                .await
                .unwrap()
        });
    })
    .unwrap();
    sched.add(job).await.expect("adding job");

    sched.start().await?;
    Ok(())
}

pub fn app_router(pg_pool: Pool<Postgres>) -> Router {
    #[cfg(debug_assertions)]
    tracing::warn!("Server is running in development mode");

    let state = AppState::new(pg_pool);
    let api_routes = api_dt_routes();
    let well_known_routes = well_known_routes();
    let ap_routes = ap_routes();
    Router::new()
        .nest(&api_routes.0, api_routes.1)
        .nest(&well_known_routes.0, well_known_routes.1)
        .nest(&ap_routes.0, ap_routes.1)
        .layer(
            ServiceBuilder::new()
                .layer(HandleErrorLayer::new(|error: BoxError| async move {
                    if error.is::<tower::timeout::error::Elapsed>() {
                        Ok(StatusCode::REQUEST_TIMEOUT)
                    } else {
                        Err((
                            StatusCode::INTERNAL_SERVER_ERROR,
                            format!("Unhandled internal error: {error}"),
                        ))
                    }
                }))
                .timeout(Duration::from_secs(10))
                .layer(TraceLayer::new_for_http())
                .layer(
                    CorsLayer::new()
                        .allow_origin(Any)
                        .allow_methods(vec![
                            Method::GET,
                            Method::POST,
                            Method::OPTIONS,
                            Method::DELETE,
                            Method::PUT,
                            Method::PATCH,
                        ])
                        .allow_headers(Any),
                )
                .into_inner(),
        )
        .with_state(state)
}

#[derive(Clone)]
pub struct AppState {
    pub pg_pool: Pool<Postgres>,
}

impl AppState {
    pub fn new(pg_pool: Pool<Postgres>) -> Self {
        Self { pg_pool }
    }
}

pub fn internal_error<E>(err: E) -> (StatusCode, String)
where
    E: std::error::Error,
{
    (StatusCode::INTERNAL_SERVER_ERROR, err.to_string())
}

impl Display for Claims {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "User Account: {}", self.sub)
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Authnz {
    pub claims: Claims,
    pub user_authz: UserAuthzInfo,
}

#[async_trait]
impl<S> FromRequestParts<S> for Authnz
where
    AppState: FromRef<S>,
    S: Send + Sync,
{
    type Rejection = ResponseError;

    async fn from_request_parts(parts: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        let shared_state = AppState::from_ref(state);
        // Extract the token from the authorization header
        let TypedHeader(Authorization(bearer)) = parts
            .extract::<TypedHeader<Authorization<Bearer>>>()
            .await
            .map_err(|_| ResponseError::MissingCredentials(None))?;
        // Decode the users data
        let token_data = decode::<Claims>(bearer.token(), &KEYS.decoding, &Validation::default())
            .map_err(|err| {
            tracing::warn!("error with token: {}", err.to_string());
            ResponseError::TokenDecodingError(None)
        })?;

        // get forwarded for
        let client_ip = ClientIp::from_request_parts(parts, state).await;
        let ip_string = if let Ok(ip) = client_ip {
            ip.0.to_string()
        } else {
            UN_KNOWN.to_string()
        };

        // get the pg pool
        let pool = shared_state.pg_pool;
        let user_authz_info =
            fetch_auth_and_mark_seen(&pool, &token_data.claims.sub, &ip_string, Utc::now()).await;
        match user_authz_info {
            Ok(info) => match &info {
                None => Err(ResponseError::NoAuthorizationsAvailable(None)),
                Some(us) => match us.status {
                    UserStatus::Active => Ok(Authnz {
                        claims: token_data.claims,
                        user_authz: us.to_owned(),
                    }),
                    UserStatus::Suspended => Err(ResponseError::UnableToAuthorize(None)),
                    UserStatus::PendingApproval => Err(ResponseError::UnableToAuthorize(None)),
                },
            },
            Err(_) => Err(ResponseError::UnableToAuthenticate(None)),
        }
    }
}

pub struct Keys {
    pub encoding: EncodingKey,
    pub decoding: DecodingKey,
}

impl Keys {
    fn new(secret: &[u8]) -> Self {
        Self {
            encoding: EncodingKey::from_secret(secret),
            decoding: DecodingKey::from_secret(secret),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
    pub sub: String,
    pub exp: u64,
}
