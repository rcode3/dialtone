use axum::{
    extract::{Query, State},
    Json,
};
use dialtone_common::webfinger::{create_wf_acct, Jrd};
use dialtone_sqlx::db::actor::fetch_public::fetch_jrd_by_wf_acct;

use crate::{
    client::webfinger::resource_params::{parse_wf_acct, ResourceParams},
    err_not_found,
    response::response_error::ResponseError,
    start_server::AppState,
};

type WfAcctResponse = Result<Json<Jrd>, ResponseError>;

pub async fn get_wf_acct_handler(
    Query(params): Query<ResourceParams>,
    State(AppState { pg_pool }): State<AppState>,
) -> WfAcctResponse {
    let wf_acct_parsts = parse_wf_acct(&params.resource)?;
    let wf_acct = create_wf_acct(&wf_acct_parsts.host_name, &wf_acct_parsts.user_name);
    let jrd = fetch_jrd_by_wf_acct(&pg_pool, &wf_acct).await?;
    match jrd {
        None => err_not_found!(),
        Some(jrd) => Ok(Json(jrd)),
    }
}
