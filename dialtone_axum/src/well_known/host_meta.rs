use axum::{extract::Host, response::IntoResponse};
use dialtone_common::utils::media_types::{JRD_CONTENT_TYPE, XRD_CONTENT_TYPE};
use hyper::header;

pub async fn get_host_meta_handler(Host(host_name): Host) -> impl IntoResponse {
    let xrd = format!(
        r#"<?xml version="1.0" encoding="UTF-8"?>
<XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
    <Link 
        rel="lrdd" 
        template="https://{host_name}/.well-known/webfinger?resource={{uri}}" 
        type="application/xrd+xml" />
</XRD>
    "#
    );
    ([(header::CONTENT_TYPE, XRD_CONTENT_TYPE)], xrd)
}

pub async fn get_host_meta_json_handler(Host(host_name): Host) -> impl IntoResponse {
    // we don't use the JRD in dialtone_commons because it is specifically profiled for Webfinger use.
    // "subject" is a SHOULD NOT in the LRDD/JRD specs but required for wF.
    let jrd = format!(
        r#"
{{
    "links":
        [
            {{
                "rel":"lrdd",
                "type":"application/jrd+json",
                "template":"https://{host_name}/.well-known/webfinger?resource={{uri}}"
            }}
        ]
}}
    "#
    );
    ([(header::CONTENT_TYPE, JRD_CONTENT_TYPE)], jrd)
}
