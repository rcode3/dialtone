use axum::{routing::get, Router};

use crate::start_server::AppState;

use super::{
    host_meta::{get_host_meta_handler, get_host_meta_json_handler},
    nodeinfo::{get_nodeinfo_instance_handler, get_nodeinfo_jrd_handler, get_x_nodeinfo2_handler},
    webfinger_handlers::get_wf_acct_handler,
};

pub fn well_known_routes() -> (String, Router<AppState>) {
    let router = Router::new()
        .route("/nodeinfo", get(get_nodeinfo_jrd_handler))
        .route("/nodeinfo-instance", get(get_nodeinfo_instance_handler))
        .route("/x-nodeinfo2", get(get_x_nodeinfo2_handler))
        .route("/host-meta", get(get_host_meta_handler))
        .route("/host-meta.json", get(get_host_meta_json_handler))
        .route("/webfinger", get(get_wf_acct_handler));
    ("/.well-known".to_string(), router)
}
