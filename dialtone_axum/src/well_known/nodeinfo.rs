use axum::{
    extract::{Host, State},
    response::IntoResponse,
    Json,
};
use dialtone_common::{ap::actor::ActorType, utils::media_types::JRD_CONTENT_TYPE};
use dialtone_common::{rest::sites::site_data::RegistrationMethod, utils::version::DT_VERSION};
use dialtone_sqlx::db::{actor::count::count_actors_by_host, site_info::fetch_site};
use hyper::header;
use serde_json::{json, Value};

use crate::{response::response_error::ResponseError, start_server::AppState};

pub async fn get_nodeinfo_jrd_handler(Host(host_name): Host) -> impl IntoResponse {
    let href = format!("https://{host_name}/.well-known/nodeinfo-instance");
    let jrd = json!(
        {
            "links":
                [
                    {
                        "rel": "http://nodeinfo.diaspora.software/ns/schema/2.1",
                        "href": href
                    }
                ]
        }
    );
    ([(header::CONTENT_TYPE, JRD_CONTENT_TYPE)], jrd.to_string())
}

pub async fn get_nodeinfo_instance_handler(
    Host(host_name): Host,
    State(AppState { pg_pool }): State<AppState>,
) -> Result<Json<Value>, ResponseError> {
    #[cfg(debug_assertions)]
    let host_name = if host_name.contains("0.0.0.0") {
        "localhost".to_string()
    } else {
        host_name
    };

    let user_count = count_actors_by_host(&pg_pool, &host_name, Some(&ActorType::Person)).await?;
    let site_info = fetch_site(&pg_pool, &host_name).await?.ok_or_else(|| {
        ResponseError::InternalServerError(Some(
            "Unknown host, maybe reverse proxy misconfiguration?".to_string(),
        ))
    })?;
    let open_registration = site_info
        .site_data
        .public
        .registration_methods
        .contains(&RegistrationMethod::Open);
    let nodeinfo = json!(
    {
        "version": "2.1",
        "software": {
            "name": "dialtone",
            "version": DT_VERSION,
            "repository": "https://codeberg.org/rcode3/dialtone.git",
            "homepage": "https://dialtone.dev"
        },
        "protocols": ["activitypub"],
        "services": {
            "inbound": [],
            "outbound": []
        },
        "openRegistrations": open_registration,
        "usage": {
            "users": {
                "total": user_count,
            },
        },
        "metadata": { }
        }
    );
    Ok(Json(nodeinfo))
}

pub async fn get_x_nodeinfo2_handler(
    Host(host_name): Host,
    State(AppState { pg_pool }): State<AppState>,
) -> Result<Json<Value>, ResponseError> {
    #[cfg(debug_assertions)]
    let host_name = if host_name.contains("0.0.0.0") {
        "localhost".to_string()
    } else {
        host_name
    };

    let base_url = format!("https://{host_name}");
    let user_count = count_actors_by_host(&pg_pool, &host_name, Some(&ActorType::Person)).await?;
    let site_info = fetch_site(&pg_pool, &host_name).await?.ok_or_else(|| {
        ResponseError::InternalServerError(Some(
            "Unknown host, maybe reverse proxy misconfiguration?".to_string(),
        ))
    })?;
    let name = site_info.site_data.public.names.long_name;
    let open_registration = site_info
        .site_data
        .public
        .registration_methods
        .contains(&RegistrationMethod::Open);
    let nodeinfo = json!(
    {
        "version": "1.0",
        "server": {
            "baseUrl": base_url,
            "name": name,
            "software": "dialtone",
            "version": DT_VERSION
        },
        "protocols": ["activitypub"],
        "services": {
            "inbound": [],
            "outbound": []
        },
        "openRegistrations": open_registration,
        "usage": {
            "users": {
                "total": user_count
            }
        }
    }
    );
    Ok(Json(nodeinfo))
}
