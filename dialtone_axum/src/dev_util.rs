#![cfg(debug_assertions)]
use dialtone_common::{
    ap::{actor::Actor, image_attr::ImageAttributes},
    rest::{
        actors::actor_model::{OwnedActor, PublicActor},
        users::web_user::WebUser,
    },
};
use regex::Regex;

/// Rewrites a URL to point to "local" media
/// for testing purposes. Use of this function
/// should be beind a debug assertion.
/// The url will be rewritten using the
/// LOCAL_MEDIA_BASE environment variable,
/// which should point to the directory
/// where local media is.
pub fn local_media(url: &str) -> String {
    let local_media_base = std::env::var("LOCAL_MEDIA_BASE");
    if let Ok(local_media_base) = local_media_base {
        let re = Regex::new(r#"(http|https)://(localhost|127\.0\.0\.1|\[::1\])/media"#).unwrap();
        let replacement = re.replace(url, local_media_base);
        replacement.to_string()
    } else {
        url.to_string()
    }
}

pub fn local_image_attr(image_attr: Option<ImageAttributes>) -> Option<ImageAttributes> {
    image_attr.map(|ia| ImageAttributes::SingleUrl(local_media(ia.first_url().unwrap().as_str())))
}

pub fn local_actor(actor: &Actor) -> Actor {
    let actor = actor.clone();
    Actor {
        icon: local_image_attr(actor.icon),
        image: local_image_attr(actor.image),
        ..actor
    }
}

pub fn local_owned_actor(actor: &OwnedActor) -> OwnedActor {
    let owned_actor = actor.clone();
    OwnedActor {
        ap: local_actor(&owned_actor.ap),
        ..owned_actor
    }
}

pub fn local_public_actor(actor: &PublicActor) -> PublicActor {
    let public_actor = actor.clone();
    PublicActor {
        ap: local_actor(&public_actor.ap),
        ..public_actor
    }
}

pub fn local_user(user: &WebUser) -> WebUser {
    let web_user = user.clone();
    WebUser {
        default_actor_data: web_user
            .default_actor_data
            .map(|public_actor| local_public_actor(&public_actor)),
        ..web_user
    }
}

#[cfg(test)]
#[allow(non_snake_case)]
mod dev_util_tests {
    use super::local_media;

    #[test]
    fn GIVEN_env_var_WHEN_sub_url_for_non_local_THEN_url_is_unchanged() {
        // GIVEN
        std::env::set_var("LOCAL_MEDIA_BASE", "file:///home/local/media");

        // WHEN
        let new_url = local_media("https://example.com/media/localhost/foobar");

        // THEN
        assert_eq!(new_url, "https://example.com/media/localhost/foobar");
    }

    #[test]
    fn GIVEN_env_var_WHEN_sub_url_for_localhost_THEN_url_starts_with_local_media_base() {
        // GIVEN
        std::env::set_var("LOCAL_MEDIA_BASE", "file:///home/local/media");

        // WHEN
        let new_url = local_media("https://localhost/media/localhost/foobar");

        // THEN
        assert_eq!(new_url, "file:///home/local/media/localhost/foobar");
    }

    #[test]
    fn GIVEN_env_var_WHEN_sub_url_for_v4loopback_THEN_url_starts_with_local_media_base() {
        // GIVEN
        std::env::set_var("LOCAL_MEDIA_BASE", "file:///home/local/media");

        // WHEN
        let new_url = local_media("https://127.0.0.1/media/localhost/foobar");

        // THEN
        assert_eq!(new_url, "file:///home/local/media/localhost/foobar");
    }

    #[test]
    fn GIVEN_env_var_WHEN_sub_url_for_v6loopback_THEN_url_starts_with_local_media_base() {
        // GIVEN
        std::env::set_var("LOCAL_MEDIA_BASE", "file:///home/local/media");

        // WHEN
        let new_url = local_media("https://[::1]/media/localhost/foobar");

        // THEN
        assert_eq!(new_url, "file:///home/local/media/localhost/foobar");
    }
}
