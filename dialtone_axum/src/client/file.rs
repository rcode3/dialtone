use super::ClientError;
use dialtone_common::containers::http::HttpGetData;
use futures_util::StreamExt;
use headers::HeaderName;
use hyper::header::{CONTENT_ENCODING, CONTENT_TYPE, DATE, ETAG, LAST_MODIFIED};
use reqwest::Response;
use tokio::{fs::File, io::AsyncWriteExt};

pub async fn fetch_file(
    url: &str,
    file_path: &str,
    max_size: u64,
) -> Result<HttpGetData, ClientError> {
    let mut file = File::create(file_path).await?;
    let response = reqwest::get(url).await?;
    if let Some(content_length) = response.content_length() {
        if content_length > max_size {
            return Err(ClientError::RequestTooLarge);
        }
    } else {
        return Err(ClientError::RequestTooLarge);
    }
    let http_get_data = HttpGetData {
        content_length: response.content_length(),
        content_type: get_header_value(&response, CONTENT_TYPE),
        content_encoding: get_header_value(&response, CONTENT_ENCODING),
        date: get_header_value(&response, DATE),
        e_tag: get_header_value(&response, ETAG),
        last_modified: get_header_value(&response, LAST_MODIFIED),
    };
    let mut bytes_stream = response.bytes_stream();
    while let Some(bytes) = bytes_stream.next().await {
        file.write_all(&bytes?).await?;
    }
    Ok(http_get_data)
}

fn get_header_value(response: &Response, name: HeaderName) -> Option<String> {
    let header_value = response.headers().get(name);
    if let Some(value) = header_value {
        if let Ok(value) = value.to_str() {
            Some(value.to_string())
        } else {
            None
        }
    } else {
        None
    }
}
