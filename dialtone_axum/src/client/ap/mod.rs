use dialtone_common::utils::media_types::{ACTIVITY_PUB_MEDIA_TYPE, JSON_TYPE};
use headers::{HeaderMap, HeaderValue};
use hyper::header::ACCEPT;
use lazy_static::lazy_static;

use crate::client::CLIENT_USER_AGENT;

pub mod fetch;
pub mod send;

lazy_static! {
    pub static ref AP_CLIENT: reqwest::Client = {
        let accepts = format!("{ACTIVITY_PUB_MEDIA_TYPE}, {JSON_TYPE}");
        let mut headers = HeaderMap::new();
        headers.insert(ACCEPT, HeaderValue::from_str(&accepts).unwrap());
        reqwest::Client::builder()
            .user_agent(CLIENT_USER_AGENT)
            .default_headers(headers)
            .build()
            .unwrap()
    };
}
