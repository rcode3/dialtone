use dialtone_common::ap::{actor::Actor, collection::Collection};

use crate::client::ClientError;

use super::AP_CLIENT;

pub async fn fetch_ap_actor(actor_id: &str) -> Result<Actor, ClientError> {
    axum::http::Uri::try_from(actor_id)?;
    let actor = AP_CLIENT
        .get(actor_id)
        .send()
        .await?
        .error_for_status()?
        .json::<Actor>()
        .await?;
    Ok(actor)
}

pub async fn fetch_ap_collection(collection_id: &str) -> Result<Collection, ClientError> {
    axum::http::Uri::try_from(collection_id)?;
    let actor = AP_CLIENT
        .get(collection_id)
        .send()
        .await?
        .error_for_status()?
        .json::<Collection>()
        .await?;
    Ok(actor)
}
