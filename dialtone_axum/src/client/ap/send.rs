use dialtone_common::{ap::activity::Activity, utils::media_types::ACTIVITY_PUB_MEDIA_TYPE};
use http::{header::CONTENT_TYPE, Method, Uri};

use crate::{client::ClientError, http_signature::sign::sign_draft12};

use super::AP_CLIENT;

pub async fn send_activity(
    target_uri: &str,
    key_id: &str,
    private_key_pem: &str,
    activity: &Activity,
) -> Result<(), ClientError> {
    let uri = target_uri.parse::<Uri>()?;
    let method = Method::POST;
    let body = serde_json::to_vec(activity)?;
    let send_headers = sign_draft12(&method, &uri, key_id, private_key_pem, Some(&body))?;
    let send = AP_CLIENT
        .request(method, target_uri)
        .header(CONTENT_TYPE, ACTIVITY_PUB_MEDIA_TYPE)
        .headers(send_headers)
        .body(body)
        .build()?;
    AP_CLIENT.execute(send).await?.error_for_status()?;
    Ok(())
}
