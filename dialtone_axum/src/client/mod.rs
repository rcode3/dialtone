use axum::http::uri::InvalidUri;
use thiserror::Error;

use crate::http_signature::HttpSignatureError;

pub mod ap;
pub mod file;
pub mod webfinger;

#[derive(Error, Debug)]
pub enum ClientError {
    #[error("Input to client request was invalid")]
    InvalidInput(String),
    #[error("Request was too large.")]
    RequestTooLarge,
    #[error(transparent)]
    ReqwestError(#[from] reqwest::Error),
    #[error(transparent)]
    InvalidUri(#[from] InvalidUri),
    #[error(transparent)]
    TokioIOError(#[from] tokio::io::Error),
    #[error(transparent)]
    HttpSignature(#[from] HttpSignatureError),
    #[error(transparent)]
    InvalidJson(#[from] serde_json::Error),
}

pub static CLIENT_USER_AGENT: &str =
    concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"),);
