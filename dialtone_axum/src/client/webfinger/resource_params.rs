use dialtone_common::rest::users::user_login::NameHostPair;
use lazy_static::lazy_static;
use regex::Regex;
use serde::{Deserialize, Serialize};

use crate::client::ClientError;

lazy_static! {
    pub static ref WF_ACCT_RE: Regex =
        Regex::new(r"^(acct:)?([^@\s]+)@([a-zA-Z0-9\-\.]+)$").unwrap();
}

pub fn parse_wf_acct(wf_acct: &str) -> Result<NameHostPair, ClientError> {
    let captures = WF_ACCT_RE.captures(wf_acct);
    if let Some(captures) = captures {
        if let Some(user_name) = captures.get(2) {
            if let Some(host_name) = captures.get(3) {
                Ok(NameHostPair {
                    user_name: user_name.as_str().to_owned(),
                    host_name: host_name.as_str().to_owned(),
                })
            } else {
                Err(ClientError::InvalidInput(format!(
                    "{wf_acct} does not have a valid host name."
                )))
            }
        } else {
            Err(ClientError::InvalidInput(format!(
                "{wf_acct} does not have a valid user name."
            )))
        }
    } else {
        Err(ClientError::InvalidInput(format!(
            "{wf_acct} is not a valid webfinger acct resource."
        )))
    }
}

#[derive(Serialize, Deserialize)]
pub struct ResourceParams {
    pub resource: String,
}

#[cfg(test)]
#[allow(non_snake_case)]
mod tests {
    use crate::client::webfinger::resource_params::parse_wf_acct;

    #[test]
    fn GIVEN_wf_with_acct_part_WHEN_parse_THEN_success() {
        // GIVEN
        let acct = "acct:foo@bar";

        // WHEN
        let result = parse_wf_acct(acct);

        // THEN
        let pair = result.unwrap();
        assert_eq!(pair.user_name, "foo");
        assert_eq!(pair.host_name, "bar");
    }

    #[test]
    fn GIVEN_wf_without_acct_part_WHEN_parse_THEN_success() {
        // GIVEN
        let acct = "foo@bar";

        // WHEN
        let result = parse_wf_acct(acct);

        // THEN
        let pair = result.unwrap();
        assert_eq!(pair.user_name, "foo");
        assert_eq!(pair.host_name, "bar");
    }

    #[test]
    fn GIVEN_wf_fqdn_WHEN_parse_THEN_success() {
        // GIVEN
        let acct = "foo@example.com";

        // WHEN
        let result = parse_wf_acct(acct);

        // THEN
        let pair = result.unwrap();
        assert_eq!(pair.user_name, "foo");
        assert_eq!(pair.host_name, "example.com");
    }
}
