use std::net::SocketAddr;

use dialtone_common::webfinger::Jrd;

use crate::client::ClientError;

use super::{resource_params::parse_wf_acct, WF_CLIENT};

/// Does an HTTPS GET of a Webfinger "acct" resource.
///
/// # Arguments
///
/// * wf_acct: a reference to the webfinger account (e.g. 'acct:foo@bar.com' or 'foo@bar.com')
/// * socket_addr: if provided, this will be the used as the authority part in the HTTP GET URL,
/// and HTTP (not HTTPS will be used). Use this for testing.
pub async fn fetch_wf_acct(
    wf_acct: &str,
    socket_addr: Option<SocketAddr>,
) -> Result<Jrd, ClientError> {
    let parts = parse_wf_acct(wf_acct).unwrap();
    let query = if let Some(socket_addr) = socket_addr {
        format!(
            "http://{}:{}/.well-known/webfinger?resource={}",
            socket_addr.ip(),
            socket_addr.port(),
            wf_acct
        )
    } else {
        format!(
            "https://{}/.well-known/webfinger?resource={}",
            parts.host_name, wf_acct
        )
    };
    let jrd = WF_CLIENT
        .get(query)
        .send()
        .await?
        .error_for_status()?
        .json::<Jrd>()
        .await?;
    Ok(jrd)
}
