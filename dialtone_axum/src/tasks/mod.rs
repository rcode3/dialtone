use dialtone_common::utils::parse_acct::ParseUserAcctError;
use dialtone_sqlx::DbError;

use crate::{client::ClientError, media::MediaError};

pub mod activity_pub;
pub mod banner;
pub mod icon;

#[derive(thiserror::Error, Debug)]
pub enum TaskError {
    #[error(transparent)]
    Http(#[from] ClientError),
    #[error("Task cannot be accomplished.")]
    UnaccomplishableTask,
    #[error(transparent)]
    IOError(#[from] std::io::Error),
    #[error("Image error: {0}")]
    ImageError(String),
    #[error(transparent)]
    MediaError(#[from] MediaError),
    #[error(transparent)]
    BadUserData(#[from] ParseUserAcctError),
    #[error(transparent)]
    DbError(#[from] DbError),
    #[error(transparent)]
    Other(#[from] anyhow::Error),
}

impl From<ril::Error> for TaskError {
    fn from(e: ril::Error) -> Self {
        TaskError::ImageError(e.to_string())
    }
}
