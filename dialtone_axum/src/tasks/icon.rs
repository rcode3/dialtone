use dialtone_common::{
    containers::file::FileData, rest::ap_objects::ap_object_model::ApObjectSystemData,
};
use dialtone_sqlx::{
    control::actor::update_image::update_actor_with_image_attr_insert,
    db::persistent_queue::{insert_job::insert_job, JobDbType},
    logic::{
        ap_object::new::new_fetched_media_ap_object,
        persistent_queue::job_details::{
            FetchRemoteBannerJobDetails, FetchRemoteIconJobDetails, JobDetails,
        },
    },
};
use lazy_static::lazy_static;
use ril::prelude::*;
use sqlx::PgPool;
use tempdir::TempDir;
use tracing::debug;

use crate::{
    client::file::fetch_file,
    media::{create_remote_media_dir, fetched_media_url, media_type_from_image_format},
};

use super::TaskError;

lazy_static! {
    pub static ref MAX_ICON_FILE_SIZE: u64 = {
        let size = std::env::var("MAX_ICON_FILE_SIZE").unwrap_or_else(|_| "5MB".to_string());
        parse_size::parse_size(size).unwrap_or(5000000)
    };
}

pub async fn fetch_remote_icon(
    pool: &PgPool,
    FetchRemoteIconJobDetails {
        actor,
        fetched_for_user,
        host_name,
    }: FetchRemoteIconJobDetails,
) -> Result<(), TaskError> {
    debug!("Max Icon Size = {}", *MAX_ICON_FILE_SIZE);
    let icon_url = actor.clone().icon.and_then(|icon| icon.first_url());
    let actor = if let Some(ref url) = icon_url {
        let temp_dir = TempDir::new("dialtone_fetch_remote_icon")?;
        let dl_file = temp_dir.path().join("downloaded.icon");
        let dl_file_string = dl_file.to_string_lossy();
        let http_get_data = fetch_file(url, &dl_file_string, *MAX_ICON_FILE_SIZE).await?;
        let image = Image::<Rgb>::open(dl_file)?.resized(400, 400, ResizeAlgorithm::Lanczos3);
        let format = image.format();
        let local_file_name = format!("actor_icon.{format}");
        let path = create_remote_media_dir(&actor.id)
            .await?
            .join(&local_file_name);
        image.save(format, &path)?;
        let local_url = fetched_media_url(&host_name, &actor.id, &local_file_name)?;
        let ap_object = new_fetched_media_ap_object(
            &host_name,
            &actor.id,
            url,
            &local_url,
            dialtone_common::ap::ap_object::ApObjectType::Image,
            media_type_from_image_format(&format),
        );
        let system_data = ApObjectSystemData {
            file_data: Some(FileData {
                file_path: path.to_string_lossy().to_string(),
            }),
            http_get_data: Some(http_get_data),
            fetched_for_user: fetched_for_user.clone(),
        };
        let mut actor = actor.clone();
        actor.icon = Some(dialtone_common::ap::image_attr::ImageAttributes::SingleUrl(
            local_url,
        ));
        update_actor_with_image_attr_insert(
            pool,
            actor.clone(),
            ap_object,
            &host_name,
            system_data,
        )
        .await?;
        actor
    } else {
        actor
    };

    if icon_url.is_none() {
        debug!("No icon to fetch for {}", &actor.id);
    }

    // queue fetch remote image
    let job_details = JobDetails::FetchRemoteBanner(FetchRemoteBannerJobDetails {
        actor,
        fetched_for_user,
        host_name,
    });
    debug!("inserting fetch remote banner job {job_details:?}");
    insert_job(
        pool,
        &JobDbType::FetchRemoteBanner,
        &job_details,
        Some(3),
        None,
    )
    .await?;
    Ok(())
}
