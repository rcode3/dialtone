use dialtone_common::{
    containers::file::FileData, rest::ap_objects::ap_object_model::ApObjectSystemData,
};
use dialtone_sqlx::{
    control::actor::update_image::update_actor_with_image_attr_insert,
    logic::{
        ap_object::new::new_fetched_media_ap_object,
        persistent_queue::job_details::FetchRemoteBannerJobDetails,
    },
};
use lazy_static::lazy_static;
use ril::prelude::*;
use sqlx::PgPool;
use tempdir::TempDir;
use tracing::debug;

use crate::{
    client::file::fetch_file,
    media::{create_remote_media_dir, fetched_media_url, media_type_from_image_format},
};

use super::TaskError;

lazy_static! {
    pub static ref MAX_BANNER_FILE_SIZE: u64 = {
        let size = std::env::var("MAX_BANNER_FILE_SIZE").unwrap_or_else(|_| "15MB".to_string());
        parse_size::parse_size(size).unwrap_or(15000000)
    };
}

pub async fn fetch_remote_banner(
    pool: &PgPool,
    FetchRemoteBannerJobDetails {
        actor,
        fetched_for_user,
        host_name,
    }: FetchRemoteBannerJobDetails,
) -> Result<(), TaskError> {
    debug!("Max Banner Size = {}", *MAX_BANNER_FILE_SIZE);
    let banner_url = actor.clone().image.and_then(|banner| banner.first_url());
    if let Some(ref url) = banner_url {
        let temp_dir = TempDir::new("dialtone_fetch_remote_banner")?;
        let dl_file = temp_dir.path().join("downloaded.banner");
        let dl_file_string = dl_file.to_string_lossy();
        let http_get_data = fetch_file(url, &dl_file_string, *MAX_BANNER_FILE_SIZE).await?;
        let image = Image::<Rgb>::open(dl_file)?.resized(1500, 500, ResizeAlgorithm::Lanczos3);
        let format = image.format();
        let local_file_name = format!("actor_banner.{format}");
        let path = create_remote_media_dir(&actor.id)
            .await?
            .join(&local_file_name);
        image.save(format, &path)?;
        let local_url = fetched_media_url(&host_name, &actor.id, &local_file_name)?;
        let ap_object = new_fetched_media_ap_object(
            &host_name,
            &actor.id,
            url,
            &local_url,
            dialtone_common::ap::ap_object::ApObjectType::Image,
            media_type_from_image_format(&format),
        );
        let system_data = ApObjectSystemData {
            file_data: Some(FileData {
                file_path: path.to_string_lossy().to_string(),
            }),
            http_get_data: Some(http_get_data),
            fetched_for_user,
        };
        let mut actor = actor.clone();
        actor.image = Some(dialtone_common::ap::image_attr::ImageAttributes::SingleUrl(
            local_url,
        ));
        update_actor_with_image_attr_insert(pool, actor, ap_object, &host_name, system_data)
            .await?;
    }
    if banner_url.is_none() {
        debug!("No banner to fetch for {}.", actor.id)
    };
    Ok(())
}
