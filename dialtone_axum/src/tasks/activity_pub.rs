use dialtone_sqlx::logic::persistent_queue::job_details::SendActivityPubJobDetails;

use crate::client::ap::send::send_activity;

use super::TaskError;

pub async fn send_activity_pub(
    SendActivityPubJobDetails {
        public_key_id,
        private_key_pem,
        target_uri,
        activity,
    }: SendActivityPubJobDetails,
) -> Result<(), TaskError> {
    send_activity(&target_uri, &public_key_id, &private_key_pem, &activity).await?;
    Ok(())
}
