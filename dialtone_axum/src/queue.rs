use dialtone_sqlx::{
    db::persistent_queue::{pop_queue::pop_queue, retry::retry_job, JobDbType},
    logic::persistent_queue::job_details::JobDetails,
};
use sqlx::{postgres::PgNotification, PgPool};
use thiserror::Error;
use tracing::{error, warn};

use crate::tasks::{
    activity_pub::send_activity_pub, banner::fetch_remote_banner, icon::fetch_remote_icon,
    TaskError,
};

#[derive(Debug, Error)]
pub enum QueueError {
    #[error(transparent)]
    DbError(#[from] sqlx::Error),
    #[error(transparent)]
    TaskError(#[from] TaskError),
}

pub async fn process_remote_icon_queue(
    pool: &PgPool,
    notification: PgNotification,
) -> Result<(), QueueError> {
    let mut tx = pool.begin().await?;
    tracing::info!("fetch remote icon queue notification {:?}", notification);
    let job = pop_queue(&mut tx, &JobDbType::FetchRemoteIcon)
        .await
        .unwrap();
    if let Some(job) = job {
        if let JobDetails::FetchRemoteIcon(job_details) = job.job_details {
            let job_task = fetch_remote_icon(pool, job_details).await;
            if let Some(err) = job_task.err() {
                match err {
                    TaskError::Http(_) => {
                        warn!("Unable to fetch icon for job {}", job.job_id);
                        let _ = retry_job(&mut tx, job.job_id, job.attempts_remaining)
                            .await
                            .map_err(|_| error!("error submitting job for retrial"));
                    }
                    _ => error!("Unrecoverable error for job {}: {err}", job.job_id),
                }
            }
        } else {
            error!("Unrecognized job in Fetch Remote Icon queue: {job:?}");
        }
    }
    tx.commit().await?;
    Ok(())
}

pub async fn process_remote_banner_queue(
    pool: &PgPool,
    notification: PgNotification,
) -> Result<(), QueueError> {
    let mut tx = pool.begin().await?;
    tracing::info!("fetch remote banner queue notification {:?}", notification);
    let job = pop_queue(&mut tx, &JobDbType::FetchRemoteBanner)
        .await
        .unwrap();
    if let Some(job) = job {
        if let JobDetails::FetchRemoteBanner(job_details) = job.job_details {
            let job_task = fetch_remote_banner(pool, job_details).await;
            if let Some(err) = job_task.err() {
                match err {
                    TaskError::Http(_) => {
                        warn!("Unable to fetch banner for job {}", job.job_id);
                        let _ = retry_job(&mut tx, job.job_id, job.attempts_remaining)
                            .await
                            .map_err(|_| error!("error submitting job for retrial"));
                    }
                    _ => error!("Unrecoverable error for job {}: {err}", job.job_id),
                }
            }
        } else {
            error!("Unrecognized job in Fetch Remote Banner queue: {job:?}");
        }
    }
    tx.commit().await?;
    Ok(())
}

pub async fn process_send_activity_pub(
    pool: &PgPool,
    notification: PgNotification,
) -> Result<(), QueueError> {
    let mut tx = pool.begin().await?;
    tracing::info!("send activity pub queue notification {:?}", notification);
    let job = pop_queue(&mut tx, &JobDbType::SendActivityPub)
        .await
        .unwrap();
    if let Some(job) = job {
        if let JobDetails::SendActivityPub(job_details) = job.job_details {
            let job_task = send_activity_pub(job_details).await;
            if let Some(err) = job_task.err() {
                match err {
                    TaskError::Http(_) => {
                        warn!("Unable to send activity pub for job {}", job.job_id);
                        let _ = retry_job(&mut tx, job.job_id, job.attempts_remaining)
                            .await
                            .map_err(|_| error!("error submitting job for retrial"));
                    }
                    _ => error!("Unrecoverable error for job {}: {err}", job.job_id),
                }
            }
        } else {
            error!("Unrecognized job in Send Activity Pub queue: {job:?}");
        }
    }
    tx.commit().await?;
    Ok(())
}
