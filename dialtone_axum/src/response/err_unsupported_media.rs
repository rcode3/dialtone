#[macro_export]
macro_rules! err_unsupported_media {
    () => {
        Err($crate::response::response_error::ResponseError::UnsupportedMedia(None))
    };
}
