pub mod err_bad_request_macro;
pub mod err_not_found_macro;
pub mod err_unauthorized_macro;
pub mod err_unsupported_media;
pub mod ok_failure_macro;
pub mod ok_success_macro;
pub mod response_error;
