#[macro_export]
macro_rules! ok_failure {
    () => {
        Ok(axum::Json(
            dialtone_common::rest::simple_exchanges::SimpleResponse::failure(),
        ))
    };
}
