#[macro_export]
macro_rules! err_bad_request {
    ($msg:expr) => {
        Err($crate::response::response_error::ResponseError::BadRequest(
            Some($msg.to_string()),
        ))
    };
    () => {
        Err($crate::response::response_error::ResponseError::BadRequest(
            None,
        ))
    };
}
