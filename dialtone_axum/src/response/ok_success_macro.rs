#[macro_export]
macro_rules! ok_success {
    () => {
        Ok(axum::Json(
            dialtone_common::rest::simple_exchanges::SimpleResponse::success(),
        ))
    };
}
