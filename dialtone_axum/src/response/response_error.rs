use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::Json;
use dialtone_common::ap::id::IdError;
use dialtone_common::ap::ActivityPubError;
use dialtone_common::rest::actors::actor_exchanges::GetActorTypeError;
use dialtone_common::rest::simple_exchanges::SimpleResponse;
use dialtone_common::utils::parse_acct::ParseUserAcctError;
use dialtone_sqlx::DbError;

use crate::client::ClientError;
use crate::http_signature::HttpSignatureError;

#[derive(Debug)]
pub enum ResponseError {
    WrongCredentials(Option<String>),
    MissingCredentials(Option<String>),
    TokenDecodingError(Option<String>),
    TokenCreation(Option<String>),
    BadTokenInHeader(Option<String>),
    UnableToAuthenticate(Option<String>),
    NoAuthorizationsAvailable(Option<String>),
    UnsupportedMedia(Option<String>),
    UnableToAuthorize(Option<String>),
    BadRequest(Option<String>),
    NotFound(Option<String>),
    InternalServerError(Option<String>),
}

impl ResponseError {
    pub fn not_found() -> ResponseError {
        ResponseError::NotFound(None)
    }
}

impl IntoResponse for ResponseError {
    fn into_response(self) -> Response {
        let (status, error_message, log_msg) = match self {
            ResponseError::WrongCredentials(ref log_msg) => {
                (StatusCode::FORBIDDEN, "Wrong credentials", log_msg)
            }
            ResponseError::MissingCredentials(ref log_msg) => {
                (StatusCode::UNAUTHORIZED, "Missing credentials", log_msg)
            }
            ResponseError::TokenDecodingError(ref log_msg) => {
                (StatusCode::BAD_REQUEST, "Token decoding error", log_msg)
            }
            ResponseError::TokenCreation(ref log_msg) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                "Token creation error",
                log_msg,
            ),
            ResponseError::BadTokenInHeader(ref log_msg) => {
                (StatusCode::BAD_REQUEST, "Bad token in header", log_msg)
            }
            ResponseError::UnableToAuthenticate(ref log_msg) => {
                (StatusCode::UNAUTHORIZED, "Wrong credentials", log_msg)
            }
            ResponseError::NoAuthorizationsAvailable(ref log_msg) => (
                StatusCode::FORBIDDEN,
                "No authorizations available",
                log_msg,
            ),
            ResponseError::UnableToAuthorize(ref log_msg) => {
                (StatusCode::FORBIDDEN, "Unable to authorize", log_msg)
            }
            ResponseError::BadRequest(ref log_msg) => {
                if let Some(msg) = log_msg {
                    (StatusCode::BAD_REQUEST, msg.as_str(), log_msg)
                } else {
                    (StatusCode::BAD_REQUEST, "Bad request", log_msg)
                }
            }
            ResponseError::UnsupportedMedia(ref log_msg) => (
                StatusCode::UNSUPPORTED_MEDIA_TYPE,
                "Unsupported Media",
                log_msg,
            ),
            ResponseError::NotFound(ref log_msg) => (StatusCode::NOT_FOUND, "Not found", log_msg),
            ResponseError::InternalServerError(ref log_msg) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                "Internal server error",
                log_msg,
            ),
        };
        if let Some(log_msg) = log_msg {
            match self {
                ResponseError::InternalServerError(_) => tracing::error!("{}", log_msg),
                _ => tracing::warn!("{}", log_msg),
            }
        }
        let body = Json(SimpleResponse::failure_with_msg(error_message));
        (status, body).into_response()
    }
}

impl From<serde_json::Error> for ResponseError {
    fn from(err: serde_json::Error) -> Self {
        ResponseError::BadRequest(Some(err.to_string()))
    }
}

impl From<DbError> for ResponseError {
    fn from(err: DbError) -> Self {
        match err {
            DbError::Internal(err) => ResponseError::InternalServerError(Some(err.to_string())),
            DbError::Sqlx(err) => ResponseError::InternalServerError(Some(err.to_string())),
            DbError::BadInput(err) => ResponseError::BadRequest(Some(err.to_string())),
        }
    }
}

impl From<anyhow::Error> for ResponseError {
    fn from(err: anyhow::Error) -> Self {
        ResponseError::InternalServerError(Some(err.to_string()))
    }
}

impl From<IdError> for ResponseError {
    fn from(err: IdError) -> Self {
        ResponseError::BadRequest(Some(err.to_string()))
    }
}

impl From<ParseUserAcctError> for ResponseError {
    fn from(_err: ParseUserAcctError) -> Self {
        ResponseError::BadRequest(None)
    }
}

impl From<ClientError> for ResponseError {
    fn from(err: ClientError) -> Self {
        match err {
            ClientError::InvalidInput(err) => ResponseError::BadRequest(Some(err)),
            ClientError::RequestTooLarge => ResponseError::BadRequest(None),
            ClientError::InvalidUri(err) => ResponseError::BadRequest(Some(err.to_string())),
            _ => ResponseError::InternalServerError(Some(err.to_string())),
        }
    }
}

impl From<HttpSignatureError> for ResponseError {
    fn from(err: HttpSignatureError) -> Self {
        ResponseError::BadRequest(Some(err.to_string()))
    }
}

impl From<ActivityPubError> for ResponseError {
    fn from(err: ActivityPubError) -> Self {
        ResponseError::BadRequest(Some(err.to_string()))
    }
}

impl From<GetActorTypeError> for ResponseError {
    fn from(err: GetActorTypeError) -> Self {
        ResponseError::BadRequest(Some(err.to_string()))
    }
}
