#[macro_export]
macro_rules! err_unauthorized {
    () => {
        Err($crate::response::response_error::ResponseError::UnableToAuthorize(None))
    };
}
