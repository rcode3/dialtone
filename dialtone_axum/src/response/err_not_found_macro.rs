#[macro_export]
macro_rules! err_not_found {
    () => {
        Err($crate::response::response_error::ResponseError::NotFound(
            None,
        ))
    };
}
