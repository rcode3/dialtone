[package]
name = "dialtone_axum"
version = "0.1.0"
edition = "2021"
publish = true
license = "MIT OR Apache-2.0"
authors = ["Andrew Newton <andy@hxr.us>"]
description = "Dialtone Axum Back-end"
repository = "https://codeberg.org/rcode3/dialtone"
keywords = ["activitypub", "activitystreams", "fediverse", "federation"]

[dependencies]

# base64 implementation
base64ct = { version = "1.5", features = ["alloc"] }

# this project
dialtone_common = { version = "0.1.0", path = "../dialtone_common" }
dialtone_sqlx = { version = "0.1.0", path = "../dialtone_sqlx" }

# anyhow (error handling)
anyhow = "1.0"

# axum (web server)
axum = { version = "0.6", features = ["headers"] }

# client IP address extractor
axum-client-ip = "0.3.0"

# chrono (time and date library)
chrono = { version = "0.4", features = ["alloc", "std", "clock", "serde"] }

# const_format (compile time constant formatting)
const_format = "0.2.21"

# futures-utils
futures-util = "0.3"

# headers (http headers)
headers = "0.3"

# http constructs
http = "0.2"

# hyper (http implementation used by axum)
hyper = { version = "0.14", features = ["full"] }

# jsonwebtoken (JSON web token)
jsonwebtoken = "8.1.0"

# lazy_static (lazy static initialization)
lazy_static = "1.4.0"

# openssl
openssl = "0.10"

# parse strings for byte sizes
parse-size = "1.0"

# percent encoding
percent-encoding = "2.2"

# regex
regex = "1"

# reqwest (http client library)
reqwest = { version = "0.11", features = ["json", "stream"] }

# rust image library
ril = {version = "0.8.0", features = ["all-pure"] }

# serde (serialization / deserialization)
serde = { version = "^1", features = ["derive"] }
serde_json = "^1"
serde_variant = "0.1.1"

# sqlx (async db)
sqlx = { version = "0.6", features = [
    "runtime-tokio-rustls",
    "postgres",
    "chrono",
    "macros",
    "json",
    "offline",
] }

# temporary directory
tempdir = "0.3"

# thiserror (error propagation)
thiserror = "1.0"

# tower (tokio/axum middleware)
tower = { version = "0.4", features = ["timeout", "util"] }
tower-http = { version = "0.3.3", features = [
    "add-extension",
    "trace",
    "cors",
] }

# tokio (async runtime)
tokio = { version = "1", features = ["full"] }
tokio-cron-scheduler = "0.8"

# tracing (logging)
tracing = "0.1"
tracing-subscriber = { version = "0.3", features = ["env-filter"] }

[dev-dependencies]

# this project
dialtone_reqwest = { path = "../dialtone_reqwest" }
dialtone_test_util = { path = "../dialtone_test_util" }

# hyper (http implementation used by axum)
hyper = { version = "0.14", features = ["full"] }

# tower (tokio/axum middleware)
tower = { version = "0.4", features = ["util"] }
