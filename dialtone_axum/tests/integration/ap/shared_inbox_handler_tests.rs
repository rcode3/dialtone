use dialtone_axum::client::ap::send::send_activity;
use dialtone_common::ap::activity::Activity;
use dialtone_common::ap::ap_object::{ApObject, ApObjectType};
use dialtone_common::ap::AP_PUBLIC_ADDRESS;
use dialtone_sqlx::db::collection::insert::insert_into_collection;
use dialtone_sqlx::db::collection::page::page_by_id;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_server;

use crate::ap::fixup_url;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_known_actor_WHEN_send_activity_to_shared_inbox_THEN_followers_inboxes_gets_activity()
{
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let known_actor_id = actors.no_role_actor.owned_actor.ap.id;
        let sockaddr = start_test_server(pool.clone()).await;
        let shared_inbox = fixup_url(
            &sockaddr,
            &actors
                .no_role_actor
                .owned_actor
                .ap
                .endpoints
                .unwrap()
                .shared_inbox
                .unwrap(),
        );
        insert_into_collection(
            &pool,
            &actors.content_admin_actor.owned_actor.ap.following.unwrap(),
            &known_actor_id,
        )
        .await
        .unwrap();
        insert_into_collection(
            &pool,
            &actors.actor_admin_actor.owned_actor.ap.following.unwrap(),
            &known_actor_id,
        )
        .await
        .unwrap();

        // WHEN
        let ap_object = ApObject::builder()
            .id(format!("{known_actor_id}/test_apobject"))
            .ap_type(ApObjectType::Note)
            .content("<p>foo</p>")
            .attributed_to(&known_actor_id)
            .to(AP_PUBLIC_ADDRESS)
            .build();
        let activity = Activity::new_for_actor(
            dialtone_common::ap::activity::ActivityType::Create,
            known_actor_id,
            dialtone_common::ap::activity::ActivityObjectType::ApObject(Box::new(ap_object)),
        );
        let private_key = actors
            .no_role_actor
            .owned_actor
            .owner_data
            .as_ref()
            .unwrap()
            .key_pairs
            .first()
            .unwrap();
        let action = send_activity(
            &shared_inbox,
            &private_key.key_id,
            &private_key.private_key_pem,
            &activity,
        )
        .await;

        // THEN
        test_action!(action);
        // check inbox collection in the database
        let follower1_inbox = actors.content_admin_actor.owned_actor.ap.inbox.unwrap();
        let page = page_by_id(&pool, None, None, 10, &follower1_inbox, None)
            .await
            .unwrap()
            .unwrap();
        assert_eq!(page.total_items.unwrap(), 1);
        let follower2_inbox = actors.actor_admin_actor.owned_actor.ap.inbox.unwrap();
        let page = page_by_id(&pool, None, None, 10, &follower2_inbox, None)
            .await
            .unwrap()
            .unwrap();
        assert_eq!(page.total_items.unwrap(), 1);
    })
    .await;
}
