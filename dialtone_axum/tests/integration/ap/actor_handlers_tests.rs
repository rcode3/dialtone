use dialtone_axum::client::ap::fetch::fetch_ap_actor;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_server;

use crate::ap::fixup_url;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_system_with_actor_WHEN_fetch_ap_actor_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let actor_id = actors.no_role_actor.owned_actor.ap.id;
        let sockaddr = start_test_server(pool).await;
        let actor_id = fixup_url(&sockaddr, &actor_id);

        // WHEN
        let action = fetch_ap_actor(&actor_id).await;

        // THEN
        test_action!(action);
    })
    .await;
}
