use dialtone_axum::client::ap::fetch::{fetch_ap_actor, fetch_ap_collection};
use dialtone_axum::client::ap::send::send_activity;
use dialtone_common::ap::activity::Activity;
use dialtone_common::ap::ap_object::{ApObject, ApObjectType};
use dialtone_common::ap::AP_PUBLIC_ADDRESS;
use dialtone_reqwest::ap::send::send;
use dialtone_sqlx::db::collection::page::page_by_id;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{TEST_NOROLEUSER_NAME, TEST_PASSWORD};
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::{start_test_server, start_test_srv_and_auth};

use crate::ap::{fixup_url, fixup_url_from_sc};

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_actor_with_no_followers_WHEN_fetch_actors_followers_THEN_followers_not_found() {
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let actor_id = actors.no_role_actor.owned_actor.ap.id;
        let sockaddr = start_test_server(pool).await;
        let actor_id = fixup_url(&sockaddr, &actor_id);
        println!("getting actor {actor_id}");
        let actor = fetch_ap_actor(&actor_id).await.unwrap();
        let followers_collection_id = actor.followers;

        // WHEN
        let collection_id = followers_collection_id.unwrap();
        let collection_id = fixup_url(&sockaddr, &collection_id);
        println!("getting collection {collection_id}");
        let action = fetch_ap_collection(&collection_id).await;

        // THEN
        test_action!(action);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_known_actor_WHEN_when_send_activity_to_inbox_THEN_inbox_gets_activity() {
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let known_actor_id = actors.no_role_actor.owned_actor.ap.id;
        let target_actor_id = actors.all_role_actor.owned_actor.ap.id;
        let target_inbox = actors.all_role_actor.owned_actor.ap.inbox.unwrap();
        let sockaddr = start_test_server(pool.clone()).await;
        let fixed_known_actor_id = fixup_url(&sockaddr, &known_actor_id);
        let fixed_target_actor_id = fixup_url(&sockaddr, &target_actor_id);
        let fixed_target_inbox = fixup_url(&sockaddr, &target_inbox);
        println!("sending from {known_actor_id}({fixed_known_actor_id}) to {target_actor_id}({fixed_target_actor_id})");

        // WHEN
        let ap_object = ApObject::builder()
            .id(format!("{known_actor_id}/test_apobject"))
            .ap_type(ApObjectType::Note)
            .content("<p>foo</p>")
            .attributed_to(&known_actor_id)
            .build();
        let activity = Activity::new_for_actor(
            dialtone_common::ap::activity::ActivityType::Create,
            known_actor_id,
            dialtone_common::ap::activity::ActivityObjectType::ApObject(Box::new(ap_object)),
        );
        let private_key = actors
            .no_role_actor
            .owned_actor
            .owner_data
            .as_ref()
            .unwrap()
            .key_pairs
            .first()
            .unwrap();
        let action = send_activity(
            &fixed_target_inbox,
            &private_key.key_id,
            &private_key.private_key_pem,
            &activity,
        )
        .await;

        // THEN
        test_action!(action);
        // check inbox collection in the database
        let page = page_by_id(&pool, None, None, 10, &target_inbox, None)
            .await
            .unwrap()
            .unwrap();
        assert_eq!(page.total_items.unwrap(), 1);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_logged_in_user_WHEN_when_send_public_ap_object_THEN_outbox_gets_activity() {
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let known_actor_id = actors.no_role_actor.owned_actor.ap.id;
        let sc = start_test_srv_and_auth(pool.clone(), TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;
        let fixed_known_actor_id = fixup_url_from_sc(&sc, &known_actor_id);
        println!("known actor id {known_actor_id} fixed to {fixed_known_actor_id}");

        // WHEN
        let ap_object = ApObject::builder()
            .id(format!("{known_actor_id}/test_apobject"))
            .ap_type(ApObjectType::Note)
            .content("<p>foo</p>")
            .attributed_to(&known_actor_id)
            .to(AP_PUBLIC_ADDRESS)
            .build();
        let activity = Activity::new_for_actor(
            dialtone_common::ap::activity::ActivityType::Create,
            known_actor_id,
            dialtone_common::ap::activity::ActivityObjectType::ApObject(Box::new(ap_object)),
        );
        let action = send(&sc, &activity).await;

        // THEN
        test_action!(action);
        // check outbox collection in the database
        let outbox = &actors.no_role_actor.owned_actor.ap.outbox.unwrap();
        let page = page_by_id(&pool, None, None, 10, outbox, None)
            .await
            .unwrap()
            .unwrap();
        assert_eq!(page.total_items.unwrap(), 1);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
#[should_panic]
async fn GIVEN_not_auth_user_WHEN_when_send_public_ap_object_THEN_outbox_gets_activity() {
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let known_actor_id = actors.all_role_actor.owned_actor.ap.id;
        let sc = start_test_srv_and_auth(pool.clone(), TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;
        let fixed_known_actor_id = fixup_url_from_sc(&sc, &known_actor_id);
        println!("known actor id {known_actor_id} fixed to {fixed_known_actor_id}");

        // WHEN
        let ap_object = ApObject::builder()
            .id(format!("{known_actor_id}/test_apobject"))
            .ap_type(ApObjectType::Note)
            .content("<p>foo</p>")
            .attributed_to(&known_actor_id)
            .to(AP_PUBLIC_ADDRESS)
            .build();
        let activity = Activity::new_for_actor(
            dialtone_common::ap::activity::ActivityType::Create,
            known_actor_id,
            dialtone_common::ap::activity::ActivityObjectType::ApObject(Box::new(ap_object)),
        );
        let action = send(&sc, &activity).await;

        // THEN
        action.unwrap();
    })
    .await;
}
