use std::net::SocketAddr;

use dialtone_reqwest::site_connection::SiteConnection;

mod actor_handlers_tests;
mod collection_handlers_tests;
mod shared_inbox_handler_tests;

pub fn fixup_url(sockaddr: &SocketAddr, url: &str) -> String {
    // gotta fix up the URI for local testing
    axum::http::Uri::builder()
        .scheme("http")
        .authority(format!("{}:{}", sockaddr.ip(), sockaddr.port()))
        .path_and_query(
            axum::http::Uri::try_from(url)
                .unwrap()
                .path_and_query()
                .unwrap()
                .to_string(),
        )
        .build()
        .unwrap()
        .to_string()
}

pub fn fixup_url_from_sc(sc: &SiteConnection, url: &str) -> String {
    axum::http::Uri::builder()
        .scheme("http")
        .authority(sc.host_addr.to_owned())
        .path_and_query(
            axum::http::Uri::try_from(url)
                .unwrap()
                .path_and_query()
                .unwrap()
                .to_string(),
        )
        .build()
        .unwrap()
        .to_string()
}
