use dialtone_common::rest::actors::actor_exchanges::GetActorRequest;
use dialtone_common::rest::actors::actor_model::{ActorSystemInfo, ActorVisibility};
use dialtone_reqwest::api_dt::actors::change_sysinfo::change_actor_sysinfo;
use dialtone_reqwest::api_dt::actors::get_sysinfo::get_actor_sysinfo;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{TEST_ACTORADMIN_NAME, TEST_PASSWORD};
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_srv_and_auth;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_actoradmin_WHEN_get_actor_sysinfo_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN actoradmin

        let actors = create_system_tst_utl(&pool).await;
        let sc = start_test_srv_and_auth(pool, TEST_ACTORADMIN_NAME, TEST_PASSWORD).await;

        // WHEN

        let actor_id = actors.no_role_actor.owned_actor.ap.id;
        let action = get_actor_sysinfo(&sc, &GetActorRequest::new(actor_id)).await;

        // THEN

        test_action!(action);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_actoradmin_WHEN_put_actor_sysinfo_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN actoradmin

        let actors = create_system_tst_utl(&pool).await;
        let sc = start_test_srv_and_auth(pool, TEST_ACTORADMIN_NAME, TEST_PASSWORD).await;

        // WHEN

        let actor_id = actors.no_role_actor.owned_actor.ap.id;
        let sysinfo = ActorSystemInfo {
            actor_id: actor_id.clone(),
            visibility: ActorVisibility::Visible,
            system_data: None,
        };
        let action = change_actor_sysinfo(&sc, &sysinfo).await;

        // THEN

        test_action!(action);
        assert!(action.unwrap());
    })
    .await;
}
