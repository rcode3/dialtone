use dialtone_reqwest::api_dt::actors::change_default::change_default_actor;
use dialtone_test_util::create_actor::create_actor_for_user_tst_utl;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{
    TEST_HOSTNAME, TEST_NOROLEUSER_ACCT, TEST_NOROLEUSER_NAME, TEST_PASSWORD,
};
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_srv_and_auth;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_user_with_2_actors_WHEN_change_default_actor_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        create_system_tst_utl(&pool).await;
        let second_actor = create_actor_for_user_tst_utl(
            &pool,
            "new_actor",
            TEST_HOSTNAME,
            TEST_NOROLEUSER_ACCT,
            false,
        )
        .await;
        let second_actor_id = second_actor.owned_actor.ap.id;
        let sc = start_test_srv_and_auth(pool, TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;

        // WHEN
        let action = change_default_actor(&sc, &second_actor_id).await;

        // THEN
        test_action!(action);
        assert!(action.unwrap());
    })
    .await;
}
