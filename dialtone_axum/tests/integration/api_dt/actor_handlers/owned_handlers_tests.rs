use dialtone_common::ap::actor::ActorType;
use dialtone_common::rest::actors::actor_exchanges::{
    GetActorRequest, NewActorRequest, PutUpdateActorRequest,
};
use dialtone_common::rest::actors::actor_model::UpdateActorAp;
use dialtone_common::rest::users::web_user::UserStatus;
use dialtone_reqwest::api_dt::actors::create::create_actor;
use dialtone_reqwest::api_dt::actors::get_owned::get_owned_actor;
use dialtone_reqwest::api_dt::actors::update::update_owned_actor;
use dialtone_reqwest::api_dt::users::authenticate::authenticate;
use dialtone_reqwest::dt_reqwest_error::DtReqwestError;
use dialtone_reqwest::site_connection::SiteConnection;
use dialtone_sqlx::db::user::change_status::change_user_status;
use dialtone_sqlx::db::user::create_user;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{TEST_HOSTNAME, TEST_NOROLEUSER_NAME, TEST_PASSWORD};
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::{start_test_server, start_test_srv_and_auth};

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_user_WHEN_create_actor_THEN_get_actor_success() {
    test_pg(move |pool| async move {
        // GIVEN
        create_system_tst_utl(&pool).await;
        let username = "test";
        let host_name = TEST_HOSTNAME;
        let acct = format!("{username}@{host_name}");
        let pw = "secretpassword";
        let action = create_user(&pool, &acct, pw).await;
        test_action!(action);

        let action = change_user_status(&pool, &acct, &UserStatus::Active).await;
        test_action!(action);

        let addr = start_test_server(pool).await;
        let sc = SiteConnection::new_site(&addr.to_string(), host_name);
        println!("sites connection = {sc:?}");

        let action = authenticate(&sc, username, pw).await;
        test_action!(action);
        let sc = action.unwrap();

        // WHEN
        let new_actor_request = NewActorRequest {
            preferred_user_name: "an_actor".to_string(),
            actor_type: ActorType::Person,
            name: None,
            summary: None,
        };
        let action = create_actor(&sc, &new_actor_request).await;
        test_action!(action);
        let actor = action.unwrap();

        // THEN
        let request = GetActorRequest {
            actor_id_or_wf_acct: actor.ap.id.clone(),
        };
        let action = get_owned_actor(&sc, &request).await;
        test_action!(action);
        let fetched_actor = action.unwrap();
        assert_eq!(fetched_actor.ap.id, actor.ap.id);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_actor_WHEN_create_actor_again_THEN_bad_request() {
    test_pg(move |pool| async move {
        // GIVEN
        create_system_tst_utl(&pool).await;
        let username = "test";
        let host_name = TEST_HOSTNAME;
        let acct = format!("{username}@{host_name}");
        let pw = "secretpassword";
        let action = create_user(&pool, &acct, pw).await;
        test_action!(action);

        let action = change_user_status(&pool, &acct, &UserStatus::Active).await;
        test_action!(action);

        let addr = start_test_server(pool).await;
        let sc = SiteConnection::new_site(&addr.to_string(), host_name);
        println!("sites connection = {sc:?}");

        let action = authenticate(&sc, username, pw).await;
        test_action!(action);
        let sc = action.unwrap();

        let new_actor_request = NewActorRequest {
            preferred_user_name: "an_actor".to_string(),
            actor_type: ActorType::Person,
            name: None,
            summary: None,
        };
        let action = create_actor(&sc, &new_actor_request).await;
        test_action!(action);

        // WHEN
        let action = create_actor(&sc, &new_actor_request).await;

        // THEN
        assert!(action.is_err());
        let err = action.err().unwrap();
        if let DtReqwestError::AuthenticationRequired = err {
            panic!()
        }
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_actor_WHEN_update_actor_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let sc = start_test_srv_and_auth(pool, TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;
        let actor_id = actors.no_role_actor.owned_actor.ap.id;

        // WHEN
        let request = PutUpdateActorRequest {
            actor_id,
            update: UpdateActorAp {
                name: Some("new name".to_string()),
                summary: None,
                icon: None,
                image: None,
            },
        };
        let action = update_owned_actor(&sc, &request).await;

        // THEN
        test_action!(action);
        assert!(action.unwrap());
    })
    .await;
}
