use dialtone_common::rest::actors::actor_exchanges::GetActorRequest;
use dialtone_reqwest::api_dt::actors::get_actor::get_public_actor;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{TEST_NOROLEUSER_NAME, TEST_PASSWORD};
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_srv_and_auth;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_system_with_actor_WHEN_fetch_public_actor_THEN_succes() {
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let actor_id = actors.no_role_actor.owned_actor.ap.id;
        let sc = start_test_srv_and_auth(pool, TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;

        // WHEN
        let action = get_public_actor(&sc, &GetActorRequest::new(actor_id)).await;

        // THEN
        test_action!(action);
    })
    .await;
}
