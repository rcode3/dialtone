use dialtone_common::rest::users::web_user::UserStatus;
use dialtone_reqwest::api_dt::users::authenticate::authenticate;
use dialtone_reqwest::api_dt::users::get_user::get_user;
use dialtone_reqwest::site_connection::SiteConnection;
use dialtone_sqlx::db::user::change_status::change_user_status;
use dialtone_sqlx::db::user::create_user;
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_server;

#[tokio::test]
async fn authenticate_test() {
    test_pg(move |pool| async move {
        let username = "test";
        let host_name = "example.com";
        let acct = format!("{username}@{host_name}");
        let pw = "secretpassword";
        let action = create_user(&pool, &acct, pw).await;
        if action.is_err() {
            println!("{:?}", action.as_ref().err())
        }
        assert!(action.is_ok());
        let action = change_user_status(&pool, &acct, &UserStatus::Active).await;
        if action.is_err() {
            println!("{:?}", action.as_ref().err())
        }
        assert!(action.is_ok());

        let addr = start_test_server(pool).await;
        let sc = SiteConnection::new_site(&addr.to_string(), host_name);
        println!("sites connection = {sc:?}");

        let action = authenticate(&sc, username, pw).await;
        if action.is_err() {
            println!("{:?}", action.as_ref().err())
        }
        assert!(action.is_ok());
        let sc = action.unwrap();

        let action = get_user(&sc, None).await;
        if action.is_err() {
            println!("{:?}", action.as_ref().err())
        }
        assert!(action.is_ok());
    })
    .await;
}
