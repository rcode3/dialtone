use dialtone_reqwest::api_dt::names::check_name_available::check_name_available;
use dialtone_reqwest::site_connection::SiteConnection;
use dialtone_sqlx::db::user::create_user;
use dialtone_test_util::test_action;
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_server;

#[tokio::test]
async fn get_name_test() {
    test_pg(move |pool| async move {
        let username = "test";
        let host_name = "example.com";
        let acct = format!("{username}@{host_name}");
        let pw = "secretpassword";
        let action = create_user(&pool, &acct, pw).await;
        test_action!(action);

        let addr = start_test_server(pool).await;
        let sc = SiteConnection::new_site(&addr.to_string(), host_name);
        println!("sites connection = {sc:?}");

        let action = check_name_available(&sc, username).await;
        test_action!(action);
        let result = action.unwrap();
        assert!(!result);

        let action = check_name_available(&sc, "some_other_user").await;
        test_action!(action);
        let result = action.unwrap();
        assert!(result);
    })
    .await;
}
