use dialtone_reqwest::api_dt::users::get_sysinfo::get_user_sysinfo;

use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{
    TEST_NOROLEUSER_ACCT, TEST_PASSWORD, TEST_USERADMIN_NAME,
};
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_srv_and_auth;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_useradmin_WHEN_get_user_sysinfo_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN useradmin

        create_system_tst_utl(&pool).await;
        let sc = start_test_srv_and_auth(pool, TEST_USERADMIN_NAME, TEST_PASSWORD).await;

        // WHEN put user sysinfo

        let action = get_user_sysinfo(&sc, TEST_NOROLEUSER_ACCT).await;

        // THEN

        test_action!(action);
    })
    .await;
}
