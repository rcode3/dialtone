use axum::http::StatusCode;

use dialtone_common::rest::users::user_exchanges::{PostUser, PostUserResponse};
use dialtone_common::rest::users::user_login::UserCredential;

use dialtone_reqwest::api_dt::users::get_user::get_user;

use dialtone_reqwest::api_dt::users::register_user::register_user;
use dialtone_reqwest::site_connection::SiteConnection;
use dialtone_test_util::create_site::{
    create_site_tst_utl, open_registration, simple_code_registration,
};
use dialtone_test_util::create_system::{
    create_bare_system_for_host_tst_utl, create_system_tst_utl,
};
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{
    TEST_ACTORADMIN_ACCT, TEST_HOSTNAME, TEST_NOROLEUSER_ACCT, TEST_NOROLEUSER_NAME, TEST_PASSWORD,
    TEST_USERADMIN_NAME,
};
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::{start_test_server, start_test_srv_and_auth};

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_useradmin_WHEN_get_other_user_info_THEN_success() {
    // NOTE: the case of a users getting their own users info is tested for in the authenticate_handlers_tests
    test_pg(move |pool| async move {
        // GIVEN useradmin
        create_system_tst_utl(&pool).await;
        let sc = start_test_srv_and_auth(pool, TEST_USERADMIN_NAME, TEST_PASSWORD).await;

        // WHEN get_user_info

        let action = get_user(&sc, Some(TEST_NOROLEUSER_ACCT)).await;

        // THEN succeed
        test_action!(action);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_user_WHEN_get_other_user_info_THEN_forbidden() {
    test_pg(move |pool| async move {
        // GIVEN no role
        create_system_tst_utl(&pool).await;
        let sc = start_test_srv_and_auth(pool, TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;

        // WHEN get_user_info for another user

        let action = get_user(&sc, Some(TEST_ACTORADMIN_ACCT)).await;

        // THEN forbidden
        assert!(action.is_err());
        println!("{:?}", action.as_ref().err().as_ref().unwrap());
        assert_eq!(action.err().unwrap().status_code(), StatusCode::FORBIDDEN);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_no_users_and_open_registration_WHEN_register_user_THEN_success_WHEN_retry_THEN_fail()
{
    test_pg(move |pool| async move {
        // GIVEN
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        open_registration(&pool, TEST_HOSTNAME).await;
        let addr = start_test_server(pool).await;
        let sc = SiteConnection::new_site(&addr.to_string(), TEST_HOSTNAME);

        // WHEN
        let action = register_user(
            &sc,
            &PostUser::OpenRegistration(UserCredential {
                user_acct: TEST_NOROLEUSER_ACCT.to_string(),
                password: TEST_PASSWORD.to_string(),
            }),
        )
        .await;

        // THEN
        test_action!(action);
        let registered = action.unwrap();
        assert_ne!(registered, PostUserResponse::AccountNotRegistered(None));

        // WHEN
        let action = register_user(
            &sc,
            &PostUser::OpenRegistration(UserCredential {
                user_acct: TEST_NOROLEUSER_ACCT.to_string(),
                password: TEST_PASSWORD.to_string(),
            }),
        )
        .await;

        // THEN
        assert!(action.is_err());
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_no_registration_WHEN_register_user_THEN_fail() {
    test_pg(move |pool| async move {
        // GIVEN
        create_site_tst_utl(&pool, TEST_HOSTNAME).await;
        let addr = start_test_server(pool).await;
        let sc = SiteConnection::new_site(&addr.to_string(), TEST_HOSTNAME);

        // WHEN
        let action = register_user(
            &sc,
            &PostUser::OpenRegistration(UserCredential {
                user_acct: TEST_NOROLEUSER_ACCT.to_string(),
                password: TEST_PASSWORD.to_string(),
            }),
        )
        .await;

        // THEN
        assert!(action.is_err());
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_simple_code_registration_WHEN_register_user_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        create_bare_system_for_host_tst_utl(&pool, TEST_HOSTNAME).await;
        simple_code_registration(&pool, TEST_HOSTNAME, "1234".to_string()).await;
        let addr = start_test_server(pool).await;
        let sc = SiteConnection::new_site(&addr.to_string(), TEST_HOSTNAME);

        // WHEN
        let action = register_user(
            &sc,
            &PostUser::SimpleCode {
                user_creds: UserCredential {
                    user_acct: TEST_NOROLEUSER_ACCT.to_string(),
                    password: TEST_PASSWORD.to_string(),
                },
                simple_code_for_registration: "1234".to_string(),
            },
        )
        .await;

        // THEN
        test_action!(action);
        let registered = action.unwrap();
        assert_ne!(registered, PostUserResponse::AccountNotRegistered(None));
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_simple_code_registration_WHEN_register_user_as_open_THEN_fail() {
    test_pg(move |pool| async move {
        // GIVEN
        create_site_tst_utl(&pool, TEST_HOSTNAME).await;
        simple_code_registration(&pool, TEST_HOSTNAME, "1234".to_string()).await;
        let addr = start_test_server(pool).await;
        let sc = SiteConnection::new_site(&addr.to_string(), TEST_HOSTNAME);

        // WHEN
        let action = register_user(
            &sc,
            &PostUser::OpenRegistration(UserCredential {
                user_acct: TEST_NOROLEUSER_ACCT.to_string(),
                password: TEST_PASSWORD.to_string(),
            }),
        )
        .await;

        // THEN
        assert!(action.is_err());
    })
    .await;
}
