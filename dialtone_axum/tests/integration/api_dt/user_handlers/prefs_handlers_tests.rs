use dialtone_common::rest::sites::theme::Theme;
use dialtone_common::rest::users::user_exchanges::PutUserPreferences;

use dialtone_common::rest::users::web_user::UserPrefs;

use dialtone_reqwest::api_dt::users::change_preferences::change_preferences;

use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{
    TEST_NOROLEUSER_ACCT, TEST_NOROLEUSER_NAME, TEST_PASSWORD,
};
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_srv_and_auth;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_norole_user_WHEN_put_user_prefs_THEN_ok() {
    test_pg(move |pool| async move {
        // GIVEN norole
        create_system_tst_utl(&pool).await;
        let sc = start_test_srv_and_auth(pool, TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;

        // WHEN put_user_prefs
        let request = PutUserPreferences {
            acct: TEST_NOROLEUSER_ACCT.to_string(),
            preferences: UserPrefs {
                theme: Theme::GreenOnBlack,
            },
        };
        let action = change_preferences(&sc, &request).await;

        // THEN ok
        test_action!(action);
    })
    .await;
}
