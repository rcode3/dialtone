use dialtone_common::rest::users::user_exchanges::PutUserStatus;

use dialtone_common::rest::users::web_user::UserStatus;

use dialtone_reqwest::api_dt::users::change_status::change_status;

use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{
    TEST_NOROLEUSER_ACCT, TEST_PASSWORD, TEST_USERADMIN_NAME,
};
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_srv_and_auth;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_useradmin_WHEN_put_user_status_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN useradmin

        create_system_tst_utl(&pool).await;
        let sc = start_test_srv_and_auth(pool, TEST_USERADMIN_NAME, TEST_PASSWORD).await;

        // WHEN put user status

        let request = PutUserStatus {
            acct: TEST_NOROLEUSER_ACCT.to_string(),
            status: UserStatus::Suspended,
        };
        let action = change_status(&sc, &request).await;

        // THEN success

        test_action!(action);
    })
    .await;
}
