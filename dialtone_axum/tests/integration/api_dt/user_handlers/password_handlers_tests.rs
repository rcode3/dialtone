use dialtone_common::rest::users::user_exchanges::PutUserPassword;

use dialtone_reqwest::api_dt::users::change_password::change_password;

use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{
    TEST_NOROLEUSER_ACCT, TEST_NOROLEUSER_NAME, TEST_PASSWORD,
};
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_srv_and_auth;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_norole_user_WHEN_put_user_password_THEN_ok() {
    test_pg(move |pool| async move {
        // GIVEN norole
        create_system_tst_utl(&pool).await;
        let sc = start_test_srv_and_auth(pool, TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;

        // WHEN put_user_password
        let request = PutUserPassword {
            acct: TEST_NOROLEUSER_ACCT.to_string(),
            password: "new_password".to_string(),
        };
        let action = change_password(&sc, &request).await;

        // THEN ok
        test_action!(action);
    })
    .await;
}
