use std::collections::HashMap;

use dialtone_common::rest::sites::theme::Theme;
use dialtone_reqwest::api_dt::sites::get_site_info::get_site_info;
use dialtone_reqwest::site_connection::SiteConnection;
use dialtone_sqlx::db::site_info::create_site;
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_server;

#[tokio::test]
async fn get_public_site_info_test() {
    test_pg(move |pool| async move {
        let action = create_site(
            &pool,
            "example.com",
            "example".to_string(),
            Some("An example sites".to_string()),
            Some(Theme::GreenOnBlack),
            HashMap::new(),
        )
        .await;
        if action.is_err() {
            println!("{:?}", action.as_ref().err())
        }
        assert!(action.is_ok());

        let addr = start_test_server(pool).await;

        let sc = SiteConnection::new_site(&addr.to_string(), "example.com");
        println!("sites connection = {sc:?}");
        let action = get_site_info(&sc).await;
        if action.is_err() {
            println!("{:?}", action.as_ref().err())
        }
        assert!(action.is_ok())
    })
    .await;
}
