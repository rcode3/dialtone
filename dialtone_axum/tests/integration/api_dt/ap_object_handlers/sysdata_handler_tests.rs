use dialtone_common::rest::ap_objects::ap_object_model::ApObjectSystemData;
use dialtone_reqwest::api_dt::ap_objects::change_sysdata::change_ap_object_sysdata;
use dialtone_test_util::create_ap_object::create_article_for_actor_tst_utl;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{
    TEST_CONTENTADMIN_NAME, TEST_NOROLEUSER_NAME, TEST_PASSWORD,
};
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_srv_and_auth;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_ap_object_and_owner_WHEN_put_sysdata_THEN_error() {
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let actor_id = actors.no_role_actor.owned_actor.ap.id;
        let ap_object = create_article_for_actor_tst_utl(&pool, &actor_id, "foo").await;
        let ap_object_id = ap_object.id.unwrap();
        let sc = start_test_srv_and_auth(pool, TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;

        // WHEN
        let sys_data = ApObjectSystemData {};
        let action = change_ap_object_sysdata(&sc, &ap_object_id, sys_data).await;

        // THEN
        assert!(action.is_err())
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_ap_object_and_contentadmin_WHEN_put_sysdata_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let actor_id = actors.no_role_actor.owned_actor.ap.id;
        let ap_object = create_article_for_actor_tst_utl(&pool, &actor_id, "foo").await;
        let ap_object_id = ap_object.id.unwrap();
        let sc = start_test_srv_and_auth(pool, TEST_CONTENTADMIN_NAME, TEST_PASSWORD).await;

        // WHEN
        let sys_data = ApObjectSystemData {};
        let action = change_ap_object_sysdata(&sc, &ap_object_id, sys_data).await;

        // THEN
        test_action!(action);
    })
    .await;
}
