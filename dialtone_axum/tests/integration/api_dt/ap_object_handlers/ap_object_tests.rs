use dialtone_reqwest::api_dt::ap_objects::get_ap_object::get_ap_object;
use dialtone_test_util::{
    create_ap_object::create_article_for_actor_tst_utl,
    create_system::create_system_tst_utl,
    test_action,
    test_constants::{TEST_NOROLEUSER_NAME, TEST_PASSWORD},
    test_pg::test_pg,
    test_server::start_test_srv_and_auth,
};

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_ap_object_id_WHEN_get_ap_object_by_id_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let actor_id = actors.no_role_actor.owned_actor.ap.id;
        let ap_object = create_article_for_actor_tst_utl(&pool, &actor_id, "foo").await;
        let sc = start_test_srv_and_auth(pool, TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;

        // WHEN
        let action = get_ap_object(&sc, &ap_object.id.unwrap()).await;

        // THEN
        test_action!(action);
    })
    .await;
}
