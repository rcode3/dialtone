use dialtone_common::ap::ap_object::ApObjectType;
use dialtone_common::rest::ap_objects::ap_object_model::{
    CreateOwnedApObject, UpdateOwnedApObject,
};
use dialtone_reqwest::api_dt::ap_objects::create::create_ap_object;
use dialtone_reqwest::api_dt::ap_objects::get_owned::get_owned_ap_object;
use dialtone_reqwest::api_dt::ap_objects::update::update_ap_object;
use dialtone_test_util::create_ap_object::create_article_for_actor_tst_utl;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{TEST_NOROLEUSER_NAME, TEST_PASSWORD};
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_srv_and_auth;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_actor_WHEN_create_ap_object_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let sc = start_test_srv_and_auth(pool, TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;
        let actor_id = actors.no_role_actor.owned_actor.ap.id;

        // WHEN
        let ap_object = CreateOwnedApObject {
            name: None,
            media_type: None,
            ap_type: ApObjectType::Note,
            content: Some("this is content".to_string()),
            summary: None,
            owner_data: None,
            to: None,
            cc: None,
            bto: None,
            bcc: None,
        };
        let action = create_ap_object(&sc, &actor_id, &ap_object).await;

        // THEN
        test_action!(action);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_ap_object_WHEN_update_ap_object_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let actor_id = actors.no_role_actor.owned_actor.ap.id;
        let ap_object = create_article_for_actor_tst_utl(&pool, &actor_id, "foo").await;
        let sc = start_test_srv_and_auth(pool, TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;

        // WHEN
        let update = UpdateOwnedApObject {
            name: None,
            content: Some("this is content".to_string()),
            summary: None,
            owner_data: None,
        };
        let action = update_ap_object(&sc, &ap_object.id.unwrap(), &update).await;

        // THEN
        test_action!(action);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_ap_object_id_WHEN_get_owned_ap_object_by_id_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let actor_id = actors.no_role_actor.owned_actor.ap.id;
        let ap_object = create_article_for_actor_tst_utl(&pool, &actor_id, "foo").await;
        let sc = start_test_srv_and_auth(pool, TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;

        // WHEN
        let action = get_owned_ap_object(&sc, &ap_object.id.unwrap()).await;

        // THEN
        test_action!(action);
    })
    .await;
}
