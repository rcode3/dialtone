use dialtone_common::rest::paging::public_ap_object::{
    PublicApObjectsByActor, PublicApObjectsByHost,
};
use dialtone_reqwest::api_dt::paging::public_ap_objects::{
    get_public_ap_objects_by_actor, get_public_ap_objects_by_host,
};
use dialtone_test_util::create_ap_object::create_article_for_actor_tst_utl;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{TEST_HOSTNAME, TEST_NOROLEUSER_NAME, TEST_PASSWORD};
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_srv_and_auth;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_ap_object_for_host_WHEN_page_by_host_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let actor_id = actors.no_role_actor.owned_actor.ap.id;
        let _ap_object = create_article_for_actor_tst_utl(&pool, &actor_id, "foo").await;
        let sc = start_test_srv_and_auth(pool, TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;

        // WHEN
        let request = PublicApObjectsByHost {
            prev_date: None,
            next_date: None,
            limit: 10,
            host_name: TEST_HOSTNAME.to_string(),
        };
        let action = get_public_ap_objects_by_host(&sc, &request).await;

        // THEN
        test_action!(action);
        let page = action.unwrap();
        assert_eq!(page.items.len(), 1);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_ap_object_for_actor_WHEN_page_by_actor_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let actor_id = actors.no_role_actor.owned_actor.ap.id;
        let _ap_object = create_article_for_actor_tst_utl(&pool, &actor_id, "foo").await;
        let sc = start_test_srv_and_auth(pool, TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;

        // WHEN
        let request = PublicApObjectsByActor {
            prev_date: None,
            next_date: None,
            limit: 10,
            actor_id: actor_id.to_string(),
        };
        let action = get_public_ap_objects_by_actor(&sc, &request).await;

        // THEN
        test_action!(action);
        let page = action.unwrap();
        assert_eq!(page.items.len(), 1);
    })
    .await;
}
