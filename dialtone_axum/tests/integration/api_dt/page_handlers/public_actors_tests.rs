use dialtone_common::rest::paging::public_actor::PublicActorsByHost;
use dialtone_reqwest::api_dt::paging::public_actors::get_public_actors_by_host;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{TEST_ACTORADMIN_NAME, TEST_HOSTNAME, TEST_PASSWORD};
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_srv_and_auth;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_hostname_WHEN_page_public_actors_THEN_succes() {
    test_pg(move |pool| async move {
        // GIVEN
        create_system_tst_utl(&pool).await;
        let sc = start_test_srv_and_auth(pool, TEST_ACTORADMIN_NAME, TEST_PASSWORD).await;

        // WHEN page_public_actors
        let request = PublicActorsByHost {
            limit: 10,
            host_name: TEST_HOSTNAME.to_string(),
            prev_date: None,
            next_date: None,
            actor_type: None,
        };
        let action = get_public_actors_by_host(&sc, &request).await;

        // THEN success
        test_action!(action);
    })
    .await;
}
