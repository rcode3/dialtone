use dialtone_common::rest::paging::owned_actor::{OwnedActorsByHost, OwnedActorsByOwner};
use dialtone_common::rest::paging::PagingProps;
use dialtone_reqwest::api_dt::paging::owned_actors::*;
use dialtone_test_util::create_actor::create_many_actors_for_existing_user_tst_utl;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{
    TEST_ACTORADMIN_NAME, TEST_HOSTNAME, TEST_NOROLEUSER_ACCT, TEST_NOROLEUSER_NAME,
    TEST_NOROLEUSER_PUN, TEST_PASSWORD,
};
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_srv_and_auth;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_actoradmin_WHEN_page_all_owned_actors_THEN_succes() {
    test_pg(move |pool| async move {
        // GIVEN actoradmin
        create_system_tst_utl(&pool).await;
        let sc = start_test_srv_and_auth(pool, TEST_ACTORADMIN_NAME, TEST_PASSWORD).await;

        // WHEN page_actors
        let request = OwnedActorsByHost {
            prev_date: None,
            next_date: None,
            limit: 10,
            visibility: None,
            actor_type: None,
            host_name: TEST_HOSTNAME.to_string(),
        };
        let action = get_owned_actors_by_host(&sc, &request).await;

        // THEN success
        test_action!(action);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_no_role_WHEN_page_actors_by_owner_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN norole
        create_system_tst_utl(&pool).await;
        let sc = start_test_srv_and_auth(pool, TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;

        // WHEN page_actors_by_owner for no_role
        let request = OwnedActorsByOwner {
            prev_date: None,
            next_date: None,
            limit: 10,
            user_acct_owner: TEST_NOROLEUSER_ACCT.to_string(),
            actor_type: None,
        };
        let action = get_owned_actors_by_owner(&sc, &request).await;

        // THEN success
        test_action!(action);
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_no_role_and_first_page_WHEN_get_next_page_THEN_pages_not_the_same() {
    test_pg(move |pool| async move {
        // GIVEN norole
        create_system_tst_utl(&pool).await;
        create_many_actors_for_existing_user_tst_utl(
            &pool,
            TEST_NOROLEUSER_PUN,
            TEST_HOSTNAME,
            TEST_NOROLEUSER_ACCT,
            10,
        )
        .await;
        let sc = start_test_srv_and_auth(pool, TEST_NOROLEUSER_NAME, TEST_PASSWORD).await;

        // and GIVEN first page
        let request = OwnedActorsByOwner {
            prev_date: None,
            next_date: None,
            limit: 3,
            user_acct_owner: TEST_NOROLEUSER_ACCT.to_string(),
            actor_type: None,
        };
        let action = get_owned_actors_by_owner(&sc, &request).await;
        test_action!(action);
        let first_page = action.unwrap();

        // WHEN get next page
        let request = OwnedActorsByOwner::next_props(request, &first_page);
        let action = get_owned_actors_by_owner(&sc, &request).await;

        // THEN success
        test_action!(action);
        let second_page = action.unwrap();
        assert!(!first_page
            .items
            .iter()
            .all(|actor| second_page.items.contains(actor)))
    })
    .await;
}
