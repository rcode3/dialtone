use dialtone_common::rest::paging::user::UsersByHost;

use dialtone_reqwest::api_dt::paging::users::get_users_by_host;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{
    TEST_HOSTNAME, TEST_PASSWORD, TEST_USERADMIN_NAME, TEST_USER_COUNT,
};
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_srv_and_auth;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_useradmin_WHEN_page_users_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN useradmin
        create_system_tst_utl(&pool).await;
        let sc = start_test_srv_and_auth(pool, TEST_USERADMIN_NAME, TEST_PASSWORD).await;

        // WHEN page users

        let request = UsersByHost {
            prev_date: None,
            next_date: None,
            limit: 10,
            host_name: TEST_HOSTNAME.to_string(),
        };
        let action = get_users_by_host(&sc, &request).await;

        // THEN success
        test_action!(action);
        let page = action.unwrap();
        assert_eq!(page.items.len(), TEST_USER_COUNT);
    })
    .await;
}
