use dialtone_axum::client::webfinger::fetch::fetch_wf_acct;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::test_server::start_test_server;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_system_with_actor_WHEN_fetch_wf_acct_THEN_success() {
    test_pg(move |pool| async move {
        // GIVEN
        let actors = create_system_tst_utl(&pool).await;
        let wf_acct = actors.no_role_actor.owned_actor.jrd.unwrap().subject;
        let sockaddr = start_test_server(pool).await;

        // WHEN
        let action = fetch_wf_acct(&wf_acct, Some(sockaddr)).await;

        // THEN
        test_action!(action);
    })
    .await;
}
