use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::{create_system::create_system_tst_utl, test_server::start_test_server};

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_system_WHEN_fetch_host_meta_THEN_host_meta_is_retreived() {
    test_pg(move |pool| async move {
        // GIVEN
        create_system_tst_utl(&pool).await;
        let sockaddr = start_test_server(pool).await;

        // WHEN
        let url = format!(
            "http://{}:{}/.well-known/host-meta",
            sockaddr.ip(),
            sockaddr.port()
        );
        let action = reqwest::get(url).await;

        // THEN
        action.unwrap().error_for_status().unwrap();
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_system_WHEN_fetch_host_meta_json_THEN_host_meta_json_is_retreived() {
    test_pg(move |pool| async move {
        // GIVEN
        create_system_tst_utl(&pool).await;
        let sockaddr = start_test_server(pool).await;

        // WHEN
        let url = format!(
            "http://{}:{}/.well-known/host-meta.json",
            sockaddr.ip(),
            sockaddr.port()
        );
        let action = reqwest::get(url).await;

        // THEN
        action.unwrap().error_for_status().unwrap();
    })
    .await;
}
