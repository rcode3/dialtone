use dialtone_test_util::test_pg::test_pg;
use dialtone_test_util::{create_system::create_system_tst_utl, test_server::start_test_server};

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_system_WHEN_fetch_nodeinfo_jrd_THEN_nodeinfo_jrd_is_retreived() {
    test_pg(move |pool| async move {
        // GIVEN
        create_system_tst_utl(&pool).await;
        let sockaddr = start_test_server(pool).await;

        // WHEN
        let url = format!(
            "http://{}:{}/.well-known/nodeinfo",
            sockaddr.ip(),
            sockaddr.port()
        );
        let action = reqwest::get(url).await;

        // THEN
        action.unwrap().error_for_status().unwrap();
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_system_WHEN_fetch_nodeinfo_instance_THEN_nodeinfo_instance_is_retreived() {
    test_pg(move |pool| async move {
        // GIVEN
        create_system_tst_utl(&pool).await;
        let sockaddr = start_test_server(pool).await;

        // WHEN
        let url = format!(
            "http://{}:{}/.well-known/nodeinfo-instance",
            sockaddr.ip(),
            sockaddr.port()
        );
        let action = reqwest::get(url).await;

        // THEN
        action.unwrap().error_for_status().unwrap();
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_system_WHEN_fetch_x_nodeinfo2_instance_THEN_x_nodeinfo2_is_retreived() {
    test_pg(move |pool| async move {
        // GIVEN
        create_system_tst_utl(&pool).await;
        let sockaddr = start_test_server(pool).await;

        // WHEN
        let url = format!(
            "http://{}:{}/.well-known/x-nodeinfo2",
            sockaddr.ip(),
            sockaddr.port()
        );
        let action = reqwest::get(url).await;

        // THEN
        action.unwrap().error_for_status().unwrap();
    })
    .await;
}
