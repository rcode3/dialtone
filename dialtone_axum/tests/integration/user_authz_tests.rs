use dialtone_axum::authz::user_authz::authz_for_actor;
use dialtone_common::authz::user_authz_info::UserAuthzInfo;
use dialtone_common::rest::users::web_user::{SystemPermission, SystemRoleType, UserStatus};
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::{
    TEST_ACTORADMIN_ACCT, TEST_HOSTNAME, TEST_NOROLEUSER_ACCT, TEST_USERADMIN_ACCT,
};
use dialtone_test_util::test_pg::test_pg;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_actor_WHEN_actoradmin_authz_for_actor_THEN_true() {
    test_pg(move |pool| async move {
        // GIVEN actor
        let test_actors = create_system_tst_utl(&pool).await;

        // WHEN actoradmin authz_for_actor
        let user_authz = UserAuthzInfo {
            status: UserStatus::Active,
            acct: TEST_ACTORADMIN_ACCT.to_string(),
            system_permissions: vec![SystemPermission {
                role: SystemRoleType::ActorAdmin,
                host_name: TEST_HOSTNAME.to_string(),
            }],
        };
        let action = authz_for_actor(
            &pool,
            &user_authz,
            &test_actors.no_role_actor.owned_actor.ap.id,
        )
        .await;

        // THEN true
        test_action!(action);
        assert!(action.unwrap());
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
/// This also covers the use case of just another user such as
/// GIVEN actor WHEN other user authz_for_actor THEN err
async fn GIVEN_actor_WHEN_useradmin_authz_for_actor_THEN_false() {
    test_pg(move |pool| async move {
        // GIVEN actor
        let test_actors = create_system_tst_utl(&pool).await;
        let actor_id = test_actors.no_role_actor.owned_actor.ap.id;

        // WHEN userdmin authz_for_actor
        let user_authz = UserAuthzInfo {
            status: UserStatus::Active,
            acct: TEST_USERADMIN_ACCT.to_string(),
            system_permissions: vec![SystemPermission {
                role: SystemRoleType::UserAdmin,
                host_name: TEST_HOSTNAME.to_string(),
            }],
        };
        let action = authz_for_actor(&pool, &user_authz, &actor_id).await;

        // THEN false
        test_action!(action);
        assert!(!action.unwrap());
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_actor_WHEN_owner_authz_for_actor_THEN_ok() {
    test_pg(move |pool| async move {
        // GIVEN actor
        let test_actors = create_system_tst_utl(&pool).await;
        let actor_id = test_actors.no_role_actor.owned_actor.ap.id;

        // WHEN owner authz_for_actor
        let user_authz = UserAuthzInfo {
            status: UserStatus::Active,
            acct: TEST_NOROLEUSER_ACCT.to_string(),
            system_permissions: vec![],
        };
        let action = authz_for_actor(&pool, &user_authz, &actor_id).await;

        // THEN ok
        test_action!(action);
        assert!(action.unwrap());
    })
    .await;
}
