use dialtone_common::containers::credentialed_actor::CredentialedActor;
use dialtone_common::utils::make_acct::make_acct;
use dialtone_sqlx::control::actor::system::create_site_actors;
use dialtone_sqlx::db::user::create_user;
use sqlx::Pool;

use crate::{
    create_actor::create_actor_for_user_tst_utl,
    create_site::create_site_tst_utl,
    create_users::{
        create_actor_admin_tst_utl, create_all_role_user_tst_utl, create_content_admin_tst_utl,
        create_no_role_user_tst_utl, create_user_admin_tst_utl,
    },
    test_constants::{
        TEST_ACTORADMIN_ACCT, TEST_ACTORADMIN_NAME, TEST_ALLROLEUSER_ACCT, TEST_ALLROLEUSER_NAME,
        TEST_CONTENTADMIN_ACCT, TEST_CONTENTADMIN_NAME, TEST_HOSTNAME, TEST_NOROLEUSER_ACCT,
        TEST_NOROLEUSER_NAME, TEST_PASSWORD, TEST_SITE_USER, TEST_USERADMIN_ACCT,
        TEST_USERADMIN_NAME,
    },
};

pub struct CreateSystemTestActors {
    pub no_role_actor: CredentialedActor,
    pub content_admin_actor: CredentialedActor,
    pub actor_admin_actor: CredentialedActor,
    pub user_admin_actor: CredentialedActor,
    pub all_role_actor: CredentialedActor,
}

pub async fn create_bare_system_for_host_tst_utl(pool: &Pool<sqlx::Postgres>, host_name: &str) {
    // create the site
    create_site_tst_utl(pool, host_name).await;
    let site_acct = make_acct(host_name, TEST_SITE_USER);
    create_user(pool, &site_acct, TEST_PASSWORD).await.unwrap();
    create_site_actors(pool, host_name, &site_acct)
        .await
        .unwrap();
}

/// This function creates a test system, with a no role user, a content admin, actor admin,
/// user admin, and an all role user.
pub async fn create_system_tst_utl(pool: &Pool<sqlx::Postgres>) -> CreateSystemTestActors {
    // create the site
    create_site_tst_utl(pool, TEST_HOSTNAME).await;
    let site_acct = make_acct(TEST_HOSTNAME, TEST_SITE_USER);
    create_user(pool, &site_acct, TEST_PASSWORD).await.unwrap();
    create_site_actors(pool, TEST_HOSTNAME, &site_acct)
        .await
        .unwrap();

    // create the no role user and default actor.
    create_no_role_user_tst_utl(pool).await;
    let no_role_actor = create_actor_for_user_tst_utl(
        pool,
        TEST_NOROLEUSER_NAME,
        TEST_HOSTNAME,
        TEST_NOROLEUSER_ACCT,
        true,
    )
    .await;

    // create the content admin and default actor.
    create_content_admin_tst_utl(pool).await;
    let content_admin_actor = create_actor_for_user_tst_utl(
        pool,
        TEST_CONTENTADMIN_NAME,
        TEST_HOSTNAME,
        TEST_CONTENTADMIN_ACCT,
        true,
    )
    .await;

    // create the actor admin and default actor.
    create_actor_admin_tst_utl(pool).await;
    let actor_admin_actor = create_actor_for_user_tst_utl(
        pool,
        TEST_ACTORADMIN_NAME,
        TEST_HOSTNAME,
        TEST_ACTORADMIN_ACCT,
        true,
    )
    .await;

    // create the user admin and default actor.
    create_user_admin_tst_utl(pool).await;
    let user_admin_actor = create_actor_for_user_tst_utl(
        pool,
        TEST_USERADMIN_NAME,
        TEST_HOSTNAME,
        TEST_USERADMIN_ACCT,
        true,
    )
    .await;

    // create all role user and default actor.
    create_all_role_user_tst_utl(pool).await;
    let all_role_actor = create_actor_for_user_tst_utl(
        pool,
        TEST_ALLROLEUSER_NAME,
        TEST_HOSTNAME,
        TEST_ALLROLEUSER_ACCT,
        true,
    )
    .await;

    CreateSystemTestActors {
        no_role_actor,
        content_admin_actor,
        actor_admin_actor,
        user_admin_actor,
        all_role_actor,
    }
}
