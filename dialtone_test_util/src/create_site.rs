use std::collections::HashMap;

use dialtone_sqlx::db::site_info::create_site;
use dialtone_sqlx::db::site_info::fetch_site;
use dialtone_sqlx::db::site_info::update::update_site;
use sqlx::Pool;

pub async fn create_site_tst_utl(pool: &Pool<sqlx::Postgres>, host_name: &str) {
    create_site(
        pool,
        host_name,
        "test".to_string(),
        Some("testy sites".to_string()),
        None,
        HashMap::new(),
    )
    .await
    .unwrap();
}

pub async fn open_registration(pool: &Pool<sqlx::Postgres>, host_name: &str) {
    let site_info = fetch_site(pool, host_name).await.unwrap().unwrap();
    let mut site_data = site_info.site_data;
    let users = "users";
    site_data.system_roles_sets.insert(users.to_owned(), vec![]);
    site_data.set_open_registration(users, true).unwrap();
    update_site(pool, host_name, site_data).await.unwrap();
}

pub async fn simple_code_registration(pool: &Pool<sqlx::Postgres>, host_name: &str, code: String) {
    let site_info = fetch_site(pool, host_name).await.unwrap().unwrap();
    let mut site_data = site_info.site_data;
    let users = "users";
    site_data.system_roles_sets.insert(users.to_owned(), vec![]);
    site_data
        .set_simple_code_registration_code(users, &code, true)
        .unwrap();
    update_site(pool, host_name, site_data).await.unwrap();
}
