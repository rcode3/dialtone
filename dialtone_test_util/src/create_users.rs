use dialtone_common::rest::users::web_user::SystemRoleType;
use dialtone_common::rest::users::web_user::UserStatus;
use sqlx::Pool;

use crate::test_constants::{
    TEST_ACTORADMIN_ACCT, TEST_ALLROLEUSER_ACCT, TEST_CONTENTADMIN_ACCT, TEST_HOSTNAME,
    TEST_NOROLEUSER_ACCT, TEST_PASSWORD, TEST_USERADMIN_ACCT,
};
use dialtone_sqlx::db::system_role::add::add_system_role;
use dialtone_sqlx::db::user::{change_status::change_user_status, create_user};

pub async fn create_no_role_user_tst_utl(pool: &Pool<sqlx::Postgres>) {
    create_user(pool, TEST_NOROLEUSER_ACCT, TEST_PASSWORD)
        .await
        .unwrap();
    change_user_status(pool, TEST_NOROLEUSER_ACCT, &UserStatus::Active)
        .await
        .unwrap();
}

pub async fn create_content_admin_tst_utl(pool: &Pool<sqlx::Postgres>) {
    create_user(pool, TEST_CONTENTADMIN_ACCT, TEST_PASSWORD)
        .await
        .unwrap();
    change_user_status(pool, TEST_CONTENTADMIN_ACCT, &UserStatus::Active)
        .await
        .unwrap();
    add_system_role(
        pool,
        &SystemRoleType::ContentAdmin,
        TEST_CONTENTADMIN_ACCT,
        TEST_HOSTNAME,
    )
    .await
    .unwrap();
}

pub async fn create_actor_admin_tst_utl(pool: &Pool<sqlx::Postgres>) {
    create_user(pool, TEST_ACTORADMIN_ACCT, TEST_PASSWORD)
        .await
        .unwrap();
    change_user_status(pool, TEST_ACTORADMIN_ACCT, &UserStatus::Active)
        .await
        .unwrap();
    add_system_role(
        pool,
        &SystemRoleType::ActorAdmin,
        TEST_ACTORADMIN_ACCT,
        TEST_HOSTNAME,
    )
    .await
    .unwrap();
}

pub async fn create_user_admin_tst_utl(pool: &Pool<sqlx::Postgres>) {
    create_user(pool, TEST_USERADMIN_ACCT, TEST_PASSWORD)
        .await
        .unwrap();
    change_user_status(pool, TEST_USERADMIN_ACCT, &UserStatus::Active)
        .await
        .unwrap();
    add_system_role(
        pool,
        &SystemRoleType::UserAdmin,
        TEST_USERADMIN_ACCT,
        TEST_HOSTNAME,
    )
    .await
    .unwrap();
}

pub async fn create_all_role_user_tst_utl(pool: &Pool<sqlx::Postgres>) {
    create_user(pool, TEST_ALLROLEUSER_ACCT, TEST_PASSWORD)
        .await
        .unwrap();
    change_user_status(pool, TEST_ALLROLEUSER_ACCT, &UserStatus::Active)
        .await
        .unwrap();
    add_system_role(
        pool,
        &SystemRoleType::ContentAdmin,
        TEST_ALLROLEUSER_ACCT,
        TEST_HOSTNAME,
    )
    .await
    .unwrap();
    add_system_role(
        pool,
        &SystemRoleType::ActorAdmin,
        TEST_ALLROLEUSER_ACCT,
        TEST_HOSTNAME,
    )
    .await
    .unwrap();
    add_system_role(
        pool,
        &SystemRoleType::UserAdmin,
        TEST_ALLROLEUSER_ACCT,
        TEST_HOSTNAME,
    )
    .await
    .unwrap();
}
