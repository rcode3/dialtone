use crate::create_actor::create_actor_tst_utl;
use dialtone_common::ap::ap_object::{ApObject, ApObjectType};
use dialtone_common::ap::id::parse_actors_host;
use dialtone_common::ap::id::parse_actors_pun;
use dialtone_common::rest::ap_objects::ap_object_model::CreateOwnedApObject;
use dialtone_sqlx::db::ap_object::insert::insert_ap_object;
use dialtone_sqlx::db::ap_object::{ApObjectDbType, ApObjectVisibilityDbType};
use dialtone_sqlx::logic::ap_object::new::new_html_ap_object;
use sqlx::Pool;

pub async fn create_article_tst_utl(
    pool: &Pool<sqlx::Postgres>,
    host_name: &str,
    content: &str,
    pun: Option<&str>,
) -> ApObject {
    let (pun, actor_id) = match pun {
        None => {
            create_actor_tst_utl(pool, "bar", host_name).await;
            ("bar".to_string(), format!("https://{host_name}/pub/p/bar"))
        }
        Some(p) => (p.to_string(), format!("https://{host_name}/pub/p/{p}")),
    };
    let create_owned_ap_object = CreateOwnedApObject {
        name: None,
        media_type: None,
        ap_type: ApObjectType::Article,
        content: Some(format!("<p>{content}</p>")),
        summary: Some(format!("<p>{content}</p>")),
        owner_data: None,
        to: None,
        cc: None,
        bto: None,
        bcc: None,
    };
    let ap_object =
        new_html_ap_object(host_name, &pun, &actor_id, &create_owned_ap_object).unwrap();
    insert_ap_object(
        pool,
        host_name,
        &ap_object,
        None,
        ApObjectDbType::Article,
        None,
        ApObjectVisibilityDbType::Visible,
    )
    .await
    .unwrap();
    ap_object
}

pub async fn create_article_for_actor_tst_utl(
    pool: &Pool<sqlx::Postgres>,
    actor_id: &str,
    content: &str,
) -> ApObject {
    let pun = parse_actors_pun(actor_id).unwrap();
    let host_name = parse_actors_host(actor_id).unwrap();
    let create_owned_ap_object = CreateOwnedApObject {
        name: None,
        media_type: None,
        ap_type: ApObjectType::Article,
        content: Some(format!("<p>{content}</p>")),
        summary: Some(format!("<p>{content}</p>")),
        owner_data: None,
        to: None,
        cc: None,
        bto: None,
        bcc: None,
    };
    let ap_object =
        new_html_ap_object(&host_name, &pun, actor_id, &create_owned_ap_object).unwrap();
    insert_ap_object(
        pool,
        &host_name,
        &ap_object,
        None,
        ApObjectDbType::Article,
        None,
        ApObjectVisibilityDbType::Visible,
    )
    .await
    .unwrap();
    ap_object
}
