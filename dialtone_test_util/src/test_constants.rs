use const_format::concatcp;

pub const TEST_HOSTNAME: &str = "localhost";
pub const TEST_PASSWORD: &str = "secretpassword";

pub const TEST_SITE_USER: &str = "_site";

// content admin
pub const TEST_CONTENTADMIN_NAME: &str = "content_admin";
pub const TEST_CONTENTADMIN_ACCT: &str = concatcp!(TEST_CONTENTADMIN_NAME, "@", TEST_HOSTNAME);
pub const TEST_CONTENTADMIN_PUN: &str = "content_admin_actor";

// actor admin
pub const TEST_ACTORADMIN_NAME: &str = "actor_admin";
pub const TEST_ACTORADMIN_ACCT: &str = concatcp!(TEST_ACTORADMIN_NAME, "@", TEST_HOSTNAME);
pub const TEST_ACTORADMIN_PUN: &str = "actor_admin_actor";

// user admin
pub const TEST_USERADMIN_NAME: &str = "user_admin";
pub const TEST_USERADMIN_ACCT: &str = concatcp!(TEST_USERADMIN_NAME, "@", TEST_HOSTNAME);
pub const TEST_USERADMIN_PUN: &str = "user_admin_actor";

// user with no roles
pub const TEST_NOROLEUSER_NAME: &str = "no_role_user";
pub const TEST_NOROLEUSER_ACCT: &str = concatcp!(TEST_NOROLEUSER_NAME, "@", TEST_HOSTNAME);
pub const TEST_NOROLEUSER_PUN: &str = "no_role_user_actor";

// user with all roles
pub const TEST_ALLROLEUSER_NAME: &str = "all_role_user";
pub const TEST_ALLROLEUSER_ACCT: &str = concatcp!(TEST_ALLROLEUSER_NAME, "@", TEST_HOSTNAME);
pub const TEST_ALLROLEUSER_PUN: &str = "all_role_user_actor";

// count of all the TEST USERS.
pub const TEST_USER_COUNT: usize = 5;
