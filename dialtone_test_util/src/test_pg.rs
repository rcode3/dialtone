use portpicker::pick_unused_port;
use sqlx::migrate::Migrator;
use sqlx::{PgPool, Pool};
use std::collections::HashMap;
use std::future::Future;
use std::path::Path;
use tempdir::TempDir;
use testcontainers::images::postgres::Postgres;
use testcontainers::{clients, RunnableImage};

pub async fn test_pg<'a, F, Fut>(test: F)
where
    F: FnOnce(Pool<sqlx::Postgres>) -> Fut,
    Fut: Future<Output = ()>,
{
    let port = pick_unused_port().expect("unable to find free local port");
    let docker = clients::Cli::default();
    let image = RunnableImage::from(Postgres::default())
        .with_tag("13.8")
        .with_mapped_port((port, 5432));
    let container = docker.run(image);
    let host_port = container.get_host_port_ipv4(5432);

    let url = format!("postgres://postgres@localhost:{host_port}/");
    println!("url is {url}");
    let pool = PgPool::connect(&url).await.unwrap();
    // sqlx::migrate!("../dialtone_sqlx/migrations")
    Migrator::new(Path::new("../dialtone_sqlx/migrations"))
        .await
        .unwrap()
        .run(&pool)
        .await
        .unwrap();
    test(pool).await;
}

pub async fn test_pg_with_env<'a, F, Fut>(test: F)
where
    F: FnOnce(Pool<sqlx::Postgres>, HashMap<String, String>) -> Fut,
    Fut: Future<Output = ()>,
{
    let port = pick_unused_port().expect("unable to find free local port");
    let docker = clients::Cli::default();
    let image = RunnableImage::from(Postgres::default())
        .with_tag("13.8")
        .with_mapped_port((port, 5432));
    let container = docker.run(image);
    let host_port = container.get_host_port_ipv4(5432);

    let url = format!("postgres://postgres@localhost:{host_port}/");
    println!("url is {url}");
    let pool = PgPool::connect(&url).await.unwrap();
    // sqlx::migrate!("../dialtone_sqlx/migrations")
    Migrator::new(Path::new("../dialtone_sqlx/migrations"))
        .await
        .unwrap()
        .run(&pool)
        .await
        .unwrap();

    let temp_dir = TempDir::new("dialtone_test").unwrap();

    let mut env = HashMap::<String, String>::new();
    env.insert("DATABASE_URL".to_string(), url);
    env.insert(
        "MEDIA".to_string(),
        temp_dir.path().to_string_lossy().to_string(),
    );
    env.insert(
        "XDG_DATA_HOME".to_string(),
        temp_dir.path().to_string_lossy().to_string(),
    );
    env.insert(
        "XDG_CONFIG_HOME".to_string(),
        temp_dir.path().to_string_lossy().to_string(),
    );
    test(pool, env).await;

    temp_dir.close().unwrap();
}
