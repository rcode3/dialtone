#[macro_export]
macro_rules! test_action {
    ( $a:ident ) => {
        if $a.is_err() {
            println!("{:?}", $a.as_ref().err())
        }
        assert!($a.is_ok());
    };
}
