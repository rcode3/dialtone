use std::thread;
use std::time::Duration;

use dialtone_common::rest::users::web_user::UserStatus;
use dialtone_common::utils::make_acct::make_acct;
use sqlx::Pool;

use dialtone_common::ap::actor::ActorType;
use dialtone_common::containers::credentialed_actor::CredentialedActor;
use dialtone_sqlx::control::actor::create_owned::create_credentialed_actor;
use dialtone_sqlx::db::user::{change_status::change_user_status, create_user};
use dialtone_sqlx::logic::actor::new_actor::NewActor;

use crate::test_constants::TEST_PASSWORD;

pub async fn create_actor_tst_utl(
    pool: &Pool<sqlx::Postgres>,
    user_name: &str,
    host_name: &str,
) -> CredentialedActor {
    let acct = make_acct(user_name, host_name);
    create_user(pool, &acct, TEST_PASSWORD).await.unwrap();
    change_user_status(pool, &acct, &UserStatus::Active)
        .await
        .unwrap();
    create_actor_for_user_tst_utl(pool, user_name, host_name, &acct, true).await
}

pub async fn create_actor_for_user_tst_utl(
    pool: &Pool<sqlx::Postgres>,
    preferred_user_name: &str,
    host_name: &str,
    acct: &str,
    default_actor: bool,
) -> CredentialedActor {
    let new_actor = NewActor::builder()
        .preferred_user_name(preferred_user_name)
        .actor_type(ActorType::Person)
        .owner(acct)
        .build();
    let action = create_credentialed_actor(pool, new_actor, default_actor, host_name).await;
    if action.is_err() {
        println!("{:?}", action.as_ref().err())
    }
    assert!(action.is_ok());
    action.unwrap().unwrap()
}

pub async fn create_many_actors_tst_utl(
    pool: &Pool<sqlx::Postgres>,
    user_name: &str,
    host_name: &str,
    num_actors: i32,
) {
    let acct = make_acct(user_name, host_name);
    let action = create_user(pool, &acct, "supersecret").await;
    assert!(action.is_ok());
    for n in 1..=num_actors {
        let pun = format!("testactor{n}");
        create_actor_for_user_tst_utl(pool, &pun, host_name, &acct, false).await;
        // insure 1 millisecond separation
        thread::sleep(Duration::from_millis(1));
    }
}

pub async fn create_many_actors_for_user_tst_utl(
    pool: &Pool<sqlx::Postgres>,
    preferred_user_name: &str,
    host_name: &str,
    acct: &str,
    num_actors: i32,
) {
    create_user(pool, acct, TEST_PASSWORD).await.unwrap();
    change_user_status(pool, acct, &UserStatus::Active)
        .await
        .unwrap();
    for n in 1..=num_actors {
        let pun = format!("{preferred_user_name}_{n}");
        create_actor_for_user_tst_utl(pool, &pun, host_name, acct, false).await;
        // insure 1 millisecond separation
        thread::sleep(Duration::from_millis(1));
    }
}

pub async fn create_many_actors_for_existing_user_tst_utl(
    pool: &Pool<sqlx::Postgres>,
    preferred_user_name: &str,
    host_name: &str,
    acct: &str,
    num_actors: i32,
) {
    change_user_status(pool, acct, &UserStatus::Active)
        .await
        .unwrap();
    for n in 1..=num_actors {
        let pun = format!("{preferred_user_name}_{n}");
        create_actor_for_user_tst_utl(pool, &pun, host_name, acct, false).await;
        // insure 1 millisecond separation
        thread::sleep(Duration::from_millis(1));
    }
}
