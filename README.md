# dialtone

Dialtone is an un-imagined Activity Pub system. What does "un-imagined" mean? Basically, there is no intent to silo this software into a specific vertical "marketplace". Instead, emergent use cases will be addressed.

This software is under active, initial development and is far from feature complete or ready for use.

Current release: 0.0.0

## Goals

1. Multi-tenant. A single stack should support multiple instances or sites.
2. Multi-personality. Allow users to have multiple actors.
3. Multi-ownership. Allow multiple users to own the same actors.
4. Do it as much as possible in Rust. Yeah, some things may require bash scripts, but there is a certain level of ease when everything is in Rust.
5. Let's try to stay away from nightly. Rust has been out for awhile now, it should be doable without experimental features.
6. Should work on Raspberry Pi and other small computers so people can run instances in their homes.

## Creating a Database via Docker

To create the docker pg database server:

```
mkdir $HOME/var
docker create --name dt_pg \
	-p 5432:5432 \
        -e "POSTGRES_USER=dt_admin" \
        -e "POSTGRES_DB=dt" \
        -e "POSTGRES_PASSWORD=supersecret" \
        -e "PGDATA=/var/lib/postgresql/data/pgdata" \
        -v $HOME/var:/var/lib/postgresql/data \
        postgres:13.1

```

Start the database server:

```
docker start dt_pg
```

If the need arises to connect to the database server via the Postgres CLI:

```
docker exec -it dt_pg psql -e -U dt_admin dt 
```

## Install Cargo Make

[cargo make](https://github.com/sagiegurari/cargo-make) is used to orchestrate complex flows.

Install it with:

```shell
cargo install --force cargo-make
```

## Install System Tools

If you are using an Ubuntu system, you can install system tools with:

```shell
cargo make install_tools
```

If not, you'll need to install your systems equivalents of pkg-config, libssl-dev, and tar.
Additionally, you'll need to explicitly run the `cargo make install_tools` command in the
'dialtone_yew' directory.

## Build and Run Tests

To build and run tests, use the default 'cargo make' makefile:

```shell
cargo make
```
## Dialtone Server Configuration

Dialtone server configuration is set via environment variables. For ease of use, environment variables are also read from
a `.env` file in the current directory or any parent directory of the current directory.

Create an `.env` file in the current directory or a parent directory like such:

```shell
# Rust logging level
RUST_LOG="dialtone_axum=trace,tower_http=trace,sqlx=debug" 

# Postgres database URL
DATABASE_URL=postgres://dt_admin:supersecret@localhost/dt 

# JWT Secret
JWT_SECRET=secret

# Directory where media is stored.
MEDIA=/home/YOURDIR/var/dialtone/media

# Base for local media.
LOCAL_MEDIA_BASE="file:///home/YOURDIR/var/dialtone/media" 

# Log SQL statements. Set to true for debugging DB problems.
LOG_STATEMENTS=false
```

## Initialize Database

Create the Postgres database on the Postgres server:

```shell
cargo run --bin dt_ctrl database create
```

Create the schema:

```shell
cargo run --bin dt_ctrl database migrate
```


## Create a Test Site

Once a database is setup and the schema is created (see above), you can create a site by running
the following command and answering all the questions:

```shell
cargo run --bin dt_ctrl site create
```

Then, start the server (in the foreground):

```shell
cargo run --bin dt_server
```

## View Site Locally

To view a local site (perhaps for development purposes), execute the following in a seperate terminal:

```shell
cd dialtone_yew
trunk serve --open
```

## Local Image Loading

To use the `LOCAL_MEDIA_BASE` override in the environment variables for local development, your browser will need to be configured for
access to local resources. For Firefox, this can be done in the `About:Config` page with the following settings:

```
"capability.policy.policynames" => "localfilelinks"
"capability.policy.localfilelinks.sites" => "http://127.0.0.1:8080"
"capability.policy.localfilelinks.checkloaduri.enabled" => "allAccess"
```

## License

Licensed under either of

 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)
  at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the
Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.