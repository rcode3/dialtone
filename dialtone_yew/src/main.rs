use std::rc::Rc;

use crate::app_types::{AppPublicSiteInfo, AppSiteConnection};
use dialtone_reqwest::api_dt::sites::get_site_info::get_site_info;
use dialtone_reqwest::site_connection::SiteConnection;
use gloo::console::log;
use yew::prelude::*;
use yew_router::prelude::*;
use yewdux::prelude::*;

use crate::main_router::*;

pub mod app_types;
pub mod components;
pub mod main_router;
pub mod utils;

const IPV4_LOCAL_ADDR: &str = "127.0.0.1";
const IPV6_LOCAL_ADDR: &str = "::1";
const LOCALHOST: &str = "localhost";

#[function_component(App)]
fn app() -> Html {
    // Get and log environment information
    log!("Starting DialTone Yew App");

    html! {
        <>
            <AppSiteInfoSetter/>
            <BrowserRouter>
                <Switch<MainRoute> render={main_route_switch} />
            </BrowserRouter>
        </>
    }
}

pub fn get_host_info() -> SiteConnection {
    let location = gloo::utils::window().location();
    let location_host = location.host().unwrap();
    let location_host_name = location.hostname().unwrap();
    let (sc_host_addr, host_name) = match location_host_name.as_str() {
        IPV4_LOCAL_ADDR => (format!("{location_host_name}:3000"), LOCALHOST.to_string()),
        IPV6_LOCAL_ADDR => (format!("{location_host_name}:3000"), LOCALHOST.to_string()),
        _ => (location_host, location_host_name),
    };
    let protocol = location.protocol().unwrap();
    let secure = protocol.eq("https:");
    log!(format!(
        "protocol = '{}', secure = {}, sc_host_addr = {}, host_name = {}",
        &protocol, secure, sc_host_addr, host_name
    ));
    SiteConnection {
        host_addr: sc_host_addr,
        secure,
        auth_data: None,
        host_name: Some(host_name),
        user_name: None,
    }
}

#[function_component(AppSiteInfoSetter)]
fn app_site_info_setter() -> Html {
    let (sc, _sc_dispatch) = use_store::<AppSiteConnection>();
    let psi_dispatch = Dispatch::<AppPublicSiteInfo>::new();
    {
        use_effect_with_deps(
            move |_| {
                wasm_bindgen_futures::spawn_local(async move {
                    let sc = Rc::clone(&sc);
                    log!(
                        "sites backend =",
                        &sc.0.host_addr,
                        "; secure =",
                        &sc.0.secure.to_string(),
                        "; host name =",
                        &sc.0.host_name().to_string()
                    );
                    let public_site_info = get_site_info(&sc.0).await.unwrap();
                    let app_public_site_info = AppPublicSiteInfo(Some(public_site_info));
                    psi_dispatch.reduce(|_| app_public_site_info.into());
                });
                || ()
            },
            (),
        )
    }
    let h = html! {
        <></>
    };
    h
}

fn main() {
    yew::Renderer::<App>::new().render();
}
