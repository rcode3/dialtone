use crate::components::common::main_layout::*;
use crate::components::common::theme_setter::*;
use crate::components::common::titled_panel::*;
use dialtone_common::rest::sites::theme::Theme;
use yew::prelude::*;

#[function_component(Themes)]
pub fn themes() -> Html {
    html! {
        <MainLayout>
            <div class="centered_form_page">
                <TitledPanel title="Themes">
                    <div class="list_of_links">
                        <p><ThemeSetter theme={Theme::GreenOnBlack}/></p>
                        <p><ThemeSetter theme={Theme::GreenOnBlackWithAmber}/></p>
                        <p><ThemeSetter theme={Theme::BlackOnLightGray}/></p>
                        <p><ThemeSetter theme={Theme::BlackOnWhite}/></p>
                        <p><ThemeSetter theme={Theme::BlackOnWhiteWithRed}/></p>
                        <p><ThemeSetter theme={Theme::BlackOnWhiteWithGreen}/></p>
                        <p><ThemeSetter theme={Theme::WhiteOnBlue}/></p>
                        <p><ThemeSetter theme={Theme::WhiteOnBlueWithGreen}/></p>
                        <p><ThemeSetter theme={Theme::AmberOnBlack}/></p>
                        <p><ThemeSetter theme={Theme::AmberOnBlackWithBlue}/></p>
                        <p><ThemeSetter theme={Theme::BlueOnWhite}/></p>
                        <p><ThemeSetter theme={Theme::BlueOnBlack}/></p>
                        <p><ThemeSetter theme={Theme::Tandy400}/></p>
                        <p><ThemeSetter theme={Theme::Botho}/></p>
                    </div>
                </TitledPanel>
            </div>
        </MainLayout>
    }
}
