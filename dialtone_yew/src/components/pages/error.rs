use crate::components::common::main_layout::*;
use crate::components::common::titled_panel::*;
use crate::main_router::MainRoute;
use dialtone_reqwest::dt_reqwest_error::DtReqwestError;
use yew::prelude::*;
use yew_router::prelude::*;

pub fn handle_error(navigator: &Navigator, error: DtReqwestError) {
    match error {
        DtReqwestError::AuthenticationRequired => navigator.push(&MainRoute::AccessDenied),
        DtReqwestError::BadRequest => navigator.push(&MainRoute::BadRequest),
        DtReqwestError::ReqwestError(reqwest_error) => match reqwest_error.status() {
            Some(inner_error) => {
                if inner_error == 404 {
                    navigator.push(&MainRoute::NotFound)
                } else if inner_error == 403 || inner_error == 401 {
                    navigator.push(&MainRoute::AccessDenied)
                } else {
                    navigator.push(&MainRoute::SomethingWentWrong)
                }
            }
            None => navigator.push(&MainRoute::SomethingWentWrong),
        },
        _ => navigator.push(&MainRoute::SomethingWentWrong),
    }
}

#[function_component(NotFoundPage)]
pub fn not_found_page() -> Html {
    html! {
        <MainLayout>
            <div class="centered_form_page">
                <TitledPanel title="Not Found">
                    <div class="one_column_form">
                        <span class="warning_text">{"The thing you are looking for does not exist."}</span>
                    </div>
                </TitledPanel>
            </div>
        </MainLayout>
    }
}

#[function_component(SomethingWentWrongPage)]
pub fn something_went_wrong_page() -> Html {
    html! {
        <MainLayout>
            <div class="centered_form_page">
                <TitledPanel title="Ooopsie!">
                    <div class="one_column_form">
                        <span class="warning_text">{"Something unexplicable went wrong!"}</span>
                    </div>
                </TitledPanel>
            </div>
        </MainLayout>
    }
}

#[function_component(BadRequestPage)]
pub fn bad_request_page() -> Html {
    html! {
        <MainLayout>
            <div class="centered_form_page">
                <TitledPanel title="Bad Request">
                    <div class="one_column_form">
                        <span class="warning_text">{"The request made was ill formed or invalid in some way."}</span>
                    </div>
                </TitledPanel>
            </div>
        </MainLayout>
    }
}

#[function_component(AccessDeniedPage)]
pub fn access_denied_page() -> Html {
    html! {
        <MainLayout>
            <div class="centered_form_page">
                <TitledPanel title="Access Denied">
                    <div class="one_column_form">
                        <span class="warning_text">{"You do not have access to the information being requested."}</span>
                    </div>
                </TitledPanel>
            </div>
        </MainLayout>
    }
}

#[function_component(NotYetImplemented)]
pub fn not_yet_implemented() -> Html {
    html! {
        <MainLayout>
            <div class="centered_form_page">
                <TitledPanel title="Not Implemented">
                    <div class="one_column_form">
                        <span>{"The feature you want is not yet implemented. Check back later."}</span>
                    </div>
                </TitledPanel>
            </div>
        </MainLayout>
    }
}
