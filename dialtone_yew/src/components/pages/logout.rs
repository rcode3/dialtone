use std::rc::Rc;

use crate::app_types::reset_app_site_connection;
use crate::components::common::dt_button::*;
use crate::components::common::main_layout::*;
use crate::components::common::titled_panel::*;
use crate::MainRoute;
use yew::prelude::*;
use yew_router::prelude::*;

struct LogoutFormState {
    pub navigator: Navigator,
}

#[function_component(Logout)]
pub fn logout() -> Html {
    let form_state = Rc::new(LogoutFormState {
        navigator: use_navigator().unwrap(),
    });

    let on_button_click = {
        Callback::from(move |_| {
            let form_state = Rc::clone(&form_state);
            reset_app_site_connection();
            form_state.navigator.push(&MainRoute::Home);
        })
    };

    html! {
        <MainLayout>
            <div class="centered_form_page">
                <TitledPanel title="Logout">
                    <form class="one_column_form" onsubmit={Callback::from(|e: SubmitEvent| e.prevent_default())}>
                        <DtButton text="Logout" disabled=false on_click={on_button_click.clone()} id="logout_button" in_progress=false/>
                    </form>
                </TitledPanel>
            </div>
        </MainLayout>
    }
}
