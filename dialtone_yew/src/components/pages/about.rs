use crate::components::common::bordered_panel::*;
use crate::components::common::main_layout::*;
use dialtone_common::utils::version::DT_VERSION;
use yew::prelude::*;

#[function_component(About)]
pub fn about() -> Html {
    html! {
        <MainLayout>
            <div class="asymmetrical_split_page">
                <div class="asymmetrical_col_left">
                    {"Stuff about this instance goes here. Like an About post"}
                </div>
                <div class="asymmetrical_col_right">
                    <BorderedPanel>
                        <div style="text-align: center">
                            <div>
                                {"dialtone v."}{DT_VERSION}
                            </div>
                            <div>
                                {"Get it on "}<a class="off_site_link" href="https://codeberg.org/rcode3/dialtone">{"Codeberg!"}</a>
                            </div>
                        </div>
                    </BorderedPanel>
                </div>
            </div>
        </MainLayout>
    }
}
