use crate::app_types::AppSiteConnection;
use crate::components::common::loading::*;
use crate::components::common::main_layout::*;
use crate::components::common::owned_actor::*;
use crate::components::pages::error::handle_error;
use crate::main_router::MainRoute;
use dialtone_common::rest::actors::actor_exchanges::GetActorRequest;
use dialtone_common::rest::actors::actor_model::OwnedActor;
use dialtone_reqwest::api_dt::actors::get_owned::get_owned_actor;
use serde::{Deserialize, Serialize};
use yew::prelude::*;
use yew_router::prelude::*;
use yewdux::prelude::*;

use super::index::AccountProps;

#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
pub struct AccountActorQueryParams {
    pub actor_id: String,
}

#[function_component(AccountActor)]
pub fn account_actor(_props: &AccountProps) -> Html {
    let (sc, _sc_dispatch) = use_store::<AppSiteConnection>();
    let navigator = use_navigator().unwrap();
    let location = use_location().unwrap();
    let actor_id = location
        .query::<AccountActorQueryParams>()
        .map(|q| Some(q.actor_id))
        .unwrap_or(None);

    if actor_id.is_none() {
        navigator.push(&MainRoute::SomethingWentWrong)
    }

    let owned_actor: UseStateHandle<Option<OwnedActor>> = use_state(|| None);

    use_effect_with_deps(
        {
            let owned_actor = owned_actor.clone();
            move |_| {
                let owned_actor = owned_actor.clone();
                wasm_bindgen_futures::spawn_local(async move {
                    let request = GetActorRequest {
                        actor_id_or_wf_acct: actor_id.unwrap(),
                    };
                    let result = get_owned_actor(&sc.0, &request).await;
                    match result {
                        Ok(result) => owned_actor.set(Some(result)),
                        Err(err) => handle_error(&navigator, err),
                    }
                });
                || ()
            }
        },
        (),
    );

    html! {
        <MainLayout>
            if let Some(owned_actor) = &*owned_actor {
                <OwnedActorComponent owned_actor={owned_actor.clone()}></OwnedActorComponent>
            } else {
                <Loading/>
            }
        </MainLayout>
    }
}
