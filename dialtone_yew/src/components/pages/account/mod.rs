use yew::prelude::*;
use yew_router::Routable;

pub mod actor;
pub mod actors;
pub mod index;

use actor::*;
use actors::*;
use index::*;

#[derive(Debug, Clone, Routable, PartialEq)]
pub enum AccountRoute {
    #[at("/account/index/:acct")]
    Index { acct: String },

    #[at("/account/actors/:acct")]
    Actors { acct: String },

    #[at("/account/actor/:acct")]
    Actor { acct: String },
}

pub fn account_switch(routes: AccountRoute) -> Html {
    match routes {
        AccountRoute::Index { acct } => html! { <Account acct={acct.clone()}/> },
        AccountRoute::Actors { acct } => html! { <AccountActors acct={acct.clone()}/> },
        AccountRoute::Actor { acct } => html! { <AccountActor acct={acct.clone()}/> },
    }
}
