use crate::components::common::actor_image_placard::*;
use crate::components::common::bordered_panel::*;
use crate::components::common::event_log::*;
use crate::components::common::horizontal_header_table::*;
use crate::components::common::loading::*;
use crate::components::common::titled_panel::*;
use crate::components::common::unadorned_panel::*;
use crate::components::common::vertical_header_table::*;
use crate::components::common::Datum;
use crate::utils::DATETIME_FORMAT;
use crate::MainRoute;
use crate::{
    app_types::AppSiteConnection,
    components::{common::main_layout::*, pages::error::handle_error},
};
use dialtone_common::ap::actor::ActorType;
use dialtone_common::rest::users::web_user::LastLoginData;
use dialtone_common::rest::users::web_user::LastSeenData;
use dialtone_common::rest::users::web_user::SystemPermission;
use dialtone_common::rest::users::web_user::SystemRoleType;
use dialtone_common::rest::users::web_user::WebUser;
use dialtone_reqwest::api_dt::users::get_user::get_user;
use std::rc::Rc;
use yew::prelude::*;
use yew_router::prelude::*;
use yewdux::prelude::*;

use super::actors::AccountActorsQueryParams;
use super::AccountRoute;

#[derive(Properties, PartialEq, Eq)]
pub struct AccountProps {
    pub acct: String,
}

struct AccountPageState {
    pub sc: Rc<AppSiteConnection>,
    pub webuser: UseStateHandle<Option<WebUser>>,
    pub acct: String,
    pub navigator: Navigator,
}

#[function_component(Account)]
pub fn account(AccountProps { acct }: &AccountProps) -> Html {
    let (sc, _sc_dispatch) = use_store::<AppSiteConnection>();
    let page_state = Rc::new(AccountPageState {
        webuser: use_state(|| None),
        acct: acct.clone(),
        navigator: use_navigator().unwrap(),
        sc,
    });

    use_effect_with_deps(
        {
            let page_state = Rc::clone(&page_state);
            move |_| {
                let page_state = Rc::clone(&page_state);
                wasm_bindgen_futures::spawn_local(async move {
                    let page_state = Rc::clone(&page_state);
                    let webuser = get_user(&page_state.sc.0, Some(&page_state.acct)).await;
                    match webuser {
                        Ok(webuser) => page_state.webuser.set(Some(webuser)),
                        Err(err) => handle_error(&page_state.navigator, err),
                    }
                });
                || ()
            }
        },
        (),
    );

    let host_name = Rc::new(page_state.sc.0.host_name().to_string());
    html! {
        <MainLayout>
            if let Some(ref webuser) = (*page_state.webuser).clone() {
                <div class="asymmetrical_split_page">
                    <div class="asymmetrical_col_left">
                        if let Some(ref default_actor) = webuser.default_actor_data {
                            <ActorImagePlacard actor_data={ActorImagePlacardData::from(default_actor)} />
                        }
                        <ActorLinkPanel
                            webuser={webuser.clone()}
                            host_name={host_name.clone()} />
                        <TitledPanel title="Account">
                            <div class="link_nav">
                                <div>
                                    <Link<MainRoute>
                                        classes={classes!("in_page_action")}
                                        to={MainRoute::NotYetImplemented}
                                            >{"Preferences"}
                                    </Link<MainRoute>>
                                </div>
                                <div>
                                    <Link<MainRoute>
                                        classes={classes!("in_page_action")}
                                        to={MainRoute::NotYetImplemented}
                                            >{"Password"}
                                    </Link<MainRoute>>
                                </div>
                            </div>
                        </TitledPanel>
                    </div>
                    <div class="asymmetrical_col_right">
                        <BorderedPanel>
                            <div style="text-align:center">
                                <span class="info_text">{page_state.acct.clone()}</span>
                            </div>
                        </BorderedPanel>
                        <AccountInfoPanel webuser={webuser.clone()}>
                        </AccountInfoPanel>
                        <LoginInfoPanel logins={webuser.last_login_data.clone()}>
                        </LoginInfoPanel>
                        <PermissionsPanel permissions={webuser.user_authz.system_permissions.clone()}/>
                        <PingsInfoPanel pings={webuser.last_seen_data.clone()}>
                        </PingsInfoPanel>
                        <EventLog event_log={webuser.event_log.clone()}>
                        </EventLog>
                    </div>
                </div>
            } else {
                <Loading/>
            }
        </MainLayout>
    }
}

#[derive(Properties, Debug, PartialEq)]
struct AccountInfoPanelProps {
    pub webuser: WebUser,
}

#[function_component(AccountInfoPanel)]
fn account_info_panel(AccountInfoPanelProps { webuser }: &AccountInfoPanelProps) -> Html {
    let rows = vec![
        VerticalHeaderTableRow {
            header: "Status".to_string(),
            data: vec![Datum::new(webuser.status.to_string())],
        },
        VerticalHeaderTableRow {
            header: "Created".to_string(),
            data: vec![Datum::new_mono(
                webuser.created_at.format(DATETIME_FORMAT).to_string(),
            )],
        },
        VerticalHeaderTableRow {
            header: "Last Modified".to_string(),
            data: vec![Datum::new_mono(
                webuser.modified_at.format(DATETIME_FORMAT).to_string(),
            )],
        },
    ];
    html! {
        <UnadornedPanel>
            <div class="centered_col_container">
                <VerticalHeaderTable rows={rows}>
                </VerticalHeaderTable>
            </div>
        </UnadornedPanel>
    }
}

#[derive(Properties, PartialEq, Debug)]
struct LoginInfoPanelProps {
    pub logins: Vec<LastLoginData>,
}

#[function_component(LoginInfoPanel)]
fn login_info_panel(LoginInfoPanelProps { logins }: &LoginInfoPanelProps) -> Html {
    let headers = vec!["From".to_string(), "When".to_string(), "Login".to_string()];
    let rows = logins
        .iter()
        .map(|login| {
            vec![
                Datum::new(login.from.to_string()),
                Datum::new_mono(login.at.format(DATETIME_FORMAT).to_string()),
                Datum::new(if login.success {
                    "Succeeded".to_string()
                } else {
                    "Failed".to_string()
                }),
            ]
        })
        .collect::<Vec<Vec<Datum>>>();
    html! {
        <TitledPanel title="Logins">
            <div class="centered_col_container">
                <HorizontalHeaderTable headers={headers} rows={rows}>
                </HorizontalHeaderTable>
            </div>
        </TitledPanel>
    }
}

#[derive(Properties, PartialEq, Clone)]
struct PingsInfoPanelProps {
    pub pings: Vec<LastSeenData>,
}

#[function_component(PingsInfoPanel)]
fn pings_info_panel(PingsInfoPanelProps { pings }: &PingsInfoPanelProps) -> Html {
    let headers = vec!["From".to_string(), "When".to_string()];
    let rows = pings
        .iter()
        .map(|ping| {
            vec![
                Datum::new(ping.from.to_string()),
                Datum::new_mono(ping.at.format(DATETIME_FORMAT).to_string()),
            ]
        })
        .collect::<Vec<Vec<Datum>>>();
    html! {
        <TitledPanel title="Pings">
            <div class="centered_col_container">
                <HorizontalHeaderTable headers={headers} rows={rows}>
                </HorizontalHeaderTable>
            </div>
        </TitledPanel>
    }
}

#[derive(Properties, PartialEq, Clone)]
struct PermissionsPanelProps {
    pub permissions: Vec<SystemPermission>,
}

#[function_component(PermissionsPanel)]
fn permissions_panel(PermissionsPanelProps { permissions }: &PermissionsPanelProps) -> Html {
    let headers = vec!["Host".to_string(), "Role".to_string()];
    let rows = permissions
        .iter()
        .map(|permission| {
            vec![
                Datum::new(permission.host_name.to_owned()),
                Datum::new(permission.role.to_string()),
            ]
        })
        .collect::<Vec<Vec<Datum>>>();
    html! {
        <TitledPanel title="Permissions">
            <div class="centered_col_container">
                <HorizontalHeaderTable headers={headers} rows={rows}/>
            </div>
        </TitledPanel>
    }
}

#[derive(Properties, PartialEq, Clone)]
struct ActorLinkPanelProps {
    pub host_name: Rc<String>,
    pub webuser: WebUser,
}

#[function_component(ActorLinkPanel)]
fn actor_link_panel(ActorLinkPanelProps { host_name, webuser }: &ActorLinkPanelProps) -> Html {
    let headers = vec![
        String::from("Type"),
        String::from("Count"),
        String::from(""),
        String::from(""),
    ];
    let mut rows: Vec<Vec<Datum>> = Vec::new();
    if webuser.is_authz(host_name, &SystemRoleType::PersonOwner) {
        rows.push(actor_link_row(
            ActorType::Person,
            webuser.actor_counts.person,
            webuser.user_authz.acct.to_string(),
        ))
    }
    if webuser.is_authz(host_name, &SystemRoleType::OrganizationOwner) {
        rows.push(actor_link_row(
            ActorType::Organization,
            webuser.actor_counts.organization,
            webuser.user_authz.acct.to_string(),
        ))
    }
    if webuser.is_authz(host_name, &SystemRoleType::GroupOwner) {
        rows.push(actor_link_row(
            ActorType::Group,
            webuser.actor_counts.group,
            webuser.user_authz.acct.to_string(),
        ))
    }
    if webuser.is_authz(host_name, &SystemRoleType::ApplicationOwner) {
        rows.push(actor_link_row(
            ActorType::Application,
            webuser.actor_counts.application,
            webuser.user_authz.acct.to_string(),
        ))
    }
    if webuser.is_authz(host_name, &SystemRoleType::ServiceOwner) {
        rows.push(actor_link_row(
            ActorType::Service,
            webuser.actor_counts.service,
            webuser.user_authz.acct.to_string(),
        ))
    }
    html! {
        <TitledPanel title="Actors">
            <div class="centered_col_container">
                <HorizontalHeaderTable headers={headers} rows={rows}/>
            </div>
        </TitledPanel>
    }
}

fn actor_link_row(actor_type: ActorType, count: u16, acct: String) -> Vec<Datum> {
    vec![
        Datum::new_with_classes(
            actor_type.to_string(),
            vec![String::from("horiz_table_align_center_text")],
        ),
        Datum::new_with_classes(
            count.to_string(),
            vec![
                String::from("mono_text"),
                String::from("horiz_table_align_center_text"),
            ],
        ),
        Datum::new_with_children(
            String::from("Create"),
            html! {
                <Link<MainRoute>
                    classes={classes!("in_page_action")}
                    to={MainRoute::NotYetImplemented}
                        >{"Create"}
                </Link<MainRoute>>

            },
        ),
        Datum::new_with_children(
            String::from("Manage"),
            html! {
                <Link<AccountRoute, AccountActorsQueryParams>
                    classes={classes!("in_page_action")}
                    query={AccountActorsQueryParams{actor_type}}
                    to={AccountRoute::Actors{acct}}
                        >{"Manage"}
                </Link<AccountRoute, AccountActorsQueryParams>>
            },
        ),
    ]
}
