use crate::components::common::actor_list_viewer::OwnedActorItemListViewer;
use crate::components::common::main_layout::*;
use crate::components::common::more_pager::*;
use crate::components::common::owned_actor::*;
use crate::components::common::text_or_children::*;
use crate::components::common::titled_panel::*;
use crate::utils::element::get_id_or_parent_id;
use crate::utils::media::is_small_screen;
use crate::utils::scroll::scroll_into_view;
use dialtone_common::ap::actor::ActorType;
use dialtone_common::rest::actors::actor_model::OwnedActor;
use dialtone_common::rest::paging::owned_actor::OwnedActorsByOwner;
use serde::Deserialize;
use serde::Serialize;
use std::marker::PhantomData;
use std::rc::Rc;
use wasm_bindgen::JsCast;
use wasm_bindgen::UnwrapThrowExt;
use web_sys::Element;
use yew::prelude::*;
use yew_router::hooks::use_location;
use yew_router::prelude::*;
use yewdux::prelude::*;

use super::actor::AccountActorQueryParams;
use super::index::AccountProps;
use super::AccountRoute;

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone, Debug)]
pub struct AccountActorsQueryParams {
    pub actor_type: ActorType,
}

struct AcountActorsPageState {
    pub actors: (Rc<ItemList<OwnedActor>>, Dispatch<ItemList<OwnedActor>>),
    pub navigator: Navigator,
    pub right_col_text: UseStateHandle<Option<String>>,
    pub right_col_children: UseStateHandle<Html>,
}

#[function_component(AccountActors)]
pub fn account_actors(AccountProps { acct }: &AccountProps) -> Html {
    let location = use_location().unwrap();
    let actor_type = location
        .query::<AccountActorsQueryParams>()
        .map(|q| Some(q.actor_type))
        .unwrap_or(None);
    let panel_title = match actor_type {
        Some(actor_type) => actor_type.to_string(),
        None => "Actors".to_string(),
    };

    let small_screen = is_small_screen();

    let page_state = Rc::new(AcountActorsPageState {
        actors: use_store::<ItemList<OwnedActor>>(),
        navigator: use_navigator().unwrap(),
        right_col_text: use_state(|| Some("Select an actor.".to_string())),
        right_col_children: use_state(|| html! {<></>}),
    });

    let on_click = if small_screen {
        let page_state = Rc::clone(&page_state);
        let acct = acct.clone();
        Callback::from(move |e: MouseEvent| {
            let event: Event = e.dyn_into().unwrap_throw();
            let event_target = event.target().unwrap_throw();
            let target: Element = event_target.dyn_into().unwrap_throw();
            let actor_id = get_id_or_parent_id(target, "clickable_div").unwrap();
            page_state
                .navigator
                .push_with_query(
                    &AccountRoute::Actor {
                        acct: acct.to_owned(),
                    },
                    &AccountActorQueryParams { actor_id },
                )
                .unwrap();
        })
    } else {
        let page_state = Rc::clone(&page_state);
        Callback::from(move |e: MouseEvent| {
            let event: Event = e.dyn_into().unwrap_throw();
            let event_target = event.target().unwrap_throw();
            let target: Element = event_target.dyn_into().unwrap_throw();
            let actor_id = get_id_or_parent_id(target, "clickable_div").unwrap();

            let owned_actor = page_state
                .actors
                .0
                .clone()
                .items
                .iter()
                .find(|oa| oa.ap.id == actor_id)
                .unwrap()
                .to_owned();

            page_state.right_col_children.set(html! {
                <>
                    <OwnedActorComponent owned_actor={owned_actor}></OwnedActorComponent>
                </>
            });
            page_state.right_col_text.set(None);
            scroll_into_view("actor_top");
        })
    };

    let initial_page_request = OwnedActorsByOwner::new(acct.to_owned());
    let page_size = initial_page_request.limit;
    let item_list_viewer = OwnedActorItemListViewer { on_click };

    html! {
        <MainLayout>
            if small_screen {
                <TitledPanel title={panel_title}>
                    <MorePager<OwnedActorsByOwner, OwnedActor, OwnedActorItemListViewer>
                        initial_page_request={initial_page_request}
                        page_size={page_size}
                        item_list_viewer={item_list_viewer}
                        classes=""
                        phantom={PhantomData}
                    />
                </TitledPanel>
            } else {
                <div class="asymmetrical_split_page">
                    <div class="asymmetrical_col_left">
                        <div id="page_top"></div>
                        <TitledPanel title={panel_title}>
                            <MorePager<OwnedActorsByOwner, OwnedActor, OwnedActorItemListViewer>
                                initial_page_request={initial_page_request}
                                page_size={page_size}
                                item_list_viewer={item_list_viewer}
                                classes="vertical_scroll_container"
                                phantom={PhantomData}
                            />
                        </TitledPanel>
                    </div>
                    <div class="asymmetrical_col_right">
                        <div id="actor_top"></div>
                        <TextOrChildren text={(*page_state.right_col_text).clone()}>
                            {(*page_state.right_col_children).clone()}
                        </TextOrChildren>
                    </div>
                </div>
            }
        </MainLayout>
    }
}
