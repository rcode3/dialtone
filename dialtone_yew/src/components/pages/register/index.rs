use crate::app_types::AppPublicSiteInfo;
use crate::components::common::main_layout::*;
use crate::components::common::titled_panel::*;
use crate::MainRoute;
use dialtone_common::rest::sites::site_data::RegistrationMethod;
use yew::prelude::*;
use yew_router::prelude::*;
use yewdux::prelude::*;

use super::RegisterRoute;

#[function_component(Register)]
pub fn register() -> Html {
    let (public_site_info, _) = use_store::<AppPublicSiteInfo>();

    if let Some(ref site_info) = public_site_info.0 {
        if site_info.registration_methods.is_empty() {
            html! {
                <MainLayout>
                    <div class="centered_form_page">
                        <TitledPanel title="Registration Closed">
                            <form class="one_column_form">
                                <span>{"Registration of new accounts is closed."}</span>
                                <span><Link<MainRoute> classes={classes!("in_page_action")} to={MainRoute::Login}>{"Login to Existing Account"}</Link<MainRoute>></span>
                            </form>
                        </TitledPanel>
                    </div>
                </MainLayout>
            }
        } else if site_info
            .registration_methods
            .contains(&RegistrationMethod::SimpleCode)
        {
            html! {<Redirect<RegisterRoute> to={RegisterRoute::SimpleCode}/>}
        } else {
            html! {<Redirect<RegisterRoute> to={RegisterRoute::Open}/>}
        }
    } else {
        html! {
            <>
                <div class="centered_form_page">
                    <TitledPanel title="Register Account">
                        <span>{"System Is Broken!"}</span>
                    </TitledPanel>
                </div>
            </>
        }
    }
}

#[function_component(RegisterSuccess)]
pub fn register_success() -> Html {
    html! {
        <MainLayout>
            <div class="centered_form_page">
                <TitledPanel title="Registration Success">
                    <div class="one_column_form">
                        <span>{"Account registration was successful."}</span>
                        <span><Link<MainRoute> classes={classes!("in_page_action")} to={MainRoute::Login}>{"Login to Account"}</Link<MainRoute>></span>
                    </div>
                </TitledPanel>
            </div>
        </MainLayout>
    }
}

#[function_component(RegisterApprovalNeeded)]
pub fn register_approval_needed() -> Html {
    html! {
        <MainLayout>
            <div class="centered_form_page">
                <TitledPanel title="Registration Success">
                    <div class="one_column_form">
                        <span>{"Account registration was successful but requires approval."}</span>
                        <span>{"Check back later."}</span>
                    </div>
                </TitledPanel>
            </div>
        </MainLayout>
    }
}
