use yew::prelude::*;
use yew_router::Routable;

pub mod index;
pub mod open;
pub mod simple_code;

use index::*;
use open::*;
use simple_code::*;

#[derive(Clone, Routable, PartialEq)]
pub enum RegisterRoute {
    #[at("/register/index")]
    Index,

    #[at("/register/open")]
    Open,

    #[at("/register/simple-code")]
    SimpleCode,

    #[at("/register/success")]
    Success,

    #[at("/register/approval_needed")]
    ApprovalNeeded,
}

pub fn register_switch(routes: RegisterRoute) -> Html {
    match routes {
        RegisterRoute::Index => html! { <Register/> },
        RegisterRoute::Open => html! { <RegisterOpen/> },
        RegisterRoute::SimpleCode => html! { <RegisterSimpleCode/> },
        RegisterRoute::Success => html! { <RegisterSuccess/> },
        RegisterRoute::ApprovalNeeded => html! { <RegisterApprovalNeeded/> },
    }
}
