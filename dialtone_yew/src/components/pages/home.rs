use crate::components::common::dt_button::*;
use crate::components::common::main_layout::*;
use crate::components::common::site_name::*;
use crate::MainRoute;
use gloo::console::log;
use yew::prelude::*;
use yew_router::prelude::*;

#[function_component(Home)]
pub fn home() -> Html {
    let on_button_click = { Callback::from(move |_| log!("button clicked")) };

    let h = html! {
        <MainLayout>
            <h1>{ "Hello World" }</h1>
            <SiteName short={false}/>
            <Link<MainRoute> classes={classes!("in_page_action")} to={MainRoute::Login}>{"Login"}</Link<MainRoute>>
            <div>
                <DtButton text="foo" disabled=false on_click={on_button_click.clone()} id="button_1" in_progress=false/>
                <DtButton text="foo" disabled=true on_click={on_button_click} id="button_2" in_progress=false/>
            </div>
        </MainLayout>
    };
    h
}
