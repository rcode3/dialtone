use yew::prelude::*;

#[derive(Properties, Debug, PartialEq)]
pub struct ClickableDivProps {
    pub children: Children,
    pub id: String,
    pub on_click: Callback<MouseEvent>,
    pub click_text: Option<String>,
}

#[function_component(ClickableDiv)]
pub fn clickable_div(
    ClickableDivProps {
        children,
        id,
        on_click,
        click_text,
    }: &ClickableDivProps,
) -> Html {
    if let Some(click_text) = click_text {
        html! {
            <div id={id.clone()} class="clickable_div clickable_div_with_click_text" onclick={on_click}>
                <div class="clickable_div_inner_content">
                    {children.clone()}
                </div>
                <div class="clickable_div_button_content">
                    <span>{click_text}</span>
                </div>
            </div>
        }
    } else {
        html! {
            <div id={id.clone()} class="clickable_div clickable_div_no_click_text" onclick={on_click}>
                <div class="clickable_div_button_content">
                    <span>{"→"}</span>
                </div>
                <div class="clickable_div_inner_content">
                    {children.clone()}
                </div>
            </div>
        }
    }
}
