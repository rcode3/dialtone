use yew::prelude::*;

#[derive(Properties, Debug, PartialEq)]
pub struct BorderedPanelProps {
    pub children: Children,
}

#[function_component(BorderedPanel)]
pub fn bordered_panel(props: &BorderedPanelProps) -> Html {
    let html = html! {
        <div class="bordered_panel">
            <div class="bordered_panel_content">
                {props.children.clone()}
            </div>
        </div>
    };
    html
}
