use crate::components::common::actor_placard::*;
use dialtone_common::{
    ap::actor::Actor,
    rest::actors::actor_model::{OwnedActor, PublicActor},
};
use yew::prelude::*;

use super::actor_placard::ActorPlacardData;

#[derive(Clone, PartialEq, Debug)]
pub struct ActorImagePlacardData {
    pub actor_placard_data: ActorPlacardData,
    pub image_url: Option<String>,
}

impl From<&OwnedActor> for ActorImagePlacardData {
    fn from(owned_actor: &OwnedActor) -> Self {
        ActorImagePlacardData {
            actor_placard_data: ActorPlacardData::from(owned_actor),
            image_url: owned_actor
                .ap
                .image
                .as_ref()
                .map(|image| image.first_url())
                .unwrap_or(None),
        }
    }
}

impl From<&PublicActor> for ActorImagePlacardData {
    fn from(public_actor: &PublicActor) -> Self {
        ActorImagePlacardData {
            actor_placard_data: ActorPlacardData::from(public_actor),
            image_url: public_actor
                .ap
                .image
                .as_ref()
                .map(|image| image.first_url())
                .unwrap_or(None),
        }
    }
}

impl From<&Actor> for ActorImagePlacardData {
    fn from(actor: &Actor) -> Self {
        ActorImagePlacardData {
            actor_placard_data: ActorPlacardData::from(actor),
            image_url: actor
                .image
                .as_ref()
                .map(|image| image.first_url())
                .unwrap_or(None),
        }
    }
}

#[derive(Properties, PartialEq, Debug)]
pub struct ActorImagePlacardProps {
    pub actor_data: ActorImagePlacardData,
}

#[function_component(ActorImagePlacard)]
pub fn actor_image_placard(ActorImagePlacardProps { actor_data }: &ActorImagePlacardProps) -> Html {
    html! {
        <div class="actor_image_placard">
            <div class="actor_image">
                if let Some(image) = &actor_data.image_url {
                    <img src={image.clone()}/>
                }
            </div>
            <div class="actor_image_placard_placard">
                <div>
                    <ActorPlacard actor_data={actor_data.actor_placard_data.clone()}></ActorPlacard>
                </div>
            </div>
        </div>
    }
}
