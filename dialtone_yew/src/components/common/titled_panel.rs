use yew::prelude::*;

#[derive(Properties, Debug, PartialEq)]
pub struct TitledPanelProps {
    pub title: String,
    pub children: Children,
}

#[function_component(TitledPanel)]
pub fn titled_panel(props: &TitledPanelProps) -> Html {
    let html = html! {
        <div class="titled_panel">
            <div class="titled_panel_header">
                <span class="titled_panel_header_span">{props.title.clone()}</span>
            </div>
            <div class="titled_panel_content">
                {props.children.clone()}
            </div>
        </div>
    };
    html
}
