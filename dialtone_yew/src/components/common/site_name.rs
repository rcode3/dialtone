use crate::AppPublicSiteInfo;
use dialtone_common::rest::sites::site_data::SiteNames;
use yew::prelude::*;
use yewdux::prelude::*;

#[derive(Properties, PartialEq)]
pub struct SiteNameProps {
    pub short: bool,
}

#[function_component(SiteName)]
pub fn site_name(SiteNameProps { short }: &SiteNameProps) -> Html {
    let (public_site_info, _) = use_store::<AppPublicSiteInfo>();
    let names = match &public_site_info.0 {
        None => SiteNames {
            short_name: "Err!".to_string(),
            long_name: "!! Site Unavailable !!".to_string(),
        },
        Some(n) => n.names.clone(),
    };
    let html = if *short {
        html! {
            <><span>{names.short_name}</span></>
        }
    } else {
        html! {
            <><span class={"dt_long_site_name"}>{names.long_name}</span>
            <span class={"dt_short_site_name"}>{names.short_name}</span></>
        }
    };
    html
}
