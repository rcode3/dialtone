use crate::app_types::AppSiteConnection;
use crate::components::common::site_name::*;
use crate::components::pages::account::AccountRoute;
use crate::components::pages::register::RegisterRoute;
use crate::utils::dialtone_svg_logo;
use crate::MainRoute;
use dialtone_common::utils::make_acct::make_acct;
use yew::prelude::*;
use yew_router::prelude::*;
use yewdux::prelude::*;

#[derive(Properties, Debug, PartialEq)]
pub struct MainLayoutProps {
    #[prop_or_default]
    pub children: Children,
}

#[function_component(MainLayout)]
pub fn main_layout(props: &MainLayoutProps) -> Html {
    let logo_svg = dialtone_svg_logo();
    let h = html! {
        <dt-main-layout>
            <div class="site_header_bar">
                <dt-site-name><SiteName short={false}/></dt-site-name>
                <dt-site-logo>{logo_svg}</dt-site-logo>
            </div>
            <div class="site_nav_bar">
                <div><Link<MainRoute> classes={classes!("in_page_action")} to={MainRoute::Home}>{"Home"}</Link<MainRoute>></div>
                <div><Link<MainRoute> classes={classes!("in_page_action")} to={MainRoute::About}>{"About"}</Link<MainRoute>></div>
                <div><Link<MainRoute> classes={classes!("in_page_action")} to={MainRoute::Themes}>{"Themes"}</Link<MainRoute>></div>
                <DtNavLogin/>
            </div>
            <dt-main-content>
                {props.children.clone()}
            </dt-main-content>
        </dt-main-layout>
    };
    h
}

#[function_component(DtNavLogin)]
fn site_login() -> Html {
    let user_name: UseStateHandle<Option<String>> = use_state_eq(|| None);
    let (sc, _sc_dispatch) = use_store::<AppSiteConnection>();
    if let Some(n) = &sc.0.user_name {
        user_name.set(Some(n.to_string()));
    } else {
        user_name.set(None);
    }
    match *user_name {
        Some(ref user_name) => {
            let acct = make_acct(user_name, sc.0.host_name());
            html! {
                <>
                <div>{"("}<Link<AccountRoute> classes={classes!("in_page_action")} to={AccountRoute::Index{acct}}>{user_name}</Link<AccountRoute>>{")"}</div>
                <div><Link<MainRoute> classes={classes!("in_page_action")} to={MainRoute::Logout}>{"Logout"}</Link<MainRoute>></div>
                </>
            }
        }
        None => {
            html! {
                <>
                <div><Link<MainRoute> classes={classes!("in_page_action")} to={MainRoute::Login}>{"Login"}</Link<MainRoute>></div>
                <div><Link<RegisterRoute> classes={classes!("in_page_action")} to={RegisterRoute::Index}>{"Register"}</Link<RegisterRoute>></div>
                </>
            }
        }
    }
}
