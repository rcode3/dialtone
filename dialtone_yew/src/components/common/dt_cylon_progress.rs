use yew::prelude::*;

#[derive(Properties, PartialEq, Clone)]
pub struct DtCylonProgressProps {
    pub in_progress: bool,
}

#[function_component(DtCylongProgress)]
pub fn dt_cylon_progress(props: &DtCylonProgressProps) -> Html {
    let mut cylon_classes = vec!["dt_progress_bar".to_string()];
    if !props.in_progress {
        cylon_classes.push("hidden".to_string());
    }
    let html = html! {
        <dt_progress>
            <div class={classes!(cylon_classes)}></div>
        </dt_progress>
    };
    html
}
