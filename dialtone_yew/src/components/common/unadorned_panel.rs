use yew::prelude::*;

#[derive(Properties, Debug, PartialEq)]
pub struct UnadornedPanelProps {
    pub children: Children,
}

#[function_component(UnadornedPanel)]
pub fn unadorned_panel(props: &UnadornedPanelProps) -> Html {
    let html = html! {
        <div class="unadorned_panel">
            <div class="unadorned_panel_content">
                {props.children.clone()}
            </div>
        </div>
    };
    html
}
