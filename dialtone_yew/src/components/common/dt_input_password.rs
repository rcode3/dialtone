use wasm_bindgen::JsCast;
use wasm_bindgen::UnwrapThrowExt;
use web_sys::Event;
use web_sys::HtmlInputElement;
use web_sys::InputEvent;
use yew::prelude::*;

#[derive(Properties, PartialEq, Clone)]
pub struct DtInputPasswordProps {
    pub name: String,
    pub disabled: bool,
    pub on_update_value: Callback<String>,
    pub id: String,
}

#[function_component(DtInputPassword)]
pub fn dt_input_password(props: &DtInputPasswordProps) -> Html {
    let DtInputPasswordProps {
        name,
        disabled,
        on_update_value,
        id,
    } = props.clone();
    let oninput = {
        Callback::from(move |e: InputEvent| {
            let event: Event = e.dyn_into().unwrap_throw();
            let event_target = event.target().unwrap_throw();
            let target: HtmlInputElement = event_target.dyn_into().unwrap_throw();
            on_update_value.emit(target.value());
        })
    };
    let html = html! {
        <input type="password" {id} {name} class="dt_input_text" {oninput} {disabled}/>
    };
    html
}
