use crate::components::common::in_page_action::*;
use yew::prelude::*;

#[derive(Properties, PartialEq, Debug)]
pub struct PrevNextProps {
    pub show: bool,
    pub prev_id: String,
    pub next_id: String,
    pub prev_disabled: bool,
    pub next_disabled: bool,
    pub prev_on_click: Callback<MouseEvent>,
    pub next_on_click: Callback<MouseEvent>,
}

#[function_component(PrevNext)]
pub fn prev_next(
    PrevNextProps {
        show,
        prev_on_click,
        next_on_click,
        prev_id,
        next_id,
        prev_disabled,
        next_disabled,
    }: &PrevNextProps,
) -> Html {
    html! {
        <>
            if *show {
                <div class="link_nav">
                    <div>
                        <InPageAction
                            text="< PREV"
                            id={prev_id.clone()}
                            on_click={prev_on_click}
                            disabled={*prev_disabled}/>
                    </div>
                    <div>
                        <InPageAction
                            text="NEXT >"
                            id={next_id.clone()}
                            on_click={next_on_click}
                            disabled={*next_disabled}/>
                    </div>
                </div>
            }
        </>
    }
}
