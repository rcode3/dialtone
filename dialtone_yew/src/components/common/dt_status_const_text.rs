use yew::prelude::*;

#[derive(Clone, PartialEq)]
pub enum ConstStatusType {
    INFO,
    WARNING,
    ERROR,
}

#[derive(Clone, PartialEq)]
pub struct ConstText {
    pub hidden: bool,
    pub status_type: ConstStatusType,
    pub message: &'static str,
}

impl ConstText {
    pub const fn new(status_type: ConstStatusType, message: &'static str) -> Self {
        ConstText {
            hidden: false,
            status_type,
            message,
        }
    }

    pub const fn blank() -> Self {
        ConstText {
            hidden: true,
            status_type: ConstStatusType::INFO,
            message: "THIS SHOULD NOT BE SEEN",
        }
    }
}

#[derive(Properties, PartialEq, Clone)]
pub struct DtStatusDynamicTextProps {
    pub status: ConstText,
}

#[function_component(DtStatusConstText)]
pub fn dt_status_const_text(props: &DtStatusDynamicTextProps) -> Html {
    let mut status_classes: Vec<String> = Vec::new();
    if props.status.hidden {
        status_classes.push("hidden".to_string())
    }
    match props.status.status_type {
        ConstStatusType::INFO => status_classes.push("info_text".to_string()),
        ConstStatusType::WARNING => status_classes.push("warning_text".to_string()),
        ConstStatusType::ERROR => status_classes.push("error_text".to_string()),
    }
    let h = html! {
        <dt_status_text class={classes!(status_classes)}>{props.status.message.clone()}</dt_status_text>
    };
    h
}
