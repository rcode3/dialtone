use yew::prelude::*;

#[derive(Properties, PartialEq, Clone)]
pub struct DtButtonProps {
    pub text: String,
    pub disabled: bool,
    pub on_click: Callback<()>,
    pub id: String,
    pub in_progress: bool,
}

#[function_component(DtButton)]
pub fn dt_button(
    DtButtonProps {
        text,
        disabled,
        on_click,
        id,
        in_progress,
    }: &DtButtonProps,
) -> Html {
    let mut button_classes = vec!["dt_button".to_string()];
    if *in_progress {
        button_classes.push("dt_button_in_progress".to_string());
    }
    let click_handler = {
        let on_click = on_click.clone();
        Callback::from(move |_| on_click.emit(()))
    };
    let html = html! {
        <button id={id.clone()} class={classes!(button_classes)} onclick={click_handler} disabled={*disabled}>{text}</button>
    };
    html
}
