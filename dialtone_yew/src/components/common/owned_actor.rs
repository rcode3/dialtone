use crate::components::common::event_log::*;
use crate::components::common::unadorned_panel::*;
use crate::components::common::vertical_header_table::*;
use crate::{components::common::actor_image_placard::*, utils::DATETIME_FORMAT};
use chrono::{DateTime, Utc};
use dialtone_common::ap::actor::ActorType;
use dialtone_common::rest::actors::actor_model::OwnedActor;
use yew::prelude::*;

use super::{vertical_header_table::VerticalHeaderTableRow, Datum};

#[derive(Properties, PartialEq, Debug)]
pub struct OwnedActorProps {
    pub owned_actor: OwnedActor,
}

#[function_component(OwnedActorComponent)]
pub fn owned_actor(OwnedActorProps { owned_actor }: &OwnedActorProps) -> Html {
    html! {
        <div>
            <ActorImagePlacard
                actor_data={ActorImagePlacardData::from(owned_actor)}>
            </ActorImagePlacard>
            <ActorStatusPanel
                wf_acct={owned_actor.jrd.as_ref().unwrap().subject.clone()}
                pun={owned_actor.ap.preferred_user_name.clone()}
                actor_id={owned_actor.ap.id.clone()}
                actor_type={owned_actor.ap.ap_type}
                owner_count={owned_actor.owner_count}
                collection_count={owned_actor.collection_count}
                modified_at={owned_actor.modified_at}
                created_at={owned_actor.created_at}>
            </ActorStatusPanel>
            <EventLog event_log={owned_actor.event_log.clone()}>
            </EventLog>
        </div>
    }
}

#[derive(Properties, Debug, PartialEq)]
struct StatusPanelProps {
    pub created_at: DateTime<Utc>,
    pub modified_at: DateTime<Utc>,
    pub actor_type: ActorType,
    pub pun: String,
    pub actor_id: String,
    pub wf_acct: String,
    pub owner_count: u16,
    pub collection_count: u16,
}

#[function_component(ActorStatusPanel)]
fn status_panel(
    StatusPanelProps {
        created_at,
        modified_at,
        actor_type,
        pun,
        actor_id,
        wf_acct,
        owner_count,
        collection_count,
    }: &StatusPanelProps,
) -> Html {
    let rows = vec![
        VerticalHeaderTableRow {
            header: "WebFinger".to_string(),
            data: vec![Datum::new(wf_acct.to_owned())],
        },
        VerticalHeaderTableRow {
            header: "Actor ID".to_string(),
            data: vec![Datum::new(actor_id.to_owned())],
        },
        VerticalHeaderTableRow {
            header: "PUN".to_string(),
            data: vec![Datum::new(pun.to_owned())],
        },
        VerticalHeaderTableRow {
            header: "Type".to_string(),
            data: vec![Datum::new(actor_type.to_string())],
        },
        VerticalHeaderTableRow {
            header: "Created".to_string(),
            data: vec![Datum::new_mono(
                created_at.format(DATETIME_FORMAT).to_string(),
            )],
        },
        VerticalHeaderTableRow {
            header: "Modified".to_string(),
            data: vec![Datum::new_mono(
                modified_at.format(DATETIME_FORMAT).to_string(),
            )],
        },
        VerticalHeaderTableRow {
            header: "Owners".to_string(),
            data: vec![Datum::new_mono(owner_count.to_string())],
        },
        VerticalHeaderTableRow {
            header: "Collections".to_string(),
            data: vec![Datum::new_mono(collection_count.to_string())],
        },
    ];
    html! {
        <UnadornedPanel>
            <div class="centered_col_container">
                <VerticalHeaderTable rows={rows}>
                </VerticalHeaderTable>
            </div>
        </UnadornedPanel>
    }
}
