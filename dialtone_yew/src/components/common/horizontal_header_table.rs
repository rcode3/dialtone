use yew::prelude::*;

use super::Datum;

#[derive(Properties, PartialEq, Debug)]
pub struct HorizontalHeaderTableProps {
    pub headers: Vec<String>,
    pub rows: Vec<Vec<Datum>>,
}

#[function_component(HorizontalHeaderTable)]
pub fn horizontal_header_table(props: &HorizontalHeaderTableProps) -> Html {
    let collapsed_hader = props
        .headers
        .iter()
        .filter(|s| !s.is_empty())
        .map(|s| s.to_string())
        .collect::<Vec<String>>()
        .join(" / ");
    html! {
        <table class="horizontal_header_table">
            <thead>
                {
                    props.headers.iter().map(|header| {
                        html! {
                            <th key={header.to_string()}>{header}</th>
                        }
                    }).collect::<Html>()
                }
            </thead>
            <tbody>
                {
                    props.rows.iter().map(|row| {
                        html! {
                            <tr>
                                <th>{&collapsed_hader}</th>
                                {
                                    row.iter().map(|datum| {
                                        html! {
                                            <td
                                                key={datum.to_string()}
                                                class={classes!(datum.classes())}
                                                >{datum.to_html()}</td>
                                        }
                                    }).collect::<Html>()
                                }
                            </tr>
                        }
                    }).collect::<Html>()
                }
            </tbody>
        </table>
    }
}
