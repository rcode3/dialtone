use crate::components::common::in_page_action::*;
use crate::utils::set_theme::set_theme;
use dialtone_common::rest::sites::theme::Theme;
use dialtone_common::utils::capitalize::Capitalize;
use dialtone_common::utils::make_id::MakeId;
use yew::prelude::*;

#[derive(Properties, PartialEq)]
pub struct ThemeSetterProps {
    pub theme: Theme,
}

#[function_component(ThemeSetter)]
pub fn theme_setter(ThemeSetterProps { theme }: &ThemeSetterProps) -> Html {
    let on_page_action = {
        let theme = theme.clone();
        Callback::from(move |_| {
            set_theme(&theme);
        })
    };

    let theme_id = theme.to_string().make_id();
    let theme_text = theme.to_string().capitalize_all();
    let h = html! {
            <InPageAction id={theme_id} text={theme_text} on_click={on_page_action} disabled={false}/>
    };
    h
}
