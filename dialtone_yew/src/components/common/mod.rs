use std::fmt::Display;

use lazy_static::lazy_static;
use yew::{html, Html};

pub mod actor_image_placard;
pub mod actor_list_viewer;
pub mod actor_placard;
pub mod bordered_panel;
pub mod clickable_div;
pub mod dt_button;
pub mod dt_cylon_progress;
pub mod dt_input_password;
pub mod dt_input_text;
pub mod dt_status_const_text;
pub mod dt_status_text;
pub mod event_log;
pub mod horizontal_header_table;
pub mod in_page_action;
pub mod loading;
pub mod main_layout;
pub mod more_pager;
pub mod owned_actor;
pub mod prev_next;
pub mod site_name;
pub mod text_or_children;
pub mod theme_setter;
pub mod titled_panel;
pub mod unadorned_panel;
pub mod vertical_header_table;

lazy_static! {
    static ref MONO_TEXT_CLASSES: Vec<String> = vec![String::from("mono_text")];
}

#[derive(Debug, PartialEq, Clone)]
pub struct Datum {
    pub value: String,
    pub monospaced: bool,
    pub children: Option<Html>,
    pub classes: Vec<String>,
}

impl Datum {
    pub fn new(value: String) -> Self {
        Self {
            value,
            monospaced: false,
            children: None,
            classes: vec![],
        }
    }

    pub fn new_mono(value: String) -> Self {
        Self {
            value,
            monospaced: true,
            children: None,
            classes: vec![],
        }
    }

    pub fn new_with_children(value: String, html: Html) -> Self {
        Self {
            value,
            monospaced: false,
            children: Some(html),
            classes: vec![],
        }
    }

    pub fn new_with_classes(value: String, classes: Vec<String>) -> Self {
        Self {
            value,
            monospaced: false,
            children: None,
            classes,
        }
    }
}

impl Display for Datum {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.value.as_ref())
    }
}

impl Datum {
    pub fn to_html(&self) -> Html {
        if let Some(children) = &self.children {
            children.clone()
        } else {
            html! {self}
        }
    }

    pub fn classes(&self) -> &[String] {
        if self.classes.is_empty() {
            if self.monospaced {
                &MONO_TEXT_CLASSES
            } else {
                &[]
            }
        } else {
            &self.classes
        }
    }
}
