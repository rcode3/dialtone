use crate::utils::dialtone_svg_logo;
use dialtone_common::{
    ap::actor::Actor,
    rest::actors::actor_model::{OwnedActor, PublicActor},
    webfinger::display_acct,
};
use yew::prelude::*;

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct ActorPlacardData {
    pub actor_id: String,
    pub pun: String,
    pub name: Option<String>,
    pub wf_acct: Option<String>,
    pub icon_url: Option<String>,
}

impl From<&OwnedActor> for ActorPlacardData {
    fn from(owned_actor: &OwnedActor) -> Self {
        ActorPlacardData {
            actor_id: owned_actor.ap.id.clone(),
            pun: owned_actor.ap.preferred_user_name.clone(),
            name: owned_actor.ap.name.clone(),
            wf_acct: owned_actor
                .jrd
                .as_ref()
                .map(|jrd| Some(jrd.subject.clone()))
                .unwrap_or(None),
            icon_url: owned_actor
                .ap
                .icon
                .as_ref()
                .map(|icon| icon.first_url())
                .unwrap_or(None),
        }
    }
}

impl From<&PublicActor> for ActorPlacardData {
    fn from(public_actor: &PublicActor) -> Self {
        ActorPlacardData {
            actor_id: public_actor.ap.id.clone(),
            pun: public_actor.ap.preferred_user_name.clone(),
            name: public_actor.ap.name.clone(),
            wf_acct: public_actor
                .jrd
                .as_ref()
                .map(|jrd| Some(jrd.subject.clone()))
                .unwrap_or(None),
            icon_url: public_actor
                .ap
                .icon
                .as_ref()
                .map(|icon| icon.first_url())
                .unwrap_or(None),
        }
    }
}

impl From<&Actor> for ActorPlacardData {
    fn from(actor: &Actor) -> Self {
        ActorPlacardData {
            actor_id: actor.id.clone(),
            pun: actor.preferred_user_name.clone(),
            name: actor.name.clone(),
            wf_acct: None,
            icon_url: actor
                .icon
                .as_ref()
                .map(|icon| icon.first_url())
                .unwrap_or(None),
        }
    }
}

#[derive(Properties, PartialEq, Eq, Debug)]
pub struct ActorPlacardProps {
    pub actor_data: ActorPlacardData,
}

#[function_component(ActorPlacard)]
pub fn actor_placard(props: &ActorPlacardProps) -> Html {
    let name = if let Some(name) = &props.actor_data.name {
        name.to_string()
    } else {
        (props.actor_data.pun).to_string()
    };
    let id = if let Some(wf_acct) = &props.actor_data.wf_acct {
        display_acct(wf_acct).to_string()
    } else {
        (props.actor_data.actor_id).to_string()
    };
    html! {
        <div class="actor_placard">
            <div class="actor_placard_icon">
                if let Some(icon) = &props.actor_data.icon_url {
                    <img src={icon.clone()}/>
                } else {
                    {dialtone_svg_logo()}
                }
            </div>
            <div class="actor_placard_name">
               <span>{name}</span>
            </div>
            <div class="actor_placard_acct">
                <span>{id}</span>
            </div>
        </div>
    }
}
