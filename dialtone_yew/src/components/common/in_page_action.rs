use yew::prelude::*;

#[derive(Properties, PartialEq, Clone)]
pub struct InPageActionProps {
    pub text: String,
    pub id: String,
    pub on_click: Callback<MouseEvent>,
    pub disabled: bool,
}

#[function_component(InPageAction)]
pub fn in_page_action(
    InPageActionProps {
        text,
        id,
        on_click,
        disabled,
    }: &InPageActionProps,
) -> Html {
    let on_click_callback = {
        let on_click = on_click.clone();
        Callback::from(move |e| on_click.emit(e))
    };
    let class = if *disabled {
        "in_page_action_disabled"
    } else {
        "in_page_action"
    };
    let html = html! {
        <span id={id.clone()} class={class} onclick={on_click_callback}>{text}</span>
    };
    html
}
