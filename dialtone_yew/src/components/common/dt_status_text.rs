use yew::prelude::*;

#[derive(Clone, PartialEq)]
pub enum StatusType {
    INFO,
    WARNING,
    ERROR,
}

#[derive(Properties, PartialEq, Clone)]
pub struct DtStatusTextProps {
    pub hidden: bool,
    pub status_type: StatusType,
    pub children: Children,
}

#[function_component(DtStatusText)]
pub fn dt_status_text(props: &DtStatusTextProps) -> Html {
    let mut status_classes: Vec<String> = Vec::new();
    if props.hidden {
        status_classes.push("hidden".to_string())
    }
    match props.status_type {
        StatusType::INFO => status_classes.push("info_text".to_string()),
        StatusType::WARNING => status_classes.push("warning_text".to_string()),
        StatusType::ERROR => status_classes.push("error_text".to_string()),
    }
    let h = html! {
        <dt_status_text class={classes!(status_classes)}>{props.children.clone()}</dt_status_text>
    };
    h
}
