use yew::prelude::*;

use super::Datum;

#[derive(Debug, PartialEq, Clone)]
pub struct VerticalHeaderTableRow {
    pub header: String,
    pub data: Vec<Datum>,
}

#[derive(Properties, Debug, PartialEq)]
pub struct VerticalHeaderTableProps {
    pub rows: Vec<VerticalHeaderTableRow>,
}

#[function_component(VerticalHeaderTable)]
pub fn vertical_header_table(props: &VerticalHeaderTableProps) -> Html {
    html! {
        <table class="vertical_header_table">
            {
                props.rows.clone().iter().map(|row| {
                    html! {
                        <>
                        <tr class="uncollapsed_row" key={row.header.to_string()}>
                            <th>{row.header.to_string()}</th>
                            {
                                row.data.iter().map(|datum| {
                                    html! {
                                        <td
                                            key={datum.to_string()}
                                            class={classes!(datum.classes())}
                                            >{datum}</td>
                                    }
                                }).collect::<Html>()
                            }
                        </tr>
                        <tr class="collapsed_row">
                            <th>{row.header.to_string()}</th>
                        </tr>
                        {
                            row.data.iter().map(|datum| {
                                html! {
                                    <tr
                                        class="collapsed_row">
                                        <td
                                            key={datum.to_string()}
                                            class={classes!(datum.classes())}
                                            >{datum}</td></tr>
                                }
                            }).collect::<Html>()
                        }
                        </>
                    }
                }).collect::<Html>()
            }
        </table>
    }
}
