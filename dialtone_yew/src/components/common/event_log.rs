use crate::components::common::horizontal_header_table::*;
use crate::components::common::titled_panel::*;
use crate::utils::DATETIME_FORMAT;
use dialtone_common::rest::event_log::LogEvent;
use yew::prelude::*;

use super::Datum;

#[derive(Properties, PartialEq, Debug)]
pub struct EventLogProps {
    pub event_log: Vec<LogEvent>,
}

#[function_component(EventLog)]
pub fn event_log(EventLogProps { event_log }: &EventLogProps) -> Html {
    let headers = vec!["When".to_string(), "Event".to_string()];
    let rows = event_log
        .iter()
        .map(|event| {
            vec![
                Datum::new_mono(event.when.format(DATETIME_FORMAT).to_string()),
                Datum::new(event.message.to_string()),
            ]
        })
        .collect::<Vec<Vec<Datum>>>();
    html! {
        <TitledPanel title="Events">
            <div class="centered_col_container">
                <HorizontalHeaderTable headers={headers} rows={rows}>
                </HorizontalHeaderTable>
            </div>
        </TitledPanel>
    }
}
