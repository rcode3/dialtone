use yew::prelude::*;

#[function_component(Loading)]
pub fn loading() -> Html {
    html! {
        <div class="centered_form_page">
            <span class="loading_text">{"Loading..."}</span>
        </div>
    }
}
