use dialtone_common::rest::paging::{Page, PagingProps};
use dialtone_reqwest::api_dt::paging::page;
use dialtone_reqwest::dt_reqwest_error::DtReqwestError;
use serde::de::DeserializeOwned;
use serde::Serialize;
use std::marker::PhantomData;
use std::rc::Rc;
use yew::prelude::*;
use yew::{function_component, html, Properties};
use yew_router::prelude::*;
use yewdux::prelude::*;

use crate::app_types::AppSiteConnection;
use crate::components::common::dt_button::*;
use crate::components::common::loading::*;
use crate::components::pages::error::handle_error;

pub trait ItemListViewer<I> {
    fn list_view(&self, item_list: &[I]) -> Html;
}

#[derive(Clone, Store, PartialEq, Eq)]
pub struct ItemList<I>
where
    I: PartialEq + 'static,
{
    pub items: Vec<I>,
}

impl<I: PartialEq + 'static> Default for ItemList<I> {
    fn default() -> Self {
        Self { items: vec![] }
    }
}

#[derive(Properties, PartialEq, Eq)]
pub struct MorePagerProps<T, I, V>
where
    T: PartialEq + PagingProps<Page<I>>,
    I: PartialEq,
    V: ItemListViewer<I> + PartialEq,
{
    pub initial_page_request: T,
    pub page_size: u16,
    pub item_list_viewer: V,
    pub classes: Classes,
    pub phantom: PhantomData<I>,
}

struct MorePagerState<T, I>
where
    I: PartialEq + 'static,
    T: PagingProps<Page<I>>,
{
    pub sc: Rc<AppSiteConnection>,
    pub page_request: UseStateHandle<T>,
    pub show_next: UseStateHandle<bool>,
    pub next_inprogress: UseStateHandle<bool>,
    pub navigator: Navigator,
    pub items: (Rc<ItemList<I>>, Dispatch<ItemList<I>>),
}

#[function_component(MorePager)]
pub fn more_pager<T, I, V>(props: &MorePagerProps<T, I, V>) -> Html
where
    T: Serialize + Clone + PartialEq + PagingProps<Page<I>> + 'static,
    I: Clone + DeserializeOwned + PartialEq + 'static,
    V: ItemListViewer<I> + PartialEq,
{
    let (sc, _sc_dispatch) = use_store::<AppSiteConnection>();
    let pager_state = Rc::new(MorePagerState {
        page_request: use_state(|| props.initial_page_request.clone()),
        show_next: use_state(|| false),
        next_inprogress: use_state(|| true),
        navigator: use_navigator().unwrap(),
        items: use_store::<ItemList<I>>(),
        sc,
    });

    let page_size = props.page_size;

    use_effect_with_deps(
        {
            let pager_state = Rc::clone(&pager_state);
            move |_| {
                let pager_state = Rc::clone(&pager_state);
                pager_state.items.1.reduce_mut(|items| {
                    items.items.clear();
                });
                wasm_bindgen_futures::spawn_local(async move {
                    let pager_state = Rc::clone(&pager_state);
                    let page: Result<Page<I>, DtReqwestError> =
                        page(&pager_state.sc.0, &*pager_state.page_request).await;
                    match page {
                        Ok(page) => {
                            if page_size as usize == page.items.len() {
                                pager_state.show_next.set(true);
                                pager_state.next_inprogress.set(false);
                            } else {
                                pager_state.show_next.set(false);
                            };
                            // get props for querying the next page
                            let next_page_request =
                                PagingProps::next_props((*pager_state.page_request).clone(), &page);
                            pager_state.page_request.set(next_page_request);
                            pager_state.items.1.reduce_mut(|items| {
                                let mut page_items = page.items;
                                items.items.append(&mut page_items);
                            });
                        }
                        Err(err) => handle_error(&pager_state.navigator, err),
                    }
                });
                || ()
            }
        },
        (),
    );

    let on_more_click = {
        let pager_state = Rc::clone(&pager_state);
        Callback::from(move |_| {
            let pager_state = Rc::clone(&pager_state);
            pager_state.next_inprogress.set(true);
            wasm_bindgen_futures::spawn_local(async move {
                let page: Result<Page<I>, DtReqwestError> =
                    page(&pager_state.sc.0, &*pager_state.page_request).await;
                match page {
                    Ok(page) => {
                        pager_state.next_inprogress.set(false);
                        if page_size as usize == page.items.len() {
                            pager_state.show_next.set(true);
                        } else {
                            pager_state.show_next.set(false);
                        };
                        // get props for querying the next page
                        let next_page_request =
                            PagingProps::next_props((*pager_state.page_request).clone(), &page);
                        pager_state.page_request.set(next_page_request);
                        pager_state.items.1.reduce_mut(|items| {
                            let mut page_items = page.items;
                            items.items.append(&mut page_items);
                        });
                    }
                    Err(err) => handle_error(&pager_state.navigator, err),
                }
            });
        })
    };

    html! {
        <div class={classes!(props.classes.clone())}>
            if pager_state.items.0.items.is_empty() {
                <Loading/>
            } else {
                <div class="more_pager_items">
                { props.item_list_viewer.list_view(&pager_state.items.clone().0.items) }
                </div>
                if *pager_state.show_next {
                    <div class="more_pager_more_button_div">
                        <DtButton
                            text="More"
                            disabled={false}
                            on_click={on_more_click.clone()}
                            id="more_button"
                            in_progress={*pager_state.next_inprogress} />
                    </div>
                }
            }
        </div>
    }
}
