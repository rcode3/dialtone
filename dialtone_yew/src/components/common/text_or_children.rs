use yew::prelude::*;

#[derive(Properties, Debug, PartialEq)]
pub struct TextOrChildrenProps {
    pub text: Option<String>,
    pub children: Children,
}

#[function_component(TextOrChildren)]
pub fn text_or_children(TextOrChildrenProps { text, children }: &TextOrChildrenProps) -> Html {
    html! {
        if let Some(text) = text {
            <div class="centered_form_page">
                <span>{text}</span>
            </div>
        } else {
            <div>
                {children.clone()}
            </div>
        }
    }
}
