use crate::components::common::actor_placard::*;
use crate::components::common::clickable_div::*;
use dialtone_common::rest::actors::actor_model::OwnedActor;
use yew::prelude::*;

use super::more_pager::ItemListViewer;

#[derive(PartialEq)]
pub struct OwnedActorItemListViewer {
    pub on_click: Callback<MouseEvent>,
}

impl ItemListViewer<OwnedActor> for OwnedActorItemListViewer {
    fn list_view(&self, item_list: &[OwnedActor]) -> Html {
        let actors = item_list
            .iter()
            .map(|actor| ActorItem {
                actor_placard_data: ActorPlacardData::from(actor),
                summary: actor.ap.summary.as_ref(),
            })
            .collect::<Vec<ActorItem>>();
        list_actors(actors, &self.on_click)
    }
}

struct ActorItem<'a> {
    pub actor_placard_data: ActorPlacardData,
    pub summary: Option<&'a String>,
}

fn list_actors(item_list: Vec<ActorItem>, on_click: &Callback<MouseEvent>) -> Html {
    html! {
        <div class="centered_col_container">
            <div class="actor_list">
            {
                item_list.into_iter().map(|actor| {
                    html! {
                        <div class="item">
                            <ClickableDiv
                                id={actor.actor_placard_data.actor_id.clone()}
                                on_click={on_click}
                                >
                                <ActorPlacard actor_data={actor.actor_placard_data}></ActorPlacard>
                                if let Some(summary) = actor.summary {
                                    <div class="summary">{summary}</div>
                                }
                            </ClickableDiv>
                        </div>
                    }
                }).collect::<Html>()
            }
            </div>
        </div>
    }
}
