use crate::get_host_info;
use dialtone_common::rest::sites::site_data::PublicSiteInfo;
use dialtone_reqwest::site_connection::SiteConnection;
use serde::{Deserialize, Serialize};
use yewdux::prelude::*;

#[derive(PartialEq, Serialize, Deserialize, Store)]
#[store(storage = "local")]
pub struct AppSiteConnection(pub SiteConnection);

impl Default for AppSiteConnection {
    fn default() -> Self {
        let site_connection = get_host_info();
        Self(site_connection)
    }
}

pub fn reset_app_site_connection() {
    let dispatch = Dispatch::<AppSiteConnection>::new();
    dispatch.reduce(|_| AppSiteConnection::default().into());
}

#[derive(PartialEq, Serialize, Deserialize, Store, Default)]
pub struct AppPublicSiteInfo(pub Option<PublicSiteInfo>);

pub fn reset_app_public_site_info() {
    let dispatch = Dispatch::<AppPublicSiteInfo>::new();
    dispatch.reduce(|_| AppPublicSiteInfo::default().into());
}
