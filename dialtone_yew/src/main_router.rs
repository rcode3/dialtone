use crate::components::common::main_layout::*;
use crate::components::pages::about::*;
use crate::components::pages::account::*;
use crate::components::pages::error::*;
use crate::components::pages::home::*;
use crate::components::pages::login::*;
use crate::components::pages::logout::*;
use crate::components::pages::register::*;
use crate::components::pages::themes::*;
use yew::prelude::*;
use yew_router::prelude::*;

#[derive(Clone, Routable, PartialEq, Debug)]
pub enum MainRoute {
    #[at("/")]
    Home,

    #[at("/login")]
    Login,

    #[at("/logout")]
    Logout,

    #[at("/register/:s")]
    RegisterSwitch,

    #[at("/p/:pun")]
    PersonProfile { pun: String },

    #[at("/themes")]
    Themes,

    #[at("/about")]
    About,

    #[at("/account/:s/:s")]
    AccountSwitch,

    #[not_found]
    #[at("/404")]
    NotFound,

    #[at("/bad_request")]
    BadRequest,

    #[at("/access_denied")]
    AccessDenied,

    #[at("/something_went_wrong")]
    SomethingWentWrong,

    #[at("/not_yet_implemented")]
    NotYetImplemented,
}

pub fn main_route_switch(routes: MainRoute) -> Html {
    match routes {
        MainRoute::Home => html! { <Home/> },
        MainRoute::Login => html! { <Login/> },
        MainRoute::Logout => html! { <Logout/> },
        MainRoute::RegisterSwitch => {
            html! { <Switch<RegisterRoute> render={register_switch} />}
        }
        MainRoute::Themes => html! { <Themes/> },
        MainRoute::About => html! { <About/> },
        MainRoute::NotFound => html! { <NotFoundPage/> },
        MainRoute::BadRequest => html! { <BadRequestPage/> },
        MainRoute::SomethingWentWrong => html! { <SomethingWentWrongPage/> },
        MainRoute::AccessDenied => html! { <AccessDeniedPage/> },
        MainRoute::NotYetImplemented => html! { <NotYetImplemented/> },
        MainRoute::PersonProfile { pun } => {
            html! { <MainLayout><h1>{format!("Person {}", pun)}</h1></MainLayout> }
        }
        MainRoute::AccountSwitch => {
            html! { <Switch<AccountRoute> render={account_switch} />}
        }
    }
}
