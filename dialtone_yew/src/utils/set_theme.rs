use dialtone_common::rest::sites::theme::Theme;
use dialtone_common::utils::make_id::MakeId;
use gloo::utils::body;

pub fn set_theme(theme: &Theme) {
    let theme_value = format!("theme_{}", theme.to_string().make_id());
    body().class_list().set_value(&theme_value);
}
