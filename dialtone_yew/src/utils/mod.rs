use yew::{html, Html};

pub mod element;
pub mod get_input_value;
pub mod media;
pub mod scroll;
pub mod set_focus;
pub mod set_theme;

pub const DATETIME_FORMAT: &str = "%a %d-%b-%Y %T Z";

pub fn dialtone_svg_logo() -> Html {
    html! {
        <svg
                width="133.26295mm"
                height="94.757332mm"
                viewBox="0 0 133.26295 94.757332"
                version="1.1"
                id="svg8"
                class="dt-logo">
            <defs
                    id="defs2" />
            <g
                    id="layer1"
                    transform="translate(-20.212842,-59.113652)">
                <rect
                        style="stroke-width:3.76499;stroke-linejoin:round;image-rendering:auto"
                        id="rect833"
                        width="129.49797"
                        height="90.99234"
                        x="22.095337"
                        y="60.996147"
                        ry="4.9131541"
                        class="logo-border" />
                <path
                        style="stroke-width:4.91396;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                        d="M 33.871577,116.84568 76.98863,70.043132"
                        id="path839"
                        class="logo-color-action" />
                <path
                        style="stroke-width:7.30413;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                        d="M 37.164318,140.93297 101.27819,71.392271"
                        id="path854"
                        class="logo-color-action" />
                <path
                        style="stroke-width:7.38753;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                        d="M 64.569415,143.3627 129.39139,73.00204"
                        id="path856"
                        class="logo-color-primary" />
                <path
                        style="stroke-width:5.58259;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                        d="M 90.885366,143.64697 140.05432,90.676453"
                        id="path858"
                        class="logo-color-primary" />
                <path
                        style="stroke-width:3.04679;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                        d="m 116.90006,142.91526 26.55086,-29.21867"
                        id="path860"
                        class="logo-color-action" />
                <path
                        style="stroke-width:2.88811;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
                        d="M 31.313973,97.123117 57.070038,70.188029"
                        id="path862"
                        class="logo-color-primary" />
            </g>
        </svg>
    }
}
