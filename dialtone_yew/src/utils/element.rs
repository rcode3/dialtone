use gloo::console::log;
use web_sys::Element;

pub fn get_id_or_parent_id(element: Element, class_name: &str) -> Option<String> {
    let id = element.id();
    let contains_class = element.class_list().contains(class_name);
    log! {"id", &id, " contains class ", contains_class};
    if id.is_empty() || !contains_class {
        let parent_element = element.parent_element();
        if let Some(parent) = parent_element {
            get_id_or_parent_id(parent, class_name)
        } else {
            None
        }
    } else {
        Some(id)
    }
}
