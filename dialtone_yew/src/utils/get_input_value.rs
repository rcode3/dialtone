use gloo::utils::document;
use wasm_bindgen::JsCast;
use web_sys::HtmlInputElement;

pub fn get_input_value(id: &str) -> Option<String> {
    let value = document()
        .query_selector(format!("#{id}").as_ref())
        .unwrap();
    if value.is_none() {
        None
    } else {
        let value = value
            .unwrap()
            .dyn_ref::<HtmlInputElement>()
            .unwrap()
            .value();
        Some(value)
    }
}
