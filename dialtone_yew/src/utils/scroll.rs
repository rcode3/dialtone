use gloo::utils::document;
use wasm_bindgen::JsCast;
use web_sys::HtmlElement;

pub fn scroll_into_view(id: &str) {
    document()
        .query_selector(format!("#{id}").as_ref())
        .unwrap()
        .unwrap()
        .dyn_ref::<HtmlElement>()
        .unwrap()
        .scroll_into_view_with_bool(true);
}
