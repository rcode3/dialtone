pub fn is_small_screen() -> bool {
    let mql = gloo::utils::window()
        .match_media("(max-width: 1100px)")
        .unwrap();
    match mql {
        Some(mql) => mql.matches(),
        None => false,
    }
}
