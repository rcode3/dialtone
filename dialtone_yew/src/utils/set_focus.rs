use gloo::utils::document;
use wasm_bindgen::JsCast;
use web_sys::HtmlElement;

pub fn set_focus(id: &str) {
    document()
        .query_selector(format!("#{id}").as_ref())
        .unwrap()
        .unwrap()
        .dyn_ref::<HtmlElement>()
        .unwrap()
        .focus()
        .unwrap();
}
