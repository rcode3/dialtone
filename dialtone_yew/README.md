# dialtone_yew

This is a [Yew](https://yew.rs/) webapp for the dialtone system.

### About dialtone

Dialtone is an un-imagined Activity Pub system. What does "un-imagined" mean? Basically, there is no intent to silo this software into a specific vertical "marketplace". Instead, emergent use cases will be addressed.

This software is under active, initial development and is far from feature complete or ready for use.

### License

Licensed under either of

 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)
  at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the
Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.

## Setup

Install necessary tools with `cargo make`

```shell
cargo make install_tools
```

## Build and/or Serve

To build:

```shell
trunk build
```

To build and serve:

```shell
trunk serve --open
```