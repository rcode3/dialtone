use crate::ap::id::{create_actor_id, create_collection_id, ExtraCollectionNames, SiteActors};

pub fn site_banners_collection_id(host_name: &str) -> String {
    let actor_id = create_actor_id(host_name, &SiteActors::SiteContent.to_string());
    create_collection_id(&actor_id, &ExtraCollectionNames::Banners.to_string())
}

pub fn site_icons_collection_id(host_name: &str) -> String {
    let actor_id = create_actor_id(host_name, &SiteActors::SiteContent.to_string());
    create_collection_id(&actor_id, &ExtraCollectionNames::Icons.to_string())
}
