use const_format::concatcp;
use git_version::git_version;

pub const GIT_VERSION: &str = git_version!(fallback = "unknown");
pub const CARGO_PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
//noinspection RsTypeCheck
pub const DT_VERSION: &str = concatcp!(CARGO_PKG_VERSION, "_", GIT_VERSION);
