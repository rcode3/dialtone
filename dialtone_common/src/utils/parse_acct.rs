use crate::rest::users::user_login::NameHostPair;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ParseUserAcctError {
    #[error("Generic parsing error.")]
    GenericParseError,
}

/// Takes a users account and breaks into users and host parts.
pub fn parse_user_acct(user_acct: &str) -> Result<NameHostPair, ParseUserAcctError> {
    let parts = user_acct.rsplit_once('@');
    if let Some(values) = parts {
        Ok(NameHostPair {
            user_name: values.0.to_string(),
            host_name: values.1.to_string(),
        })
    } else {
        Err(ParseUserAcctError::GenericParseError)
    }
}

#[cfg(test)]
mod parse_user_acct_tests {
    use crate::rest::users::user_login::NameHostPair;

    use super::parse_user_acct;

    #[test]
    #[allow(non_snake_case)]
    fn GIVEN_invalid_separator_in_acct_WHEN_parse_THEN_error() {
        // GIVEN
        let acct = "foo!!bar";

        // WHEN
        let action = parse_user_acct(acct);

        // THEN
        assert!(action.is_err());
    }

    #[test]
    #[allow(non_snake_case)]
    fn GIVEN_foo_at_bar_WHEN_parse_THEN_user_is_foo_and_host_is_bar() {
        // GIVEN
        let acct = "foo@bar";

        // WHEN
        let action = parse_user_acct(acct);

        // THEN
        assert_eq!(
            action.unwrap(),
            NameHostPair {
                user_name: "foo".to_string(),
                host_name: "bar".to_string()
            }
        );
    }

    #[test]
    #[allow(non_snake_case)]
    fn GIVEN_foo_at_bar_at_bar_WHEN_parse_THEN_user_is_foo_at_bar_and_host_is_bar() {
        // GIVEN
        let acct = "foo@bar@bar";

        // WHEN
        let action = parse_user_acct(acct);

        // THEN
        assert_eq!(
            action.unwrap(),
            NameHostPair {
                user_name: "foo@bar".to_string(),
                host_name: "bar".to_string()
            }
        );
    }
}
