#[macro_export]
macro_rules! api_dt {
    ( $( $i:ident, $l:literal ), *) => {
        pub const API_DT: &str = "/api/dt";
        pub mod path {
            $(
                pub const $i: &str = $l;
            )*
        }
        pub mod full_path {
            use const_format::concatcp;
            $(
                pub const $i: &str = concatcp!(super::API_DT, $l);
            )*
        }
    };
}

#[cfg(test)]
mod api_dt_tests {

    api_dt!(FOO, "/foo", BAR, "/bar");

    #[test]
    #[allow(non_snake_case)]
    fn WHEN_paths_are_macroed_THEN_consts_in_both_modules() {
        // WHEN

        // THEN
        assert_eq!(path::FOO, "/foo");
        assert_eq!(path::BAR, "/bar");

        assert_eq!(full_path::FOO, "/api/dt/foo");
        assert_eq!(full_path::BAR, "/api/dt/bar");
    }
}
