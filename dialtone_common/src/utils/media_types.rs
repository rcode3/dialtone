pub const JRD_SUB_TYPE: &str = "jrd";
pub const JRD_CONTENT_TYPE: &str = "application/jrd+json";

pub const XRD_SUB_TYPE: &str = "xrd";
pub const XRD_CONTENT_TYPE: &str = "application/xrd+json";

pub const ACTIVITY_PUB_SUB_TYPE: &str = "activity";
pub const ACTIVITY_PUB_MEDIA_TYPE: &str = "application/activity+json";

pub const ACTIVITY_STREAMS_SUB_TYPE: &str = "ld";
pub const ACTIVITY_STREAMS_PROFILE_PARAMETER: &str = "https://www.w3.org/ns/activitystreams";
pub const ACTIVITY_STREAMS_MEDIA_TYPE: &str =
    "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"";

pub const TEXT_PLAIN_TYPE: &str = "text/plain";
pub const OCTET_STREAM_TYPE: &str = "application/octet-stream";

pub const JSON_TYPE: &str = "application/json";

pub const HTML_TYPE: &str = "text/html";
