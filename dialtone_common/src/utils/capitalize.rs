pub fn capitalize_all(s: &str) -> String {
    let v = s.split(&[' '][..]).collect::<Vec<&str>>();
    v.iter()
        .map(|word| capitalize(word))
        .collect::<Vec<String>>()
        .join(" ")
}

pub fn capitalize(s: &str) -> String {
    let mut iter = s.chars();
    match iter.next() {
        None => String::new(),
        Some(c) => c.to_uppercase().to_string() + iter.as_str(),
    }
}

pub trait Capitalize {
    fn capitalize(&self) -> String;
    fn capitalize_all(&self) -> String;
}

impl Capitalize for String {
    fn capitalize(&self) -> String {
        capitalize(self)
    }

    fn capitalize_all(&self) -> String {
        capitalize_all(self)
    }
}

#[cfg(test)]
mod capitalize_tests {
    use super::*;

    #[test]
    fn test_capitalize() {
        assert_eq!(capitalize("foo"), "Foo");
    }

    #[test]
    fn test_impl_capitalize() {
        assert_eq!("foo".to_string().capitalize(), "Foo");
    }

    #[test]
    fn test_capitalize_all() {
        assert_eq!(capitalize_all("foo bar"), "Foo Bar");
    }

    #[test]
    fn test_impl_capitalize_all() {
        assert_eq!("foo bar".to_string().capitalize_all(), "Foo Bar");
    }
}
