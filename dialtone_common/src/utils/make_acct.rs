/// Makes a user account string from a user name and host_name.
pub fn make_acct(user_name: &str, host_name: &str) -> String {
    format!("{user_name}@{host_name}")
}
