/// Takes a string slice and replaces all spaces with underscore.
/// This is useful for making HTML ids.
pub fn make_id(s: &str) -> String {
    s.to_lowercase().replace(&[' '][..], "_")
}

pub trait MakeId {
    fn make_id(&self) -> String;
}

impl MakeId for String {
    fn make_id(&self) -> String {
        make_id(self)
    }
}

#[cfg(test)]
mod make_id_tests {
    use super::*;

    #[test]
    fn test_make_id() {
        assert_eq!(make_id("foo"), "foo".to_string());
        assert_eq!(make_id("foo bar"), "foo_bar".to_string());
        assert_eq!(make_id("foo  bar"), "foo__bar".to_string());
    }

    #[test]
    fn test_impl_make_id() {
        assert_eq!("foo".to_string().make_id(), "foo".to_string());
        assert_eq!("foo bar".to_string().make_id(), "foo_bar".to_string());
    }
}
