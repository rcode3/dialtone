use crate::{ap::id::parse_actors_host, rest::users::web_user::SystemRoleType};

use super::{host::authz_on_host, user_authz_info::UserAuthzInfo};

pub fn authz_actor_on_host(
    user_authz_info: &UserAuthzInfo,
    role: &SystemRoleType,
    actor_id: &str,
) -> bool {
    let host_name = parse_actors_host(actor_id);
    if let Ok(host_name) = host_name {
        authz_on_host(user_authz_info, role, &host_name)
    } else {
        false
    }
}
