use serde::{Deserialize, Serialize};

use crate::rest::users::web_user::{SystemPermission, UserStatus};

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Default)]
pub struct UserAuthzInfo {
    pub status: UserStatus,
    pub acct: String,
    pub system_permissions: Vec<SystemPermission>,
}
