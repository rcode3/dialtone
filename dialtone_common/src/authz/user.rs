use crate::{rest::users::web_user::SystemRoleType, utils::parse_acct::parse_user_acct};

use super::{host::authz_on_host, user_authz_info::UserAuthzInfo};

pub fn authz_user_on_host(
    user_authz_info: &UserAuthzInfo,
    role: &SystemRoleType,
    user_acct: &str,
) -> bool {
    let name_host = parse_user_acct(user_acct);
    if let Ok(name_host) = name_host {
        authz_on_host(user_authz_info, role, &name_host.host_name)
    } else {
        false
    }
}
