use crate::rest::users::web_user::{SystemPermission, SystemRoleType};

use super::user_authz_info::UserAuthzInfo;

pub fn authz_on_host(
    user_authz_info: &UserAuthzInfo,
    role: &SystemRoleType,
    host_name: &str,
) -> bool {
    let perm = SystemPermission {
        role: role.to_owned(),
        host_name: host_name.to_owned(),
    };
    user_authz_info.system_permissions.contains(&perm)
}

#[cfg(test)]
mod authz_on_host_tests {
    use dialtone_test_util::test_constants::{
        TEST_ACTORADMIN_ACCT, TEST_HOSTNAME, TEST_USERADMIN_ACCT,
    };

    use crate::{
        authz::{host::authz_on_host, user_authz_info::UserAuthzInfo},
        rest::users::web_user::{SystemPermission, SystemRoleType, UserStatus},
    };

    #[test]
    #[allow(non_snake_case)]
    fn GIVEN_useradmin_user_authz_info_WHEN_authz_useradmin_THEN_ok() {
        let user_authz = UserAuthzInfo {
            status: UserStatus::Active,
            acct: TEST_USERADMIN_ACCT.to_string(),
            system_permissions: vec![SystemPermission {
                role: SystemRoleType::UserAdmin,
                host_name: TEST_HOSTNAME.to_string(),
            }],
        };
        assert!(authz_on_host(
            &user_authz,
            &SystemRoleType::UserAdmin,
            TEST_HOSTNAME
        ));
    }

    #[test]
    #[allow(non_snake_case)]
    fn GIVEN_actoradmin_user_authz_info_WHEN_authz_actoradmin_THEN_ok() {
        let user_authz = UserAuthzInfo {
            status: UserStatus::Active,
            acct: TEST_ACTORADMIN_ACCT.to_string(),
            system_permissions: vec![SystemPermission {
                role: SystemRoleType::ActorAdmin,
                host_name: TEST_HOSTNAME.to_string(),
            }],
        };
        assert!(authz_on_host(
            &user_authz,
            &SystemRoleType::ActorAdmin,
            TEST_HOSTNAME
        ));
    }

    #[test]
    #[allow(non_snake_case)]
    fn GIVEN_useradmin_user_authz_info_WHEN_authz_actoradmin_THEN_err() {
        let user_authz = UserAuthzInfo {
            status: UserStatus::Active,
            acct: TEST_USERADMIN_ACCT.to_string(),
            system_permissions: vec![SystemPermission {
                role: SystemRoleType::UserAdmin,
                host_name: TEST_HOSTNAME.to_string(),
            }],
        };
        assert!(!authz_on_host(
            &user_authz,
            &SystemRoleType::ActorAdmin,
            TEST_HOSTNAME
        ));
    }
}
