use crate::{ap::id::parse_ap_object_id_host, rest::users::web_user::SystemRoleType};

use super::{host::authz_on_host, user_authz_info::UserAuthzInfo};

pub fn authz_ap_object_on_host(
    user_authz_info: &UserAuthzInfo,
    role: &SystemRoleType,
    ap_object_id: &str,
) -> bool {
    let host_name = parse_ap_object_id_host(ap_object_id);
    if let Ok(host_name) = host_name {
        authz_on_host(user_authz_info, role, &host_name)
    } else {
        false
    }
}
