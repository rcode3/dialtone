use std::iter;

use serde::{Deserialize, Serialize};
use serde_json::Value;

use crate::utils::media_types::ACTIVITY_STREAMS_PROFILE_PARAMETER;

use super::AP_PUBLIC_ADDRESS;

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
#[serde(untagged)]
pub enum ApObjects {
    SingleHref(String),
    MultipleHrefs(Vec<String>),
    SingleObj(Box<ApObject>),
    MultipleObjs(Vec<ApObject>),
}

impl ApObjects {
    fn ap_objects_vec(&self) -> Vec<ApObject> {
        match self {
            ApObjects::SingleHref(href) => {
                vec![ApObject::from(href.to_string())]
            }
            ApObjects::MultipleHrefs(hrefs) => hrefs
                .iter()
                .map(|href| ApObject::from(href.to_string()))
                .collect(),
            ApObjects::SingleObj(obj) => {
                vec![*obj.clone()]
            }
            ApObjects::MultipleObjs(objs) => objs.clone(),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
#[serde(untagged)]
pub enum ApObjectAddresses {
    SingleUrl(String),
    MultipleUrls(Vec<String>),
}

impl ApObjectAddresses {
    pub fn any(&self, address: &str) -> bool {
        match self {
            ApObjectAddresses::SingleUrl(single_address) => address.eq(single_address),
            ApObjectAddresses::MultipleUrls(addresses) => addresses.iter().any(|a| address.eq(a)),
        }
    }

    /// Gets an iterator over the addresses.
    ///
    /// The iterators used are marked as Send, so the return type is safe as Send.
    pub fn iter<'a>(&'a self) -> Box<dyn std::iter::Iterator<Item = &str> + 'a + Send> {
        match self {
            ApObjectAddresses::SingleUrl(single_address) => {
                Box::new(iter::once(single_address).map(|s| s.as_ref()))
            }
            ApObjectAddresses::MultipleUrls(addresses) => {
                Box::new(addresses.iter().map(|s| s.as_ref()))
            }
        }
    }
}

impl From<&str> for ApObjectAddresses {
    fn from(value: &str) -> Self {
        ApObjectAddresses::SingleUrl(value.to_string())
    }
}

impl From<String> for ApObjectAddresses {
    fn from(value: String) -> Self {
        ApObjectAddresses::SingleUrl(value)
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub enum ApObjectType {
    Article,
    Note,
    Document,
    Image,
    Video,
    Audio,
    Page,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub enum ApObjectMediaType {
    #[serde(rename = "text/html")]
    TextHtml,

    #[serde(rename = "image/png")]
    ImagePng,

    #[serde(rename = "image/jpeg")]
    ImageJpeg,

    #[serde(rename = "image/gif")]
    ImageGif,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct ApObject {
    #[serde(rename = "@context")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ap_context: Option<Value>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,

    #[serde(rename = "mediaType")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub media_type: Option<ApObjectMediaType>,

    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub ap_type: Option<ApObjectType>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub content: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub summary: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub actor: Option<String>,

    #[serde(rename = "attributedTo")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub attributed_to: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub to: Option<ApObjectAddresses>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub cc: Option<ApObjectAddresses>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub bto: Option<ApObjectAddresses>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub bcc: Option<ApObjectAddresses>,
}

#[buildstructor::buildstructor]
impl ApObject {
    #[builder]
    pub fn new(
        _ap_context: Option<Value>,
        name: Option<String>,
        id: Option<String>,
        url: Option<String>,
        media_type: Option<ApObjectMediaType>,
        ap_type: Option<ApObjectType>,
        content: Option<String>,
        summary: Option<String>,
        actor: Option<String>,
        attributed_to: Option<String>,
        to: Option<ApObjectAddresses>,
        cc: Option<ApObjectAddresses>,
        bto: Option<ApObjectAddresses>,
        bcc: Option<ApObjectAddresses>,
    ) -> Self {
        Self {
            ap_context: Some(Value::String(ACTIVITY_STREAMS_PROFILE_PARAMETER.to_owned())),
            name,
            id,
            url,
            media_type,
            ap_type,
            content,
            summary,
            actor,
            attributed_to,
            to,
            cc,
            bto,
            bcc,
        }
    }

    /// Gets an iterator over the addresses (to and cc).
    ///
    /// The iterators used are marked as Send, so the return type is safe as Send.
    pub fn addresses<'a>(&'a self) -> Box<dyn std::iter::Iterator<Item = &str> + 'a + Send> {
        Box::new(
            self.to
                .as_ref()
                .map(|addrs| addrs.iter())
                .unwrap_or_else(|| Box::new(iter::empty()))
                .chain(
                    self.cc
                        .as_ref()
                        .map(|addrs| addrs.iter())
                        .unwrap_or_else(|| Box::new(iter::empty())),
                ),
        )
    }

    /// Gets an iterator over the blind addresses (bto and bcc).
    ///
    /// The iterators used are marked as Send, so the return type is safe as Send.
    pub fn blind_addresses<'a>(&'a self) -> Box<dyn std::iter::Iterator<Item = &str> + 'a + Send> {
        Box::new(
            self.bto
                .as_ref()
                .map(|addrs| addrs.iter())
                .unwrap_or_else(|| Box::new(iter::empty()))
                .chain(
                    self.bcc
                        .as_ref()
                        .map(|addrs| addrs.iter())
                        .unwrap_or_else(|| Box::new(iter::empty())),
                ),
        )
    }

    pub fn any_address(&self, address: &str) -> bool {
        self.addresses().any(|a| address.eq(a))
    }

    pub fn any_blind_address(&self, address: &str) -> bool {
        self.blind_addresses().any(|a| address.eq(a))
    }

    pub fn public_addressed(&self) -> bool {
        self.any_address(AP_PUBLIC_ADDRESS)
    }
}

impl From<String> for ApObject {
    fn from(url: String) -> Self {
        ApObject {
            ap_context: Some(Value::String(ACTIVITY_STREAMS_PROFILE_PARAMETER.to_owned())),
            name: None,
            id: None,
            url: Option::from(url),
            media_type: None,
            ap_type: None,
            content: None,
            summary: None,
            actor: None,
            attributed_to: None,
            to: None,
            cc: None,
            bto: None,
            bcc: None,
        }
    }
}

impl From<&str> for ApObject {
    fn from(url: &str) -> Self {
        ApObject::from(url.to_owned())
    }
}

#[cfg(test)]
#[allow(non_snake_case)]
mod ap_object_tests {
    use serde_json::json;

    use super::*;

    #[test]
    fn GIVEN_single_address_WHEN_any_THEN_true() {
        // GIVEN
        let address = ApObjectAddresses::SingleUrl("foo".to_string());

        // WHEN
        let found = address.any("foo");

        // THEN
        assert!(found);
    }

    #[test]
    fn GIVEN_multiple_addresses_WHEN_any_THEN_true() {
        // GIVEN
        let address = ApObjectAddresses::MultipleUrls(vec!["bar".to_string(), "foo".to_string()]);

        // WHEN
        let found = address.any("foo");

        // THEN
        assert!(found);
    }

    #[test]
    fn parse_single_url() {
        let u1 = "http://example.com/icon.png";
        let json = format!("\"{u1}\"");
        let ap_obj: ApObjects = serde_json::from_str(&json).unwrap();
        let vec = ap_obj.ap_objects_vec();
        assert_eq!(1, vec.len());
        assert_eq!(u1.to_string(), vec[0].url.clone().unwrap());
    }

    #[test]
    fn parse_array_of_urls() {
        let u1 = "http://example.com/icon.png";
        let u2 = "http://example.org/foo.png";
        let json = format!("[\"{u1}\",\"{u2}\"]");
        let ap_obj: ApObjects = serde_json::from_str(&json).unwrap();
        let vec = ap_obj.ap_objects_vec();
        assert_eq!(2, vec.len());
        assert_eq!(u1.to_string(), vec[0].url.clone().unwrap());
        assert_eq!(u2.to_string(), vec[1].url.clone().unwrap());
    }

    #[test]
    fn parse_obj() {
        let u1 = "http://example.com/icon.png";
        let value = json!({
            "@context": ACTIVITY_STREAMS_PROFILE_PARAMETER,
            "url": u1
        });
        let json = serde_json::to_string(&value).unwrap();
        let ap_obj: ApObjects = serde_json::from_str(&json).unwrap();
        let vec = ap_obj.ap_objects_vec();
        assert_eq!(1, vec.len());
        assert_eq!(u1.to_string(), vec[0].url.clone().unwrap());
    }

    #[test]
    fn parse_multiple_objs() {
        let u1 = "http://example.com/icon.png";
        let u2 = "http://example.org/foo.png";
        let value = json!([
            {
                "@context": ACTIVITY_STREAMS_PROFILE_PARAMETER,
                "url": u1
            },
            {
                "@context": ACTIVITY_STREAMS_PROFILE_PARAMETER,
                "url": u2
            }
        ]);
        let json = serde_json::to_string(&value).unwrap();
        let ap_obj: ApObjects = serde_json::from_str(&json).unwrap();
        let vec = ap_obj.ap_objects_vec();
        assert_eq!(2, vec.len());
        assert_eq!(u1.to_string(), vec[0].url.clone().unwrap());
        assert_eq!(u2.to_string(), vec[1].url.clone().unwrap());
    }

    #[test]
    fn size_of_icons() {
        println!("Size of Icons is {}", std::mem::size_of::<ApObjects>())
    }

    #[test]
    fn roundtrip_mastodon_example_test() {
        let ex = r#"
            {
               "@context" : [
                  "https://www.w3.org/ns/activitystreams",
                  {
                     "atomUri" : "ostatus:atomUri",
                     "blurhash" : "toot:blurhash",
                     "conversation" : "ostatus:conversation",
                     "focalPoint" : {
                        "@container" : "@list",
                        "@id" : "toot:focalPoint"
                     },
                     "inReplyToAtomUri" : "ostatus:inReplyToAtomUri",
                     "ostatus" : "http://ostatus.org#",
                     "sensitive" : "as:sensitive",
                     "toot" : "http://joinmastodon.org/ns#",
                     "votersCount" : "toot:votersCount"
                  }
               ],
               "atomUri" : "https://fosstodon.org/users/xpil/statuses/107831067777241377",
               "attachment" : [
                  {
                     "blurhash" : "UHH.KH00t7M{xuWBaya|WBj[fQayofWVj[ay",
                     "height" : 249,
                     "mediaType" : "image/jpeg",
                     "name" : null,
                     "type" : "Document",
                     "url" : "https://cdn.fosstodon.org/media_attachments/files/107/831/067/339/133/535/original/7117410b8c95755d.jpg",
                     "width" : 599
                  }
               ],
               "attributedTo" : "https://fosstodon.org/users/xpil",
               "cc" : [
                  "https://fosstodon.org/users/xpil/followers"
               ],
               "mediaType": "text/html",
               "content" : "<p>I was 4 years old when this was published. Could as well be yesterday.</p>",
               "contentMap" : {
                  "en" : "<p>I was 4 years old when this was published. Could as well be yesterday.</p>"
               },
               "conversation" : "tag:fosstodon.org,2022-02-20:objectId=43364305:objectType=Conversation",
               "id" : "https://fosstodon.org/users/xpil/statuses/107831067777241377",
               "inReplyTo" : null,
               "inReplyToAtomUri" : null,
               "published" : "2022-02-20T15:38:37Z",
               "replies" : {
                  "first" : {
                     "items" : [],
                     "next" : "https://fosstodon.org/users/xpil/statuses/107831067777241377/replies?only_other_accounts=true&page=true",
                     "partOf" : "https://fosstodon.org/users/xpil/statuses/107831067777241377/replies",
                     "type" : "CollectionPage"
                  },
                  "id" : "https://fosstodon.org/users/xpil/statuses/107831067777241377/replies",
                  "type" : "Collection"
               },
               "sensitive" : false,
               "summary" : null,
               "tag" : [],
               "to" : [
                  "https://www.w3.org/ns/activitystreams#Public"
               ],
               "type" : "Note",
               "url" : "https://fosstodon.org/@xpil/107831067777241377"
            }
        "#;
        let ap_obj: ApObject = serde_json::from_str(ex).unwrap();
        let j = serde_json::to_string(&ap_obj).unwrap();
        let _ao2: ApObject = serde_json::from_str(&j).unwrap();
    }

    #[test]
    fn roundtrip_pleforma_example_test() {
        let ex = r#"
            {
               "@context" : [
                  "https://www.w3.org/ns/activitystreams",
                  "https://kiwifarms.cc/schemas/litepub-0.1.jsonld",
                  {
                     "@language" : "und"
                  }
               ],
               "actors" : "https://kiwifarms.cc/users/Harmful-if-Swallowed",
               "attachment" : [
                  {
                     "mediaType" : "image/png",
                     "name" : "",
                     "type" : "Document",
                     "url" : "https://kiwifarms.cc/media/9decd4556cac13af33b069d53520bdbfbda083d73fe8da26cf92a052b23db3c9.png"
                  }
               ],
               "attributedTo" : "https://kiwifarms.cc/users/Harmful-if-Swallowed",
               "cc" : [
                  "https://kiwifarms.cc/users/Harmful-if-Swallowed/followers"
               ],
               "content" : "<span class=\"h-card\"><a class=\"u-url mention\" data-users=\"A34BHSwtq9XQSf9M3c\" href=\"https://kiwifarms.cc/users/vinnegan\" rel=\"ugc\">@<span>vinnegan</span></a></span> <span class=\"h-card\"><a class=\"u-url mention\" data-users=\"A38VtEtI3uaDG561NA\" href=\"https://kiwifarms.cc/users/Not_a_cat\" rel=\"ugc\">@<span>Not_a_cat</span></a></span> <span class=\"h-card\"><a class=\"u-url mention\" data-users=\"A36kcmMTTYAz8hR6O0\" href=\"https://kiwifarms.cc/users/Pagliano\" rel=\"ugc\">@<span>Pagliano</span></a></span> archiving this already because you know the guy is gonna purge everything as per usual",
               "context" : "https://kiwifarms.cc/contexts/4628da99-30ad-4016-82db-686e1397ab85",
               "conversation" : "https://kiwifarms.cc/contexts/4628da99-30ad-4016-82db-686e1397ab85",
               "id" : "https://kiwifarms.cc/objects/3a10192b-6113-490c-9f66-c0a6615121fc",
               "published" : "2022-02-20T23:37:27.762136Z",
               "sensitive" : null,
               "source" : "@vinnegan @Not_a_cat @Pagliano archiving this already because you know the guy is gonna purge everything as per usual",
               "summary" : "",
               "tag" : [
                  {
                     "href" : "https://kiwifarms.cc/users/Not_a_cat",
                     "name" : "@Not_a_cat",
                     "type" : "Mention"
                  },
                  {
                     "href" : "https://kiwifarms.cc/users/Pagliano",
                     "name" : "@Pagliano",
                     "type" : "Mention"
                  },
                  {
                     "href" : "https://kiwifarms.cc/users/vinnegan",
                     "name" : "@vinnegan",
                     "type" : "Mention"
                  }
               ],
               "to" : [
                  "https://www.w3.org/ns/activitystreams#Public",
                  "https://kiwifarms.cc/users/Not_a_cat",
                  "https://kiwifarms.cc/users/Pagliano",
                  "https://kiwifarms.cc/users/vinnegan"
               ],
               "type" : "Note"
            }
        "#;
        let ap_obj: ApObject = serde_json::from_str(ex).unwrap();
        let j = serde_json::to_string(&ap_obj).unwrap();
        let _ao2: ApObject = serde_json::from_str(&j).unwrap();
    }
}
