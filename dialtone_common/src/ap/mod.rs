use thiserror::Error;

pub mod activity;
pub mod actor;
pub mod ap_object;
pub mod collection;
pub mod id;
pub mod image_attr;
pub mod pun;

pub const AP_PUBLIC_ADDRESS: &str = "https://www.w3.org/ns/activitystreams#Public";

#[derive(Error, Debug)]
pub enum ActivityPubError {
    #[error("Invalid Preferred User Name")]
    InvalidPreferredUserName,
    #[error("Invalid Collection Name")]
    InvalidCollectionName,
    #[error("Unsupported Object in Activity")]
    UnsupportedObjectInActivity,
    #[error("ActivityPub ID is Missing.")]
    MissingActivityPubId,
}
