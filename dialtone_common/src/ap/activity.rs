use serde::{Deserialize, Serialize};
use serde_json::Value;
use strum_macros::{Display, EnumString, EnumVariantNames};

use crate::utils::media_types::ACTIVITY_STREAMS_PROFILE_PARAMETER;

use super::{actor::Actor, ap_object::ApObject, collection::Collection, id::create_activity_id};

#[derive(
    Serialize, Deserialize, Debug, PartialEq, Eq, Copy, Clone, Display, EnumString, EnumVariantNames,
)]
#[strum(serialize_all = "title_case")]
pub enum ActivityType {
    Create,
    Update,
    Delete,
    Follow,
    Undo,
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone, Debug)]
#[serde(untagged)]
pub enum ActivityObjectType {
    Id(String),
    Actor(Box<Actor>),
    ApObject(Box<ApObject>),
    Collection(Box<Collection>),
    Activity(Box<Activity>),
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct Activity {
    #[serde(rename = "@context")]
    pub ap_context: Option<Value>,

    #[serde(rename = "type")]
    pub activity_type: ActivityType,

    pub id: String,

    #[serde(rename = "actor")]
    pub actor_id: String,

    pub object: ActivityObjectType,
}

#[buildstructor::buildstructor]
impl Activity {
    #[builder]
    pub fn new(
        activity_type: ActivityType,
        id: String,
        actor_id: String,
        object: ActivityObjectType,
    ) -> Self {
        Self {
            ap_context: Some(Value::String(ACTIVITY_STREAMS_PROFILE_PARAMETER.to_owned())),
            activity_type,
            id,
            actor_id,
            object,
        }
    }

    #[builder(entry = "actor")]
    pub fn new_for_actor(
        activity_type: ActivityType,
        actor_id: String,
        object: ActivityObjectType,
    ) -> Self {
        Self {
            ap_context: Some(Value::String(ACTIVITY_STREAMS_PROFILE_PARAMETER.to_owned())),
            activity_type,
            id: create_activity_id(&actor_id),
            actor_id,
            object,
        }
    }
}

pub fn create_ap_object_activity(actor_id: &str, ap_object: &ApObject) -> Activity {
    Activity::new_for_actor(
        ActivityType::Create,
        actor_id.to_owned(),
        ActivityObjectType::ApObject(Box::new(ap_object.to_owned())),
    )
}

#[cfg(test)]
#[allow(non_snake_case)]
mod tests {
    use crate::ap::ap_object::{ApObject, ApObjectType};

    use super::Activity;

    #[test]
    fn GIVEN_create_activity_WHEN_parse_THEN_success() {
        // GIVEN
        let actor_id = "https://example.com/foo";
        let ap_object = ApObject::builder()
            .id(format!("{actor_id}/test_apobject"))
            .ap_type(ApObjectType::Note)
            .content("<p>foo</p>")
            .attributed_to(actor_id)
            .build();
        let activity = Activity::new_for_actor(
            super::ActivityType::Create,
            actor_id.to_string(),
            super::ActivityObjectType::ApObject(Box::new(ap_object)),
        );
        let json = serde_json::to_string_pretty(&activity).unwrap();
        println!("{json}");

        // WHEN
        let actual = serde_json::from_str::<Activity>(&json);

        // THEN
        actual.unwrap();
    }

    #[test]
    fn GIVEN_mastodon_delete_activity_WHEN_parse_THEN_success() {
        // GIVEN
        let json = r#"
            {
                "@context" : "https://www.w3.org/ns/activitystreams",
                "actor" : "https://fosstodon.org/users/grasek",
                "id" : "https://fosstodon.org/users/grasek#delete",
                "object" : "https://fosstodon.org/users/grasek",
                "signature" : {
                    "created" : "2023-01-15T23:40:17Z",
                    "creator" : "https://fosstodon.org/users/grasek#main-key",
                    "signatureValue" : "G2Zt35m4tHORZgC6n1Y4bRkwPALJMk89N/5SioLxtOFie70mG+YBH9w0YFGOBKggXxRRDmiRMtd1FWQvG9CBYYA/XZdh8uZOiNo1SvvqIWh0W33W31xdZHC6OSDww1hKKuc5sE9ZGhDEJUqliNeXmAsVmajOLRHGOYd2F7M6U6oOeGJZZ7V1upgXT0GNcFLAqr1h/IcJuFLOEOVTdDP4BNrGbJvjL7Nx9/9jbgPbxqGNBBw21W+rMDTj4qXYZVnuNAiQ7eIZiiqsO10rAk45uXDvNu4hintgPwaWwRbvYx0yEWE9QfwhiNGreFLUGTNWWQnCevZB9RyrpnEQFYeN5w==",
                    "type" : "RsaSignature2017"
                },
                "to" : [
                    "https://www.w3.org/ns/activitystreams#Public"
                ],
                "type" : "Delete"
            }
        "#;

        // WHEN
        let activity = serde_json::from_str::<Activity>(json);

        // THEN
        activity.unwrap();
    }

    #[test]
    fn GIVEN_mastodon_dm_activity_WHEN_parse_THEN_success() {
        // GIVEN
        let json = r#"
        {
           "@context" : [
              "https://www.w3.org/ns/activitystreams",
              {
                 "atomUri" : "ostatus:atomUri",
                 "conversation" : "ostatus:conversation",
                 "inReplyToAtomUri" : "ostatus:inReplyToAtomUri",
                 "ostatus" : "http://ostatus.org#",
                 "sensitive" : "as:sensitive",
                 "toot" : "http://joinmastodon.org/ns#",
                 "votersCount" : "toot:votersCount"
              }
           ],
           "actor" : "https://fosstodon.org/users/rcode3",
           "cc" : [],
           "id" : "https://fosstodon.org/users/rcode3/statuses/109699061812792939/activity",
           "object" : {
              "atomUri" : "https://fosstodon.org/users/rcode3/statuses/109699061812792939",
              "attachment" : [],
              "attributedTo" : "https://fosstodon.org/users/rcode3",
              "cc" : [],
              "content" : "<p><span class=\"h-card\"><a href=\"https://dt-dev.snark.fail/pub/p/test-dude230116a\" class=\"u-url mention\">@<span>test-dude230116a</span></a></span> this is a test dm</p>",
              "contentMap" : {
                 "en" : "<p><span class=\"h-card\"><a href=\"https://dt-dev.snark.fail/pub/p/test-dude230116a\" class=\"u-url mention\">@<span>test-dude230116a</span></a></span> this is a test dm</p>"
              },
              "conversation" : "tag:fosstodon.org,2023-01-16:objectId=79979287:objectType=Conversation",
              "id" : "https://fosstodon.org/users/rcode3/statuses/109699061812792939",
              "inReplyTo" : null,
              "inReplyToAtomUri" : null,
              "published" : "2023-01-16T13:14:04Z",
              "replies" : {
                 "first" : {
                    "items" : [],
                    "next" : "https://fosstodon.org/users/rcode3/statuses/109699061812792939/replies?only_other_accounts=true&page=true",
                    "partOf" : "https://fosstodon.org/users/rcode3/statuses/109699061812792939/replies",
                    "type" : "CollectionPage"
                 },
                 "id" : "https://fosstodon.org/users/rcode3/statuses/109699061812792939/replies",
                 "type" : "Collection"
              },
              "sensitive" : false,
              "summary" : null,
              "tag" : [
                 {
                    "href" : "https://dt-dev.snark.fail/pub/p/test-dude230116a",
                    "name" : "@test-dude230116a@dt-dev.snark.fail",
                    "type" : "Mention"
                 }
              ],
              "to" : [
                 "https://dt-dev.snark.fail/pub/p/test-dude230116a"
              ],
              "type" : "Note",
              "url" : "https://fosstodon.org/@rcode3/109699061812792939"
           },
           "published" : "2023-01-16T13:14:04Z",
           "to" : [
              "https://dt-dev.snark.fail/pub/p/test-dude230116a"
           ],
           "type" : "Create"
        }            
        "#;

        // WHEN
        let activity = serde_json::from_str::<Activity>(json);

        // THEN
        activity.unwrap();
    }

    #[test]
    fn GIVEN_mastodon_follow_activity_WHEN_parse_THEN_success() {
        // GIVEN
        let json = r#"
        {
           "@context" : "https://www.w3.org/ns/activitystreams",
           "actor" : "https://fosstodon.org/users/rcode3",
           "id" : "https://fosstodon.org/edcc4f8d-8a37-43ba-8c96-bb194a749bd1",
           "object" : "https://dt-dev.snark.fail/pub/p/test-dude230116a",
           "type" : "Follow"
        }            
        "#;

        // WHEN
        let activity = serde_json::from_str::<Activity>(json);

        // THEN
        activity.unwrap();
    }

    #[test]
    fn GIVEN_mastodon_undo_follow_activity_WHEN_parse_THEN_success() {
        // GIVEN
        let json = r#"
        {
           "@context" : "https://www.w3.org/ns/activitystreams",
           "actor" : "https://fosstodon.org/users/rcode3",
           "id" : "https://fosstodon.org/users/rcode3#follows/2230857/undo",
           "object" : {
              "actor" : "https://fosstodon.org/users/rcode3",
              "id" : "https://fosstodon.org/8a9b36b0-fbc8-4a06-b69c-410ffd15a153",
              "object" : "https://dt-dev.snark.fail/pub/p/test-dude230116a",
              "type" : "Follow"
           },
           "type" : "Undo"
        }
        "#;

        // WHEN
        let activity = serde_json::from_str::<Activity>(json);

        // THEN
        activity.unwrap();
    }

    #[test]
    fn GIVEN_pleroma_dm_activity_WHEN_parse_THEN_success() {
        // GIVEN
        let json = r#"
        {
           "@context" : [
              "https://www.w3.org/ns/activitystreams",
              "https://gleasonator.com/schemas/litepub-0.1.jsonld",
              {
                 "@language" : "und"
              }
           ],
           "actor" : "https://gleasonator.com/users/rcode3",
           "cc" : [
              "https://gleasonator.com/users/rcode3/followers"
           ],
           "context" : "https://gleasonator.com/contexts/136f7d26-c1f2-4b8a-9b79-5936d0cf4467",
           "directMessage" : false,
           "id" : "https://gleasonator.com/activities/ef25175e-e35d-4156-9c09-e04a4f9ef46f",
           "object" : {
              "actor" : "https://gleasonator.com/users/rcode3",
              "attachment" : [],
              "attributedTo" : "https://gleasonator.com/users/rcode3",
              "cc" : [
                 "https://gleasonator.com/users/rcode3/followers"
              ],
              "content" : "<span class=\"h-card\"><a class=\"u-url mention\" data-user=\"ARhbpe6S2cXuUjqlM0\" href=\"https://dt-dev.snark.fail/pub/p/test-dude230116a\" rel=\"ugc\">@<span>test-dude230116a</span></a></span> this is a test message",
              "contentMap" : {
                 "en" : "<span class=\"h-card\"><a class=\"u-url mention\" data-user=\"ARhbpe6S2cXuUjqlM0\" href=\"https://dt-dev.snark.fail/pub/p/test-dude230116a\" rel=\"ugc\">@<span>test-dude230116a</span></a></span> this is a test message"
              },
              "context" : "https://gleasonator.com/contexts/136f7d26-c1f2-4b8a-9b79-5936d0cf4467",
              "conversation" : "https://gleasonator.com/contexts/136f7d26-c1f2-4b8a-9b79-5936d0cf4467",
              "id" : "https://gleasonator.com/objects/029f4ba3-110e-4e0d-ac4e-1e1a9f796dfe",
              "published" : "2023-01-16T13:46:01.025366Z",
              "sensitive" : false,
              "source" : {
                 "content" : "@test-dude230116a@dt-dev.snark.fail this is a test message",
                 "mediaType" : "text/plain"
              },
              "summary" : "",
              "tag" : [
                 {
                    "href" : "https://dt-dev.snark.fail/pub/p/test-dude230116a",
                    "name" : "@test-dude230116a@dt-dev.snark.fail",
                    "type" : "Mention"
                 }
              ],
              "to" : [
                 "https://www.w3.org/ns/activitystreams#Public",
                 "https://dt-dev.snark.fail/pub/p/test-dude230116a"
              ],
              "type" : "Note"
           },
           "published" : "2023-01-16T13:46:01.025257Z",
           "to" : [
              "https://www.w3.org/ns/activitystreams#Public",
              "https://dt-dev.snark.fail/pub/p/test-dude230116a"
           ],
           "type" : "Create"
        }
        "#;

        // WHEN
        let activity = serde_json::from_str::<Activity>(json);

        // THEN
        activity.unwrap();
    }

    #[test]
    fn GIVEN_pleroma_delete_follow_activity_WHEN_parse_THEN_success() {
        // GIVEN
        let json = r#"
        {
           "@context" : [
              "https://www.w3.org/ns/activitystreams",
              "https://gleasonator.com/schemas/litepub-0.1.jsonld",
              {
                 "@language" : "und"
              }
           ],
           "actor" : "https://gleasonator.com/users/rcode3",
           "bcc" : [],
           "bto" : [],
           "cc" : [],
           "id" : "https://gleasonator.com/activities/5b1c3862-77dd-4eca-95e8-7c996547c1b2",
           "object" : "https://gleasonator.com/objects/029f4ba3-110e-4e0d-ac4e-1e1a9f796dfe",
           "to" : [
              "https://dt-dev.snark.fail/pub/p/test-dude230116a",
              "https://gleasonator.com/users/rcode3/followers",
              "https://www.w3.org/ns/activitystreams#Public"
           ],
           "type" : "Delete"
        }
        "#;

        // WHEN
        let activity = serde_json::from_str::<Activity>(json);

        // THEN
        activity.unwrap();
    }

    #[test]
    fn GIVEN_pleroma_follow_activity_WHEN_parse_THEN_success() {
        // GIVEN
        let json = r#"
        {
           "@context" : [
              "https://www.w3.org/ns/activitystreams",
              "https://gleasonator.com/schemas/litepub-0.1.jsonld",
              {
                 "@language" : "und"
              }
           ],
           "actor" : "https://gleasonator.com/users/rcode3",
           "bcc" : [],
           "bto" : [],
           "cc" : [],
           "id" : "https://gleasonator.com/activities/7f95d49f-de23-4a9a-8eaa-a6527fd910f2",
           "object" : "https://dt-dev.snark.fail/pub/p/test-dude230116a",
           "state" : "pending",
           "to" : [
              "https://dt-dev.snark.fail/pub/p/test-dude230116a"
           ],
           "type" : "Follow"
        }
        "#;

        // WHEN
        let activity = serde_json::from_str::<Activity>(json);

        // THEN
        activity.unwrap();
    }
}
