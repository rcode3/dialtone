use serde::{Deserialize, Serialize};
use serde_json::Value;

use crate::utils::media_types::ACTIVITY_STREAMS_PROFILE_PARAMETER;

use super::{actor::Actor, ap_object::ApObject, image_attr::ImageAttributes};

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Copy, Clone)]
pub enum CollectionType {
    Collection,
    OrderedCollection,
    CollectionPage,
    OrderedCollectionPage,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct Collection {
    #[serde(rename = "@context")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ap_context: Option<Value>,

    pub id: String,

    pub name: String,

    #[serde(rename = "type")]
    pub collection_type: CollectionType,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub summary: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon: Option<ImageAttributes>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub image: Option<ImageAttributes>,

    pub items: Vec<CollectionItem>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub next: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub prev: Option<String>,

    #[serde(rename = "totalItems")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub total_items: Option<u64>,
}

#[buildstructor::buildstructor]
impl Collection {
    #[builder]
    pub fn new(
        _ap_context: Option<Value>,
        id: String,
        name: String,
        collection_type: CollectionType,
        summary: Option<String>,
        icon: Option<ImageAttributes>,
        image: Option<ImageAttributes>,
        items: Vec<CollectionItem>,
        next: Option<String>,
        prev: Option<String>,
        total_items: Option<u64>,
    ) -> Self {
        Self {
            ap_context: Some(Value::String(ACTIVITY_STREAMS_PROFILE_PARAMETER.to_owned())),
            id,
            name,
            collection_type,
            summary,
            icon,
            image,
            items,
            next,
            prev,
            total_items,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
#[serde(untagged)]
pub enum CollectionItem {
    Actor(Box<Actor>),
    ApObject(Box<ApObject>),
    Collection(Box<Collection>),
}
