use crate::rest::{sites::theme::Theme, users::web_user::SystemRoleType};
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;
use strum_macros::Display;
use thiserror::Error;

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct SiteInfo {
    pub host_name: String,
    pub site_data: SiteData,
    pub created_at: DateTime<Utc>,
    pub modified_at: DateTime<Utc>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Default)]
pub struct SiteData {
    pub public: PublicSiteInfo,
    pub system_roles_sets: HashMap<String, Vec<SystemRoleType>>,
    pub open_registration: OpenRegistration,
    pub simple_code_registration: SimpleCodeRegistration,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Default)]
pub struct PublicSiteInfo {
    pub names: SiteNames,
    pub client_defaults: ClientDefaults,
    pub registration_methods: Vec<RegistrationMethod>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Hash, Display)]
pub enum RegistrationMethod {
    Open,
    SimpleCode,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Default)]
pub struct SimpleCodeRegistration {
    pub codes: HashMap<String, SimpleCodeRegistrationData>,
}

impl SimpleCodeRegistration {
    pub fn is_auto_approved(&self, code: &str) -> bool {
        match self.codes.get(code) {
            Some(data) => data.auto_approve,
            None => false,
        }
    }

    pub fn set_auto_approve(&mut self, code: &str, approve: bool) -> Result<(), RegistrationError> {
        match self.codes.get_mut(code) {
            Some(data) => {
                data.auto_approve = approve;
                Ok(())
            }
            None => Err(RegistrationError::NoSuchSimpleCode),
        }
    }

    pub fn system_roles_set_name(&self, code: &str) -> Result<&str, RegistrationError> {
        match self.codes.get(code) {
            Some(data) => Ok(&data.system_roles_set_name),
            None => Err(RegistrationError::NoSuchSimpleCode),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Default)]
pub struct SimpleCodeRegistrationData {
    pub system_roles_set_name: String,
    pub auto_approve: bool,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Default)]
pub struct OpenRegistration {
    pub system_roles_set_name: Option<String>,
    pub auto_approve: bool,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Default)]
pub struct SiteNames {
    pub short_name: String,
    pub long_name: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Default)]
pub struct ClientDefaults {
    pub dialtone_webclient: DialtoneWebClient,
    #[serde(flatten)]
    pub other_clients: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct DialtoneWebClient {
    pub default_theme: Theme,
}

impl Default for DialtoneWebClient {
    fn default() -> Self {
        Self {
            default_theme: Theme::GreenOnBlack,
        }
    }
}

#[derive(Debug, Error)]
pub enum RegistrationError {
    #[error("Invalid System Roles Set Name.")]
    InvalidSystemRolesSetName,
    #[error("No System Role Set Name.")]
    NoSystemRolesSetName,
    #[error("No Such Simple Code.")]
    NoSuchSimpleCode,
}

impl SiteData {
    pub fn is_open_registration_auto_approved(&self) -> bool {
        self.open_registration.auto_approve
    }

    pub fn is_simple_code_registration_auto_approved(&self, code: &str) -> bool {
        self.simple_code_registration.is_auto_approved(code)
    }

    pub fn set_open_registration_auto_approved(
        &mut self,
        approve: bool,
    ) -> Result<(), RegistrationError> {
        if self.open_registration.system_roles_set_name.is_none() {
            return Err(RegistrationError::InvalidSystemRolesSetName);
        } else {
            self.open_registration.auto_approve = approve;
        };
        Ok(())
    }

    pub fn set_simple_code_registration_auto_approved(
        &mut self,
        code: &str,
        approve: bool,
    ) -> Result<(), RegistrationError> {
        self.simple_code_registration
            .set_auto_approve(code, approve)
    }

    pub fn open_registration_system_roles_set(
        &self,
    ) -> Result<&[SystemRoleType], RegistrationError> {
        if let Some(ref set_name) = self.open_registration.system_roles_set_name {
            self.system_roles_sets
                .get(set_name)
                .map(|v| v.as_slice())
                .ok_or(RegistrationError::InvalidSystemRolesSetName)
        } else {
            Err(RegistrationError::NoSystemRolesSetName)
        }
    }

    pub fn simple_code_registration_system_roles_set(
        &self,
        code: &str,
    ) -> Result<&[SystemRoleType], RegistrationError> {
        let set_name = self.simple_code_registration.system_roles_set_name(code)?;
        self.system_roles_sets
            .get(set_name)
            .map(|v| v.as_slice())
            .ok_or(RegistrationError::NoSystemRolesSetName)
    }

    pub fn is_registration_method_available(&self, method: &RegistrationMethod) -> bool {
        self.public.registration_methods.contains(method)
    }

    pub fn is_open_registration_setup(&self) -> bool {
        self.open_registration.system_roles_set_name.is_some()
    }

    pub fn make_available_registration_methods(&mut self) {
        let mut methods = vec![];
        if self.open_registration.system_roles_set_name.is_some() {
            methods.push(RegistrationMethod::Open);
        }
        if !self.simple_code_registration.codes.is_empty() {
            methods.push(RegistrationMethod::SimpleCode);
        }
        self.public.registration_methods = methods;
    }

    pub fn set_open_registration(
        &mut self,
        system_roles_set_name: &str,
        auto_approve: bool,
    ) -> Result<(), RegistrationError> {
        let set = self.system_roles_sets.get(system_roles_set_name);
        if set.is_some() {
            self.open_registration.system_roles_set_name = Some(system_roles_set_name.to_string());
            self.open_registration.auto_approve = auto_approve;
            self.make_available_registration_methods();
            Ok(())
        } else {
            Err(RegistrationError::InvalidSystemRolesSetName)
        }
    }

    pub fn remove_open_registration(&mut self) {
        self.open_registration.system_roles_set_name = None;
        self.make_available_registration_methods();
    }

    pub fn set_simple_code_registration_code(
        &mut self,
        system_roles_set_name: &str,
        code: &str,
        auto_approve: bool,
    ) -> Result<(), RegistrationError> {
        let set = self.system_roles_sets.get(system_roles_set_name);
        if set.is_some() {
            let data = SimpleCodeRegistrationData {
                system_roles_set_name: system_roles_set_name.to_string(),
                auto_approve,
            };
            self.simple_code_registration
                .codes
                .insert(code.to_string(), data);
            self.make_available_registration_methods();
            Ok(())
        } else {
            Err(RegistrationError::InvalidSystemRolesSetName)
        }
    }

    pub fn remove_simple_code_registration_code(&mut self, code: &str) {
        self.simple_code_registration.codes.remove(code);
        self.make_available_registration_methods();
    }

    pub fn remove_all_simple_code_registration_codes(&mut self) {
        self.simple_code_registration.codes.clear();
        self.make_available_registration_methods();
    }
}

#[cfg(test)]
mod site_data_tests {
    use serde_json::Error;

    use super::*;

    #[test]
    #[allow(non_snake_case)]
    fn GIVEN_json_for_client_defaults_WHEN_parsed_THEN_default_theme_is_correct() {
        let input = r#"
            {
                "dialtone_webclient": 
                {
                    "default_theme": "green on black"
                }
            }
        "#;

        let json: Result<ClientDefaults, Error> = serde_json::from_str(input);

        let output = json.expect("parsing client defaults");
        assert_eq!(output.dialtone_webclient.default_theme, Theme::GreenOnBlack);
    }

    #[test]
    #[allow(non_snake_case)]
    fn GIVEN_json_for_client_defaults_with_unknown_client_WHEN_parsed_THEN_unknown_default_is_parsed(
    ) {
        let input = r#"
            {
                "dialtone_webclient": 
                {
                    "default_theme": "green on black"
                },
                "foo_client": {
                    "bar": true
                }
            }
        "#;

        let json: Result<ClientDefaults, Error> = serde_json::from_str(input);

        let output = json.expect("parsing client defaults");
        assert_eq!(output.dialtone_webclient.default_theme, Theme::GreenOnBlack);
        output
            .other_clients
            .get("foo_client")
            .expect("foo client not in hashmap");
    }
}
