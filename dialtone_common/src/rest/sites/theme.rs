use serde::{Deserialize, Serialize};
use serde_variant::to_variant_name;
use std::str::FromStr;

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum Theme {
    #[serde(rename = "green on black")]
    GreenOnBlack,

    #[serde(rename = "green on black with amber")]
    GreenOnBlackWithAmber,

    #[serde(rename = "black on light gray")]
    BlackOnLightGray,

    #[serde(rename = "black on white")]
    BlackOnWhite,

    #[serde(rename = "black on white with red")]
    BlackOnWhiteWithRed,

    #[serde(rename = "black on white with green")]
    BlackOnWhiteWithGreen,

    #[serde(rename = "white on blue")]
    WhiteOnBlue,

    #[serde(rename = "white on blue with green")]
    WhiteOnBlueWithGreen,

    #[serde(rename = "amber on black")]
    AmberOnBlack,

    #[serde(rename = "amber on black with blue")]
    AmberOnBlackWithBlue,

    #[serde(rename = "blue on white")]
    BlueOnWhite,

    #[serde(rename = "blue on black")]
    BlueOnBlack,

    #[serde(rename = "tandy 400")]
    Tandy400,

    #[serde(rename = "botho")]
    Botho,
}

impl FromStr for Theme {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.to_lowercase().replace(&['-', '_'][..], " ");
        match s {
            a if a == Theme::GreenOnBlack.to_string() => Ok(Theme::GreenOnBlack),
            a if a == Theme::AmberOnBlack.to_string() => Ok(Theme::AmberOnBlack),
            _ => Err("no matching theme"),
        }
    }
}

impl ToString for Theme {
    fn to_string(&self) -> String {
        to_variant_name(&self).unwrap().to_string()
    }
}

#[cfg(test)]
mod theme_tests {
    use crate::rest::sites::theme::Theme;
    use std::str::FromStr;

    #[test]
    fn test_from_str() {
        assert_eq!(
            Ok(Theme::GreenOnBlack),
            Theme::from_str(Theme::GreenOnBlack.to_string().as_str())
        );
        assert_eq!(Ok(Theme::GreenOnBlack), Theme::from_str("green-on-black"));
        assert_eq!(Ok(Theme::GreenOnBlack), Theme::from_str("green_on_black"));

        assert_eq!(
            Ok(Theme::AmberOnBlack),
            Theme::from_str(Theme::AmberOnBlack.to_string().as_str())
        );
        assert_eq!(Ok(Theme::AmberOnBlack), Theme::from_str("amber-on-black"));
        assert_eq!(Ok(Theme::AmberOnBlack), Theme::from_str("amber_on_black"));
    }

    #[test]
    fn test_to_string() {
        assert_eq!("amber on black", Theme::AmberOnBlack.to_string());
        assert_eq!("green on black", Theme::GreenOnBlack.to_string());
    }
}
