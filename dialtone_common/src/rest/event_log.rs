use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct LogEvent {
    pub when: DateTime<Utc>,
    pub message: String,
}

impl LogEvent {
    pub fn new(message: String) -> Self {
        LogEvent {
            when: Utc::now(),
            message,
        }
    }
}
