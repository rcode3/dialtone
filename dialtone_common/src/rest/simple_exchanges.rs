use serde::{Deserialize, Serialize};

/// A simple response.
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct SimpleResponse {
    /// True if the request succeeded.
    pub result: bool,

    /// Optional error message
    pub msg: Option<String>,
}

impl SimpleResponse {
    pub fn success() -> SimpleResponse {
        SimpleResponse {
            result: true,
            msg: None,
        }
    }

    pub fn failure() -> SimpleResponse {
        SimpleResponse {
            result: false,
            msg: None,
        }
    }

    pub fn failure_with_msg(msg: &str) -> SimpleResponse {
        SimpleResponse {
            result: false,
            msg: Some(msg.to_string()),
        }
    }
}
