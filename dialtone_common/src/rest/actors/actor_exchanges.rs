use crate::{
    ap::{actor::ActorType, id::is_generic_actor_id_valid},
    webfinger::is_valid_wf_acct,
};
use serde::{Deserialize, Serialize};
use thiserror::Error;

use super::actor_model::UpdateActorAp;

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct NewActorRequest {
    pub preferred_user_name: String,
    pub actor_type: ActorType,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub summary: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct PutUpdateActorRequest {
    pub actor_id: String,
    pub update: UpdateActorAp,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct PutActorRequest {
    pub actor_id: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct GetActorRequest {
    pub actor_id_or_wf_acct: String,
}

pub enum GetActorType {
    ActorId(String),
    WfAcct(String),
}

impl GetActorRequest {
    pub fn get_type(self) -> Result<GetActorType, GetActorTypeError> {
        if let Some(wf_acct) = is_valid_wf_acct(&self.actor_id_or_wf_acct) {
            Ok(GetActorType::WfAcct(wf_acct))
        } else if is_generic_actor_id_valid(&self.actor_id_or_wf_acct) {
            Ok(GetActorType::ActorId(self.actor_id_or_wf_acct))
        } else {
            Err(GetActorTypeError::UnableToDetermineActorType)
        }
    }

    pub fn new(actor_id_or_wf_acct: String) -> Self {
        GetActorRequest {
            actor_id_or_wf_acct,
        }
    }
}

#[derive(Debug, Error)]
pub enum GetActorTypeError {
    #[error("Invalid Actor ID.")]
    InvalidActorId,
    #[error("Invalid Webfinger Account")]
    InvalidWebfingerAccount,
    #[error("Unable to determine actor type.")]
    UnableToDetermineActorType,
}
