use crate::ap::actor::Actor;
use crate::webfinger::Jrd;
use crate::{ap::image_attr::ImageAttributes, rest::event_log::LogEvent};
use buildstructor::Builder;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

/// This struct consists of the "public" information about
/// an actor, which is the Activity Pub Actor data and
/// the JRD data.
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Builder)]
pub struct PublicActor {
    pub ap: Actor,
    pub jrd: Option<Jrd>,
}

/// An owned actors is one typically owned by a users
/// or many users. These actors have other non-activity-pub
/// data associated with them, primarily web-finger JRD data.
/// However, some actors are seen by the system through activity pub
/// or webfinger and therefore are orphaned actors.
/// For this system, the "owned" aspect means the owner gets
/// to make changes to the actors data, whether it is public data
/// or non-public data (i.e. owner_data). For orphaned actors only
/// the system gets to make those changes.
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Builder)]
pub struct OwnedActor {
    pub ap: Actor,
    pub jrd: Option<Jrd>,
    pub collection_count: u16,
    pub owner_count: u16,
    pub owner_data: Option<OwnerActorData>,
    pub event_log: Vec<LogEvent>,
    pub created_at: DateTime<Utc>,
    pub modified_at: DateTime<Utc>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Builder)]
pub struct OwnerActorData {
    pub key_pairs: Vec<KeyPair>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Builder)]
pub struct KeyPair {
    pub key_id: String,
    pub key_type: KeyType,
    pub private_key_pem: String,
    pub public_key_pem: String,
    pub created_at: DateTime<Utc>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub enum KeyType {
    Rsa2048,
}

/// For updating actors activity pub data.
/// This includes only the parts of AP data that may
/// be updated, such as 'names' and 'summary'. Some parts
/// of an AP actors are immutable (or server controlled)
/// and therefore cannot be updated.
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Builder)]
pub struct UpdateActorAp {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub summary: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon: Option<ImageAttributes>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub image: Option<ImageAttributes>,
}

/// This structure contains information that is more
/// of the "administrative" type about an Actor and only
/// intended to be modified by system operators or
/// moderators.
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Builder)]
pub struct ActorSystemInfo {
    pub actor_id: String,
    pub visibility: ActorVisibility,
    pub system_data: Option<SystemActorData>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct SystemActorData {}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub enum ActorVisibility {
    Visible,
    Invisible,
    Banned,
}
