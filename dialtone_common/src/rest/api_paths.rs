use crate::api_dt;

#[rustfmt::skip]
api_dt!(
    ACTOR,                            "/actor",
    ACTOR__OWNED,                     "/actor/owned",
    ACTOR__SYSINFO,                   "/actor/sysinfo",
    ACTOR__DEFAULT,                   "/actor/default",
    AP_OBJECT,                        "/ap_object",
    AP_OBJECT__OWNED,                 "/ap_object/owned",
    AP_OBJECT__SYSINFO,               "/ap_object/sysinfo",
    AP_OBJECT__SYSDATA,               "/ap_object/sysdata",
    AP_OBJECT__VISIBILITY,            "/ap_object/visibility",
    AUTHENTICATE,                     "/authenticate",
    NAME,                             "/name",
    PAGE,                             "/page",
    SITES,                            "/sites/:host_name",
    USER,                             "/user",
    USER__PASSWORD,                   "/user/password",
    USER__STATUS,                     "/user/status",
    USER__PREFS,                      "/user/prefs",
    USER__SYSINFO,                    "/user/sysinfo",
    USER__SYSDATA,                    "/user/sysdata"
);
