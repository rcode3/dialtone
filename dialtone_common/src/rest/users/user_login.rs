use std::fmt::Display;

use serde::{Deserialize, Serialize};

use crate::utils::make_acct::make_acct;

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct UserCredential {
    pub user_acct: String,
    pub password: String,
}

#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
pub struct NameHostPair {
    pub user_name: String,
    pub host_name: String,
}

impl Display for NameHostPair {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let acct = make_acct(&self.user_name, &self.host_name);
        f.write_str(&acct)
    }
}

#[derive(Debug, Deserialize, Serialize, PartialEq, Clone)]
pub struct AuthBody {
    pub access_token: String,
    pub token_type: String,
}

impl AuthBody {
    pub fn new(access_token: String) -> Self {
        Self {
            access_token,
            token_type: "Bearer".to_string(),
        }
    }
}

#[cfg(test)]
mod user_login_tests {
    #[test]
    fn roundtrip_serde() {
        let u = super::UserCredential {
            user_acct: "bob".to_string(),
            password: "secretpassword".to_string(),
        };
        let j = serde_json::to_string(&u);
        let u2 = serde_json::from_str(&j.unwrap()).unwrap();
        assert_eq!(u, u2)
    }
}
