use crate::rest::actors::actor_model::PublicActor;
use crate::rest::event_log::LogEvent;
use crate::rest::sites::theme::Theme;
use crate::rest::users::web_user::UserStatus::PendingApproval;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use strum_macros::{Display, EnumString, EnumVariantNames};

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
/// Represents a web users. Does not include authentication data.
pub struct WebUser {
    pub user_authz: UserAuthorization,
    pub preferences: Option<UserPrefs>,
    pub last_seen_data: Vec<LastSeenData>,
    pub last_login_data: Vec<LastLoginData>,
    pub actor_counts: ActorCounts,
    pub default_actor_data: Option<PublicActor>,
    pub status: UserStatus,
    pub event_log: Vec<LogEvent>,
    pub created_at: DateTime<Utc>,
    pub modified_at: DateTime<Utc>,
}

impl WebUser {
    pub fn is_authz(&self, host_name: &str, role: &SystemRoleType) -> bool {
        self.user_authz
            .system_permissions
            .iter()
            .any(|permission| permission.host_name == host_name && &permission.role == role)
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct UserAuthorization {
    /// An account is the unique identifier for a users
    /// principal. It is in the form "users@host" such as
    /// "bob@example.com"
    pub acct: String,
    pub system_permissions: Vec<SystemPermission>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct ActorCounts {
    #[serde(rename = "Person")]
    pub person: u16,
    #[serde(rename = "Organization")]
    pub organization: u16,
    #[serde(rename = "Group")]
    pub group: u16,
    #[serde(rename = "Service")]
    pub service: u16,
    #[serde(rename = "Application")]
    pub application: u16,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct UserPrefs {
    pub theme: Theme,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct LastSeenData {
    pub from: String,
    pub at: DateTime<Utc>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct LastLoginData {
    pub from: String,
    pub at: DateTime<Utc>,
    pub success: bool,
}

#[derive(
    Serialize, Deserialize, Debug, PartialEq, Eq, Clone, EnumString, Display, EnumVariantNames,
)]
#[strum(serialize_all = "title_case")]
pub enum UserStatus {
    Active,
    Suspended,

    #[serde(rename = "Pending Approval")]
    PendingApproval,
}

impl Default for UserStatus {
    fn default() -> Self {
        PendingApproval
    }
}

/// Permissions a user can possess.
/// Administrators can change things of other users and do things
/// regular users cannot do.
#[derive(
    Serialize, Deserialize, Debug, PartialEq, Eq, Clone, EnumString, Display, EnumVariantNames,
)]
#[strum(serialize_all = "title_case")]
pub enum SystemRoleType {
    #[serde(rename = "User Administrator")]
    #[strum(serialize = "User Admin")]
    #[strum(serialize = "User Administrator")]
    UserAdmin,
    #[serde(rename = "Actor Administrator")]
    #[strum(serialize = "Actor Admin")]
    #[strum(serialize = "Actor Administrator")]
    ActorAdmin,
    #[serde(rename = "Content Administrator")]
    #[strum(serialize = "Content Admin")]
    #[strum(serialize = "Content Administrator")]
    ContentAdmin,

    #[serde(rename = "Person Owner")]
    PersonOwner,
    #[serde(rename = "Organization Owner")]
    OrganizationOwner,
    #[serde(rename = "Group Owner")]
    GroupOwner,
    #[serde(rename = "Application Owner")]
    ApplicationOwner,
    #[serde(rename = "Service Owner")]
    ServiceOwner,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct SystemPermission {
    pub role: SystemRoleType,
    pub host_name: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct UserSystemInfo {
    pub system_data: Option<UserSystemData>,
    pub created_at: DateTime<Utc>,
    pub modified_at: DateTime<Utc>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct UserSystemData {}

#[cfg(test)]
mod web_user_tests {
    use super::*;

    #[test]
    fn roundtrip_serde() {
        let w = super::UserAuthorization {
            acct: "bob@test.example".to_string(),
            system_permissions: vec![SystemPermission {
                role: SystemRoleType::ActorAdmin,
                host_name: "example.com".to_string(),
            }],
        };
        let j = serde_json::to_string(&w);
        let w2 = serde_json::from_str(&j.unwrap()).unwrap();
        assert_eq!(w, w2)
    }
}
