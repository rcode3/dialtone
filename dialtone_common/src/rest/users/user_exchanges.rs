use crate::rest::users::web_user::{UserPrefs, UserStatus, UserSystemData};
use serde::{Deserialize, Serialize};

use super::user_login::UserCredential;

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct GetUser {
    pub acct: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct PutUserPassword {
    pub acct: String,
    pub password: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct PutUserPreferences {
    pub acct: String,
    pub preferences: UserPrefs,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct PutUserStatus {
    pub acct: String,
    pub status: UserStatus,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct PutUserSysData {
    pub acct: String,
    pub sysinfo: UserSystemData,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum PostUser {
    OpenRegistration(UserCredential),
    SimpleCode {
        user_creds: UserCredential,
        simple_code_for_registration: String,
    },
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub enum PostUserResponse {
    AccountRegistered(Option<String>),
    AccountNotRegistered(Option<String>),
    AccountNeedsApproval(Option<String>),
}
