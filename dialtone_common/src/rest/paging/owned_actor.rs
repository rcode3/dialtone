use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use crate::{
    ap::actor::ActorType,
    rest::actors::actor_model::{ActorVisibility, OwnedActor},
};

use super::{Page, PagingProps, DEFAULT_PAGE_LIMIT};

impl From<Vec<OwnedActor>> for Page<OwnedActor> {
    fn from(actors: Vec<OwnedActor>) -> Self {
        Page {
            next: actors.last().map(|a| a.modified_at),
            prev: actors.first().map(|a| a.modified_at),
            items: actors,
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct OwnedActorsByHost {
    pub prev_date: Option<DateTime<Utc>>,
    pub next_date: Option<DateTime<Utc>>,
    pub limit: u16,
    pub visibility: Option<ActorVisibility>,
    pub actor_type: Option<ActorType>,
    pub host_name: String,
}

impl OwnedActorsByHost {
    pub fn new(host_name: String) -> Self {
        OwnedActorsByHost {
            prev_date: None,
            next_date: None,
            limit: DEFAULT_PAGE_LIMIT,
            visibility: None,
            actor_type: None,
            host_name,
        }
    }
}

impl PagingProps<Page<OwnedActor>> for OwnedActorsByHost {
    fn next_props(props: Self, page: &Page<OwnedActor>) -> Self {
        OwnedActorsByHost {
            prev_date: page.prev,
            next_date: None,
            ..props
        }
    }

    fn prev_props(props: Self, page: &Page<OwnedActor>) -> Self {
        OwnedActorsByHost {
            prev_date: None,
            next_date: page.next,
            ..props
        }
    }

    const PAGE_PATH: &'static str = "all_owned_actors";
    const AUTHN_REQUIRED: bool = true;
}

#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub struct OwnedActorsByOwner {
    pub prev_date: Option<DateTime<Utc>>,
    pub next_date: Option<DateTime<Utc>>,
    pub limit: u16,
    pub user_acct_owner: String,
    pub actor_type: Option<ActorType>,
}

impl OwnedActorsByOwner {
    pub fn new(user_acct_owner: String) -> Self {
        OwnedActorsByOwner {
            prev_date: None,
            next_date: None,
            limit: DEFAULT_PAGE_LIMIT,
            user_acct_owner,
            actor_type: None,
        }
    }
}

impl PagingProps<Page<OwnedActor>> for OwnedActorsByOwner {
    fn prev_props(props: Self, page: &Page<OwnedActor>) -> Self {
        OwnedActorsByOwner {
            prev_date: page.prev,
            next_date: None,
            ..props
        }
    }

    fn next_props(props: Self, page: &Page<OwnedActor>) -> Self {
        OwnedActorsByOwner {
            prev_date: None,
            next_date: page.next,
            ..props
        }
    }

    const PAGE_PATH: &'static str = "owned_actors_by_owner";
    const AUTHN_REQUIRED: bool = true;
}
