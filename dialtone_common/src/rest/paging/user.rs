use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use crate::rest::users::web_user::WebUser;

use super::{Page, PagingProps, DEFAULT_PAGE_LIMIT};

impl From<Vec<WebUser>> for Page<WebUser> {
    fn from(actors: Vec<WebUser>) -> Self {
        Page {
            next: actors.last().map(|a| a.modified_at),
            prev: actors.first().map(|a| a.modified_at),
            items: actors,
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub struct UsersByHost {
    pub prev_date: Option<DateTime<Utc>>,
    pub next_date: Option<DateTime<Utc>>,
    pub limit: u16,
    pub host_name: String,
}

impl UsersByHost {
    pub fn new(host_name: String) -> Self {
        UsersByHost {
            prev_date: None,
            next_date: None,
            limit: DEFAULT_PAGE_LIMIT,
            host_name,
        }
    }
}

impl PagingProps<Page<WebUser>> for UsersByHost {
    fn prev_props(props: Self, page: &Page<WebUser>) -> Self {
        UsersByHost {
            prev_date: page.prev,
            next_date: None,
            ..props
        }
    }

    fn next_props(props: Self, page: &Page<WebUser>) -> Self {
        UsersByHost {
            prev_date: None,
            next_date: page.next,
            ..props
        }
    }

    const PAGE_PATH: &'static str = "users_by_host";
    const AUTHN_REQUIRED: bool = true;
}
