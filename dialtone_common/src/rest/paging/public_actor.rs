use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use crate::{ap::actor::ActorType, rest::actors::actor_model::PublicActor};

use super::{Page, PagingProps, DEFAULT_PAGE_LIMIT};

#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub struct PublicActorsByHost {
    pub prev_date: Option<DateTime<Utc>>,
    pub next_date: Option<DateTime<Utc>>,
    pub limit: u16,
    pub actor_type: Option<ActorType>,
    pub host_name: String,
}

impl PublicActorsByHost {
    pub fn new(host_name: String) -> Self {
        PublicActorsByHost {
            prev_date: None,
            next_date: None,
            limit: DEFAULT_PAGE_LIMIT,
            actor_type: None,
            host_name,
        }
    }
}

impl PagingProps<Page<PublicActor>> for PublicActorsByHost {
    fn prev_props(props: Self, page: &Page<PublicActor>) -> Self {
        PublicActorsByHost {
            prev_date: page.prev,
            next_date: None,
            ..props
        }
    }

    fn next_props(props: Self, page: &Page<PublicActor>) -> Self {
        PublicActorsByHost {
            prev_date: None,
            next_date: page.next,
            ..props
        }
    }

    const PAGE_PATH: &'static str = "public_actors_by_host";
    const AUTHN_REQUIRED: bool = false;
}
