use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use super::api_paths::{path::PAGE, API_DT};

pub mod owned_actor;
pub mod owned_ap_object;
pub mod public_actor;
pub mod public_ap_object;
pub mod user;

#[derive(Serialize, Deserialize)]
pub struct Page<T> {
    pub next: Option<DateTime<Utc>>,
    pub prev: Option<DateTime<Utc>>,
    pub items: Vec<T>,
}

impl<T> Default for Page<T> {
    fn default() -> Self {
        Self {
            next: None,
            prev: None,
            items: Vec::new(),
        }
    }
}

pub trait PagingProps<T> {
    const PAGE_PATH: &'static str;
    const AUTHN_REQUIRED: bool;
    fn next_props(props: Self, page: &T) -> Self;
    fn prev_props(props: Self, page: &T) -> Self;
    fn api_dt_path() -> String {
        format!("{}/{}", PAGE, Self::PAGE_PATH)
    }
    fn full_path() -> String {
        format!("{}{}/{}", API_DT, PAGE, Self::PAGE_PATH)
    }
}

pub const DEFAULT_PAGE_LIMIT: u16 = 20;
