use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use crate::rest::ap_objects::ap_object_model::{ApObjectVisibilityType, OwnedApObject};

use super::{Page, PagingProps, DEFAULT_PAGE_LIMIT};

impl From<Vec<OwnedApObject>> for Page<OwnedApObject> {
    fn from(objects: Vec<OwnedApObject>) -> Self {
        Page {
            next: objects.last().map(|o| o.modified_at),
            prev: objects.first().map(|o| o.modified_at),
            items: objects,
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub struct OwnedApObjectsByActor {
    pub prev_date: Option<DateTime<Utc>>,
    pub next_date: Option<DateTime<Utc>>,
    pub limit: u16,
    pub actor_id: String,
    pub visibility: Option<ApObjectVisibilityType>,
}

impl OwnedApObjectsByActor {
    pub fn new(actor_id: String) -> Self {
        OwnedApObjectsByActor {
            prev_date: None,
            next_date: None,
            limit: DEFAULT_PAGE_LIMIT,
            actor_id,
            visibility: None,
        }
    }
}

impl PagingProps<Page<OwnedApObject>> for OwnedApObjectsByActor {
    fn next_props(props: Self, page: &Page<OwnedApObject>) -> Self {
        OwnedApObjectsByActor {
            prev_date: None,
            next_date: page.next,
            ..props
        }
    }

    fn prev_props(props: Self, page: &Page<OwnedApObject>) -> Self {
        OwnedApObjectsByActor {
            prev_date: page.prev,
            next_date: None,
            ..props
        }
    }

    const PAGE_PATH: &'static str = "owned_ap_objects_by_actor";
    const AUTHN_REQUIRED: bool = true;
}
