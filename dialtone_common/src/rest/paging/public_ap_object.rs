use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use crate::ap::ap_object::ApObject;

use super::{Page, PagingProps, DEFAULT_PAGE_LIMIT};

#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub struct PublicApObjectsByActor {
    pub prev_date: Option<DateTime<Utc>>,
    pub next_date: Option<DateTime<Utc>>,
    pub limit: u16,
    pub actor_id: String,
}

impl PublicApObjectsByActor {
    pub fn new(actor_id: String) -> Self {
        PublicApObjectsByActor {
            prev_date: None,
            next_date: None,
            limit: DEFAULT_PAGE_LIMIT,
            actor_id,
        }
    }
}

impl PagingProps<Page<ApObject>> for PublicApObjectsByActor {
    fn next_props(props: Self, page: &Page<ApObject>) -> Self {
        PublicApObjectsByActor {
            prev_date: None,
            next_date: page.next,
            ..props
        }
    }

    fn prev_props(props: Self, page: &Page<ApObject>) -> Self {
        PublicApObjectsByActor {
            prev_date: page.prev,
            next_date: None,
            ..props
        }
    }

    const PAGE_PATH: &'static str = "public_ap_object_by_actor";
    const AUTHN_REQUIRED: bool = false;
}

#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub struct PublicApObjectsByHost {
    pub prev_date: Option<DateTime<Utc>>,
    pub next_date: Option<DateTime<Utc>>,
    pub limit: u16,
    pub host_name: String,
}

impl PublicApObjectsByHost {
    pub fn new(host_name: String) -> Self {
        PublicApObjectsByHost {
            prev_date: None,
            next_date: None,
            limit: DEFAULT_PAGE_LIMIT,
            host_name,
        }
    }
}

impl PagingProps<Page<ApObject>> for PublicApObjectsByHost {
    fn next_props(props: Self, page: &Page<ApObject>) -> Self {
        PublicApObjectsByHost {
            prev_date: None,
            next_date: page.next,
            ..props
        }
    }

    fn prev_props(props: Self, page: &Page<ApObject>) -> Self {
        PublicApObjectsByHost {
            prev_date: page.prev,
            next_date: None,
            ..props
        }
    }

    const PAGE_PATH: &'static str = "public_ap_object_by_host";
    const AUTHN_REQUIRED: bool = false;
}
