use serde::{Deserialize, Serialize};

use super::ap_object_model::{
    ApObjectSystemData, ApObjectVisibilityType, CreateOwnedApObject, UpdateOwnedApObject,
};

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct PostApObject {
    pub actor_id: String,
    pub create_ap_object: CreateOwnedApObject,
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct PutApObject {
    pub ap_object_id: String,
    pub update_ap_object: UpdateOwnedApObject,
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct GetApObject {
    pub ap_object_id: String,
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct ApObjectIdResponse {
    pub ap_object_id: String,
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct PutApObjectSysData {
    pub ap_object_id: String,
    pub system_data: ApObjectSystemData,
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct PutApObjectVisibility {
    pub ap_object_id: String,
    pub visibility: ApObjectVisibilityType,
}
