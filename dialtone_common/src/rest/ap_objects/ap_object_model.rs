use crate::{
    ap::ap_object::{ApObject, ApObjectAddresses, ApObjectMediaType, ApObjectType},
    containers::{file::FileData, http::HttpGetData},
    rest::event_log::LogEvent,
};
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct OwnedApObject {
    pub ap: ApObject,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub owner_data: Option<ApObjectOwnerData>,

    pub event_log: Vec<LogEvent>,

    pub created_at: DateTime<Utc>,

    pub modified_at: DateTime<Utc>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct ApObjectOwnerData {}

/// This struct is used to create an ApObject.
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct CreateOwnedApObject {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,

    #[serde(rename = "mediaType")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub media_type: Option<ApObjectMediaType>,

    #[serde(rename = "type")]
    pub ap_type: ApObjectType,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub content: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub summary: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub owner_data: Option<ApObjectOwnerData>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub to: Option<ApObjectAddresses>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub cc: Option<ApObjectAddresses>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub bto: Option<ApObjectAddresses>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub bcc: Option<ApObjectAddresses>,
}

/// This struct is used to update an ApObject.
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct UpdateOwnedApObject {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub content: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub summary: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub owner_data: Option<ApObjectOwnerData>,
}

/// Defines if an AP object can be seen.
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum ApObjectVisibilityType {
    /// Not to be seen by others just yet (aka "draft").
    /// Once moved out of PreVisible, an ap object cannot
    /// move back to PreVisible because other Activity Pub
    /// systems will have seen it and probably cached it.
    PreVisible,

    /// Publicly visible.
    Visible,

    /// Not publicly seen. The difference between PreVisible
    /// and Invisible is that only admins can move an AP object
    /// from Invisible to Visible. Note that other Activity Pub
    /// systems may have seen and cached the AP object that was
    /// visible, so Invisible cannot make it go away
    /// from the Internet, just the site hosting it.
    Invisible,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct ApObjectSystemInfo {
    pub ap_object_id: String,

    pub ap_object_type: ApObjectType,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub actor_owner: Option<String>,

    pub visibility: ApObjectVisibilityType,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub system_data: Option<ApObjectSystemData>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct ApObjectSystemData {
    pub file_data: Option<FileData>,
    pub http_get_data: Option<HttpGetData>,
    pub fetched_for_user: Option<String>,
}
