use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum ApObjectReferenceType {
    Dislike,
    Flag,
    Like,
    Read,
}
