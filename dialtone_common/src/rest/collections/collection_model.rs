use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use crate::{
    ap::{collection::Collection, image_attr::ImageAttributes},
    rest::event_log::LogEvent,
};

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct OwnedCollection {
    pub collection: Collection,
    pub event_log: Vec<LogEvent>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub owner_data: Option<CollectionOwnerData>,
    pub created_at: DateTime<Utc>,
    pub modified_at: DateTime<Utc>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct UpdateOwnedCollectionData {
    pub ap_data: UpdateCollectionAp,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub owner_data: Option<CollectionOwnerData>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct CollectionOwnerData {}

/// Defines if a collection can be seen.
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum CollectionVisibilityType {
    /// Not to be seen by others just yet (aka "draft").
    /// Once moved out of PreVisible, a collection cannot
    /// move back to PreVisible because other Activity Pub
    /// systems will have seen it and probably cached it.
    PreVisible,

    /// Publicly visible.
    Visible,

    /// Not publicly seen. The difference between PreVisible
    /// and Invisible is that only admins can move a collection
    /// from Invisible to Visible. Note that other Activity Pub
    /// systems may have seen and cached a collection that was
    /// visible, so Invisible cannot make the collection go away
    /// from the Internet, just the site hosting it.
    Invisible,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct CollectionSystemInfo {
    pub collection_id: String,
    pub visibility: CollectionVisibilityType,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub system_data: Option<CollectionSystemData>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct CollectionSystemData {}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct UpdateCollectionAp {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub summary: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon: Option<ImageAttributes>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub image: Option<ImageAttributes>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct CollectionPage {
    pub min_created_at: DateTime<Utc>,
    pub max_created_at: DateTime<Utc>,
    pub page_values: Vec<Collection>,
}
