pub use jrd::create_wf_acct;
pub use jrd::display_acct;
pub use jrd::is_valid_wf_acct;
pub use jrd::normalize_acct;
pub use jrd::Jrd;
pub use jrd::JrdLink;

mod jrd;

pub const SELF: &str = "self";

pub const PROFILE_PAGE_URL_ID: &str = "http://webfinger.example/rel/profile-page";
pub const AVATAR_REL_URL_ID: &str = "http://webfinger.net/rel/avatar";
