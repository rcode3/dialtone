use buildstructor::Builder;
use serde::{Deserialize, Serialize};

use crate::utils::media_types::ACTIVITY_PUB_MEDIA_TYPE;

use super::SELF;

/// This is a JSON Resource Descriptor (JRD, which is the JSON form of an XRD)
/// specialized for Webfinger use.
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Builder)]
pub struct Jrd {
    pub subject: String,
    pub links: Vec<JrdLink>,
}

impl Jrd {
    pub fn ap_url(&self) -> Option<String> {
        self.links
            .iter()
            .filter(|link| {
                if let Some(ref media_type) = link.media_type {
                    media_type == ACTIVITY_PUB_MEDIA_TYPE
                } else {
                    false
                }
            })
            .filter(|link| link.href.is_some())
            .map(|link| Some(link.href.as_ref().unwrap().to_owned()))
            .collect()
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Builder)]
pub struct JrdLink {
    #[serde(default = "default_rel")]
    pub rel: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub href: Option<String>,

    #[serde(rename = "type")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub media_type: Option<String>,
}

pub fn create_wf_acct(host_name: &str, preferred_user_name: &str) -> String {
    format!("acct:{preferred_user_name}@{host_name}")
}

/// This function takes in a string slice and attempts to normalize it as a webfinger account id
/// (e.g. "acct:foo@example.com"). So "@foo@example.com" and "foo@example.com" and "acct:foo@example.com"
/// will all end up being "acct:foo@example.com".
pub fn normalize_acct(acct: &str) -> String {
    let acct = acct.trim_start_matches('@');
    if acct.starts_with("acct:") {
        acct.to_string()
    } else {
        format!("acct:{acct}")
    }
}

pub fn is_valid_wf_acct(acct: &str) -> Option<String> {
    let acct = normalize_acct(acct);
    if acct.starts_with("acct:") && acct.contains('@') {
        Some(acct)
    } else {
        None
    }
}

pub fn display_acct(acct: &str) -> &str {
    if let Some(stripped) = acct.strip_prefix("acct:") {
        stripped
    } else {
        acct
    }
}

fn default_rel() -> String {
    SELF.to_string()
}

#[cfg(test)]
#[allow(non_snake_case)]
mod jrd_tests {
    use super::display_acct;
    use super::normalize_acct;
    use super::Jrd;
    use super::SELF;
    use crate::webfinger::create_wf_acct;

    #[test]
    fn GIVEN_acct_prefixed_with_at_sign_WHEN_normalize_THEN_correct_syntax() {
        // GIVEN
        let acct = "@foo@example.com";

        // WHEN
        let acct = normalize_acct(acct);

        // THEN
        assert_eq!(acct, "acct:foo@example.com");
    }

    #[test]
    fn GIVEN_acct_without_acct_prefix_WHEN_normalize_THEN_correct_syntax() {
        // GIVEN
        let acct = "foo@example.com";

        // WHEN
        let acct = normalize_acct(acct);

        // THEN
        assert_eq!(acct, "acct:foo@example.com");
    }

    #[test]
    fn GIVEN_acct_with_acct_prefix_WHEN_normalize_THEN_correct_syntax() {
        // GIVEN
        let acct = "acct:foo@example.com";

        // WHEN
        let acct = normalize_acct(acct);

        // THEN
        assert_eq!(acct, "acct:foo@example.com");
    }

    #[test]
    fn deserialize_without_rel() {
        let j = r#"
            {
              "subject" : "acct:bob@example.com",
              "links" :
              [
                {
                  "rel" : "http://webfinger.example/rel/profile-page",
                  "href" : "https://www.example.com/~bob/",
                  "type": "text/html"
                },
                {
                  "href" : "https://www.example.com/~bob/bob.vcf",
                  "type": "application/vcard"
                }
              ]
            }
        "#;
        let d: Jrd = serde_json::from_str(j).unwrap();
        assert_eq!(2, d.links.len());
        assert_eq!(
            super::super::PROFILE_PAGE_URL_ID.to_string(),
            d.links[0].rel
        );
        assert_eq!(SELF.to_string(), d.links[1].rel);
    }

    #[test]
    fn create_acct_test() {
        let acct = create_wf_acct("example.com", "test");
        assert_eq!("acct:test@example.com", acct)
    }

    #[test]
    #[allow(non_snake_case)]
    fn GIVEN_acct_WHEN_display_acct_THEN_get_back_without_prefix() {
        // GIVEN
        let acct = "acct:foo@example.com";

        // WHEN
        let display = display_acct(acct);

        // THEN
        assert_eq!(display, "foo@example.com");
    }

    #[test]
    #[allow(non_snake_case)]
    fn GIVEN_acct_with_no_prefix_WHEN_display_acct_THEN_get_back_input() {
        // GIVEN
        let acct = "foo@example.com";

        // WHEN
        let display = display_acct(acct);

        // THEN
        assert_eq!(display, "foo@example.com");
    }

    #[test]
    #[allow(non_snake_case)]
    fn GIVEN_jrd_with_ap_href_WHEN_ap_url_called_THEN_return_url() {
        // GIVEN
        let j = r#"
            {
              "subject" : "acct:bob@example.com",
              "links" :
              [
                {
                  "rel" : "http://example.com/actor",
                  "href" : "https://example.com/bob",
                  "type": "application/activity+json"
                },
                {
                  "href" : "https://www.example.com/~bob/bob.vcf",
                  "type": "application/vcard"
                }
              ]
            }
        "#;
        let d: Jrd = serde_json::from_str(j).unwrap();

        // WHEN
        let url = d.ap_url();

        // THEN
        let url = url.unwrap();
        assert_eq!(url, "https://example.com/bob");
    }
}
