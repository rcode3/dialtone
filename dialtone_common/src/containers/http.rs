use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct HttpGetData {
    pub content_length: Option<u64>,
    pub content_type: Option<String>,
    pub content_encoding: Option<String>,
    pub date: Option<String>,
    pub e_tag: Option<String>,
    pub last_modified: Option<String>,
}
