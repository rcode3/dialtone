use crate::rest::actors::actor_model::OwnedActor;

/// Represents an actors with its credentials, which is the primary key for the actors.
#[derive(Debug)]
pub struct CredentialedActor {
    pub owned_actor: OwnedActor,
    pub creating_user_acct: String,
}
