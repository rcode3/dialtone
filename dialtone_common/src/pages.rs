use crate::ap::{actor::ActorType, ap_object::ApObjectType};

pub const HTML_MEDIA_TYPE: &str = "text/html";

// actors
const APPLICATION_PROFILE_SEGMENT: &str = "a";
const GROUP_PROFILE_SEGMENT: &str = "g";
const PERSON_PROFILE_SEGMENT: &str = "p";
const ORGANIZATION_PROFILE_SEGMENT: &str = "o";
const SERVICE_PROFILE_SEGMENT: &str = "s";

// ap objects
const NOTE_PROFILE_SEGMENT: &str = "n";
const ARTICLE_PROFILE_SEGMENT: &str = "e"; // e = essay, which is similar to an article
const DOCUMENT_PROFILE_SEGMENT: &str = "d";
const IMAGE_PROFILE_SEGMENT: &str = "i";
const AUDIO_PROFILE_SEGMENT: &str = "b";
const VIDEO_PROFILE_SEGMENT: &str = "v";
const PAGE_PROFILE_SEGMENT: &str = "w";

pub fn create_base_url(host_name: &str) -> String {
    format!("https://{host_name}")
}

pub fn create_actor_profile_url(
    host_name: &str,
    preferred_user_name: &str,
    actor_type: &ActorType,
) -> String {
    let actor_segment = match actor_type {
        ActorType::Application => APPLICATION_PROFILE_SEGMENT,
        ActorType::Group => GROUP_PROFILE_SEGMENT,
        ActorType::Organization => ORGANIZATION_PROFILE_SEGMENT,
        ActorType::Person => PERSON_PROFILE_SEGMENT,
        ActorType::Service => SERVICE_PROFILE_SEGMENT,
    };
    format!(
        "{}/{}/{}",
        create_base_url(host_name),
        actor_segment,
        preferred_user_name
    )
}

pub fn create_ap_object_page_url(
    host_name: &str,
    ap_object_type: &ApObjectType,
    local_id: &str,
) -> String {
    let ap_object_segment = match ap_object_type {
        ApObjectType::Article => ARTICLE_PROFILE_SEGMENT,
        ApObjectType::Note => NOTE_PROFILE_SEGMENT,
        ApObjectType::Document => DOCUMENT_PROFILE_SEGMENT,
        ApObjectType::Image => IMAGE_PROFILE_SEGMENT,
        ApObjectType::Video => VIDEO_PROFILE_SEGMENT,
        ApObjectType::Audio => AUDIO_PROFILE_SEGMENT,
        ApObjectType::Page => PAGE_PROFILE_SEGMENT,
    };
    format!(
        "{}/{}/{}",
        create_base_url(host_name),
        ap_object_segment,
        local_id
    )
}
