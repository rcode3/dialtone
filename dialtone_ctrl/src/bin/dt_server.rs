use dialtone_axum::start_server::start_server;
use dialtone_sqlx::db::get_pooled_connection;

#[tokio::main(flavor = "multi_thread")]
async fn main() -> anyhow::Result<()> {
    dotenv::dotenv().ok();
    let pg_pool = get_pooled_connection().await?;
    start_server(pg_pool).await
}
