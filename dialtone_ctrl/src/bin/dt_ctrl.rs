use anyhow::Context;
use clap::Command;
use dialtone_common::utils::version::DT_VERSION;
use dialtone_ctrl::commands::{
    code_registration::{code_registration_command, do_code_registration, CODE_REGISTRATION},
    database::{database_command, do_database, DATABASE},
    media::{do_media, media_command, MEDIA},
    open_registration::{do_open_registration, open_registration_command, OPEN_REGISTRATION},
    roles::{do_roles, roles_command, ROLES},
    site::{do_site, site_command, SITE},
    user::{do_user, user_command, USER},
};
use simplelog::*;

#[tokio::main]
async fn main() {
    dotenv::dotenv().ok();
    TermLogger::init(
        LevelFilter::Info,
        Config::default(),
        TerminalMode::Stderr,
        ColorChoice::Auto,
    )
    .unwrap();

    let mut command = Command::new("dt_ctrl")
        .about("Dialtone server control utility.")
        .version(DT_VERSION)
        .propagate_version(true)
        .subcommand(database_command())
        .subcommand(site_command())
        .subcommand(open_registration_command())
        .subcommand(code_registration_command())
        .subcommand(roles_command())
        .subcommand(user_command())
        .subcommand(media_command());

    let matches = command.clone().get_matches();

    let action = match matches.subcommand() {
        Some((DATABASE, sub_matches)) => do_database(sub_matches).await,
        Some((SITE, sub_matches)) => do_site(sub_matches).await,
        Some((OPEN_REGISTRATION, sub_matches)) => do_open_registration(sub_matches).await,
        Some((CODE_REGISTRATION, sub_matches)) => do_code_registration(sub_matches).await,
        Some((ROLES, sub_matches)) => do_roles(sub_matches).await,
        Some((USER, sub_matches)) => do_user(sub_matches).await,
        Some((MEDIA, sub_matches)) => do_media(sub_matches).await,
        _ => command.print_help().with_context(|| "unable to write help"),
    };

    if let Err(err) = action {
        error!("dt_ctrl command execution ended in error: {err}");

        #[cfg(debug_assertions)]
        panic!("{err}");
    }
}
