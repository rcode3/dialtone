use anyhow::Context;
use clap::{ArgMatches, Command};
use dialtone_ccli_util::{
    input::{
        host_name::{get_host_name, host_name_arg},
        json::{get_json_arg, json_arg, JsonOutput},
        password::{get_password, password_arg},
        system_roles::{get_roles, get_system_roles, roles_arg, system_roles_arg},
        user_name::{get_user_name, user_name_arg},
        user_status::{get_user_status, user_status_arg},
    },
    output::web_user::{
        web_user_base_info, web_user_default_actor, web_user_event_log, web_user_last_login,
        web_user_last_seen, web_user_permissions,
    },
};
use dialtone_common::{ap::pun::is_valid_pun, utils::make_acct::make_acct};
use dialtone_sqlx::{
    control::{
        system_role::{add::add_system_roles, remove::remove_system_roles},
        user::{
            change_auth::change_user_auth, check_name_availability::check_user_name_availability,
            create_with_actor::create_user_with_default_actor,
        },
    },
    db::{
        get_pooled_connection,
        site_info::fetch_site,
        user::{change_status::change_user_status, fetch_info::fetch_user_info},
    },
};
use simplelog::*;

pub const USER: &str = "user";

const CREATE: &str = "create";
const GET_INFO: &str = "get-info";
const ADD_ROLES: &str = "add-roles";
const REMOVE_ROLES: &str = "remove-roles";
const CHANGE_STATUS: &str = "change-status";
const CHANGE_PASSWORD: &str = "change-password";

pub fn user_command() -> Command<'static> {
    Command::new(USER)
        .about("Manager users.")
        .subcommand(
            Command::new(CREATE)
                .about("Create a new user.")
                .arg(host_name_arg())
                .arg(user_name_arg())
                .arg(password_arg())
                .arg(user_status_arg())
                .arg(system_roles_arg()),
        )
        .subcommand(
            Command::new(GET_INFO)
                .about("Get information about a user.")
                .arg(host_name_arg())
                .arg(user_name_arg())
                .arg(json_arg()),
        )
        .subcommand(
            Command::new(ADD_ROLES)
                .about("Add a role to a user.")
                .arg(host_name_arg())
                .arg(user_name_arg())
                .arg(roles_arg()),
        )
        .subcommand(
            Command::new(REMOVE_ROLES)
                .about("Remove a role to a user.")
                .arg(host_name_arg())
                .arg(user_name_arg())
                .arg(roles_arg()),
        )
        .subcommand(
            Command::new(CHANGE_STATUS)
                .about("Change the status of a user.")
                .arg(host_name_arg())
                .arg(user_name_arg())
                .arg(user_status_arg()),
        )
        .subcommand(
            Command::new(CHANGE_PASSWORD)
                .about("Change the password of a user.")
                .arg(host_name_arg())
                .arg(user_name_arg())
                .arg(password_arg()),
        )
}

pub async fn do_user(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    match sub_matches.subcommand() {
        Some((CREATE, sub_matches)) => do_user_create(sub_matches).await,
        Some((GET_INFO, sub_matches)) => do_user_get_info(sub_matches).await,
        Some((ADD_ROLES, sub_matches)) => do_user_add_roles(sub_matches).await,
        Some((REMOVE_ROLES, sub_matches)) => do_user_remove_roles(sub_matches).await,
        Some((CHANGE_STATUS, sub_matches)) => do_user_change_status(sub_matches).await,
        Some((CHANGE_PASSWORD, sub_matches)) => do_user_change_password(sub_matches).await,
        _ => user_command()
            .print_help()
            .with_context(|| "Unable to pring help"),
    }
}

async fn do_user_create(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let user_name = get_user_name(sub_matches);
    let password = get_password(sub_matches);
    let user_status = get_user_status(sub_matches)?;
    let system_roles = get_system_roles(sub_matches);

    let pg_pool = get_pooled_connection().await?;
    let site_info = fetch_site(&pg_pool, &host_name)
        .await?
        .with_context(|| "no sites by that names could be found")?;

    let valid_name = is_valid_pun(&user_name, false);
    if !valid_name {
        info!("{} contains characters that are not allowed actors names. A default actors is created when creating a users.", &user_name);
        return Err(anyhow::anyhow!("{user_name} is not valid."));
    }

    // get system roles
    let role_set = site_info.site_data.system_roles_sets.get(&system_roles);
    let role_set = if let Some(role_set) = role_set {
        role_set
    } else {
        return Err(anyhow::anyhow!(
            "{role_set:?} is not a valid set of system roles"
        ));
    };

    //check users names
    let available = check_user_name_availability(&pg_pool, &user_name, &host_name).await?;

    if !available {
        return Err(anyhow::anyhow!(
            "{} at {} is already used",
            &user_name,
            &host_name
        ));
    } else {
        //create the users
        info!("Creating user {} at {}", &user_name, &host_name);
        create_user_with_default_actor(&pg_pool, &user_name, &host_name, &password, role_set)
            .await?;
    }

    let acct = make_acct(&user_name, &host_name);
    info!("Changing {} to Active", acct);
    change_user_status(&pg_pool, &acct, &user_status).await?;

    Ok(())
}

async fn do_user_get_info(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let user_name = get_user_name(sub_matches);
    let json_output = get_json_arg(sub_matches);

    let acct = make_acct(&user_name, &host_name);

    if let JsonOutput::NoJson = json_output {
        info!("Fetching user {acct}.")
    }

    let pg_pool = get_pooled_connection().await?;
    let web_user = fetch_user_info(&pg_pool, &acct)
        .await?
        .ok_or(anyhow::anyhow!(format!("Cannot find user {}", &acct)))?;

    match json_output {
        JsonOutput::OneLine => {
            let s = serde_json::to_string(&web_user);
            println!("{}", s.unwrap());
        }
        JsonOutput::Pretty => {
            let s = serde_json::to_string_pretty(&web_user);
            println!("{}", s.unwrap());
        }
        JsonOutput::NoJson => {
            web_user_base_info(&web_user);
            web_user_default_actor(&web_user.default_actor_data);
            web_user_permissions(&web_user.user_authz.system_permissions);
            web_user_last_login(&web_user.last_login_data);
            web_user_last_seen(&web_user.last_seen_data);
            web_user_event_log(&web_user.event_log);
        }
    }

    Ok(())
}

async fn do_user_add_roles(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let user_name = get_user_name(sub_matches);
    let roles = get_roles(sub_matches)?;

    let pg_pool = get_pooled_connection().await?;
    let acct = make_acct(&user_name, &host_name);
    let role_names = roles
        .iter()
        .map(|r| r.to_string())
        .collect::<Vec<String>>()
        .join(", ");

    info!("Adding {} to '{}'.", role_names, acct);
    add_system_roles(&pg_pool, &roles, &acct, &host_name).await
        .with_context(||
            format!("Unable to add roles {role_names} to '{acct}'. Does the users exist? Is the users already a member of the role?"))?;

    Ok(())
}

async fn do_user_remove_roles(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let user_name = get_user_name(sub_matches);
    let roles = get_roles(sub_matches)?;

    let pg_pool = get_pooled_connection().await?;
    let acct = make_acct(&user_name, &host_name);
    let role_names = roles
        .iter()
        .map(|r| r.to_string())
        .collect::<Vec<String>>()
        .join(", ");

    info!("Remove roles {} from '{}'.", role_names, acct);
    remove_system_roles(&pg_pool, &roles, &acct, &host_name)
        .await
        .with_context(|| {
            format!("Unable to remove roles {role_names} to '{acct}'. Does the users exist?",)
        })?;

    Ok(())
}

async fn do_user_change_status(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let user_name = get_user_name(sub_matches);
    let user_status = get_user_status(sub_matches)?;

    let pg_pool = get_pooled_connection().await?;
    let acct = make_acct(&user_name, &host_name);

    info!("Changing {} to {}", acct, user_status);
    change_user_status(&pg_pool, &acct, &user_status)
        .await
        .with_context(|| format!("Unable to change {acct} status. Does the users exist?"))?;
    Ok(())
}

async fn do_user_change_password(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let user_name = get_user_name(sub_matches);
    let password = get_password(sub_matches);

    let pg_pool = get_pooled_connection().await?;
    let acct = make_acct(&user_name, &host_name);

    info!("Changing using password for {}.", acct);
    change_user_auth(&pg_pool, &acct, &password)
        .await
        .with_context(|| format!("Unable to change password for {acct}. Does the users exist?",))?;
    Ok(())
}
