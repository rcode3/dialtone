use crate::util::media::process_media;
use anyhow::Context;
use clap::{ArgMatches, Command};
use dialtone_axum::media::create_local_media_dir;
use dialtone_ccli_util::output::media::output_media_reports;
use dialtone_ccli_util::{
    input::{
        host_name::{get_host_name, host_name_arg},
        json::{get_json_arg, json_arg, JsonOutput},
        password::{get_sysop_password, password_arg},
        site_name::{
            get_long_site_name, get_short_site_name, long_site_name_arg, short_site_name_arg,
        },
        user_name::{get_sysop_name, user_name_arg},
    },
    output::site_info::{
        basic_info, code_registration, open_registration, registration_methods, system_roles,
    },
};
use dialtone_common::ap::actor::ActorType;
use dialtone_common::ap::id::ExtraCollectionNames;
use dialtone_common::rest::users::web_user::{SystemRoleType, UserStatus};
use dialtone_common::utils::make_acct::make_acct;
use dialtone_sqlx::control::actor::create_owned::{
    create_credentialed_actor_with_options, CreateCredentialedOptions,
};
use dialtone_sqlx::control::actor::system::create_site_actors;
use dialtone_sqlx::control::user::create_with_actor::create_user_with_default_actor_tx;
use dialtone_sqlx::db::site_info::create_site;
use dialtone_sqlx::db::user::change_status::change_user_status;
use dialtone_sqlx::db::user::create_user;
use dialtone_sqlx::db::{get_pooled_connection, site_info::fetch_site};
use dialtone_sqlx::logic::actor::new_actor::NewActor;
use simplelog::*;
use std::collections::HashMap;

pub const SITE: &str = "site";

const INFO: &str = "info";
const CREATE: &str = "create";
const DELETE: &str = "delete";

const SYSOP: &str = "sysop";
const GENPOP: &str = "genpop";

pub const SITE_USER_NAME: &str = "_site";

pub fn site_command() -> Command<'static> {
    Command::new(SITE)
        .about("Create, view and delete sites.")
        .subcommand(
            Command::new(INFO)
                .about("Get site info.")
                .arg(host_name_arg())
                .arg(json_arg()),
        )
        .subcommand(
            Command::new(CREATE)
                .about("Creat a new site.")
                .arg(host_name_arg())
                .arg(short_site_name_arg())
                .arg(long_site_name_arg())
                .arg(user_name_arg())
                .arg(password_arg()),
        )
        .subcommand(
            Command::new(DELETE)
                .about("Delete a site.")
                .arg(host_name_arg()),
        )
}

pub async fn do_site(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    match sub_matches.subcommand() {
        Some((INFO, sub_matches)) => do_site_info(sub_matches).await,
        Some((CREATE, sub_matches)) => do_site_create(sub_matches).await,
        Some((DELETE, sub_matches)) => do_site_delete(sub_matches).await,
        _ => site_command()
            .print_help()
            .with_context(|| "unable to print help"),
    }
}

async fn do_site_info(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let json_output = get_json_arg(sub_matches);

    if let JsonOutput::NoJson = json_output {
        info!("Fetching sites {}.\n", host_name);
    }

    let pg_pool = get_pooled_connection().await?;
    let site_info = fetch_site(&pg_pool, &host_name)
        .await?
        .with_context(|| "no sites by that names could be found")?;

    match json_output {
        JsonOutput::OneLine => {
            let s = serde_json::to_string(&site_info);
            println!("{}", s.unwrap());
        }
        JsonOutput::Pretty => {
            let s = serde_json::to_string_pretty(&site_info);
            println!("{}", s.unwrap());
        }
        JsonOutput::NoJson => {
            basic_info(&site_info);
            registration_methods(&site_info.site_data.public);
            system_roles(&site_info);
            open_registration(&site_info);
            code_registration(&site_info);
        }
    }
    Ok(())
}

async fn do_site_create(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let short_name = get_short_site_name(sub_matches);
    let long_name = get_long_site_name(sub_matches);
    let user_name = get_sysop_name(sub_matches)?;
    let user_password = get_sysop_password(sub_matches);

    info!("Creating site {}.", host_name);

    // create directories
    info!("Creating media directories.");
    create_local_media_dir(&host_name, &ExtraCollectionNames::Banners.to_string()).await?;
    create_local_media_dir(&host_name, &ExtraCollectionNames::Icons.to_string()).await?;

    let mut system_roles_sets: HashMap<String, Vec<SystemRoleType>> = HashMap::new();
    info!("Creating '{SYSOP}' role set.");
    let sysop_roles = vec![
        SystemRoleType::ActorAdmin,
        SystemRoleType::UserAdmin,
        SystemRoleType::ContentAdmin,
        SystemRoleType::PersonOwner,
        SystemRoleType::OrganizationOwner,
        SystemRoleType::GroupOwner,
        SystemRoleType::ApplicationOwner,
        SystemRoleType::ServiceOwner,
    ];
    system_roles_sets.insert(SYSOP.to_string(), sysop_roles.clone());
    info!("Creating '{GENPOP}' role set.");
    system_roles_sets.insert(
        GENPOP.to_string(),
        vec![
            SystemRoleType::PersonOwner,
            SystemRoleType::OrganizationOwner,
            SystemRoleType::GroupOwner,
            SystemRoleType::ApplicationOwner,
        ],
    );

    info!("Registering {} in the database.", host_name);
    let pg_pool = get_pooled_connection().await?;
    let mut tx = pg_pool.begin().await?;
    create_site(
        &mut tx,
        &host_name,
        short_name,
        Some(long_name),
        None,
        system_roles_sets,
    )
    .await?;

    info!("Creating system users and actors.");
    let acct = make_acct(SITE_USER_NAME, &host_name);
    let password: String =
        rand::Rng::sample_iter(rand::thread_rng(), &rand::distributions::Alphanumeric)
            .take(100)
            .map(char::from)
            .collect();
    create_user(&mut tx, &acct, &password).await?;
    create_site_actors(&mut tx, &host_name, &acct).await?;

    info!("Processing system media.");
    let reports = process_media(&mut tx, &host_name).await?;
    output_media_reports(&reports);

    info!("Creating sysop user {user_name}.");
    let user_create = create_user_with_default_actor_tx(
        &mut tx,
        &user_name,
        &host_name,
        &user_password,
        &sysop_roles,
    )
    .await?;

    info!("Changing {user_name} to Active");
    change_user_status(
        &mut tx,
        &user_create.creating_user_acct,
        &UserStatus::Active,
    )
    .await?;

    if user_name != SYSOP {
        info!("Creating '{SYSOP}' actor for '{user_name}'.");
        let summary = format!("System operator of {host_name}.");
        let sysop_actor = NewActor::builder()
            .preferred_user_name(SYSOP)
            .actor_type(ActorType::Person)
            .owner(&user_create.creating_user_acct)
            .name("System Operator")
            .summary(summary)
            .build();
        let options = CreateCredentialedOptions {
            extra_collections: None,
            add_to_site_collections: true,
        };
        create_credentialed_actor_with_options(&mut tx, sysop_actor, false, &host_name, options)
            .await?;
    }
    tx.commit().await?;
    Ok(())
}

pub async fn do_site_delete(_sub_matches: &ArgMatches) -> anyhow::Result<()> {
    error!("Sorry. This is not yet implemented.");
    Ok(())
}
