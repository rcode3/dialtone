use anyhow::Context;
use clap::{ArgMatches, Command};
use dialtone_ccli_util::input::auto_approve::{auto_approve_arg, get_auto_approve, AUTO_APPROVE};
use dialtone_ccli_util::input::host_name::{get_host_name, host_name_arg};
use dialtone_ccli_util::input::system_roles::{get_system_roles, system_roles_arg};
use dialtone_sqlx::db::get_pooled_connection;
use dialtone_sqlx::db::site_info::fetch_site;
use dialtone_sqlx::db::site_info::update::update_site;
use simplelog::*;

pub const OPEN_REGISTRATION: &str = "open-registration";

const DISABLE: &str = "disable";
const ENABLE: &str = "enable";

pub fn open_registration_command() -> Command<'static> {
    Command::new(OPEN_REGISTRATION)
        .about("Manage account open registration.")
        .subcommand(
            Command::new(DISABLE)
                .about("Disable open registration.")
                .arg(host_name_arg()),
        )
        .subcommand(
            Command::new(ENABLE)
                .about("Enable open registration.")
                .arg(host_name_arg())
                .arg(system_roles_arg())
                .arg(auto_approve_arg()),
        )
        .subcommand(
            Command::new(AUTO_APPROVE)
                .about("Set accounts to be automatically approved.")
                .arg(host_name_arg()),
        )
}

pub async fn do_open_registration(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    match sub_matches.subcommand() {
        Some((ENABLE, sub_matches)) => do_open_registration_enable(sub_matches).await,
        Some((DISABLE, sub_matches)) => do_open_registration_disable(sub_matches).await,
        Some((AUTO_APPROVE, sub_matches)) => do_open_registration_auto_approve(sub_matches).await,
        _ => open_registration_command()
            .print_help()
            .with_context(|| "error writing help"),
    }
}

async fn do_open_registration_enable(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let system_roles_name = get_system_roles(sub_matches);
    let auto_approve = get_auto_approve(sub_matches);

    let pg_pool = get_pooled_connection().await?;
    let mut site_info = fetch_site(&pg_pool, &host_name)
        .await?
        .with_context(|| "host_name is not a valid site")?;
    let result = site_info
        .site_data
        .set_open_registration(&system_roles_name, auto_approve);
    match result {
        Ok(_) => {
            update_site(&pg_pool, &host_name, site_info.site_data).await?;
            info!("Open registrations is now enabled.");
            Ok(())
        }
        Err(err) => Err(
            anyhow::anyhow!(
                format!(
                    "Unable to enable open registration. Perhaps the system roles name is not correct. {err:?}"
                    )
                )
            ),
    }
}

async fn do_open_registration_disable(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;

    let pg_pool = get_pooled_connection().await?;
    let mut site_info = fetch_site(&pg_pool, &host_name)
        .await?
        .with_context(|| "host_name is not a valid site")?;
    site_info.site_data.remove_open_registration();
    update_site(&pg_pool, &host_name, site_info.site_data).await?;
    info!("Open registrations are disabled.");
    Ok(())
}

async fn do_open_registration_auto_approve(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let auto_approve = get_auto_approve(sub_matches);

    let pg_pool = get_pooled_connection().await?;
    let mut site_info = fetch_site(&pg_pool, &host_name)
        .await?
        .with_context(|| "host_name is not a valid site")?;
    let result = site_info
        .site_data
        .set_open_registration_auto_approved(auto_approve);
    match result {
        Ok(_) => {
            update_site(&pg_pool, &host_name, site_info.site_data).await?;
            info!("Open registrations will now be automatically approved.");
            Ok(())
        }
        Err(err) => Err(
            anyhow::anyhow!(
                format!(
                    "Unable to set auto approval for open registration. Perhaps open registration is not setup. {err:?}"
                    )
                )
            ),
    }
}
