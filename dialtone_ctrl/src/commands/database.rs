use std::path::Path;

use anyhow::anyhow;
use anyhow::Context;
use clap::{ArgMatches, Command};
use dialtone_ccli_util::input::{
    path::{get_pathbuf_arg, path_arg},
    yes::{get_yes_with_prompt, yes_arg},
};
use dialtone_sqlx::db::get_database_url;
use simplelog::{error, info, warn};
use sqlx::migrate::Migrator;
use sqlx::PgPool;
use sqlx::{migrate::MigrateDatabase, Postgres};

pub const DATABASE: &str = "database";

const CREATE: &str = "create";
const MIGRATE: &str = "migrate";
const DROP: &str = "drop";

pub fn database_command() -> Command<'static> {
    Command::new(DATABASE)
        .about("Manage Postgres database.")
        .subcommand(Command::new(CREATE).about("Create Postgres database."))
        .subcommand(
            Command::new(MIGRATE)
                .about("Migrate Postgres database schema.")
                .arg(path_arg("Database migrations directory.")),
        )
        .subcommand(
            Command::new(DROP)
                .about("Drop Postgres database.")
                .arg(yes_arg("Confirm database drop.")),
        )
}

pub async fn do_database(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    match sub_matches.subcommand() {
        Some((CREATE, _sub_matches)) => do_database_create().await,
        Some((MIGRATE, sub_matches)) => do_database_migrate(sub_matches).await,
        Some((DROP, sub_matches)) => do_database_drop(sub_matches).await,
        _ => database_command()
            .print_help()
            .with_context(|| "Unable to print help"),
    }
}

async fn do_database_create() -> anyhow::Result<()> {
    info!("Creating database.");
    let database_url = get_database_url();
    Postgres::create_database(&database_url).await?;
    Ok(())
}

async fn do_database_migrate(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let path = Path::new("./migrations");

    #[cfg(debug_assertions)]
    let path = if Path::new("dialtone_sqlx/migrations").is_dir() {
        Path::new("dialtone_sqlx/migrations")
    } else {
        path
    };

    let path_arg = get_pathbuf_arg(sub_matches);
    let path = if path_arg.is_some() {
        path_arg.as_ref().unwrap().as_path()
    } else {
        path
    };

    info!("Running database migrations from {path:?}");
    if !path.is_dir() {
        Err(anyhow!("Migrations directory {path:?} does not exist"))
    } else {
        let url = get_database_url();
        let pool = PgPool::connect(&url).await.unwrap();
        Migrator::new(path).await.unwrap().run(&pool).await.unwrap();
        Ok(())
    }
}

async fn do_database_drop(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let yes = get_yes_with_prompt(sub_matches, "Drop database?");
    if yes {
        warn!("Dropping database.");
        let database_url = get_database_url();
        Postgres::drop_database(&database_url).await?;
    } else {
        error!("Database not dropped due to lack of confirmation.");
    }
    Ok(())
}
