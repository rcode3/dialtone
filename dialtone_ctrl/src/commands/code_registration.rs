use anyhow::Context;
use clap::{ArgMatches, Command};
use dialtone_sqlx::db::{
    get_pooled_connection,
    site_info::{fetch_site, update::update_site},
};

use dialtone_ccli_util::input::{
    auto_approve::{auto_approve_arg, get_auto_approve, AUTO_APPROVE},
    code::{code_arg, get_code},
    host_name::{get_host_name, host_name_arg},
    system_roles::{get_system_roles, system_roles_arg},
};
use simplelog::*;

pub const CODE_REGISTRATION: &str = "code-registration";

const DISABLE: &str = "disable";
const DISABLE_ALL: &str = "disable-all";
const ENABLE: &str = "enable";

pub fn code_registration_command() -> Command<'static> {
    Command::new(CODE_REGISTRATION)
        .about("Manage account registration with simple codes.")
        .subcommand(
            Command::new(DISABLE)
                .about("Disable a specific registration code.")
                .arg(host_name_arg())
                .arg(code_arg()),
        )
        .subcommand(
            Command::new(DISABLE_ALL)
                .about("Disable a specific registration code.")
                .arg(host_name_arg()),
        )
        .subcommand(
            Command::new(ENABLE)
                .about("Enable a specific registration code.")
                .arg(host_name_arg())
                .arg(code_arg())
                .arg(system_roles_arg())
                .arg(auto_approve_arg()),
        )
        .subcommand(
            Command::new(AUTO_APPROVE)
                .about("Set accounts to be automatically approved.")
                .arg(host_name_arg())
                .arg(code_arg()),
        )
}

pub async fn do_code_registration(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    match sub_matches.subcommand() {
        Some((ENABLE, sub_matches)) => do_code_registration_enable(sub_matches).await,
        Some((DISABLE, sub_matches)) => do_code_registration_disable(sub_matches).await,
        Some((DISABLE_ALL, sub_matches)) => do_code_registration_disable_all(sub_matches).await,
        Some((AUTO_APPROVE, sub_matches)) => do_code_registration_auto_approve(sub_matches).await,
        _ => code_registration_command()
            .print_help()
            .with_context(|| "error writing help"),
    }
}

async fn do_code_registration_disable(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let code = get_code(sub_matches);

    let pg_pool = get_pooled_connection().await?;
    let mut site_info = fetch_site(&pg_pool, &host_name)
        .await?
        .with_context(|| "host_name is not a valid site")?;
    site_info
        .site_data
        .remove_simple_code_registration_code(&code);
    update_site(&pg_pool, &host_name, site_info.site_data).await?;
    info!("Code registrations for '{code}' are disabled.");
    Ok(())
}

async fn do_code_registration_disable_all(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;

    let pg_pool = get_pooled_connection().await?;
    let mut site_info = fetch_site(&pg_pool, &host_name)
        .await?
        .with_context(|| "host_name is not a valid site")?;
    site_info
        .site_data
        .remove_all_simple_code_registration_codes();
    update_site(&pg_pool, &host_name, site_info.site_data).await?;
    info!("All code registrations disabled.");
    Ok(())
}

async fn do_code_registration_enable(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let code = get_code(sub_matches);
    let system_roles_name = get_system_roles(sub_matches);
    let auto_approve = get_auto_approve(sub_matches);

    let pg_pool = get_pooled_connection().await?;
    let mut site_info = fetch_site(&pg_pool, &host_name)
        .await?
        .with_context(|| "host_name is not a valid site")?;
    let result = site_info.site_data.set_simple_code_registration_code(
        &system_roles_name,
        &code,
        auto_approve,
    );
    match result {
        Ok(_) => {
            update_site(&pg_pool, &host_name, site_info.site_data).await?;
            info!("Code registrations for '{code}' is now enabled.");
            Ok(())
        }
        Err(err) => Err(
            anyhow::anyhow!(
                format!(
                    "Unable to enable code registration. Perhaps the system roles name is not correct. {err:?}"
                    )
                )
            ),
    }
}

async fn do_code_registration_auto_approve(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let code = get_code(sub_matches);
    let auto_approve = get_auto_approve(sub_matches);

    let pg_pool = get_pooled_connection().await?;
    let mut site_info = fetch_site(&pg_pool, &host_name)
        .await?
        .with_context(|| "host_name is not a valid site")?;
    let result = site_info
        .site_data
        .set_simple_code_registration_auto_approved(&code, auto_approve);
    match result {
        Ok(_) => {
            update_site(&pg_pool, &host_name, site_info.site_data).await?;
            info!("Code registrations for '{code}' will now be automatically approved.");
            Ok(())
        }
        Err(err) => Err(
            anyhow::anyhow!(
                format!(
                    "Unable to set auto approval for code registration. Perhaps the code registration is not setup. {err:?}"
                    )
                )
            ),
    }
}
