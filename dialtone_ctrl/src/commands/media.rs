use crate::util::media::process_media;
use anyhow::Context;
use clap::{ArgMatches, Command};
use dialtone_ccli_util::{
    input::host_name::{get_host_name, host_name_arg},
    output::media::output_media_reports,
};
use dialtone_sqlx::db::get_pooled_connection;
use simplelog::*;

pub const MEDIA: &str = "media";

const PROCESS: &str = "process";

pub fn media_command() -> Command<'static> {
    Command::new(MEDIA)
        .about("Manage system media.")
        .subcommand(
            Command::new(PROCESS)
                .about("Process system media.")
                .arg(host_name_arg()),
        )
}

pub async fn do_media(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    match sub_matches.subcommand() {
        Some((PROCESS, sub_matches)) => do_media_process(sub_matches).await,
        _ => media_command()
            .print_help()
            .with_context(|| "unable to print help"),
    }
}

async fn do_media_process(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let pg_pool = get_pooled_connection().await?;
    info!("Processing system media.");
    let reports = process_media(&pg_pool, &host_name).await?;
    output_media_reports(&reports);
    Ok(())
}
