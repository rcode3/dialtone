use anyhow::Context;
use clap::{ArgMatches, Command};
use dialtone_ccli_util::{
    input::{
        host_name::{get_host_name, host_name_arg},
        json::{get_json_arg, json_arg, JsonOutput},
        system_roles::{get_roles, get_system_roles, roles_arg, system_roles_arg},
    },
    output::site_info::system_roles,
};
use dialtone_sqlx::db::{
    get_pooled_connection,
    site_info::{fetch_site, update::update_site},
};
use simplelog::*;

pub const ROLES: &str = "roles";

const ADD_SET: &str = "add-set";
const DELETE_SET: &str = "delete-set";
const LIST_SETS: &str = "list-sets";

pub fn roles_command() -> Command<'static> {
    Command::new(ROLES)
        .about("Add, list and delete role sets.")
        .subcommand(
            Command::new(LIST_SETS)
                .about("List role sets.")
                .arg(host_name_arg())
                .arg(json_arg()),
        )
        .subcommand(
            Command::new(ADD_SET)
                .about("Add a role set.")
                .arg(host_name_arg())
                .arg(system_roles_arg())
                .arg(roles_arg()),
        )
        .subcommand(
            Command::new(DELETE_SET)
                .about("Delete a role set.")
                .arg(host_name_arg())
                .arg(system_roles_arg()),
        )
}

pub async fn do_roles(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    match sub_matches.subcommand() {
        Some((LIST_SETS, sub_matches)) => do_roles_list_sets(sub_matches).await,
        Some((ADD_SET, sub_matches)) => do_roles_add_set(sub_matches).await,
        Some((DELETE_SET, sub_matches)) => do_roles_delete_set(sub_matches).await,
        _ => roles_command()
            .print_help()
            .with_context(|| "unable to print help"),
    }
}

async fn do_roles_list_sets(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let json_output = get_json_arg(sub_matches);

    if let JsonOutput::NoJson = json_output {
        info!("Fetching sites {}.\n", host_name);
    }

    let pg_pool = get_pooled_connection().await?;
    let site_info = fetch_site(&pg_pool, &host_name)
        .await?
        .with_context(|| "no sites by that names could be found")?;

    match json_output {
        JsonOutput::OneLine => {
            let s = serde_json::to_string(&site_info.site_data.system_roles_sets);
            println!("{}", s.unwrap());
        }
        JsonOutput::Pretty => {
            let s = serde_json::to_string_pretty(&site_info.site_data.system_roles_sets);
            println!("{}", s.unwrap());
        }
        JsonOutput::NoJson => {
            system_roles(&site_info);
        }
    }
    Ok(())
}

async fn do_roles_add_set(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let system_roles_set_name = get_system_roles(sub_matches);
    let roles = get_roles(sub_matches)?;

    let pg_pool = get_pooled_connection().await?;
    let mut site_info = fetch_site(&pg_pool, &host_name)
        .await?
        .with_context(|| "no sites by that names could be found")?;

    site_info
        .site_data
        .system_roles_sets
        .insert(system_roles_set_name.clone(), roles);

    info!("Adding role set {system_roles_set_name}.");
    update_site(&pg_pool, &host_name, site_info.site_data).await?;
    Ok(())
}

async fn do_roles_delete_set(sub_matches: &ArgMatches) -> anyhow::Result<()> {
    let host_name = get_host_name(sub_matches)?;
    let set_name = get_system_roles(sub_matches);

    let pg_pool = get_pooled_connection().await?;
    let mut site_info = fetch_site(&pg_pool, &host_name)
        .await?
        .with_context(|| "no sites by that names could be found")?;

    let set = site_info.site_data.system_roles_sets.get(&set_name);
    if set.is_none() {
        return Err(anyhow::anyhow!("set name does not exist."));
    }

    if let Some(ref open_set_name) = site_info.site_data.open_registration.system_roles_set_name {
        if open_set_name == &set_name {
            return Err(anyhow::anyhow!(
                "set name is being used in open registration."
            ));
        }
    }

    for code in &site_info.site_data.simple_code_registration.codes {
        let (_, v) = code;
        if v.system_roles_set_name == set_name {
            return Err(anyhow::anyhow!(
                "set name is being used in a code registration."
            ));
        }
    }

    info!("Deleting set '{set_name}' on {}.\n", host_name);

    site_info.site_data.system_roles_sets.remove(&set_name);

    update_site(&pg_pool, &host_name, site_info.site_data).await?;
    Ok(())
}
