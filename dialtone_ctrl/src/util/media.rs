use std::{collections::HashMap, future::Future};

use dialtone_axum::media::{
    local_media_dir_exists, local_media_file_names, media_type_from_file_name, owned_media_url,
    MediaError,
};
use dialtone_ccli_util::output::media::Report;
use dialtone_common::{
    ap::{
        ap_object::ApObjectType,
        id::{create_actor_id, create_collection_id, ExtraCollectionNames, SiteActors},
    },
    rest::ap_objects::ap_object_model::CreateOwnedApObject,
};
use dialtone_sqlx::{
    control::ap_object::create::create_ap_object,
    db::{ap_object::fetch::fetch_ap_object, collection::insert::insert_into_collection},
    logic::ap_object::{
        local_id::make_non_temporal_local_id, new::new_media_ap_object_with_local_id,
    },
};
use sqlx::{Acquire, Postgres};

#[allow(clippy::manual_async_fn)]
pub fn process_media<'a, 'c, A>(
    db: A,
    host_name: &'a str,
) -> impl Future<Output = anyhow::Result<HashMap<String, Report>>> + Send + 'a
where
    A: Acquire<'c, Database = Postgres> + Send + 'a,
{
    async move {
        let mut exec = db.acquire().await?;
        let mut reports: HashMap<String, Report> = HashMap::new();
        let report = process_actor_media(
            &mut *exec,
            host_name,
            &SiteActors::SiteContent.to_string(),
            &ExtraCollectionNames::Banners.to_string(),
        )
        .await?;
        reports.insert(ExtraCollectionNames::Banners.to_string(), report);
        let report = process_actor_media(
            &mut *exec,
            host_name,
            &SiteActors::SiteContent.to_string(),
            &ExtraCollectionNames::Icons.to_string(),
        )
        .await?;
        reports.insert(ExtraCollectionNames::Icons.to_string(), report);
        Ok(reports)
    }
}

#[allow(clippy::manual_async_fn)]
fn process_actor_media<'a, 'c, A>(
    db: A,
    host_name: &'a str,
    pun: &'a str,
    collection_name: &'a str,
) -> impl Future<Output = anyhow::Result<Report>> + Send + 'a
where
    A: Acquire<'c, Database = Postgres> + Send + 'a,
{
    async move {
        let mut exec = db.acquire().await?;
        let mut report = Report::default();
        if local_media_dir_exists(host_name, pun).await? {
            let file_names = local_media_file_names(host_name, pun).await?;
            for file_name in file_names.iter() {
                let new_ap_object = CreateOwnedApObject {
                    name: None,
                    media_type: media_type_from_file_name(file_name),
                    ap_type: ApObjectType::Image,
                    content: None,
                    summary: None,
                    owner_data: None,
                    to: None,
                    cc: None,
                    bto: None,
                    bcc: None,
                };
                let url = owned_media_url(host_name, pun, file_name)?;
                let actor_id = create_actor_id(host_name, pun);
                let ap_object_local_id = make_non_temporal_local_id(Some(pun), file_name);
                let ap_object = new_media_ap_object_with_local_id(
                    host_name,
                    &actor_id,
                    &url,
                    &new_ap_object,
                    &ap_object_local_id,
                );
                let ap_object_id = ap_object
                    .id
                    .as_ref()
                    .ok_or(MediaError::ErrorWhileVisiting)?;
                let fetch_result = fetch_ap_object(&mut *exec, ap_object_id).await?;
                if fetch_result.is_some() {
                    report.existed += 1;
                } else {
                    let ap_object_id = create_ap_object(&mut *exec, &ap_object, host_name).await?;
                    let collection_id = create_collection_id(&actor_id, collection_name);
                    insert_into_collection(&mut *exec, &collection_id, &ap_object_id).await?;
                    report.added += 1;
                }
            }
        }
        Ok(report)
    }
}
