use assert_cmd::Command;
use dialtone_common::ap::id::create_actor_id;
use dialtone_common::ap::id::SiteActors;
use dialtone_common::rest::users::web_user::UserStatus;
use dialtone_common::utils::make_acct::make_acct;
use dialtone_ctrl::commands::site::SITE_USER_NAME;
use dialtone_sqlx::db::actor::fetch_owned::fetch_owned_actor_by_id;
use dialtone_sqlx::db::site_info::fetch_site;
use dialtone_sqlx::db::user::fetch_info::fetch_user_info;
use dialtone_test_util::test_action;
use dialtone_test_util::test_constants::*;
use dialtone_test_util::test_pg::test_pg_with_env;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_clean_db_WHEN_create_site_THEN_all_items_created() {
    test_pg_with_env(move |pool, env| async move {
        // GIVEN
        //   nothing to do, setup by runner

        // WHEN
        let mut cmd = Command::cargo_bin("dt_ctrl").unwrap();
        cmd.env_clear()
            .envs(env)
            .args([
                "site",
                "create",
                "-H",
                TEST_HOSTNAME,
                "-L",
                "a long name",
                "-S",
                "shorty",
                "-u",
                TEST_NOROLEUSER_NAME,
                "-p",
                TEST_PASSWORD,
            ])
            .assert()
            .success();

        // THEN
        //   site is created
        let site = fetch_site(&pool, TEST_HOSTNAME).await;
        test_action!(site);
        let site = site.unwrap().unwrap();
        assert_eq!(site.host_name, TEST_HOSTNAME);

        //   role sets
        assert!(site.site_data.system_roles_sets.contains_key("sysop"));
        assert!(site.site_data.system_roles_sets.contains_key("genpop"));

        //   system user
        let sys_media_user = make_acct(SITE_USER_NAME, TEST_HOSTNAME);
        let action = fetch_user_info(&pool, &sys_media_user).await;
        test_action!(action);
        action.unwrap().unwrap();

        //   system media actors
        let actor_id = create_actor_id(TEST_HOSTNAME, &SiteActors::SiteContent.to_string());
        let action = fetch_owned_actor_by_id(&pool, &actor_id).await;
        test_action!(action);
        action.unwrap().unwrap();

        //   sysop user
        let sysop_user = make_acct(TEST_NOROLEUSER_NAME, TEST_HOSTNAME);
        let action = fetch_user_info(&pool, &sysop_user).await;
        test_action!(action);
        let user = action.unwrap().unwrap();
        assert_eq!(user.status, UserStatus::Active);

        //   sysop actor
        let actor_id = create_actor_id(TEST_HOSTNAME, "sysop");
        let action = fetch_owned_actor_by_id(&pool, &actor_id).await;
        test_action!(action);
        action.unwrap().unwrap();
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_bad_host_name_WHEN_create_site_THEN_failure() {
    test_pg_with_env(move |_pool, env| async move {
        // GIVEN
        let host_name = "foo/com";

        // WHEN
        let mut cmd = Command::cargo_bin("dt_ctrl").unwrap();
        let assert = cmd
            .env_clear()
            .envs(env)
            .args([
                "site",
                "create",
                "-H",
                host_name,
                "-L",
                "a long name",
                "-S",
                "shorty",
                "-u",
                TEST_NOROLEUSER_NAME,
                "-p",
                TEST_PASSWORD,
            ])
            .assert();

        // THEN
        assert.failure();
    })
    .await;
}

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_bad_user_name_WHEN_create_site_THEN_failure() {
    test_pg_with_env(move |_pool, env| async move {
        // GIVEN
        let user_name = "foo*com";

        // WHEN
        let mut cmd = Command::cargo_bin("dt_ctrl").unwrap();
        let assert = cmd
            .env_clear()
            .envs(env)
            .args([
                "site",
                "create",
                "-H",
                TEST_HOSTNAME,
                "-L",
                "a long name",
                "-S",
                "shorty",
                "-u",
                user_name,
                "-p",
                TEST_PASSWORD,
            ])
            .assert();

        // THEN
        assert.failure();
    })
    .await;
}
