use assert_cmd::Command;
use dialtone_test_util::create_system::create_system_tst_utl;
use dialtone_test_util::test_constants::*;
use dialtone_test_util::test_pg::test_pg_with_env;

#[tokio::test]
#[allow(non_snake_case)]
async fn GIVEN_system_WHEN_list_site_with_json_THEN_parsable() {
    test_pg_with_env(move |pool, env| async move {
        // GIVEN
        create_system_tst_utl(&pool).await;

        // WHEN
        let mut cmd = Command::cargo_bin("dt_ctrl").unwrap();
        let assert = cmd
            .env_clear()
            .envs(env)
            .args(["site", "info", "-H", TEST_HOSTNAME, "-J", "one-line"])
            .assert();

        // THEN
        let output = assert.get_output();
        let _: serde_json::Value = serde_json::from_slice(&output.stdout).unwrap();
    })
    .await;
}
